#ifndef _typedefine_h
#define _typedefine_h
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：系统外部存储器（SPI_FLASH，使用W25QXX系列）
 *实现功能：1、需要提供SPI接口支持，相关Flash接口支持
 *          2、读写函数
 *          3、COMMAND_writeFlash  COMMAND_eraseFlash命令支持
 *
 *撰 写 人：Alpscale LCD Application development team
 *撰写时间：2019-7-3
 *测 试 人：Alpscale LCD Application development team
 *测试时间：2019-7-3
 *版 本 号：V1.01_2019-7-3
 *版本说明：
 *                              
 *          接口支持          读写函数          命令支持 
 *
 *V1.01：   功能实现          功能实现          ok 2019/07/04
 *
 *V1.02：
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /* exact-width signed integer types */
typedef   signed          char int8_t;
typedef   signed short     int int16_t;
typedef   signed           int int32_t;

    /* exact-width unsigned integer types */
typedef unsigned          char uint8_t;
typedef unsigned short     int uint16_t;
typedef unsigned           int uint32_t;

#ifndef NULL
#define NULL 0
#endif

#define USE_PRINTF_DEBUG                    1//使用printf进行debug
#define DISP_DOWNLOAD_TIPS                  1//显示下载提示

//2、位置信息定义
typedef struct _LCD_POSITION{
	uint16_t x;
	uint16_t y;
}LCD_POSITION;
typedef LCD_POSITION*  pLCD_POSITION;
typedef struct _LCD_RECTANGLE{
	LCD_POSITION startPos;
	LCD_POSITION endPos;
}LCD_RECTANGLEN;
typedef LCD_RECTANGLEN*  pLCD_RECTANGLEN;

typedef enum _LCD_SCAN{
	//LCD扫描方向定义
	DIR_HORIZONTAL=0,      //横向扫描
	DIR_VERTICAL,          //纵向扫描
}LCD_SCAN;

typedef struct _LCD_PARAMETER{
	uint16_t width;					//LCD 宽度
	uint16_t height;				//LCD 高度
	uint32_t backcolor;				//LCD 使用的背景颜色
	uint32_t fontcolor;				//LCD 使用的字体颜色
	uint16_t id;					//LCD ID   
	LCD_SCAN dir;					//横屏还是竖屏控制：0，竖屏；1，横屏。	
}LCD_PARAMETER;
typedef LCD_PARAMETER* pLCD_PARAMETER;
//1、时间类型定义
typedef struct _RTC_TIME{
	uint8_t mSecond;
	uint8_t Second;
	uint8_t Minute;
	uint8_t hour;
}RTC_TIME;
typedef RTC_TIME*  pRTC_TIME;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//2、日期类型定义
typedef struct _RTC_DATE{
	uint8_t Day;
	uint8_t Weekday;//0-6 Sunday---Saturday
	uint8_t Month;
	uint8_t Year;   //00-99
}RTC_DATE;
typedef RTC_DATE*  pRTC_DATE;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//3、日期时间类型定义
typedef struct _RTC_TIMEDATE{
	RTC_TIME time;
	RTC_DATE date;
}RTC_TIMEDATE;
typedef RTC_TIMEDATE*  pRTC_TIMEDATE;
#endif 
