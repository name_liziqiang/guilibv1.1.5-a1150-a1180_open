#include "lcdInit.h"
#include "lcdDriver.h"
#include "m1130_gpio.h"

/*
*********************************************************************************************************************
*函数功能：LCDinitDelay LCD初始化模块延时函数
*入口参数：dly  延时时间
*出口参数：无
*********************************************************************************************************************
*/
void LCDinitDelay(unsigned int dly)
{
    unsigned int i,j;

    for(i=0;i<dly;i++)
    	for(j=0;j<255;j++);
}
/*
*********************************************************************************************************************
*函数功能：LCD_Init，LCD初始化，初始化LCD的硬件
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
void LCD_Init(void)
{
	//初始化GPIO	
	GPIO0_IT->DIR_SET=0xffff0000;//DB0~15输出 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_10,GPIO_FUNCTION_0);//LCD_BG 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_11,GPIO_FUNCTION_0);//LCD_RD 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_4,GPIO_FUNCTION_0);//LCD_WR 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_26,GPIO_FUNCTION_0);//LCD_RS 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_27,GPIO_FUNCTION_0);//LCD_CS 

	
	GPIO_SetPinDir(GPIO0, GPIO_Pin_4,GPIO_Mode_OUT);//LCD_WR 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_10,GPIO_Mode_OUT);//LCD_BG 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_11,GPIO_Mode_OUT);//LCD_RD 	
	GPIO_SetPinDir(GPIO1, GPIO_Pin_26,GPIO_Mode_OUT);//LCD_RS 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_27,GPIO_Mode_OUT);//LCD_CS 	
	
	LCD_BG_CLOSE();
	Lcd_Driver.writeCMD(0x01);//software reset
	LCDinitDelay(300);
	
	//关闭显示
	Lcd_Driver.writeCMD(0x28); 
	//Set the gray scale voltage to adjust the gamma characteristics of the TFT panel
	//设置灰度电压，调整TFT面板的伽马特性	
	Lcd_Driver.writeCMD(0xE0); 
	Lcd_Driver.writeData(0x0f); 
	Lcd_Driver.writeData(0x0B); 
	Lcd_Driver.writeData(0x18); 
	Lcd_Driver.writeData(0x0C); 
	Lcd_Driver.writeData(0x19) ;
	Lcd_Driver.writeData(0x0C); 
	Lcd_Driver.writeData(0x45); 
	Lcd_Driver.writeData(0x58); 
	Lcd_Driver.writeData(0x53); 
	Lcd_Driver.writeData(0x03); 
	Lcd_Driver.writeData(0x0B); 
	Lcd_Driver.writeData(0x0A); 
	Lcd_Driver.writeData(0x1D); 
	Lcd_Driver.writeData(0x20); 
	Lcd_Driver.writeData(0x0F); 
	//Set the gray scale voltage to adjust the gamma characteristics of the TFT panel.
	//设置灰度电压，调整TFT面板的伽马特性 
	Lcd_Driver.writeCMD(0xE1); 
	Lcd_Driver.writeData(0x0f); 
	Lcd_Driver.writeData(0x16); 
	Lcd_Driver.writeData(0x19); 
	Lcd_Driver.writeData(0x02); 
	Lcd_Driver.writeData(0x0E); 
	Lcd_Driver.writeData(0x03); 
	Lcd_Driver.writeData(0x2B); 
	Lcd_Driver.writeData(0X44); 
	Lcd_Driver.writeData(0x39); 
	Lcd_Driver.writeData(0x02); 
	Lcd_Driver.writeData(0x06); 
	Lcd_Driver.writeData(0x05); 
	Lcd_Driver.writeData(0x29); 
	Lcd_Driver.writeData(0x35); 
	Lcd_Driver.writeData(0x0F); 
	//设置VREG1OUT,VREG2OUT
	Lcd_Driver.writeCMD(0xC0); 
	Lcd_Driver.writeData(0x15); 
	Lcd_Driver.writeData(0x12); 
	//设置升压电路中使用的系数
	Lcd_Driver.writeCMD(0xC1); 
	Lcd_Driver.writeData(0x41); 
	//VCOM Control
	Lcd_Driver.writeCMD(0xC5); 
	Lcd_Driver.writeData(0x00); 
	Lcd_Driver.writeData(0x5a); //22
	Lcd_Driver.writeData(0x80); 
	//此命令定义帧内存的读/写扫描方向
	Lcd_Driver.writeCMD(0x36); 
	Lcd_Driver.writeData(0x48);//my=0,mx=0,mv=0,ml=0;使用rgb,MH=0(刷新方向)
	//接口的像素格式
	Lcd_Driver.writeCMD(0x3A); 
	Lcd_Driver.writeData(0x55);//RGB 16 bits/pixel  mcu 16 bits/pixel
	//Interface Mode Control
	Lcd_Driver.writeCMD(0XB0); 
	Lcd_Driver.writeData(0x00); 
	//Frame Rate Control帧速率控制
	Lcd_Driver.writeCMD(0xB1);
	Lcd_Driver.writeData(0xa0);//91.15
	//Display Inversion Control显示反转控制
	Lcd_Driver.writeCMD(0xB4); 
	Lcd_Driver.writeData(0x02);   
	//Display Function Control显示功能控制
	Lcd_Driver.writeCMD(0xB6); //RGB/MCU Interface Control
	Lcd_Driver.writeData(0x02); 
	Lcd_Driver.writeData(0x02); // 改成02 则 ss=0 ，会反显
	//是否启用24位数据总线;用户可以使用DB23~DB0作为24位数据输入。
	Lcd_Driver.writeCMD(0xE9); 
	Lcd_Driver.writeData(0x00);//关闭
	//Adjust Control 3 
	Lcd_Driver.writeCMD(0XF7);    
	Lcd_Driver.writeData(0xA9); 
	Lcd_Driver.writeData(0x51); 
	Lcd_Driver.writeData(0x2C); 
	Lcd_Driver.writeData(0x82);
	//此命令关闭睡眠模式
	Lcd_Driver.writeCMD(0x11); 
	//开启反显，正常0000显示黑色，ffff显示白色，如果不发这个命令，会显示反的
	Lcd_Driver.writeCMD(0x21); 
	//开启显示
	Lcd_Driver.writeCMD(0x29); 
	LCDinitDelay(300);
	Lcd_Driver.para->dir=Lcd_Driver.scan_dir(DIR_HORIZONTAL);//测试程序切换到横屏
	LCDinitDelay(300);
	LCD_BG_OPEN();
}
