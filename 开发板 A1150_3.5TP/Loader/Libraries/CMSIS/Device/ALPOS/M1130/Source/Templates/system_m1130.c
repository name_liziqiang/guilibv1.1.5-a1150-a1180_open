#include "m1130_rcc.h"

void delay(unsigned long time)
{
	time=time+time*2;

	while ( time )time--; 
}

void SystemInit (void)
{    
	uint32_t	timeout = 0;
	
  RCC->OSC12_CTRL = 0x80000062;
	RCC->AHBCLKCTRL0_SET = (1<<AHBCLK_BIT_GPIO) | (1<<AHBCLK_BIT_DMA) | (1<<AHBCLK_BIT_IOCON);
	RCC->SYSAHBCLKDIV = 4;							//FPGA��  96M/4=48M , 1130��  384M/4=96M

	/* open sys_pll clk */
	RCC->PDRUNCFG = RCC->PDRUNCFG & 0xFFFFFEEF;		//bit4=SYSPLL bit0=EXT12M

	/*set sys_pll clk to 480MHz*/
	RCC->SYSPLLCTRL = RCC_SYSPLLCTRL_FORCELOCK | (1 << 30) | (22 << 0);	//12M*(22+10)=384M

	while((RCC->SYSPLLSTAT) != RCC_SYSPLLSTAT_LOCK)
	{
		if((timeout++) >= 0x8000)
			break;
	}

 	delay(1000);
	
	/*switch main clk source to syspll_out*/
	RCC->MAINCLKSEL = 1;
	/*make switch available*/
	RCC->MAINCLKUEN = 0;
	RCC->MAINCLKUEN = 1;	
}
/******************* (C) COPYRIGHT 2013 Alphascale *****END OF FILE****/
