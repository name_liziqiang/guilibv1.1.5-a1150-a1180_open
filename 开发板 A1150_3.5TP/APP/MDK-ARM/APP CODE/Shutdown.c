#include "DemoApp.h"
#include "guiFunc.h"
#include "stdio.h"
#include "string.h"
#include "m1130_gpio.h"
#include "lcdInit.h"

static gui_uint8 picid;
gui_uint8 Shutdown=0;
gui_Err ShutPageEnterEvent(gui_int32 argc , const char **argv)
{	
	guiCreateWidget((const void *)&ShutPage);//创建的第一个控件一定是page
	guiCreateWidget((const void *)&ShutpagePic);
	guiCreateWidget((const void *)&ShutpageTimer);
	guiCreateWidget((const void *)&ShutpagePic2);
	
	
	guiRegisterEvent(EVENT_REGISTER_TYPE_PRESS , ShutpagePic2wID , TouchToShut_pressEvent);
	guiRegisterEvent(EVENT_REGISTER_TYPE_RELEASE , ShutpagePic2wID , TouchToShut_releaseEvent);
	guiRegisterEvent(EVENT_REGISTER_TYPE_TIMEOUT , ShutpageTimerwID , TouchToShut_timeoutEvent);	
	guiRegisterEvent(EVENT_REGISTER_TYPE_RELEASE , ShutpagewID , BackToHome_releaseEvent);
	guiRegisterEvent(EVENT_REGISTER_TYPE_RELEASE , ShutpagePicwID , BackToHome_releaseEvent);
	return GUI_EOK;
}
gui_Err ShutPageLeaveEvent(gui_int32 argc , const char **argv)
{
	
	return GUI_EOK;
}


static gui_Err TouchToShut_pressEvent(gui_int32 argc , const char **argv)
{
	guiSetWidgetEnable(ShutpageTimerwID,1);
	return GUI_EOK;
}
static gui_Err TouchToShut_releaseEvent(gui_int32 argc , const char **argv)
{
	guiSetWidgetEnable(ShutpageTimerwID,0);
	guiSetWidgetPictureID(ShutpagePic2wID , 117);
	picid=0;
	return GUI_EOK;
}
static gui_Err TouchToShut_timeoutEvent(gui_int32 argc , const char **argv)
{
	picid++;
	switch(picid)
	{
		case 1:{guiSetWidgetPictureID(ShutpagePic2wID , 101);break;}
		case 2:{guiSetWidgetPictureID(ShutpagePic2wID , 102);break;}
		case 3:{guiSetWidgetPictureID(ShutpagePic2wID , 103);break;}
		case 4:{guiSetWidgetPictureID(ShutpagePic2wID , 104);break;}
		case 5:{guiSetWidgetPictureID(ShutpagePic2wID , 105);break;}
		case 6:{guiSetWidgetPictureID(ShutpagePic2wID , 106);break;}
		case 7:{guiSetWidgetPictureID(ShutpagePic2wID , 107);break;}
		case 8:{guiSetWidgetPictureID(ShutpagePic2wID , 108);break;}
		case 9:{guiSetWidgetPictureID(ShutpagePic2wID , 109);break;}
		case 10:{guiSetWidgetPictureID(ShutpagePic2wID , 110);break;}
		case 11:{guiSetWidgetPictureID(ShutpagePic2wID , 111);break;}
		case 12:{guiSetWidgetPictureID(ShutpagePic2wID , 112);break;}
		case 13:{guiSetWidgetPictureID(ShutpagePic2wID , 113);break;}
		case 14:{guiSetWidgetPictureID(ShutpagePic2wID , 114);break;}
		case 15:{guiSetWidgetPictureID(ShutpagePic2wID , 115);break;}
		case 16:{guiSetWidgetPictureID(ShutpagePic2wID , 116);picid=0;Lcd_Driver.SetDim(0);Shutdown=1;guiJumpPage(LOGOwID,LOGOPageLeaveEvent,LOGOPageEnterEvent);printf("Shutdown\r\n");break;}
		default:break;
	}

	return GUI_EOK;
}
static gui_Err BackToHome_releaseEvent(gui_int32 argc , const char **argv)
{
	guiJumpPage(MPwID,MainPageLeaveEvent,MainPageEnterEvent);
	return GUI_EOK;
}






















