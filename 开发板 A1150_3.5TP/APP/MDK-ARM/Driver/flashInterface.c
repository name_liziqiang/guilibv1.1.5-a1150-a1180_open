#include "flashInterface.h"
#include "m1130_xip_write.h"

const FLASH_DESCRIPTOR Flash_Des={
	/*addr flash物理地址      count擦除个数 本工程为扇区个数 每扇区4K bytes*/
	XIP_EraseFlash_Block_4K,
	/*buf  待写入的数据缓冲区 addr 写入的flash物理地址                  count写入的字节个数，该接口不负责擦除*/
	XIP_WriteFlash_NoCheck,
	/*addr 读取的物理地址         buf读取的缓冲区 count 读取的字节个数*/
	loadData8FromQspi0
};

