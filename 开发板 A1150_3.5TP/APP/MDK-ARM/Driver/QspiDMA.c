#include "QspiDMA.h"
#include "m1130.h"
#include "m1130_rcc.h"
#include "m1130_gpio.h"
#include "m1130_qspi.h"
#include "m1130_eval_qspi.h"
#include <stdio.h>
#include <string.h>

#define MAX_QSPI_DMA_BUSY_WATI	 0x00800000
__align(4) uint8_t flashOperationBuff[8];
// static uint8_t _4ByteAddrMode = 0;       //flash 是否使用4字节地址  0不使用 1使用

/*
*********************************************************************************************************************
*函数功能：QSPIDMA_WaitEnd QSPI 等待 DMA通道传输完成
*入口参数：QSPIptr  操作的QSPI
					DMAchannel DMA通道 0-3
*出口参数：0  成功
					-1 超时
					-2 无效通道
*********************************************************************************************************************
*/
int QSPIDMA_WaitEnd(QSPI_TypeDef *QSPIptr , uint8_t DMAchannel)
{
	int result = 0;
	if(DMAchannel > DMA_CHANNEL_MAX)return -2;//无效通道
	else{
		int waitchannel = MAX_QSPI_DMA_BUSY_WATI;
		while (((DMA->ChEnReg & (1<<DMAchannel)) || ((QSPIptr->STATUS & QSPI_STATUS_SPI_BUSY)!=RESET)) && (waitchannel--));
		//while (((DMA->ChEnReg & (1<<DMAchannel))) && (waitchannel--));
		if(waitchannel <= 0)result = -1;
		QSPIptr->CTRL1_CLR = QSPI_CTRL1_DMA_ENABLE;//关闭DMA
	}
	return result;
}
/*
*********************************************************************************************************************
*函数功能：QSPIDMA_SendBytes  QSPI使用DMA 发送
*入口参数：QSPIptr    操作的QSPI
					DMAchannel DMA通道 0-3
					data       等待发送数据缓存区
					len        需要发送的字节长度
*出口参数：0  成功
					-1 超时
					-2 无效通道
*********************************************************************************************************************
*/
int QSPIDMA_SendBytes(QSPI_TypeDef *QSPIptr , uint8_t DMAchannel , const uint8_t *data , uint32_t len)
{
	if(DMAchannel > DMA_CHANNEL_MAX)return -2;//无效通道
	uint32_t sendBytes=0;
	uint8_t  sendWidth=0;
	if(len>511){//判断一下，DMA的传输是否超过了511字节，否则就需要调整传输块的大小
		sendWidth = 1;
		sendBytes = len/2;
		if(len%2)sendBytes ++;
	}
	else {
		sendWidth = 0;
		sendBytes = len;
	}
	QSPIptr->CTRL1_SET = QSPI_CTRL1_DMA_ENABLE;//使能DMA	
	//QSPIptr->CTRL0_CLR = 0x20000000;
	QSPIptr->XFER = sendBytes;
	QSPIptr->CTRL0_SET = QSPI_HalfDuplex;
	// QSPIptr->CTRL0_SET = QSPI_Transfer_Write;//200927 好像没什么用
	QSPIptr->CTRL0_SET = 0x20000000;
	
	DMAChannel_TypeDef *sendChannel;
	sendChannel = (DMAChannel_TypeDef *)(DMA_BASE + DMAchannel*0x58);
	
	uint32_t temreg = 0x00000000+(0<<0)    //INT_EN, ch0 irq disable
		                       	  +(0<<1)      // DST_TR_WIDTH, des transfer width, should set to HSIZE, here is 000, means 8bit
                              +(sendWidth<<4)      // SRC_TR_WIDTH, sor transfer width, should set to HSIZE, here is 000, means 8bit
                              +(2<<7)      // DINC, des addr increment, des is SPI, so should set to 1x, means no change
                              +(0<<9)      // SINC, sor addr increment, src is sram, so should set to 00, means to increase 
                              +(1<<11)     // DEST_MSIZE, des burst length, set to 001 means 4 DST_TR_WIDTH per burst transcation
                              +(1<<14)     // SRC_MSIZE, sor burst length, set to 001 means 4 SOR_TR_WIDTH per burst transcation
                              +(1<<20)     // TT_FC,transfer type and flow control,001 means memory to peripheral,dma is flow controller
                              +(0<<23)     // DMS, des master select, 0 means ahb master 0
                              +(0<<25)     // SMS, sor master select, 1 means ahb master 1
                              +(0<<27)     // LLP_DST_EN, des block chaining enable, set to 0 disable it
                              +(0<<28) ;   // LLP_SOR_EN, sor block chaining enable, set to 0 disable it	
	
	sendChannel->SAR = (uint32_t)(data);
	sendChannel->DAR = (uint32_t)(&QSPIptr->DATA);
	sendChannel->CTL_L = temreg;
	sendChannel->CTL_H = sendBytes;//DMA 传输字节数
	sendChannel->LLP =0;
	if(QSPIptr == QSPI0){
		sendChannel->CFG_H = 0x00000000 + (8 << 7) + (8 << 11);
		sendChannel->CFG_L = 7<<5;//通道优先级
	}
	else if(QSPIptr == QSPI1){
		sendChannel->CFG_H = 0x00000000 + (10 << 7) + (10 << 11);
		sendChannel->CFG_L = 6<<5;//通道优先级
	}
	else if(QSPIptr == QSPI2){
		sendChannel->CFG_H = 0x00000000 + (14 << 7) + (14 << 11);
		sendChannel->CFG_L = 5<<5;//通道优先级
	}
	DMA->DmaCfgReg = 1;
	DMA->ChEnReg = (1<<(DMAchannel + 8))|(1<<DMAchannel);
	return 0;
}
/*
*********************************************************************************************************************
*函数功能：QSPIDMA_ReadBytes  QSPI使用DMA 接收
*入口参数：QSPIptr    操作的QSPI
					DMAchannel DMA通道 0-3
					data       等待发送数据缓存区
					len        需要发送的字节长度
*出口参数：0  成功
					-1 超时
					-2 无效通道
					-3 缓存区首地址 没有字节对齐（4字节对齐）
*********************************************************************************************************************
*/
int QSPIDMA_ReadBytes(QSPI_TypeDef *QSPIptr , uint8_t DMAchannel , uint8_t *data , uint32_t len)
{
	if(DMAchannel > DMA_CHANNEL_MAX)return -2;//无效通道
	if(((uint32_t)data)&0x03)return -3;

	QSPIptr->CTRL1_SET = QSPI_CTRL1_DMA_ENABLE;//使能DMA	
	//QSPIptr->CTRL0_CLR = 0x20000000;
	QSPIptr->XFER = len;
	QSPIptr->CTRL0_SET = QSPI_HalfDuplex;
	QSPIptr->CTRL0_SET = QSPI_Transfer_Read;
	QSPIptr->CTRL0_SET = 0x20000000;
	
	DMAChannel_TypeDef *readChannel;
	readChannel = (DMAChannel_TypeDef *)(DMA_BASE + DMAchannel*0x58);
	
	uint32_t temreg = 0x00000000+(0<<0)    //INT_EN, ch0 irq disable
															+(2<<1)    // DST_TR_WIDTH, des transfer width, should set to HSIZE, here is 010, means 32bit
															+(2<<4)    // SRC_TR_WIDTH, sor transfer width, should set to HSIZE, here is 010, means 32bit
															+(0<<7)    // DINC, des addr increment, des is sram, so should set to 00, means to increase
															+(2<<9)    // SINC, sor addr increment, src is SPI, so should set to 1x, means no change 
															+(0<<11)   // DEST_MSIZE, des burst length, set to 000 means 1 DST_TR_WIDTH per burst transcation
															+(0<<14)   // SRC_MSIZE, sor burst length, set to 000 means 1 SOR_TR_WIDTH per burst transcation
															+(2<<20)   // TT_FC,transfer type and flow control,010 means peripheral to memory,dma is flow controller
															+(0<<23)   // DMS, des master select, 0 means ahb master 0
															+(0<<25)   // SMS, sor master select, 1 means ahb master 1
															+(0<<27)   // LLP_DST_EN, des block chaining enable, set to 0 disable it
															+(0<<28);  // LLP_SOR_EN, sor block chaining enable, set to 0 disable it	

	readChannel->SAR = (uint32_t)(&QSPIptr->DATA);
	readChannel->DAR = (uint32_t)(data);
	readChannel->CTL_L = temreg;
	uint32_t readCount = len>>2;
	if((len%4)!=0)readCount++;
	readChannel->CTL_H = readCount;//DMA 传输字节数
	readChannel->LLP =0;
	readChannel->CFG_L=0;
	if(QSPIptr == QSPI0){
		readChannel->CFG_H = 0x00000000 + (9 << 7) + (9 << 11);
		readChannel->CFG_L = 4<<5;//通道优先级
	}
	else if(QSPIptr == QSPI1){
		readChannel->CFG_H = 0x00000000 + (11 << 7) + (11 << 11);
		readChannel->CFG_L = 3<<5;//通道优先级
	}
	else if(QSPIptr == QSPI2){
		readChannel->CFG_H = 0x00000000 + (15 << 7) + (15 << 11);
		readChannel->CFG_L = 2<<5;//通道优先级
	}
	DMA->DmaCfgReg = 1;
	DMA->ChEnReg = (1<<(DMAchannel + 8))|(1<<DMAchannel);
	return 0;
}
/*
*********************************************************************************************************************
*函数功能：enter_XIPCritical  进入XIP临界区。
					将MCU退出XIP模式，只运行code的前面8K；
					什么情况下使用该方法：
					1、用户需要将大批数据读取到SRAM 然后 通过其他方式传输到LCD
					2、通常刷单色是不需要的；
					3、通常用于刷图；
					用户应该保证：
					1、调用该函数之后，APP代码不调用8K之外的代码；
					2、中断服务函数 不会调用8K之外的代码；
*入口参数：无
*出口参数：0  成功
*********************************************************************************************************************
*/
int enter_XIPCritical(void)
{
	CODE_QSPI->CTRL0_CLR = 0x22000000;						//clear MEMP/RUN
  CODE_QSPI->CTRL1_SET = QSPI_CTRL1_DMA_ENABLE;	//开启DMA 借助DMA自动清除标志的特性
	//发送8个0x00, 退出连续读模式
	for(uint32_t i=0; i<8; i++){
		flashOperationBuff[i] = 0x00;
	}
	CODE_QSPI->CTRL0_SET = QSPI_CTRL0_LOCK_CS;	
  QSPIDMA_SendBytes(CODE_QSPI, FLASH_QSPIDMA_WRITE_CHANNEL, flashOperationBuff, 8);
	QSPIDMA_WaitEnd(CODE_QSPI, FLASH_QSPIDMA_WRITE_CHANNEL);
	CODE_QSPI->CTRL0_CLR = QSPI_CTRL0_LOCK_CS;

	//CTRL1切换为单线，D2D3 pinmux切换到GPIO//200927
  CODE_QSPI->CTRL1 = (CODE_QSPI->CTRL1 & 0xfffffff8) | 0x00;
	PIO1->PIN0 = PIO1->PIN0 & 0xfffffffc;
	PIO1->PIN3 = PIO1->PIN3 & 0xfffffffc;

	//__disable_irq();

	return 0;
}
/*
*********************************************************************************************************************
*函数功能：quit_XIPCritical 退出XIP临界区
*入口参数：无
*出口参数：0  成功
*********************************************************************************************************************
*/
int quit_XIPCritical(void)
{
	//D2D3 pinmux切换到QSPI,CTRL1保持单线状态，XIP自动切换到4线
	PIO1->PIN0 = (PIO1->PIN0 & 0xfffffffc) | 0x01;
	PIO1->PIN3 = (PIO1->PIN3 & 0xfffffffc) | 0x01;
	DATA_QSPI->CTRL1 = (CODE_QSPI->CTRL1 & 0xfffffff8) | 0x02;
	//disable DMA
  CODE_QSPI->CTRL1_CLR = QSPI_CTRL1_DMA_ENABLE;

	//MEMP:bit 25
	//RUN:bit 29
	CODE_QSPI->CTRL0_SET = 0x02000000;	//set MEMP/RUN
	//__enable_irq();
	return 0;
}
/*
*********************************************************************************************************************
*函数功能：flash_ConfigReadFlash  配置数据flash读取数据信息
*入口参数：QSPIptr 操作的QSPI外设
					DMAchannel 操作的DMA通道
					dataAddr   需要读取的数据地址
*出口参数：0  成功
*********************************************************************************************************************
*/
int flash_ConfigReadFlashMsg(QSPI_TypeDef *QSPIptr , uint8_t DMAchannel , uint32_t dataAddr)
{
#if 0
	flashOperationBuff[0] = QUAD_SPI_CMD_FAST_READ_OCTAL_QUAD_IO;//命令
	QSPIDMA_SendBytes(QSPIptr, DMAchannel, flashOperationBuff, 1);//
	QSPIDMA_WaitEnd(QSPIptr, DMAchannel);
	//D2D3 pinmux切换到QSPI,CTRL1保持单线状态，切换到4线
	PIO1->PIN0 = (PIO1->PIN0 & 0xfffffffc) | 0x01;
	PIO1->PIN3 = (PIO1->PIN3 & 0xfffffffc) | 0x01;
	DATA_QSPI->CTRL1 = (CODE_QSPI->CTRL1 & 0xfffffff8) | 0x02;
	flashOperationBuff[0] = (dataAddr>>16)&0xff;//flash数据地址
	flashOperationBuff[1] = (dataAddr>>8)&0xff;
	flashOperationBuff[2] = dataAddr&0xff;
	flashOperationBuff[3] = 0xff;               //MID
	flashOperationBuff[4] = 0x00;//Dummy
	flashOperationBuff[5] = 0x00;//Dummy
	QSPIDMA_SendBytes(QSPIptr, DMAchannel, flashOperationBuff, 6);//
	QSPIDMA_WaitEnd(QSPIptr, DMAchannel);
#else
	flashOperationBuff[0] = 0x6b;//命令
	flashOperationBuff[1] = (dataAddr>>16)&0xff;//flash数据地址
	flashOperationBuff[2] = (dataAddr>>8)&0xff;
	flashOperationBuff[3] = dataAddr&0xff;
	flashOperationBuff[4] = 0x00;//Dummy
	flashOperationBuff[5] = 0x00;//Dummy
	QSPIDMA_SendBytes(QSPIptr, DMAchannel, flashOperationBuff, 6);//
	QSPIDMA_WaitEnd(QSPIptr, DMAchannel);
	//D2D3 pinmux切换到QSPI,CTRL1，切换到4线
	PIO1->PIN0 = (PIO1->PIN0 & 0xfffffffc) | 0x01;
	PIO1->PIN3 = (PIO1->PIN3 & 0xfffffffc) | 0x01;
	DATA_QSPI->CTRL1 = (CODE_QSPI->CTRL1 & 0xfffffff8) | 0x02;
#endif
	return 0;
}
