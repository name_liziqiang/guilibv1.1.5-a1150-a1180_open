#ifndef _memoryInterface_H
#define _memoryInterface_H
#include "typedefine.h"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：存储器接口（本工程使用的是外部 SPI FLASH）
 *实现功能：
 *
 *撰 写 人：Alpscale LCD Application development team
 *撰写时间：2019-7-8
 *测 试 人：Alpscale LCD Application development team
 *测试时间：2019-7-8
 *版 本 号：V1.01_2019-7-8
 *版本说明：
 *                              
 *          
 *
 *V1.01：   
 *
 *V1.02：
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef struct _MEMORY_DESCRIPTOR{
	/*addr flash物理地址      count擦除个数 本工程为扇区个数 每扇区4K bytes*/
	void (* erase)(uint32_t addr , uint32_t count);
	/*buf  待写入的数据缓冲区 addr 写入的flash物理地址                  count写入的字节个数，该接口不负责擦除*/
	void (* write)(uint8_t *buf , uint32_t addr , uint32_t count);
	/*addr 读取的物理地址         buf读取的缓冲区 count 读取的字节个数*/
	void (* read)(uint32_t addr , uint8_t *buf , uint32_t count);
}MEMORY_DESCRIPTOR;

extern const MEMORY_DESCRIPTOR Memory_Des;

#endif
