#ifndef _comInterface_h
#define _comInterface_h
#include "typedefine.h"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：通讯接口定义
 *实现功能：1、
 *
 *撰 写 人：Alpscale LCD Application development team
 *撰写时间：2019-7-3
 *测 试 人：Alpscale LCD Application development team
 *测试时间：2019-7-3
 *版 本 号：V1.01_2019-7-3
 *版本说明：
 *                              
 *          接口支持          读写函数          命令支持 
 *
 *V1.01：   功能实现          功能实现          ok 2019/07/04
 *
 *V1.02：
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define COM_RECEIVED_NO                 0//没有接收到
#define COM_RECEIVED                    1//接收到数据


typedef struct _COM_DESCRIPTOR{
	/*驱动层应该提供的接口定义*/
	void  (*outputChar)(uint8_t c);
	void  (*outputString)(const uint8_t *c , uint32_t len);

	/*驱动层应该调用的接口定义*/
	uint32_t(*getOutputBuffMaxLen)(void);
	uint8_t* (* getOutputBuff)(void);
	uint32_t(*getInputBuffMaxLen)(void);
	uint8_t* (* getInputBuff)(void);
	uint32_t (*getInputLen)(void);
	uint8_t  (*getInputStatus)(void);

	void  (*inputChar)(uint8_t c);
	void  (*inputString)(const uint8_t *c , uint32_t len);
	void  (*inputEnd)(void);

	void  (*clearOutputBuff)(void);
	void  (*clearInputBuff)(void);
}COM_DESCRIPTOR;

extern const COM_DESCRIPTOR Com_Des;

#endif

