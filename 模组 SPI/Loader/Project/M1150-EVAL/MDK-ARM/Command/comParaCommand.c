#include "command.h"
#include "comInterface.h"


CMD_State COMMAND_readCOM(void *result,const void *charc,uint32_t c_len)
{
	uint8_t *rdata=(uint8_t *)result;
	rdata[0]=0x80;
	rdata[1]=0x01;
	rdata[2]=0x05;//数据长度
	rdata[3]=0x00;//通讯类型 0usart
	rdata[4]=0x00;
	rdata[5]=0x01;
	rdata[6]=0xC2;
	rdata[7]=0x00;//通讯波特率，固定为115200，后期根据实际波特率调整
	rdata[8]=0;//校验和
	for(int i=0;i<8;i++)rdata[8]+=rdata[i];	
	
	Com_Des.outputString(rdata,9);//返回结果
	return OK_CMD;
}

CMD_State COMMAND_writeCOM(void *result,const void *charc,uint32_t c_len)
{
	return OK_CMD;
}

