#include "lcddriver.h"
#include "stdio.h"
#include "m1130_gpio.h"
#include "LcdSpi.h"

static uint16_t  lcdSendBuff[LCD_SENDBUF_MAX * 2] __attribute__((at(0x20009000)));

/*
*********************************************************************************************************************
*函数功能：LCD 常用参数定义
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
LCD_PARAMETER lcdPara={
	LCD_WIDTH,                          //LCD 宽度
	LCD_HEIGHT,                         //LCD 高度
	0xf800,                             //LCD 使用的背景颜色
	RED,                                //LCD 使用的字体颜色
	0,                                  //LCD ID   
	DIR_VERTICAL,                       //横屏还是竖屏控制
};
/*
*********************************************************************************************************************
*函数功能：LCD_Read_DATA，从LCD读取一个数据
*入口参数：无
*出口参数：uint16_t，读取到的数据
*********************************************************************************************************************
*/
static uint16_t LCD_Read_DATA(void)
{
	return 0;
}
/*
*********************************************************************************************************************
*函数功能：LCD_Write_CMD  写一个命令到LCD
*入口参数：cmd 写入的命令
*出口参数：无
*三线SPI  数据是9bit  第一个bit  是数据命令选择   bit=0 写命令  bit=1 写数据
*********************************************************************************************************************
*/
static void LCD_Write_CMD(uint16_t cmd)
{
	LCD_RS_D();
	*(uint8_t *)(lcdSendBuff) = cmd;
	Lcd_SendBytes((uint8_t *)(lcdSendBuff) , 1);
	Lcd_WaitSendEnd();
}
/*
*********************************************************************************************************************
*函数功能：LCD_Write_DATA  写一个数据到LCD
*入口参数：data 写入的数据
*出口参数：无
*三线SPI  数据是9bit  第一个bit  是数据命令选择   bit=0 写命令  bit=1 写数据
*********************************************************************************************************************
*/
static void LCD_Write_DATA(uint16_t data)
{
	LCD_RS_G();
	*(uint8_t *)(lcdSendBuff) = data;
	Lcd_SendBytes((uint8_t *)(lcdSendBuff) , 1);
	Lcd_WaitSendEnd();
}
/*
*********************************************************************************************************************
*函数功能：LCD_WriteReg，LCD写一个寄存器的值
*入口参数：LCD_Reg 寄存器地址
*          LCD_RegValue 需要的寄存器的值
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_WriteReg(uint16_t LCD_Reg,uint16_t LCD_RegValue)
{	
	LCD_Write_CMD(LCD_Reg);  
	LCD_Write_DATA(LCD_RegValue);	    		 
}
/*
*********************************************************************************************************************
*函数功能：LCD_SetCursor，LCD设置光标位置,开始位置和结束位置
*入口参数：Xpos:横坐标
*          Xepos:结束横坐标
*          Ypos:纵坐标
*          Yepos:结束纵坐标
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_SetCursor(uint16_t Xpos, uint16_t Ypos, uint16_t Xepos, uint16_t Yepos)
{	    
	LCD_Write_CMD(0X2A); 
	LCD_Write_DATA(Xpos>>8);LCD_Write_DATA(Xpos&0XFF);
	LCD_Write_DATA(Xepos>>8);LCD_Write_DATA(Xepos&0XFF); 		
	LCD_Write_CMD(0X2B); 
	LCD_Write_DATA(Ypos>>8);LCD_Write_DATA(Ypos&0XFF); 		
	LCD_Write_DATA(Yepos>>8);LCD_Write_DATA(Yepos&0XFF); 	
} 
/*
*********************************************************************************************************************
*函数功能：LCD_DisplayOn LCD开启显示
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_DisplayOn(void)
{					   
	LCD_Write_CMD(0x29);
}	
/*
*********************************************************************************************************************
*函数功能：LCD_DisplayOff，LCD关闭显示
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_DisplayOff(void)
{	   
	LCD_Write_CMD(0x28);
}  
/*
*********************************************************************************************************************
*函数功能：LCD_Scan_Dir，设置扫描方向
*入口参数：dir，新的扫描方向
*出口参数：LCD_SCAN，返回新的扫描方向
*********************************************************************************************************************
*/  
static LCD_SCAN LCD_Scan_Dir(LCD_SCAN dir)
{
	if(dir==DIR_HORIZONTAL){
		//横屏
		LCD_WriteReg(0x36,0xe8);//0x36寄存器控制着LCD的扫描方向
	}
	else{
		LCD_WriteReg(0x36,0x48);//0x36寄存器控制着LCD的扫描方向，写入的都是RGB，bit3为0的时候 显示屏显示RGB  1的时候显示屏显示BGR，我们应该是BGR
	}
	uint16_t width_x=lcdPara.width;
	lcdPara.width=lcdPara.height;
	lcdPara.height=width_x;
	return(dir);
} 
/*
*********************************************************************************************************************
*函数功能：LCD_Scroll，设置滚动区域（该驱动待测）
*入口参数：sx sy起点坐标
*          ex ey终点坐标
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_Scroll(uint16_t sx,uint16_t sy,uint16_t ex,uint16_t ey)
{
	uint16_t y_offset=sx-ex;
	LCD_Write_CMD(0x33);
	LCD_Write_DATA(sx>>8);LCD_Write_DATA(sx&0XFF);//顶部行
	LCD_Write_DATA(y_offset>>8);LCD_Write_DATA(y_offset&0XFF);
	LCD_Write_DATA(ey>>8);LCD_Write_DATA(ey&0XFF);//底部行
	LCD_Write_CMD(0x37);
	LCD_Write_DATA(ex>>8);LCD_Write_DATA(ex&0XFF);//底部行
}
/*
*********************************************************************************************************************
*函数功能：LCD_Clear，lcd清屏显示，单色，LCD描述符的背景色，全屏
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_Clear(void)
{ 
	uint32_t totalpoint=Lcd_Driver.para->width;
	totalpoint*=Lcd_Driver.para->height; 			  //得到总点数
	
	LCD_SetCursor(0,0,lcdPara.width-1,lcdPara.height-1);
	LCD_Write_CMD(0x2c);
	LCD_RS_G();
	
	uint32_t sendBytes = 0;
	
	Data_CopyHalfWord(lcdPara.backcolor , lcdSendBuff , LCD_SENDBUF_MAX);//清屏只有一个颜色，填充完 之后 不停的刷这个缓存区即可
	
	while(1){
		if(totalpoint >= LCD_SENDBUF_MAX)sendBytes = LCD_SENDBUF_MAX;
		else sendBytes = totalpoint;
		Lcd_WaitSendEnd();
		Lcd_SendBytes((uint8_t *)lcdSendBuff , sendBytes*2);
		totalpoint -= sendBytes;
		if(totalpoint == 0){
			Lcd_WaitSendEnd();
			break;
		}
	}
}
const LCD_DRIVER Lcd_Driver={   
	&lcdPara,   
	LCD_Write_DATA,
	LCD_Read_DATA,
	LCD_Write_CMD, 			 //写命令
	LCD_SetCursor,       //LCD 设置光标起始位置和结束位置
	LCD_Scan_Dir,        //LCD扫描方向
	LCD_DisplayOn,
	LCD_DisplayOff,
	LCD_Scroll,
	LCD_Clear
};//LCD描述符	 
