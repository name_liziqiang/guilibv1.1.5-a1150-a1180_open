#include "LcdSpi.h"
#include "m1130.h"
#include "m1130_rcc.h"
#include "m1130_gpio.h"
#include "m1130_qspi.h"
#include "m1130_eval_qspi.h"

#define MAX_QSPI_DMA_BUSY_WATI	 0x200000

#define LCD_DMA_SEND_CHANNLE     3//LCD DMA 发送通道
#define DATA_COPY_R2R_CHANNLE    2//DATA 数据拷贝 SRAM->SRAM DMA通道

#define  LCD_SPI                 QSPI1

void Lcd_SPI_Init(uint32_t rcc_clk_div,int internal_clk_div, int internal_clk_rate)
{
	QSPI_InitTypeDef QSPI_InitStructure;
	
	QSPI_InitStructure.QSPI_CPOL = QSPI_CPOL_Low;
	QSPI_InitStructure.QSPI_CPHA = QSPI_CPHA_1Edge;
	
	QSPI_InitStructure.QSPI_FirstBit = QSPI_FirstBit_MSB;
	QSPI_InitStructure.QSPI_ClockDiv = (internal_clk_div<<8);
	QSPI_InitStructure.QSPI_ClockRate = internal_clk_rate;

	RCC->QUADSPI1CLKDIV = rcc_clk_div;
	GPIO_SetPinMux(GPIO0, GPIO_Pin_26, GPIO_FUNCTION_3);//SPI MOSI
	GPIO_SetPinMux(GPIO0, GPIO_Pin_27, GPIO_FUNCTION_3);//SPI CLK
	GPIO_SetPinMux(GPIO0, GPIO_Pin_30, GPIO_FUNCTION_3);//SPI MISO
	GPIO_SetPinMux(GPIO0, GPIO_Pin_31, GPIO_FUNCTION_3);//SPI CS  LCD
	
	RCC_ResetAHBCLK(1 << AHBCLK_BIT_DMA);
	QSPI_HwInit(LCD_SPI, &QSPI_InitStructure);
}

void Lcd_WaitSendEnd(void)
{
	int waitchannel = MAX_QSPI_DMA_BUSY_WATI;
	while (((DMA->ChEnReg & (1<<LCD_DMA_SEND_CHANNLE)) || (QSPI_GetFlagStatus(LCD_SPI, QSPI_STATUS_SPI_BUSY) == SET)) && (waitchannel--));
	LCD_SPI->CTRL1_CLR = QSPI_CTRL1_DMA_ENABLE;//关闭DMA
	LCD_SPI->CTRL0 &= ~QSPI_CTRL0_LOCK_CS;
}

void Data_WaitCopyEnd(void)
{
	int waitchannel = MAX_QSPI_DMA_BUSY_WATI;
	while ((DMA->ChEnReg & (1<<DATA_COPY_R2R_CHANNLE)) && (waitchannel--));
}

void Lcd_SendBytes(const uint8_t *data , uint32_t len)
{
	LCD_SPI->CTRL0 |= QSPI_CTRL0_LOCK_CS;		
	
	LCD_SPI->XFER = len;
	LCD_SPI->CTRL0 = 0x20000000 | QSPI_HalfDuplex | QSPI_Transfer_Write;
	
	LCD_SPI->CTRL1_SET = QSPI_CTRL1_DMA_ENABLE;//使能DMA
	
	DMAChannel_TypeDef *LcdSendChannel;
	LcdSendChannel = (DMAChannel_TypeDef *)(DMA_BASE + LCD_DMA_SEND_CHANNLE*0x58);
	
	uint32_t temreg = 0x00000000+(0<<0)    //INT_EN, ch0 irq disable
		                       	  +(0<<1)      // DST_TR_WIDTH, des transfer width, should set to HSIZE, here is 000, means 8bit
                              +(0<<4)      // SRC_TR_WIDTH, sor transfer width, should set to HSIZE, here is 000, means 8bit
                              +(2<<7)      // DINC, des addr increment, des is SPI, so should set to 1x, means no change
                              +(0<<9)      // SINC, sor addr increment, src is sram, so should set to 00, means to increase 
                              +(1<<11)     // DEST_MSIZE, des burst length, set to 001 means 4 DST_TR_WIDTH per burst transcation
                              +(1<<14)     // SRC_MSIZE, sor burst length, set to 001 means 4 SOR_TR_WIDTH per burst transcation
                              +(1<<20)     // TT_FC,transfer type and flow control,001 means memory to peripheral,dma is flow controller
                              +(0<<23)     // DMS, des master select, 0 means ahb master 0
                              +(0<<25)     // SMS, sor master select, 1 means ahb master 1
                              +(0<<27)     // LLP_DST_EN, des block chaining enable, set to 0 disable it
                              +(0<<28) ;   // LLP_SOR_EN, sor block chaining enable, set to 0 disable it	
	
	LcdSendChannel->SAR = (u32)(data);
	LcdSendChannel->DAR = (u32)(&LCD_SPI->DATA);
	LcdSendChannel->CTL_L = temreg;
	LcdSendChannel->CTL_H = len;//DMA 传输字节数
	LcdSendChannel->LLP =0;
	LcdSendChannel->CFG_L=0;
	if(LCD_SPI == QSPI0){
		LcdSendChannel->CFG_H = 0x00000000 + (8 << 7) + (8 << 11);
	}
	else if(LCD_SPI == QSPI1){
		LcdSendChannel->CFG_H = 0x00000000 + (10 << 7) + (10 << 11);
	}
	DMA->DmaCfgReg = 1;
	DMA->ChEnReg = (1<<(LCD_DMA_SEND_CHANNLE + 8))|(1<<LCD_DMA_SEND_CHANNLE);
}

void Data_CopyHalfWord(uint16_t Sdata , uint16_t *Ddata , uint32_t len)
{
	DMAChannel_TypeDef *DataCopyChannel;
	DataCopyChannel = (DMAChannel_TypeDef *)(DMA_BASE + DATA_COPY_R2R_CHANNLE*0x58);
	
	uint32_t temreg = 0x00000000+(0<<0)    //INT_EN, ch0 irq disable
		                       	  +(1<<1)     // DST_TR_WIDTH, des transfer width, should set to HSIZE, here is 000, means 8bit
                              +(1<<4)     // SRC_TR_WIDTH, sor transfer width, should set to HSIZE, here is 000, means 8bit
                              +(0<<7)     // DINC, des addr increment, des is SPI, so should set to 1x, means no change
                              +(2<<9)     // SINC, sor addr increment, src is sram, so should set to 00, means to increase 
                              +(1<<11)    // DEST_MSIZE, des burst length, set to 001 means 4 DST_TR_WIDTH per burst transcation
                              +(1<<14)    // SRC_MSIZE, sor burst length, set to 001 means 4 SOR_TR_WIDTH per burst transcation
                              +(0<<20)    // TT_FC,transfer type and flow control,001 means memory to peripheral,dma is flow controller
                              +(0<<23)    // DMS, des master select, 0 means ahb master 0
                              +(0<<25)    // SMS, sor master select, 1 means ahb master 1
                              +(0<<27)    // LLP_DST_EN, des block chaining enable, set to 0 disable it
                              +(0<<28) ;  // LLP_SOR_EN, sor block chaining enable, set to 0 disable it	
	
	DataCopyChannel->SAR = (u32)(&Sdata);
	DataCopyChannel->DAR = (u32)(Ddata);
	DataCopyChannel->CTL_L = temreg;
	DataCopyChannel->CTL_H = len;//DMA 传输字节数
	DataCopyChannel->LLP =0;
	DataCopyChannel->CFG_L=0;
	DataCopyChannel->CFG_H = 0;
	DMA->DmaCfgReg = 1;
	DMA->ChEnReg = (1<<(DATA_COPY_R2R_CHANNLE + 8))|(1<<DATA_COPY_R2R_CHANNLE);
}


