#include "lcdInit.h"
#include "lcdDriver.h"
#include "m1130_gpio.h"
#include "LcdSpi.h"
#define writeCOM Lcd_Driver.writeCMD
#define writeDAT Lcd_Driver.writeData

/*
*********************************************************************************************************************
*函数功能：LCDinitDelay LCD初始化模块延时函数
*入口参数：dly  延时时间
*出口参数：无
*********************************************************************************************************************
*/
void LCDDelay(unsigned int dly)
{
    unsigned int i,j;
    for(i=0;i<dly;i++)
    	for(j=0;j<255;j++);
}
void mxg(void)
{
	
 		writeCOM(0xb1);   //frame rate control (in normal mode/full colors)
		writeDAT(0x05);
		writeDAT(0x3c);
		writeDAT(0x3c);

     writeCOM(0xb2);   //frame rate control (in idle mode/8-colors)
		writeDAT(0x05);
		writeDAT(0x3c);
		writeDAT(0x3c);
	
		writeCOM(0xb3);   //frame rate control (in partial mode/full-colors)
		writeDAT(0x05);
		writeDAT(0x3c);
		writeDAT(0x3c);
		writeDAT(0x05);
		writeDAT(0x3c);
		writeDAT(0x3c);

		writeCOM(0xb4);   //display inversion control
		writeDAT(0x07);   //0x00 dot inversion	 0x07 column inversion

		writeCOM(0xc0);   //power control 1
		writeDAT(0x28);
		writeDAT(0x08);
		writeDAT(0x04);
	
		writeCOM(0xc1);   //power control 2
		writeDAT(0xc0);  
	
		writeCOM(0xc2);   //power control 3 (in normal mode/full colors)
		writeDAT(0x0d);
		writeDAT(0x00);
	
		writeCOM(0xc3);   //power control 4 (in idle mode/8-colors)
		writeDAT(0x8d);
		writeDAT(0x2a);
	
		writeCOM(0xc4);   //power control 5 (in partial mode/full-colors)
		writeDAT(0x8d);
		writeDAT(0xee);
			
		writeCOM(0xc5);   //VCOM control 1
		writeDAT(0x05);	  //0x00-0x3f
			
		writeCOM(0xc7);	   //vcom offset control
		writeDAT(0x10);	   //0x00-0x1f

		writeCOM(0x36);   //memory data access control
		writeDAT(0xc8);   //c0

		writeCOM(0xe0);   //gamma positive correction
		writeDAT(0x04);
		writeDAT(0x22);
		writeDAT(0x07);
		writeDAT(0x0a);
		writeDAT(0x2e);
		writeDAT(0x30);
		writeDAT(0x25);
		writeDAT(0x2a);
		writeDAT(0x28);
		writeDAT(0x26);
		writeDAT(0x2e);
		writeDAT(0x3a);
		writeDAT(0x00);
		writeDAT(0x01);
		writeDAT(0x03);
		writeDAT(0x13);

		writeCOM(0xe1);   //gamma negative correction
		writeDAT(0x04);
		writeDAT(0x16);
		writeDAT(0x06);
		writeDAT(0x0d);
		writeDAT(0x2d);
		writeDAT(0x26);
		writeDAT(0x23);
		writeDAT(0x27);
		writeDAT(0x27);
		writeDAT(0x25);
		writeDAT(0x2d);
		writeDAT(0x3b);
		writeDAT(0x00);
		writeDAT(0x01);
		writeDAT(0x04);
		writeDAT(0x13);

		writeCOM(0x3a);    //set_pixel_format
		writeDAT(0x05);	   //16 bit/pixel
				
     writeCOM(0x2A);	  //column address set
		writeDAT(0x00);
		writeDAT(0x00);	  //0x02
		writeDAT(0x00);
		writeDAT(0x7f);	  //129

		writeCOM(0x2B);   //row address set
		writeDAT(0x00);
		writeDAT(0x00);	  //0x01
		writeDAT(0x00);
		writeDAT(0x9f);   //160
	
		writeCOM(0x2c);    //memory write

		writeCOM(0x29);    // Display On
}
/*
*********************************************************************************************************************
*函数功能：LCD_Init，LCD初始化，初始化LCD的硬件
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
void LCD_Init(void)
{
    Lcd_SPI_Init(4,4,0);//48MHZ
	
	GPIO_SetPinMux(GPIO0, GPIO_Pin_29, GPIO_FUNCTION_0);
	GPIO_SetPinDir(GPIO0, GPIO_Pin_29,GPIO_Mode_OUT);//SD CS
	GPIO_SetBits(GPIO0, GPIO_Pin_29);
	
	GPIO_SetPinMux(GPIO0, GPIO_Pin_24,GPIO_FUNCTION_0);
	GPIO_SetPinDir(GPIO0, GPIO_Pin_24,GPIO_Mode_OUT);//BG
	
	GPIO_SetPinMux(GPIO1, GPIO_Pin_10,GPIO_FUNCTION_0);
	GPIO_SetPinDir(GPIO1, GPIO_Pin_10,GPIO_Mode_OUT);//RD
	
	GPIO_SetPinMux(GPIO1, GPIO_Pin_11, GPIO_FUNCTION_0);
	GPIO_SetPinDir(GPIO1, GPIO_Pin_11,GPIO_Mode_OUT);//RS
	
	LCD_RS_G();
	LCD_RD_G();
	
	//关闭显示
	Lcd_Driver.writeCMD(0x28); 
	//Set the gray scale voltage to adjust the gamma characteristics of the TFT panel
	//设置灰度电压，调整TFT面板的伽马特性
	Lcd_Driver.writeCMD(0xCF);  
	Lcd_Driver.writeData(0x00); 
	Lcd_Driver.writeData(0xC1); 
	Lcd_Driver.writeData(0X30);
	
	Lcd_Driver.writeCMD(0xED);  
	Lcd_Driver.writeData(0x64); 
	Lcd_Driver.writeData(0x03); 
	Lcd_Driver.writeData(0X12); 
	Lcd_Driver.writeData(0X81); 
	
	Lcd_Driver.writeCMD(0xE8);  
	Lcd_Driver.writeData(0x85); 
	Lcd_Driver.writeData(0x10); 
	Lcd_Driver.writeData(0x7A);
	
	Lcd_Driver.writeCMD(0xCB);  
	Lcd_Driver.writeData(0x39); 
	Lcd_Driver.writeData(0x2C); 
	Lcd_Driver.writeData(0x00); 
	Lcd_Driver.writeData(0x34); 
	Lcd_Driver.writeData(0x02); 
	
	Lcd_Driver.writeCMD(0xF7);  
	Lcd_Driver.writeData(0x20); 
	
	Lcd_Driver.writeCMD(0xEA);  
	Lcd_Driver.writeData(0x00); 
	Lcd_Driver.writeData(0x00);
	//设置VREG1OUT,VREG2OUT	
	Lcd_Driver.writeCMD(0xC0);    //Power control 
	Lcd_Driver.writeData(0x1B);   //VRH[5:0] 
	//设置升压电路中使用的系数
	Lcd_Driver.writeCMD(0xC1);    //Power control 
	Lcd_Driver.writeData(0x01);   //SAP[2:0];BT[3:0] 
	
	Lcd_Driver.writeCMD(0xC5);    //VCM control 
	Lcd_Driver.writeData(0x30); 	 //3F
	Lcd_Driver.writeData(0x30); 	 //3C
	
	Lcd_Driver.writeCMD(0xC7);    //VCM control2 
	Lcd_Driver.writeData(0XB7); 
	//此命令定义帧内存的读/写扫描方向
	Lcd_Driver.writeCMD(0x36);    // Memory Access Control 
	Lcd_Driver.writeData(0x28);
	//接口的像素格式	
	Lcd_Driver.writeCMD(0x3A);   
	Lcd_Driver.writeData(0x55); 
	//Frame Rate Control帧速率控制
	Lcd_Driver.writeCMD(0xB1);   
	Lcd_Driver.writeData(0x00);   
	Lcd_Driver.writeData(0x1A);
	//Display Function Control显示功能控制
	Lcd_Driver.writeCMD(0xB6);    // Display Function Control 
	Lcd_Driver.writeData(0x82); //0x0A 
	Lcd_Driver.writeData(0x02); //0xA2 
	Lcd_Driver.writeCMD(0xF2);    // 3Gamma Function Disable 
	Lcd_Driver.writeData(0x00); 
	Lcd_Driver.writeCMD(0x26);    //Gamma curve selected 
	Lcd_Driver.writeData(0x01); 
	//设置灰度电压，调整TFT面板的伽马特性
	Lcd_Driver.writeCMD(0xE0);    //Set Gamma 
	Lcd_Driver.writeData(0x0F); 
	Lcd_Driver.writeData(0x2A); 
	Lcd_Driver.writeData(0x28); 
	Lcd_Driver.writeData(0x08); 
	Lcd_Driver.writeData(0x0E); 
	Lcd_Driver.writeData(0x08); 
	Lcd_Driver.writeData(0x54); 
	Lcd_Driver.writeData(0XA9); 
	Lcd_Driver.writeData(0x43); 
	Lcd_Driver.writeData(0x0A); 
	Lcd_Driver.writeData(0x0F); 
	Lcd_Driver.writeData(0x00); 
	Lcd_Driver.writeData(0x00); 
	Lcd_Driver.writeData(0x00); 
	Lcd_Driver.writeData(0x00); 
	//设置灰度电压，调整TFT面板的伽马特性 	
	Lcd_Driver.writeCMD(0XE1);    //Set Gamma 
	Lcd_Driver.writeData(0x00); 
	Lcd_Driver.writeData(0x15); 
	Lcd_Driver.writeData(0x17); 
	Lcd_Driver.writeData(0x07); 
	Lcd_Driver.writeData(0x11); 
	Lcd_Driver.writeData(0x06); 
	Lcd_Driver.writeData(0x2B); 
	Lcd_Driver.writeData(0x56); 
	Lcd_Driver.writeData(0x3C); 
	Lcd_Driver.writeData(0x05); 
	Lcd_Driver.writeData(0x10); 
	Lcd_Driver.writeData(0x0F); 
	Lcd_Driver.writeData(0x3F); 
	Lcd_Driver.writeData(0x3F); 
	Lcd_Driver.writeData(0x0F); 
	Lcd_Driver.writeData(0x2B); 
	Lcd_Driver.writeData(0x00);
	Lcd_Driver.writeData(0x00);
	Lcd_Driver.writeData(0x00);
	Lcd_Driver.writeData(0xef);	
	Lcd_Driver.writeData(0x2A); 
	Lcd_Driver.writeData(0x00);
	Lcd_Driver.writeData(0x00);
	Lcd_Driver.writeData(0x01);
	Lcd_Driver.writeData(0x3f);	 
	//此命令关闭睡眠模式
	Lcd_Driver.writeCMD(0x11); 
	//开启反显，正常0000显示黑色，ffff显示白色，如果不发这个命令，会显示反的
	Lcd_Driver.writeCMD(0x21); 
	//开启显示
	Lcd_Driver.writeCMD(0x29); 

	Lcd_Driver.para->dir=Lcd_Driver.scan_dir(1);//测试程序切换到横屏
	LCD_BG_OPEN();
}
