#ifndef _lcdInterface_h
#define _lcdInterface_h
#include "typedefine.h"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：LCD驱动接口
 *实现功能：1、
 *
 *
 *撰 写 人：Alpscale LCD Application development team
 *撰写时间：2019-7-22
 *测 试 人：Alpscale LCD Application development team
 *测试时间：2019-7-22
 *版 本 号：V1.01_2019-7-22
 *版 本 号：V1.02_2019-10-21  
 *版本说明：
 *                              
 *          函数清单
 *
 *V1.01：   功能实现
 *
 *V1.02： 
 */
 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "resConfig.h"
#include "widgetbinfo.h"

typedef struct _LCD_DESCRIPTOR{
	pLCD_PARAMETER para; 
	const SYSTEM_FONTLIB* font;
	void (*drawPoint)(uint16_t x,uint16_t y);
	uint32_t (*readPoint)(uint16_t x,uint16_t y);
	void (*clear)(void);
	void (*fill)(uint16_t sx,uint16_t sy,uint16_t ex,uint16_t ey);
	void (*fillUseOneData)(LCD_POSITION sPos,LCD_POSITION ePos,uint16_t Data,uint32_t WriteCount);
	void (*fillUseContinuousData)(LCD_POSITION sPos,const uint16_t* Data,uint16_t dataWidth,uint16_t dataHeight);
	void (*fillUsePartContinuousData)(const uint16_t* DataBase,uint16_t WidthOffset,WIDGET_LOCATION dispLoc,LCD_POSITION dataPos);
	uint16_t (*getMaxWidthUseContinuousDataSplitBit)(const uint8_t* DataBase,uint16_t widthOffset,uint16_t heightOffset);
	uint16_t (*fillUseContinuousDataSplitBit)(const uint8_t* DataBase,uint16_t CharWidthOffset,WIDGET_LOCATION dispLoc);
	uint16_t (*fillUseContinuousDataSplitBitHasBack)(const uint8_t* DataBase,uint16_t CharWidthOffset,WIDGET_LOCATION dispLoc,uint16_t picId);
}LCD_DESCRIPTOR;
extern const  LCD_DESCRIPTOR Lcd_Des;

#endif

