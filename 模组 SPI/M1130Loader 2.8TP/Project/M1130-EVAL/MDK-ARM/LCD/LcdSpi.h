#ifndef _LcdSpi_h
#define _LcdSpi_h
#include "typedefine.h"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：LcdSPI接口驱动
 *实现功能：1、LCD SPI接口驱动
 *
 *
 *撰 写 人：Alpscale LCD Application development team
 *撰写时间：2020-5-20
 *测 试 人：Alpscale LCD Application development team
 *测试时间：2020-5-20
 *版 本 号：V1.01_2020-5-20
 *版本说明：
 *                              
 *          函数清单
 *
 *V1.01：   功能实现
 *
 */
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Lcd_SPI_Init(uint32_t rcc_clk_div,int internal_clk_div, int internal_clk_rate);
void Lcd_WaitSendEnd(void);
void Data_WaitCopyEnd(void);
void Lcd_SendBytes(const uint8_t *data , uint32_t len);
void Data_CopyHalfWord(uint16_t Sdata , uint16_t *Ddata , uint32_t len);

#endif
