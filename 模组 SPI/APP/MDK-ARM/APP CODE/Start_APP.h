#ifndef _Start_APP_h
#define _Start_APP_h
#include "gWidgetInfo.h"
#include "gStdint.h"
#include "gReturn.h"
#include "gResMsg.h"
#define StartwID 0x0001
static const PAGE_INFO StartPage={
	{.wType = WIDGET_TYPE_PAGE , .wId = StartwID , .wVscope = WIDGET_VS_PRIVATE},
	{.x = 0 , .y = 0 , .width = 128 , .height = 160},
	.backMode = WIDGET_BACKMODE_PIC , 
	.backColor = 0x00FF,
	.picId = 0,
	.enterEvent = GUI_NULL,
	.leaveEvent = GUI_NULL,
	.pressEvent = GUI_NULL,
	.releaseEvent = GUI_NULL,
};
#define StartGTimerwID 0x0002
static const GTIMER_INFO StartPageGTimer={
 {.wType = WIDGET_TYPE_GTIMER , .wId = StartGTimerwID , .wVscope = WIDGET_VS_PRIVATE},
 .timeOut = 100,
 .enable = 1,                         //定时器开关位，0关，1开
 .timeValue = 0,                     //定时器当前计数值（周期是1MS）低16位是当前计数器值，高16位是定时器溢出标志，或者其他标识
 .timeoutEvent = GUI_NULL
};
#define LBatteryPicwID 0x0003//左电池
static const PICTUREBOX_INFO LBatteryPic={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = LBatteryPicwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 0 , .y = 5 , .width = 40 , .height = 10},    
 .picId = 12,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
#define RBatteryPicwID 0x0004//右电池
static const PICTUREBOX_INFO RBatteryPic={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = RBatteryPicwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 88 , .y = 5 , .width = 40 , .height = 10},     
 .picId = 6,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
#define LPointPicwID 0x0005//左箭头
static const PICTUREBOX_INFO LPointPic={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = LPointPicwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 0 , .y = 140 , .width = 30 , .height = 20},    
 .picId = 13,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
#define RPointPicwID 0x0006//右箭头
static const PICTUREBOX_INFO RPointPic={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = RPointPicwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 98 , .y = 140 , .width = 30 , .height = 20},    
 .picId = 16,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
#define NumGePicwID 0x0007//大数字个
static const PICTUREBOX_INFO NumGePic={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = NumGePicwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 39 , .y = 35 , .width = 18 , .height = 20},    
 .picId = 19,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
#define NumShiPicwID 0x0008//大数字十
static const PICTUREBOX_INFO NumShiPic={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = NumShiPicwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 21 , .y = 35 , .width = 18 , .height = 20},    
 .picId = 20,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
#define NumBaiPicwID 0x0009//大数字小数
static const PICTUREBOX_INFO NumBaiPic={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = NumBaiPicwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 63 , .y = 35 , .width = 18 , .height = 20},    
 .picId = 19,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
#define NumPointPicwID 0x000A//大数字小数点
static const PICTUREBOX_INFO NumPointPic={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = NumPointPicwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 57 , .y = 45 , .width = 6 , .height = 10},    
 .picId = 29,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
#define IGePicwID 0x000B//I个
static const PICTUREBOX_INFO IGePic={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = IGePicwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 61 , .y = 74 , .width = 6 , .height = 10},    
 .picId = 41,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
#define IShiPicwID 0x000C//I小数
static const PICTUREBOX_INFO IShiPic={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = IShiPicwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 70 , .y = 74 , .width = 6 , .height = 10},    
 .picId = 40,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
#define RGePicwID 0x000D//R个
static const PICTUREBOX_INFO RGePic={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = RGePicwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 61 , .y = 95 , .width = 6 , .height = 10},    
 .picId = 31,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
#define RShiPicwID 0x000E//R小数
static const PICTUREBOX_INFO RShiPic={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = RShiPicwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 70 , .y = 95 , .width = 6 , .height = 10},    
 .picId = 30,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
#define UGePicwID 0x000F//U个
static const PICTUREBOX_INFO UGePic={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = UGePicwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 61 , .y = 119 , .width = 6 , .height = 10},    
 .picId = 31,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
#define UShiPicwID 0x0010//U小数
static const PICTUREBOX_INFO UShiPic={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = UShiPicwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 70 , .y = 119 , .width = 6 , .height = 10},    
 .picId = 30,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};






void guiMainPageInit(void);
gui_Err logoEnterEvent(gui_int32 argc , const char **argv);
static gui_Err DemoGTimer_timeoutEvent(gui_int32 argc , const char **argv);
void CMD_Handle(void);



#endif

