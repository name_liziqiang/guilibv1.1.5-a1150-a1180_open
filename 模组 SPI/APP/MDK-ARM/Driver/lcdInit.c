#include "lcdInit.h"
#include "m1130.h"
#include "m1130_rcc.h"
#include "lcdDriver.h"
#include "m1130_gpio.h"
#include "m1130_qspi.h"
#include "QspiDMA.h"
#include "m1130_tim.h"

#define writeCOM Lcd_Driver.writeCMD
#define writeDAT Lcd_Driver.writeData
/*
*********************************************************************************************************************
*函数功能：LCDinitDelay LCD初始化模块延时函数
*入口参数：dly  延时时间
*出口参数：无
*********************************************************************************************************************
*/
void LCDDelay(unsigned int dly)
{
    unsigned int i,j;
    for(i=0;i<dly;i++)
    	for(j=0;j<255;j++);
}
void LCD_DIM_init(uint16_t arr,uint16_t psc,uint16_t pulse)
{
	//TIM15  CH1 输出PWM
	RCC->AHBCLKCTRL0_SET|=(1<<24);//开启定时器时钟
	RCC->AHBCLKCTRL0_SET|=(1<<3);
	RCC->PWMCLKDIV = 0X01;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	GPIO_InitTypeDef GPIO_InitStructure; 
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Function = GPIO_FUNCTION_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_Init(GPIO1, &GPIO_InitStructure);
	
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Prescaler=psc;//96Mhz/(95+1)=1Mhz
	TIM_TimeBaseStructure.TIM_Period=arr ;//fpwm=9600/(1199+1)=80Khz
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);
	
	//SYSAHBCLKDIV=4 在SystemInit已经初始化了 AHB时钟96Mhz	
	TIM_OCStructInit(&TIM_OCInitStructure);
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; //选择定时器模式:TIM脉冲宽度调制模式2
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; //比较输出使能
	TIM_OCInitStructure.TIM_Pulse = pulse; //设置待装入捕获比较寄存器的脉冲值
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; //输出极性:TIM输出比较极性高
	TIM_OC1Init(TIM4, &TIM_OCInitStructure);  //根据TIM_OCInitStruct中指定的参数初始化外设TIMx     //TIM4_CH1

	TIM_OC1PreloadConfig(TIM4,TIM_OCPreload_Enable);
	TIM_ARRPreloadConfig(TIM4,ENABLE);
	TIM_Cmd(TIM4,ENABLE);
}

void LcdSetDim(uint16_t pulse)
{
	TIM_SetCompare1(TIM4 , pulse);
}
/*
*********************************************************************************************************************
*函数功能：Lcd_SPI_Init 初始化LCD SPI接口
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
static void Lcd_SPI_Init(uint32_t rcc_clk_div,int internal_clk_div, int internal_clk_rate)
{
	QSPI_InitTypeDef QSPI_InitStructure;
	
	QSPI_InitStructure.QSPI_CPOL = QSPI_CPOL_Low;
	QSPI_InitStructure.QSPI_CPHA = QSPI_CPHA_1Edge;
	
	QSPI_InitStructure.QSPI_FirstBit = QSPI_FirstBit_MSB;
	QSPI_InitStructure.QSPI_ClockDiv = (internal_clk_div<<8);
	QSPI_InitStructure.QSPI_ClockRate = internal_clk_rate;
	QSPI_InitStructure.QSPI_SlaveMode = QSPI_MASTER_MODE;
	QSPI_InitStructure.QSPI_ModeSelect = QSPI_STD;
	QSPI_InitStructure.QSPI_FrameLength = QSPI_FRAME_LENGTH_8Bit;

	RCC->QUADSPI1CLKDIV = rcc_clk_div;
	GPIO_SetPinMux(GPIO0, GPIO_Pin_26, GPIO_FUNCTION_3);//SPI MOSI
	GPIO_SetPinMux(GPIO0, GPIO_Pin_27, GPIO_FUNCTION_3);//SPI CLK
	GPIO_SetPinMux(GPIO0, GPIO_Pin_30, GPIO_FUNCTION_3);//SPI MISO
	GPIO_SetPinMux(GPIO0, GPIO_Pin_31, GPIO_FUNCTION_3);//SPI CS  LCD
	
	RCC_ResetAHBCLK(1 << AHBCLK_BIT_DMA);
	// DmaStop(LCD_QSPIDMA_SEND_CHANNEL);
	// DmaStop(FLASH_QSPIDMA_READ_CHANNEL);
	// DmaStop(FLASH_QSPIDMA_WRITE_CHANNEL);
	QSPI_HwInit(LCD_QSPI, &QSPI_InitStructure);
}
void mxg(void)
{
	
	  writeCOM(0xb1);   //frame rate control (in normal mode/full colors)
		writeDAT(0x05);
		writeDAT(0x3c);
		writeDAT(0x3c);

     writeCOM(0xb2);   //frame rate control (in idle mode/8-colors)
		writeDAT(0x05);
		writeDAT(0x3c);
		writeDAT(0x3c);
	
		writeCOM(0xb3);   //frame rate control (in partial mode/full-colors)
		writeDAT(0x05);
		writeDAT(0x3c);
		writeDAT(0x3c);
		writeDAT(0x05);
		writeDAT(0x3c);
		writeDAT(0x3c);

		writeCOM(0xb4);   //display inversion control
		writeDAT(0x07);   //0x00 dot inversion	 0x07 column inversion

		writeCOM(0xc0);   //power control 1
		writeDAT(0x28);
		writeDAT(0x08);
		writeDAT(0x04);
	
		writeCOM(0xc1);   //power control 2
		writeDAT(0xc0);  
	
		writeCOM(0xc2);   //power control 3 (in normal mode/full colors)
		writeDAT(0x0d);
		writeDAT(0x00);
	
		writeCOM(0xc3);   //power control 4 (in idle mode/8-colors)
		writeDAT(0x8d);
		writeDAT(0x2a);
	
		writeCOM(0xc4);   //power control 5 (in partial mode/full-colors)
		writeDAT(0x8d);
		writeDAT(0xee);
			
		writeCOM(0xc5);   //VCOM control 1
		writeDAT(0x05);	  //0x00-0x3f
			
		writeCOM(0xc7);	   //vcom offset control
		writeDAT(0x10);	   //0x00-0x1f

		writeCOM(0x36);   //memory data access control
		writeDAT(0xc8);   //c0

		writeCOM(0xe0);   //gamma positive correction
		writeDAT(0x04);
		writeDAT(0x22);
		writeDAT(0x07);
		writeDAT(0x0a);
		writeDAT(0x2e);
		writeDAT(0x30);
		writeDAT(0x25);
		writeDAT(0x2a);
		writeDAT(0x28);
		writeDAT(0x26);
		writeDAT(0x2e);
		writeDAT(0x3a);
		writeDAT(0x00);
		writeDAT(0x01);
		writeDAT(0x03);
		writeDAT(0x13);

		writeCOM(0xe1);   //gamma negative correction
		writeDAT(0x04);
		writeDAT(0x16);
		writeDAT(0x06);
		writeDAT(0x0d);
		writeDAT(0x2d);
		writeDAT(0x26);
		writeDAT(0x23);
		writeDAT(0x27);
		writeDAT(0x27);
		writeDAT(0x25);
		writeDAT(0x2d);
		writeDAT(0x3b);
		writeDAT(0x00);
		writeDAT(0x01);
		writeDAT(0x04);
		writeDAT(0x13);

		writeCOM(0x3a);    //set_pixel_format
		writeDAT(0x05);	   //16 bit/pixel
				
       writeCOM(0x2A);	  //column address set
		writeDAT(0x00);
		writeDAT(0x00);	  //0x02
		writeDAT(0x00);
		writeDAT(0x7f);	  //129

		writeCOM(0x2B);   //row address set
		writeDAT(0x00);
		writeDAT(0x00);	  //0x01
		writeDAT(0x00);
		writeDAT(0x9f);   //160
	
//		writeCOM(0x2c);    //memory write

//		writeCOM(0x29);    // Display On
}
/*
*********************************************************************************************************************
*函数功能：LCD_Init，LCD初始化，初始化LCD的硬件
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
void LCD_Init(void)
{
  Lcd_SPI_Init(4,2,0);//48MHZ
	
	GPIO_SetPinMux(GPIO0, GPIO_Pin_29, GPIO_FUNCTION_0);
	GPIO_SetPinDir(GPIO0, GPIO_Pin_29,GPIO_Mode_OUT);//SD CS
	GPIO_SetPin(GPIO0, GPIO_Pin_29);
	
	GPIO_SetPinMux(GPIO1, GPIO_Pin_10,GPIO_FUNCTION_0);
	GPIO_SetPinDir(GPIO1, GPIO_Pin_10,GPIO_Mode_OUT);//BG
	
	GPIO_SetPinMux(GPIO1, GPIO_Pin_11,GPIO_FUNCTION_0);
	GPIO_SetPinDir(GPIO1, GPIO_Pin_11,GPIO_Mode_OUT);//Rst
	
	GPIO_SetPinMux(GPIO0, GPIO_Pin_28, GPIO_FUNCTION_0);
	GPIO_SetPinDir(GPIO0, GPIO_Pin_28,GPIO_Mode_OUT);//RS
	
	LCD_RS_G();
	
	LCD_RST_D();
	LCDDelay(50);
	LCD_RST_G();
	LCDDelay(50);
	//关闭显示
	Lcd_Driver.writeCMD(0x28); 
	//Set the gray scale voltage to adjust the gamma characteristics of the TFT panel
	//设置灰度电压，调整TFT面板的伽马特性
	mxg(); 
//	//此命令关闭睡眠模式
	Lcd_Driver.writeCMD(0x11); 
//	//开启反显，正常0000显示黑色，ffff显示白色，如果不发这个命令，会显示反的
	Lcd_Driver.writeCMD(0x20); 
	//开启显示
	Lcd_Driver.writeCMD(0x29);

	Lcd_Driver.scan_dir(DIR_VERTICAL);//测试程序切换到横屏
	LCD_DIM_init(100,95,LCD_LuminanceMax);  //10Khz 50占空比	
}
