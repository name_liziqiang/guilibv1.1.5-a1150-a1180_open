#include "rtcDriver.h"
#include "m1130_i2c.h"
#include "m1130_utility_i2c.h"
#include "stdio.h"
#include "m1130_gpio.h"

#define ReadWriteBuff_Max               8//8字节缓冲区
static uint8_t ReadWriteBuff[ReadWriteBuff_Max];
/*
*********************************************************************************************************************
*函数功能：BcdToChar 将一个BCD码转换成十六进制
*入口参数：bcdData，BCD码
*出口参数：十六进制
*********************************************************************************************************************
*/ 
static inline uint8_t BcdToChar(uint8_t bcdData)
{
	uint8_t rdata,zdata;
	zdata=(bcdData>>4)&0x0f;
	if(zdata>9)zdata=9;
	rdata=zdata*10;
	zdata=bcdData&0x0f;
	if(zdata>9)zdata=9;
	rdata+=zdata;
	return rdata;
}
/*
*********************************************************************************************************************
*函数功能：CharToBcd 将一个十六进制转换成BCD
*入口参数：charData 十六进制
*出口参数：BCD码
*********************************************************************************************************************
*/ 
static uint8_t CharToBcd(uint8_t charData)
{
	uint8_t rdata,zdata;
	zdata=charData%100/10;
	rdata=zdata<<4;
	zdata=charData%10;
	rdata+=zdata;
	return rdata;
}
/*
*********************************************************************************************************************
*函数功能：RTC_Init  时钟初始化
*入口参数：无
*出口参数：-1 失败  0成功  其他值未定义
*********************************************************************************************************************
*/ 
int8_t RTC_Init(void)
{
	I2C_Configuration(I2C_SPEED_STD, I2C_GP7);
	return 0;
}
/*
*********************************************************************************************************************
*函数功能：RTC_WriteTime 写一个时间到RTC
*入口参数：rtcTime 待写入的时间
*出口参数：-1 失败  0成功  其他值未定义
*********************************************************************************************************************
*/ 
static int8_t RTC_WriteTime(pRTC_TIME rtcTime)
{
	ReadWriteBuff[0]=CharToBcd(rtcTime->Second);
	ReadWriteBuff[1]=CharToBcd(rtcTime->Minute);
	ReadWriteBuff[2]=CharToBcd(rtcTime->Hour);
	
	if(I2C_DmaWrite(0xA2>>1, 2, 1, ReadWriteBuff, 3)<0){
		return -1;//read failed
	}
	else{
		return 0;
	}
}
/*
*********************************************************************************************************************
*函数功能：RTC_WriteDate 写一个日期到RTC
*入口参数：rtcDate 待写入的日期
*出口参数：-1 失败  0成功  其他值未定义
*********************************************************************************************************************
*/ 
static int8_t RTC_WriteDate(pRTC_DATE rtcDate)
{
	ReadWriteBuff[0]=CharToBcd(rtcDate->Day);
	ReadWriteBuff[1]=CharToBcd(rtcDate->Weekday);
	ReadWriteBuff[2]=CharToBcd(rtcDate->Month);
	ReadWriteBuff[3]=CharToBcd(rtcDate->Year);
	
	if(I2C_DmaWrite(0xA2>>1, 5, 1, ReadWriteBuff, 4)<0){
		return -1;//read failed
	}
	else{
		return 0;
	}
}
/*
*********************************************************************************************************************
*函数功能：RTC_WriteDateTime 写一个时间日期到RTC
*入口参数：dateTime 待写入的日期时间
*出口参数：-1 失败  0成功  其他值未定义
*********************************************************************************************************************
*/ 
static int8_t RTC_WriteDateTime(pRTC_TIMEDATE dateTime)
{
	ReadWriteBuff[0]=CharToBcd(dateTime->time.Second);
	ReadWriteBuff[1]=CharToBcd(dateTime->time.Minute);
	ReadWriteBuff[2]=CharToBcd(dateTime->time.Hour);
	ReadWriteBuff[3]=CharToBcd(dateTime->date.Day);
	ReadWriteBuff[4]=CharToBcd(dateTime->date.Weekday);
	ReadWriteBuff[5]=CharToBcd(dateTime->date.Month);
	ReadWriteBuff[6]=CharToBcd(dateTime->date.Year);
	
	if(I2C_DmaWrite(0xA2>>1, 2, 1, ReadWriteBuff, 7)<0){
		printf("write error\r\n");
		return -1;//read failed
	}
	else{
		return 0;
	}
}
/*
*********************************************************************************************************************
*函数功能：RTC_ReadTime 读时间到缓存区
*入口参数：读取的缓冲区
*出口参数：-1 失败  0成功  其他值未定义
*********************************************************************************************************************
*/ 
static int8_t RTC_ReadTime(pRTC_TIME rtcTime)
{
	uint8_t i;
	#define READ_TIME_COUNT       3
	if(I2C_DmaRead(0xA3>>1, 2, 1, ReadWriteBuff, READ_TIME_COUNT)<0){
		return -1;//read failed
	}
	else{
		ReadWriteBuff[0]&=0x7f;
		ReadWriteBuff[1]&=0x7f;
		ReadWriteBuff[2]&=0x3f;
		for(i=0;i<READ_TIME_COUNT;i++){
			ReadWriteBuff[i]=BcdToChar(ReadWriteBuff[i]);
		}
		rtcTime->Second=ReadWriteBuff[0];
		rtcTime->Minute=ReadWriteBuff[1];
		rtcTime->Hour=ReadWriteBuff[2];
		return 0;
	}
}
/*
*********************************************************************************************************************
*函数功能：RTC_ReadDate 读取日期到缓存区
*入口参数：读取的缓冲区
*出口参数：-1 失败  0成功  其他值未定义
*********************************************************************************************************************
*/ 
static int8_t RTC_ReadDate(pRTC_DATE rtcDate)
{
	uint8_t i;
	#define READ_Date_COUNT       4
	if(I2C_DmaRead(0xA3>>1, 5, 1, ReadWriteBuff, READ_Date_COUNT)<0){
		return -1;//read failed
	}
	else{
		ReadWriteBuff[0]&=0x3f;
		ReadWriteBuff[1]&=0x07;
		ReadWriteBuff[2]&=0x1f;
		for(i=0;i<READ_Date_COUNT;i++){
			ReadWriteBuff[i]=BcdToChar(ReadWriteBuff[i]);
		}
		rtcDate->Day=ReadWriteBuff[0];
		rtcDate->Weekday=ReadWriteBuff[1];
		rtcDate->Month=ReadWriteBuff[2];
		rtcDate->Year=ReadWriteBuff[3];
		return 0;
	}
}
/*
*********************************************************************************************************************
*函数功能：RTC_ReadDateTime 读取日期和时间到缓存区
*入口参数：读取的缓冲区
*出口参数：-1 失败  0成功  其他值未定义
*********************************************************************************************************************
*/ 
static int8_t RTC_ReadDateTime(pRTC_TIMEDATE dateTime)
{
	uint8_t i;
	#define READ_DateTime_COUNT       7
	if(I2C_DmaRead(0xA3>>1, 2, 1, ReadWriteBuff, READ_DateTime_COUNT)<0){
		return -1;//read failed
	}
	else{
		ReadWriteBuff[0]&=0x7f;
		ReadWriteBuff[1]&=0x7f;
		ReadWriteBuff[2]&=0x3f;
		ReadWriteBuff[3]&=0x3f;
		ReadWriteBuff[4]&=0x07;
		ReadWriteBuff[5]&=0x1f;
		for(i=0;i<READ_DateTime_COUNT;i++){
			ReadWriteBuff[i]=BcdToChar(ReadWriteBuff[i]);
		}
		dateTime->time.Second=ReadWriteBuff[0];
		dateTime->time.Minute=ReadWriteBuff[1];
		dateTime->time.Hour=ReadWriteBuff[2];
		dateTime->date.Day=ReadWriteBuff[3];
		dateTime->date.Weekday=ReadWriteBuff[4];
		dateTime->date.Month=ReadWriteBuff[5];
		dateTime->date.Year=ReadWriteBuff[6];
		return 0;
	}
}
/*
*********************************************************************************************************************
*功能：关于RTC操作的设备描述符，包含对RTC操作的接口
*********************************************************************************************************************
*/ 
const RTC_DES Rtc_Des={
	RTC_WriteTime,
	RTC_WriteDate,
	RTC_WriteDateTime,
	RTC_ReadTime,
	RTC_ReadDate,
	RTC_ReadDateTime
};


void systimread_config(void)
{	
		GPIO_SetPinMux(GPIO0, GPIO_Pin_0, GPIO_FUNCTION_2);  /*SDA*/
		GPIO_SetPinMux(GPIO0, GPIO_Pin_1, GPIO_FUNCTION_2);  /*SCL*/		
		GPIO_SetPinMux(GPIO1, GPIO_Pin_28, GPIO_FUNCTION_0);  /*SDA*/
		GPIO_SetPinMux(GPIO1, GPIO_Pin_29, GPIO_FUNCTION_0);  /*SCL*/
}
