/**
  ******************************************************************************
  * @file    m1130_adc.h
  * @author  Alpscale Application Team
  * @version V1.0.1
  * @date    27-12-2018
  * @brief   This file contains all the functions prototypes for the XIP firmware 
  *          library
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __M1130_XIP_WRITE_H
#define __M1130_XIP_WRITE_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "m1130.h"
  
void xipWriteTest(void);
int xipTX(QSPI_TypeDef *QSPIptr, uint8_t *data, uint32_t num);
int xipRX(QSPI_TypeDef* QSPIptr,uint8_t *data,uint32_t num);
void XIP_EraseFlash_Block_4K(uint32_t faddr,uint32_t count);
void XIP_WriteFlash_NoCheck(uint8_t* pBuffer,uint32_t WriteAddr,uint32_t NumByteToWrite);
void loadData8FromQspi0(uint32_t addr, uint8_t* buf, uint32_t count);
void loadData16FromQspi0(uint32_t addr, uint16_t* buf, uint32_t count);
void loadData32FromQspi0(uint32_t addr, uint32_t* buf, uint32_t count);

#ifdef __cplusplus
}
#endif

#endif /*__M1130_XIP_WRITE_H */
