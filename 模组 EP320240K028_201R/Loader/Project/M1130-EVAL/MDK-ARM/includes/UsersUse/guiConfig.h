/*
 * @Author: your name
 * @Date: 2020-03-23 09:45:38
 * @LastEditTime: 2020-03-23 09:57:21
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \LoaderLib V1.0.1\includes\UsersUse\guiConfig.h
 */
#ifndef _guiConfig_h
#define _guiConfig_h
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：控制lib全局开关的变量定义
 *实现功能：
 *
 *撰 写 人：Alpscale LCD Application development team
 *撰写时间：2019-7-22
 *测 试 人：Alpscale LCD Application development team
 *测试时间：2019-7-22
 *版 本 号：V1.01_2019-7-22
 *版本说明：
 *                              
 *          函数清单
 *
 *V1.01：   功能实现
 *
 *V1.02：
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "gStdint.h"

#ifdef __cplusplus
 extern "C" {
#endif
	 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *储存器FLASH 配置信息
 *使用宏定义的内容 用户是不可以修改的但是可以使用   通过变量定义的配置项  用户可以修改他的值	
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
extern const gui_uint32  FLASH_TOTAL_SIZE;
extern const gui_uint32  FLASH_SECTOR_SIZE;
extern const gui_uint32  FLASH_RES_START_ADDR;
	 
	 
void checkGuiLibMsg(void);

#ifdef __cplusplus
}
#endif

#endif 
