/*
 * @Author: your name
 * @Date: 2020-03-23 09:45:38
 * @LastEditTime: 2020-03-23 14:49:09
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \LoaderLib V1.0.1\includes\UsersUse\guiConfig.c
 */
#include "guiConfig.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *储存器FLASH 配置信息
 *客户在储存 资源 文件得时候 应该 在FLASH_RES_START_ADDR+FLASH_SECTOR_SIZE，开始写入，因为第一个扇区，被用来储存一些LIB固化信息
 */
////////////////////////////////////////////////////////////////////////////////////////////*///////////////////////////////////////////
const gui_uint32  FLASH_TOTAL_SIZE       =0x01000000;		 																		 //储存器flash 总容量大小
const gui_uint32  FLASH_SECTOR_SIZE      =4096;                                              //储存器flash 扇区大小
const gui_uint32  FLASH_RES_START_ADDR   =4096;                                        //资源储存开始地址
                                                                                             //资源数据包含：GUI资源数据、GUI使用的一些系统数据

