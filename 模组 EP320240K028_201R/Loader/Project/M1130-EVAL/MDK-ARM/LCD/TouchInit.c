#include "TouchInit.h"
#include "m1130_gpio.h"
#include "m1130_qspi.h"


/*
*********************************************************************************************************************
*函数功能：TouchInit，触摸控制器启动初始化
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/ 

void TouchInit(void)
{

	GPIO_SetPinMux(GPIO1, GPIO_Pin_17,GPIO_FUNCTION_0);//QSPI2_XPT_CS 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_17,GPIO_Mode_OUT);  //QSPI2_XPT_CS 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_15,GPIO_FUNCTION_0);//QSPI2_TF_CS 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_15,GPIO_Mode_OUT);  //QSPI2_TF_CS 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_14,GPIO_FUNCTION_0);//QSPI2_MISO 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_14,GPIO_Mode_IN);   //QSPI2_MISO 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_10,GPIO_FUNCTION_0);//QSPI2_MOSI 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_10,GPIO_Mode_OUT);  //QSPI12_MOSI 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_11,GPIO_FUNCTION_0);//QSPI2_CLK 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_11,GPIO_Mode_OUT);  //QSPI2_CLK 	
	GPIO_SetPinMux(GPIO1, GPIO_Pin_16,GPIO_FUNCTION_0);//PEN
	GPIO_SetPinDir(GPIO1, GPIO_Pin_16,GPIO_Mode_IN);  //PEN
}



