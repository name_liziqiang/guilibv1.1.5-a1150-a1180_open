#include "lcddriver.h"
#include "stdio.h"
#include "m1130_gpio.h"
#include "ascii.h"
/*
*********************************************************************************************************************
*函数功能：LCD 常用参数定义
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/

LCD_PARAMETER lcdPara={
	LCD_WIDTH,                          //LCD 宽度
	LCD_HEIGHT,                         //LCD 高度
	0xe73c,                             //LCD 使用的背景颜色
	RED,                                //LCD 使用的字体颜色
	0,                                  //LCD ID   
	DIR_VERTICAL,                       //横屏还是竖屏控制
};
/*
*********************************************************************************************************************
*函数功能：LCD_Read_DATA，从LCD读取一个数据
*入口参数：无
*出口参数：uint16_t，读取到的数据
*********************************************************************************************************************
*/
static uint16_t LCD_Read_DATA(void)
{
	return 0;
}
/*
*********************************************************************************************************************
*函数功能：LCD_Write_DATA  写一个数据到LCD
*入口参数：data 写入的数据
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_Write_DATA(uint16_t data)
{
	LCD_RS_G();
	LCD_RD_G();
	LCD_WR_G();
	LCD_CS_D();
	
	/*写一个数据到LCD*/
	LCD_LOCK_OTHER_PORT();//锁住无关端口
	GPIO0->DT=(uint32_t)data<<16;
	LCD_UNLOCK_OTHER_PORT();//释放无关端口
	
//	LCD_WR_D();
	LCD_WR_G();
	LCD_CS_G();
}
/*
*********************************************************************************************************************
*函数功能：LCD_Write_CMD  写一个命令到LCD
*入口参数：cmd 写入的命令
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_Write_CMD(uint16_t cmd)
{
	LCD_RS_D();
	LCD_RD_G();
	LCD_WR_G();
	LCD_CS_D();

	/*写一个数据到LCD*/
	LCD_LOCK_OTHER_PORT();//锁住无关端口
	GPIO0->DT=(uint32_t)cmd<<16;
	LCD_UNLOCK_OTHER_PORT();//释放无关端口

	LCD_WR_G();
	LCD_CS_G();
	LCD_RS_G();
}
/*
*********************************************************************************************************************
*函数功能：LCD_WriteReg，LCD写一个寄存器的值
*入口参数：LCD_Reg 寄存器地址
*          LCD_RegValue 需要的寄存器的值
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_WriteReg(uint16_t LCD_Reg,uint16_t LCD_RegValue)
{	
	LCD_Write_CMD(LCD_Reg);  
	LCD_Write_DATA(LCD_RegValue);	    		 
}
/*
*********************************************************************************************************************
*函数功能：LCD_SetCursor，LCD设置光标位置,开始位置和结束位置
*入口参数：Xpos:横坐标
*          Xepos:结束横坐标
*          Ypos:纵坐标
*          Yepos:结束纵坐标
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_SetCursor(uint16_t Xpos, uint16_t Ypos, uint16_t Xepos, uint16_t Yepos)
{	    
	LCD_Write_CMD(0X2A); 
	LCD_Write_DATA(Xpos>>8);LCD_Write_DATA(Xpos&0XFF);
	LCD_Write_DATA(Xepos>>8);LCD_Write_DATA(Xepos&0XFF); 		
	LCD_Write_CMD(0X2B); 
	LCD_Write_DATA(Ypos>>8);LCD_Write_DATA(Ypos&0XFF); 		
	LCD_Write_DATA(Yepos>>8);LCD_Write_DATA(Yepos&0XFF); 	
} 
/*
*********************************************************************************************************************
*函数功能：LCD_DisplayOn LCD开启显示
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_DisplayOn(void)
{					   
	LCD_Write_CMD(0x29);
}	
/*
*********************************************************************************************************************
*函数功能：LCD_DisplayOff，LCD关闭显示
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_DisplayOff(void)
{	   
	LCD_Write_CMD(0x28);
}  
/*
*********************************************************************************************************************
*函数功能：LCD_Scan_Dir，设置扫描方向
*入口参数：dir，新的扫描方向
*出口参数：LCD_SCAN，返回新的扫描方向
*********************************************************************************************************************
*/  
static LCD_SCAN LCD_Scan_Dir(LCD_SCAN dir)
{
		if(dir==DIR_HORIZONTAL){
		//横屏
		LCD_WriteReg(0x36,0xA8);//0x36寄存器控制着LCD的扫描方向

	}
	else{
		LCD_WriteReg(0x36,0x48);//0x36寄存器控制着LCD的扫描方向，写入的都是RGB，bit3为0的时候 显示屏显示RGB  1的时候显示屏显示BGR，我们应该是BGR
	}
		uint16_t width_x=lcdPara.width;
		lcdPara.width=lcdPara.height;
		lcdPara.height=width_x;
  return dir;
} 

/*
*********************************************************************************************************************
*函数功能：LCD_Scroll，设置滚动区域（该驱动待测）
*入口参数：sx sy起点坐标
*          ex ey终点坐标
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_Scroll(uint16_t sx,uint16_t sy,uint16_t ex,uint16_t ey)
{
	uint16_t y_offset=sx-ex;
	LCD_Write_CMD(0x33);
	LCD_Write_DATA(sx>>8);LCD_Write_DATA(sx&0XFF);//顶部行
	LCD_Write_DATA(y_offset>>8);LCD_Write_DATA(y_offset&0XFF);
	LCD_Write_DATA(ey>>8);LCD_Write_DATA(ey&0XFF);//底部行
	LCD_Write_CMD(0x37);
	LCD_Write_DATA(ex>>8);LCD_Write_DATA(ex&0XFF);//底部行
}
/*
*********************************************************************************************************************
*函数功能：LCD_Clear，lcd清屏显示，单色，LCD描述符的背景色，全屏
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_Clear(void)
{ 
	uint32_t totalpoint=Lcd_Driver.para->width;
	totalpoint*=Lcd_Driver.para->height; 			  //得到总点数
	
	LCD_SetCursor(0,0,lcdPara.width-1,lcdPara.height-1);
	LCD_Write_CMD(0x2c);
	
	LCD_RS_G();
	LCD_CS_D();	

	LCD_LOCK_OTHER_PORT();//锁住无关端口
	//写入数据
	uint32_t i;
	for(i=0;i<totalpoint;i++){
		GPIO0->DT=(uint32_t)lcdPara.backcolor<<16;
//		LCD_WR_D();
		LCD_WR_G();		
	}
	LCD_UNLOCK_OTHER_PORT();//释放无关端口
	LCD_CS_G();
}
/*
*********************************************************************************************************************
*函数功能：LCD_Clear，lcd清屏显示，单色，LCD描述符的背景色，全屏
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
 void LCD_showcolor(uint16_t color)
{ 
	uint32_t totalpoint=Lcd_Driver.para->width;
	totalpoint*=Lcd_Driver.para->height; 			  //得到总点数
	
	LCD_SetCursor(0,0,lcdPara.width-1,lcdPara.height-1);
	LCD_Write_CMD(0x2c);
	
	LCD_RS_G();
	LCD_CS_D();	

	LCD_LOCK_OTHER_PORT();//锁住无关端口
	//写入数据
	uint32_t i;
	for(i=0;i<totalpoint;i++){
		GPIO0->DT=(uint32_t) color<<16;
//		LCD_WR_D();
		LCD_WR_G();		
	}
	LCD_UNLOCK_OTHER_PORT();//释放无关端口
	LCD_CS_G();
}
static void LCD_Fill_UseOneData(LCD_POSITION sPos,LCD_POSITION ePos,const uint16_t Data,uint32_t WriteCount)
{
	LCD_SetCursor(sPos.x,sPos.y,ePos.x,ePos.y);
	Lcd_Driver.writeCMD(0x2c);//发送写GRAM指令
	LCD_RS_G();
	LCD_CS_D();	

	LCD_LOCK_OTHER_PORT();//锁住无关端口
	//写入数据
	uint32_t i;
	for(i=0;i<WriteCount;i++){
		GPIO0->DT=(uint32_t)Data<<16;
		LCD_WR_G();
	}
	LCD_UNLOCK_OTHER_PORT();//释放无关端口
	LCD_CS_G();
}

/**
  * 函数功能: 对LCD显示器的某一点以某种颜色进行填充
  * 输入参数: usX ：在特定扫描方向下窗口的起点X坐标
  *           usY ：在特定扫描方向下窗口的起点Y坐标
  *           usColor ：颜色
  * 返 回 值: 无
  * 说    明：无
  */
void LCD_SetPointPixel(uint16_t usX,uint16_t usY,uint16_t usColor)	
{	
		LCD_POSITION sPos={usX,usY};
	LCD_POSITION ePos={usX,usY};
	LCD_Fill_UseOneData(sPos,ePos,usColor,1);	
}
/**
  * 函数功能: 在 LCD 显示器上使用 Bresenham 算法画线段
  * 输入参数: usX1 ：在特定扫描方向下窗口的起点X坐标
  *           usY1 ：在特定扫描方向下窗口的起点Y坐标
  *           usX2 ：在特定扫描方向下线段的另一个端点X坐标
  *           usY2 ：在特定扫描方向下线段的另一个端点Y坐标
  *           usColor ：线段的颜色
  * 返 回 值: 无
  * 说    明：无
  */
void LCD_DrawLine(uint16_t usX1,uint16_t usY1,uint16_t usX2,uint16_t usY2,uint16_t usColor)
{
	uint16_t us; 
	uint16_t usX_Current, usY_Current;
	int32_t lError_X=0,lError_Y=0,lDelta_X,lDelta_Y,lDistance; 
	int32_t lIncrease_X, lIncrease_Y;
	
	lDelta_X=usX2-usX1; //计算坐标增量 
	lDelta_Y=usY2-usY1; 
	usX_Current = usX1; 
	usY_Current = usY1; 
	
	if(lDelta_X>0)
  {
    lIncrease_X=1; //设置单步方向 
	}
  else if(lDelta_X==0)
  {
		lIncrease_X=0;//垂直线 
	}
	else 
  { 
    lIncrease_X=-1;
    lDelta_X=-lDelta_X;
  }
  
	if(lDelta_Y>0)
  {
		lIncrease_Y=1;
	}
	else if(lDelta_Y==0)
  {
		lIncrease_Y=0;//水平线 
	}
	else
  {
    lIncrease_Y=-1;
    lDelta_Y=-lDelta_Y;
  }
	
	if(lDelta_X>lDelta_Y)
  {
		lDistance=lDelta_X; //选取基本增量坐标轴 
	}
	else
  {
		lDistance=lDelta_Y; 
  }
	
	for(us=0;us<=lDistance+1;us++)//画线输出 
	{
		LCD_SetPointPixel(usX_Current,usY_Current,usColor);//画点 
		lError_X+=lDelta_X; 
		lError_Y+=lDelta_Y;
		if(lError_X>lDistance)
		{ 
			lError_X-=lDistance; 
			usX_Current+=lIncrease_X; 
		}
		if(lError_Y>lDistance) 
		{ 
			lError_Y-=lDistance; 
			usY_Current+=lIncrease_Y; 
		}		
	}
}

/**
  * 函数功能: 校正触摸时画十字专用 
  * 输入参数: x：十字中点x轴
  *           y：十字中点y轴
  * 返 回 值: 无
  * 说    明：无
  */
void LCD_DrawCross(uint16_t x,uint16_t y)
{
  LCD_DrawLine(x-10,y,x+10,y,RED);
  LCD_DrawLine(x,y-10,x,y+10,RED);
}

/**
  * 函数功能: 在LCD显示器上开辟一个窗口
  * 输入参数: usX ：在特定扫描方向下窗口的起点X坐标
  *           usY ：在特定扫描方向下窗口的起点Y坐标
  *           usWidth ：窗口的宽度
  *           usHeight ：窗口的高度
  * 返 回 值: 无
  * 说    明：无
  */
void LCD_OpenWindow(uint16_t usX, uint16_t usY, uint16_t usWidth, uint16_t usHeight)
{	
		LCD_Write_CMD(0x2A ); 				       /* 设置X坐标 */
	LCD_Write_DATA(usX>>8);	             /* 设置起始点：先高8位 */
	LCD_Write_DATA(usX&0xff);	           /* 然后低8位 */
	LCD_Write_DATA((usX+usWidth-1)>>8);  /* 设置结束点：先高8位 */
	LCD_Write_DATA((usX+usWidth-1)&0xff);/* 然后低8位 */

	LCD_Write_DATA(0x2B); 			           /* 设置Y坐标*/
	LCD_Write_DATA(usY>>8);              /* 设置起始点：先高8位 */
	LCD_Write_DATA(usY&0xff);            /* 然后低8位 */
	LCD_Write_DATA((usY+usHeight-1)>>8); /* 设置结束点：先高8位 */
	LCD_Write_DATA((usY+usHeight-1)&0xff);/* 然后低8位 */
}
/**
  * 函数功能: 在 LCD 显示器上显示一个英文字符
  * 输入参数: usX：在特定扫描方向下字符的起始X坐标
  *           usY ：在特定扫描方向下该点的起始Y坐标
  *           cChar ：要显示的英文字符
  *           usColor_Background ：选择英文字符的背景色
  *           usColor_Foreground ：选择英文字符的前景色
  *           font：字体选择
  *             参数：USB_FONT_16 ：16号字体
  *                   USB_FONT_24 ：24号字体 
  * 返 回 值: 无
  * 说    明：该函数必须与ascii.h内容对应使用
  */
void LCD_DispChar_EN( uint16_t usX, uint16_t usY, const char cChar, uint16_t usColor_Background, uint16_t usColor_Foreground )
{
	uint8_t ucTemp, ucRelativePositon, ucPage, ucColumn;
  
  /* 检查输入参数是否合法 */
//  assert_param(IS_USB_FONT(font));
  
	ucRelativePositon = cChar - ' ';
		LCD_POSITION sPos={usX,usY};
	  LCD_POSITION ePos={usX+8,usY+16};
		LCD_SetCursor(sPos.x,sPos.y,ePos.x-1,ePos.y-1);

	  Lcd_Driver.writeCMD(0x2c);//发送写GRAM指令
    
    for(ucPage=0;ucPage<16;ucPage++)
    {
      ucTemp=ucAscii_1608[ucRelativePositon][ucPage];		
      for(ucColumn=0;ucColumn<8;ucColumn++)
      {
        if(ucTemp&0x01)
				{
					LCD_RS_G();
					LCD_CS_D();	
					LCD_LOCK_OTHER_PORT();//锁住无关端口
					//写入数据
						GPIO0->DT=(uint32_t)usColor_Foreground<<16;
						LCD_WR_G();
					  LCD_UNLOCK_OTHER_PORT();//释放无关端口
					  LCD_CS_G();
//          LCD_Write_DATA(usColor_Foreground);	

				}					
        else
				{
									LCD_RS_G();
					LCD_CS_D();	
					LCD_LOCK_OTHER_PORT();//锁住无关端口
					//写入数据
						GPIO0->DT=(uint32_t)usColor_Background<<16;
						LCD_WR_G();
					  LCD_UNLOCK_OTHER_PORT();//释放无关端口
					  LCD_CS_G();
//          LCD_Write_DATA(usColor_Background);	

				}					
          ucTemp >>= 1;					
      }
    }    
  	
}

/**
  * 函数功能: 在 LCD 显示器上显示英文字符串
  * 输入参数: usX：在特定扫描方向下字符的起始X坐标
  *           usY ：在特定扫描方向下该点的起始Y坐标
  *           pStr ：要显示的英文字符串的首地址
  *           usColor_Background ：选择英文字符的背景色
  *           usColor_Foreground ：选择英文字符的前景色
  *           font：字体选择
  *             参数：USB_FONT_16 ：16号字体
  *                   USB_FONT_24 ：24号字体 
  * 返 回 值: 无
  * 说    明：该函数必须与ascii.h内容对应使用
  */
void LCD_DispString_EN ( uint16_t usX, uint16_t usY, const char * pStr, uint16_t usColor_Background, uint16_t usColor_Foreground )
{
  /* 检查输入参数是否合法 */
//  assert_param(IS_USB_FONT(font));
  
	while ( * pStr != '\0' )
	{
    
      if ( ( usX +  8 ) > 480 )
      {
        usX = 0;
        usY += 16;
      }      
      if ( ( usY +  16 ) > 320 )
      {
        usX = 0;
        usY = 0;
      }      
      LCD_DispChar_EN ( usX, usY, * pStr, usColor_Background, usColor_Foreground);
      pStr ++;      
      usX += 8;
    
	}
}

const LCD_DRIVER Lcd_Driver={   
	&lcdPara,   
	LCD_Write_DATA,
	LCD_Read_DATA,
	LCD_Write_CMD, 			 //写命令
	LCD_SetCursor,       //LCD 设置光标起始位置和结束位置
	LCD_Scan_Dir,        //LCD扫描方向
	LCD_DisplayOn,
	LCD_DisplayOff,
	LCD_Scroll,
	LCD_Clear
};//LCD描述符	 
