#include "touchdriver.h"
#include "m1130.h"
#include "m1130_qspi.h"
#include "m1130_xip_write.h"
#include "m1130_gpio.h"
 /**
  * 函数功能: us 级别延时，不是很精确
  * 输入参数: cnt：延时时间
  * 返 回 值: 无
  * 说    明：无
  */
static void XPT2046_DelayUS ( __IO uint32_t ulCount )
{
	uint32_t i;
	for ( i = 0; i < ulCount; i ++ )
	{
		uint8_t uc = 12;     //设置值为12，大约延1微秒  	      
		while ( uc -- );     //延1微秒	
	}	
}
/*
*********************************************************************************************************************
*函数功能：TP_Write_Byte,触摸控制器写入一个字节，内部调用
*入口参数：num，写入的值
*出口参数：无
*********************************************************************************************************************
*/
static void TP_Write_Byte(uint8_t num)    
{  
	uint8_t count=0;   
	for(count=0;count<8;count++)  
	{ 	  
		if((num&0x80)!=0)TDIN_G();  
		else TDIN_D();   
		__NOP();__NOP();__NOP();__NOP();
		__NOP();__NOP();__NOP();__NOP();
//		XPT2046_DelayUS(30);
		TCLK_D(); 	
  	__NOP();__NOP();__NOP();__NOP();
	  __NOP();__NOP();__NOP();__NOP();
//		XPT2046_DelayUS(30);
		num<<=1;    
		TCLK_G();		//上升沿有效	
		
	}		 			    
}

 
static uint16_t TP_Read_AD(uint8_t CMD)	  
{ 	 
	uint8_t count=0; 	  
	uint16_t Num=0; 
	uint32_t data;
	TCS_D(); 		//选中触摸屏IC
	__NOP();__NOP();__NOP();__NOP();
	__NOP();__NOP();__NOP();__NOP();
	__NOP();__NOP();__NOP();__NOP();
	TCLK_D();		//先拉低时钟 	 
	TDIN_D(); 	//拉低数据线
	TP_Write_Byte(CMD);//发送命令字
	__NOP();__NOP();__NOP();__NOP();
	__NOP();__NOP();__NOP();__NOP();
	__NOP();__NOP();__NOP();__NOP();

	TCLK_D();
	__NOP();__NOP();__NOP();__NOP();
	__NOP();__NOP();__NOP();__NOP();
	__NOP();__NOP();__NOP();__NOP();

	TCLK_G();		//给1个时钟，清除BUSY	    	
	__NOP();__NOP();__NOP();__NOP();
	__NOP();__NOP();__NOP();__NOP();
	__NOP();__NOP();__NOP();__NOP();
	TCLK_D();  	    
	for(count=0;count<16;count++)//读出16位数据,只有高12位有效 
	{ 				  
		Num<<=1; 	 
		TCLK_G();  //下降沿有效  	
		__NOP();__NOP();__NOP();__NOP();	
		__NOP();__NOP();__NOP();__NOP();
		__NOP();__NOP();__NOP();__NOP();		
		data=DOUTPROT;
		data&=DOUTPIN;
		if(data!=0)Num++; 		 		
		TCLK_D();
		XPT2046_DelayUS(3);

	}  	
	Num>>=4;   	//只有高12位有效.
	TCS_G();		  //释放片选	 
	return(Num);   
}


uint16_t touchReadX(void)
{
	return TP_Read_AD(0x90);
}

uint16_t touchReadY(void)
{
	return TP_Read_AD(0xd0);
}

