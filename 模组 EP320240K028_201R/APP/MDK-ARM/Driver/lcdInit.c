#include "lcdInit.h"
#include "lcdDriver.h"
#include "m1130_gpio.h"
#include "m1130_tim.h"
#include "stdio.h"

#define LLCD_WRITE_CMD Lcd_Driver.writeCMD
#define LLCD_WRITE_DATA Lcd_Driver.writeData
/*
*********************************************************************************************************************
*函数功能：LCDinitDelay LCD初始化模块延时函数
*入口参数：dly  延时时间
*出口参数：无
*********************************************************************************************************************
*/
void LCDDelay(unsigned int dly)
{
    unsigned int i,j;
    for(i=0;i<dly;i++)
    	for(j=0;j<255;j++);
}
void LCD_DIM_init(uint16_t arr,uint16_t psc,uint16_t pulse)
{
	//TIM1  CH1 输出PWM
	RCC->AHBCLKCTRL0_SET|=(1<<17);//开启定时器时钟
	RCC->AHBCLKCTRL0_SET|=(1<<3);

	RCC->PWMCLKDIV = 0X01;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	GPIO_InitTypeDef GPIO_InitStructure; 
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Function = GPIO_FUNCTION_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_Init(GPIO0, &GPIO_InitStructure);
	
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Prescaler=psc;//96Mhz/(95+1)=1Mhz
	TIM_TimeBaseStructure.TIM_Period=arr ;//fpwm=9600/(1199+1)=80Khz
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);
	
	//SYSAHBCLKDIV=4 在SystemInit已经初始化了 AHB时钟96Mhz	
	TIM_OCStructInit(&TIM_OCInitStructure);
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; //选择定时器模式:TIM脉冲宽度调制模式2
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; //比较输出使能
	TIM_OCInitStructure.TIM_Pulse = pulse; //设置待装入捕获比较寄存器的脉冲值
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; //输出极性:TIM输出比较极性高
	TIM_OC1Init(TIM1, &TIM_OCInitStructure);  //根据TIM_OCInitStruct中指定的参数初始化外设TIMx     //TIM1_CH1

	TIM_OC1PreloadConfig(TIM1,TIM_OCPreload_Enable);
	TIM_ARRPreloadConfig(TIM1,ENABLE);
	 TIM_CtrlPWMOutputs(TIM1, ENABLE);
	TIM_Cmd(TIM1,ENABLE);
}

void LcdSetDim(uint16_t pulse)
{
	TIM_SetCompare1(TIM1 , pulse);
}
void ILI9341_HSD28_Initial(void)
{
// VCI=2.8V
//************* Reset LCD Driver ****************//

//************* Start Initial Sequence **********//
LLCD_WRITE_CMD(0xCF);
LLCD_WRITE_DATA(0x00);
LLCD_WRITE_DATA(0xC1);
LLCD_WRITE_DATA(0x30);

LLCD_WRITE_CMD(0xED);
LLCD_WRITE_DATA(0x64);
LLCD_WRITE_DATA(0x03);
LLCD_WRITE_DATA(0x12);
LLCD_WRITE_DATA(0x81);

LLCD_WRITE_CMD(0xE8);
LLCD_WRITE_DATA(0x85);
LLCD_WRITE_DATA(0x01);
LLCD_WRITE_DATA(0x7A);

LLCD_WRITE_CMD(0xCB);
LLCD_WRITE_DATA(0x39);
LLCD_WRITE_DATA(0x2C);
LLCD_WRITE_DATA(0x00);
LLCD_WRITE_DATA(0x34);
LLCD_WRITE_DATA(0x02);

LLCD_WRITE_CMD(0xF7);
LLCD_WRITE_DATA(0x20);

LLCD_WRITE_CMD(0xEA);
LLCD_WRITE_DATA(0x00);
LLCD_WRITE_DATA(0x00);

LLCD_WRITE_CMD(0xC0); //Power control
LLCD_WRITE_DATA(0x21); //VRH[5:0]

LLCD_WRITE_CMD(0xC1); //Power control
LLCD_WRITE_DATA(0x11); //SAP[2:0];BT[3:0]

LLCD_WRITE_CMD(0xC5); //VCM control
LLCD_WRITE_DATA(0x31);
LLCD_WRITE_DATA(0x3C);

LLCD_WRITE_CMD(0xC7); //VCM control2
LLCD_WRITE_DATA(0x9f);

LLCD_WRITE_CMD(0x36); // Memory Access Control
LLCD_WRITE_DATA(0x08);

LLCD_WRITE_CMD(0x3A); // Memory Access Control
LLCD_WRITE_DATA(0x55);

LLCD_WRITE_CMD(0xB1);
LLCD_WRITE_DATA(0x00);
LLCD_WRITE_DATA(0x1B);

LLCD_WRITE_CMD(0xB6); // Display Function Control
LLCD_WRITE_DATA(0x0A);
LLCD_WRITE_DATA(0xA2);

LLCD_WRITE_CMD(0xF2); // 3Gamma Function Disable
LLCD_WRITE_DATA(0x00);

LLCD_WRITE_CMD(0x26); //Gamma curve selected
LLCD_WRITE_DATA(0x01);

LLCD_WRITE_CMD(0xE0); //Set Gamma
LLCD_WRITE_DATA(0x0F);
LLCD_WRITE_DATA(0x20);
LLCD_WRITE_DATA(0x1d);
LLCD_WRITE_DATA(0x0b);
LLCD_WRITE_DATA(0x10);
LLCD_WRITE_DATA(0x0a);
LLCD_WRITE_DATA(0x49);
LLCD_WRITE_DATA(0xa9);
LLCD_WRITE_DATA(0x3b);
LLCD_WRITE_DATA(0x0a);
LLCD_WRITE_DATA(0x15);
LLCD_WRITE_DATA(0x06);
LLCD_WRITE_DATA(0x0c);
LLCD_WRITE_DATA(0x06);
LLCD_WRITE_DATA(0x00);
LLCD_WRITE_CMD(0XE1); //Set Gamma
LLCD_WRITE_DATA(0x00);
LLCD_WRITE_DATA(0x1f);
LLCD_WRITE_DATA(0x22);
LLCD_WRITE_DATA(0x04);
LLCD_WRITE_DATA(0x0f);
LLCD_WRITE_DATA(0x05);
LLCD_WRITE_DATA(0x36);
LLCD_WRITE_DATA(0x46);
LLCD_WRITE_DATA(0x46);
LLCD_WRITE_DATA(0x05);
LLCD_WRITE_DATA(0x0b);
LLCD_WRITE_DATA(0x09);
LLCD_WRITE_DATA(0x33);
LLCD_WRITE_DATA(0x39);
LLCD_WRITE_DATA(0x0F);

LLCD_WRITE_CMD(0x11); // Sleep out
LCDDelay(120);
//LLCD_WRITE_CMD(0x29); // Display on
//LLCD_WRITE_CMD(0x2c)
}

/*
*********************************************************************************************************************
*函数功能：LCD_Init，LCD初始化，初始化LCD的硬件
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
void LCD_Init(void)
{
	//初始化GPIO
	GPIO0_IT->DIR_SET=0xffff0000;//DB0~15输出 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_11,GPIO_FUNCTION_0);//LCD_BG 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_8,GPIO_FUNCTION_0);//LCD_RD 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_4,GPIO_FUNCTION_0);//LCD_WR 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_10,GPIO_FUNCTION_0);//LCD_RS 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_9,GPIO_FUNCTION_0);//LCD_CS 
	
	GPIO_SetPinDir(GPIO0, GPIO_Pin_4,GPIO_Mode_OUT);//LCD_WR 
	GPIO_SetPinDir(GPIO0, GPIO_Pin_11,GPIO_Mode_OUT);//LCD_BG 
	GPIO_SetPinDir(GPIO0, GPIO_Pin_8,GPIO_Mode_OUT);//LCD_RD 	
	GPIO_SetPinDir(GPIO0, GPIO_Pin_10,GPIO_Mode_OUT);//LCD_RS 
	GPIO_SetPinDir(GPIO0, GPIO_Pin_9,GPIO_Mode_OUT);//LCD_CS 	
	
	GPIO_SetPinMux(GPIO1, GPIO_Pin_15,GPIO_FUNCTION_0);//QSPI1_TF_CS 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_15,GPIO_Mode_OUT);  //QSPI1_TF_CS 
	GPIO_SetPin(GPIO1, GPIO_Pin_15);
	LCD_BG_CLOSE();
	Lcd_Driver.writeCMD(0x01);//software reset
  LCDDelay(120);
	LCD_BG_CLOSE();	
	//关闭显示
	Lcd_Driver.writeCMD(0x28); 

  ILI9341_HSD28_Initial();

//	//此命令关闭睡眠模式
//	Lcd_Driver.writeCMD(0x11); 
//	//开启反显，正常0000显示黑色，ffff显示白色，如果不发这个命令，会显示反的
	Lcd_Driver.writeCMD(0x20); 
//	//开启显示
	Lcd_Driver.writeCMD(0x29); 

	Lcd_Driver.scan_dir(DIR_HORIZONTAL);//测试程序切换到横屏
//	LCD_BG_OPEN();	
	LCD_DIM_init(100,95,0);  //10Khz 50占空比	
printf("LCD_Init  \r\n");
}
