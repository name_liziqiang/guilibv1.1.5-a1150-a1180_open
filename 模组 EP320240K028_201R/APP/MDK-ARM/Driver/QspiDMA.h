#ifndef _QspiDMA_h
#define _QspiDMA_h
#include "gStdint.h"
#include "m1130.h"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：QSPI Flash DMA接口驱动
 *实现功能：1、QSPI Flash DMA接口驱动
 *
 *
 *撰 写 人：Epitome Design Systems   LCD Application development team
 *撰写时间：2020-5-20
 *测 试 人：Epitome Design Systems   LCD Application development team
 *测试时间：2020-5-20
 *版 本 号：V1.01_2020-5-20
 *版本说明：
 *                              
 *          函数清单
 *
 *V1.01：   功能实现
 *
 */
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define QUAD_SPI_FLASH_CMD_WRITE_STATUS									0x01
#define QUAD_SPI_FLASH_CMD_READ													0x03
#define QUAD_SPI_FLASH_CMD_WRITE												0x02
#define QUAD_SPI_FLASH_CMD_WRITE_ENABLE									0x06
#define QUAD_SPI_FLASH_CMD_WRITE_DISABLE								0x04
#define QUAD_SPI_FLASH_CMD_READ_STATUS1									0x05
#define QUAD_SPI_FLASH_CMD_READ_STATUS2									0x35
#define QUAD_SPI_FLASH_CMD_WRITE_STATUS2								0x31
#define QUAD_SPI_FLASH_CMD_READ_STATUS3									0x15
#define QUAD_SPI_FLASH_CMD_WRITE_STATUS3								0x11
#define QUAD_SPI_FLASH_CMD_READ_ID											0x9F
#define QUAD_SPI_FLASH_CMD_ERASE_4K											0x20
#define QUAD_SPI_FLASH_CMD_ERASE_32K										0x52
#define QUAD_SPI_FLASH_CMD_ERASE_64K										0xD8
#define QUAD_SPI_FLASH_CMD_ERASE_ALL										0x60 // 0xC7
#define QUAD_SPI_CMD_FAST_READ_DUAL_OUTPUT							0x3B 
#define QUAD_SPI_CMD_FAST_READ_DUAL_IO									0xBB
#define QUAD_SPI_CMD_FAST_READ_OCTAL_QUAD_IO						0xEB
#define QUAD_SPI_FLASH_CMD_EQPI													0x38
#define QUAD_SPI_FLASH_CMD_FAST_READ										0x0B

#define QUAD_SPI_CMD_ENTER_4BYTE_ADDR_MODE							0xB7
#define QUAD_SPI_CMD_EXIT_4BYTE_ADDR_MODE								0xE9
#define QUAD_SPI_FLASH_CMD_READ_WITH_4BYTE_ADDR         0x13
#define QUAD_SPI_FLASH_CMD_ERASE_4K_WITH_4BYTE_ADDR     0x21
#define QUAD_SPI_FLASH_CMD_WRITE_WITH_4BYTE_ADDR        0x12

#define QSPI_FLASH_PAGESHIFT  													8 // 256 Byte

#define DMA_CHANNEL_MAX          										    3//DMA传输通道最大值

#define FLASH_QSPIDMA_READ_CHANNEL											3//FLASH DMA读取通道
#define FLASH_QSPIDMA_WRITE_CHANNEL											3//FLASH DMA发送通道

#define  DATA_QSPI																			QSPI1//保存数据的flash的QSPI
#define  CODE_QSPI																			QSPI1//保存代码的flash的QSPI

int QSPIDMA_WaitEnd(QSPI_TypeDef *QSPIptr , uint8_t DMAchannel);
int QSPIDMA_SendBytes(QSPI_TypeDef *QSPIptr , uint8_t DMAchannel , const uint8_t *data , uint32_t len);
int QSPIDMA_ReadBytes(QSPI_TypeDef *QSPIptr , uint8_t DMAchannel , uint8_t *data , uint32_t len);
int enter_XIPCritical(void);
int quit_XIPCritical(void);
int flash_ConfigReadFlashMsg(QSPI_TypeDef *QSPIptr , uint8_t DMAchannel , uint32_t dataAddr);

#endif
