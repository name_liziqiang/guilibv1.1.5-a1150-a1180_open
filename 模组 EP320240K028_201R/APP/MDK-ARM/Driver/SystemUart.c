#include "SystemUart.h"
#include <stdio.h>
#include <string.h>
#include "m1130_uart.h"
#include "m1130_gpio.h"

void SystemUart_Init(uint32_t BaudRate)
{
	GPIO_ConfigPull(GPIO0, GPIO_Pin_3,GPIO_PULL_UP);
	RCC_ResetAHBCLK(1 << AHBCLK_BIT_UART0);
	GPIO_SetPinMux(GPIO0, GPIO_Pin_3, GPIO_FUNCTION_2);
  GPIO_SetPinMux(GPIO0, GPIO_Pin_2, GPIO_FUNCTION_2);//设置引脚复用
	UART_InitTypeDef UART_InitStruct;
	UART_StructInit(&UART_InitStruct);
	UART_InitStruct.UART_BaudRate						 =BaudRate;
	UART_InitStruct.UART_RXIFLSEL   				 =UART_RXIFLSEL_2;
	UART_InitStruct.UART_TXIFLSEL   				 =UART_TXIFLSEL_2;
	UART_InitStruct.UART_FEN        				 =UART_FEN_Enable;
	UART_InitStruct.UART_SoftwareFlowControl =UART_SoftwareFlowControl_None;
	UART_InitStruct.UART_HardwareFlowControl =UART_HardwareFlowControl_None;
	UART_InitStruct.UART_Mode                =UART_Mode_Rx|UART_Mode_Tx;
	UART_InitStruct.UART_Parity              =UART_Parity_No;
	UART_InitStruct.UART_StopBits 					 = UART_StopBits_1;
	UART_InitStruct.UART_WordLength 				 = UART_WordLength_8b;
	
	RCC_SetAHBCLK(1<<AHBCLK_BIT_UART1,ENABLE);
	RCC_UARTCLKSel(RCC_UARTCLK_SOURCE_SYSPLL);
	RCC_SETCLKDivider(RCC_CLOCKFREQ_UART1CLK, 32);
	UART_Reset(UART1);
	UART_Init(UART1, &UART_InitStruct);
	
//	NVIC_InitTypeDef NVIC_InitStruct;
//	NVIC_InitStruct.NVIC_IRQChannel = UART0_IRQn;
//	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
//  NVIC_InitStruct.NVIC_IRQChannelPriority = 3;
//  NVIC_Init(&NVIC_InitStruct);
	
	/* set timeout to 8 uart clk cycle len */
  UART1->CTRL0_CLR = UART_CTRL0_RXTIMEOUT;
  UART1->CTRL0_SET = 16 << 16;

  UART_ITConfig(UART1, UART_IT_RXIEN | UART_IT_RTIEN, ENABLE);//开启接收中断和接收超时中断
  UART_Cmd(UART1, ENABLE);
}
void SystemUart_Init2(uint32_t BaudRate)
{
	GPIO_ConfigPull(GPIO0, GPIO_Pin_0,GPIO_PULL_UP);
	RCC_ResetAHBCLK(1 << AHBCLK_BIT_UART0);
	GPIO_SetPinMux(GPIO0, GPIO_Pin_0, GPIO_FUNCTION_3);
  GPIO_SetPinMux(GPIO0, GPIO_Pin_1, GPIO_FUNCTION_3);//设置引脚复用
	UART_InitTypeDef UART_InitStruct;
	UART_StructInit(&UART_InitStruct);
	UART_InitStruct.UART_BaudRate						 =BaudRate;
	UART_InitStruct.UART_RXIFLSEL   				 =UART_RXIFLSEL_2;
	UART_InitStruct.UART_TXIFLSEL   				 =UART_TXIFLSEL_2;
	UART_InitStruct.UART_FEN        				 =UART_FEN_Enable;
	UART_InitStruct.UART_SoftwareFlowControl =UART_SoftwareFlowControl_None;
	UART_InitStruct.UART_HardwareFlowControl =UART_HardwareFlowControl_None;
	UART_InitStruct.UART_Mode                =UART_Mode_Rx|UART_Mode_Tx;
	UART_InitStruct.UART_Parity              =UART_Parity_No;
	UART_InitStruct.UART_StopBits 					 = UART_StopBits_1;
	UART_InitStruct.UART_WordLength 				 = UART_WordLength_8b;
	
	RCC_SetAHBCLK(1<<AHBCLK_BIT_UART2,ENABLE);
	RCC_UARTCLKSel(RCC_UARTCLK_SOURCE_SYSPLL);
	RCC_SETCLKDivider(RCC_CLOCKFREQ_UART2CLK, 32);
	UART_Reset(UART2);
	UART_Init(UART2, &UART_InitStruct);
	
//	NVIC_InitTypeDef NVIC_InitStruct;
//	NVIC_InitStruct.NVIC_IRQChannel = UART2_IRQn;
//	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
//  NVIC_InitStruct.NVIC_IRQChannelPriority = 3;
//  NVIC_Init(&NVIC_InitStruct);
	
	/* set timeout to 8 uart clk cycle len */
  UART2->CTRL0_CLR = UART_CTRL0_RXTIMEOUT;
  UART2->CTRL0_SET = 16 << 16;

  UART_ITConfig(UART2, UART_IT_RXIEN | UART_IT_RTIEN, ENABLE);//开启接收中断和接收超时中断
  UART_Cmd(UART2, ENABLE);
}
void printString(const char *c,uint32_t len)
{
	for(int i=0;i<len;i++)
	{
		while((UART1->STAT&UART_FLAG_TXFE)==0);//等待上一次串口数据发送完成  
		UART1->DATA = *(c+i);//写DR,串口1将发送数据	
	}
}

void printChar(char c)
{
	while((UART0->STAT&UART_FLAG_TXFE)==0);//等待上一次串口数据发送完成  
	UART0->DATA = c;//写DR,串口1将发送数据	
}

#include "comInterface.h"
const COM_DESCRIPTOR Com_Des={
	printString,
	GUI_NULL,
};


const COM_DESCRIPTOR DebugCom_Des={
	printString,
	GUI_NULL,
};

