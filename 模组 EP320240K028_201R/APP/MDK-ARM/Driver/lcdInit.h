#ifndef _lcdInit_h
#define _lcdInit_h

#include "gStdint.h"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：LCD 初始化接口，单独成一个文件的原因在于这一段代码不需要高速执行，单独拿出来
 *实现功能：1、
 *
 *撰 写 人：Alpscale LCD Application development team
 *撰写时间：2019-7-22
 *测 试 人：Alpscale LCD Application development team
 *测试时间：2019-7-22
 *版 本 号：V1.01_2019-7-22
 *版本说明：
 *                              
 *          函数清单
 *
 *V1.01：   功能实现
 *
 *V1.02：
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void LCD_Init(void);
void LcdSetDim(gui_uint16 pulse);
void LCD_DIM_init(gui_uint16 arr,gui_uint16 psc,gui_uint16 pulse);



#endif
