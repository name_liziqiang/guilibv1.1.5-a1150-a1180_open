#ifndef _SystemUart_h
#define _SystemUart_h
#include "m1130.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：系统串口
 *实现功能：1、串口初始化
 *          2、接收缓冲区处理，发送缓存处理
 *          3、实现使用DMA处理数据
 *          4、实现不使用DMA处理数据
 *          5、readCOM实现
 *          6、writeCOM实现
 *
 *硬件接口：USART1，DMA1通道4发送，DMA通道5接收，（移植到其他平台之后，一下接口需要重新实现）
 *
 *撰 写 人：Alpscale LCD Application development team
 *撰写时间：2019-7-1
 *测 试 人：Alpscale LCD Application development team
 *测试时间：2019-7-2
 *版 本 号：V1.01_2019-7-2
 *版本说明：
 *                              
 *          初始化程序       使用DMA数据处理          不使用DMA数据处理            readCOM功能码            writeCOM功能码
 *
 *V1.01：   功能实现         功能实现                 功能实现                     功能实现                 功能实现
 *
 *V1.02：
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void SystemUart_Init(uint32_t BaudRate);//串口硬件接口初始化--用户实现

void SystemUart_Init2(uint32_t BaudRate);//串口硬件接口初始化--用户实现
#endif
