#ifndef _Shutdown_h
#define _Shutdown_h
#include "gWidgetInfo.h"
#include "gStdint.h"
#include "gReturn.h"
#include "gResMsg.h"



#define ShutpagewID          220
static const PAGE_INFO ShutPage={
	{.wType = WIDGET_TYPE_PAGE , .wId = ShutpagewID , .wVscope = WIDGET_VS_PRIVATE},
	{.x = 0 , .y = 0 , .width = 480 , .height = 320},
	.backMode = WIDGET_BACKMODE_NULL , 
	.backColor = 0x00FF,
	.picId = 0,
	.enterEvent = GUI_NULL,
	.leaveEvent = GUI_NULL,
	.pressEvent = GUI_NULL,
	.releaseEvent = GUI_NULL,
};
#define ShutpagePicwID       221
static const PICTUREBOX_INFO  ShutpagePic={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = ShutpagePicwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 0 , .y = 30 , .width = 480 , .height = 260},    
 .picId = 118,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
#define ShutpageTimerwID     222
static const GTIMER_INFO ShutpageTimer={
 {.wType = WIDGET_TYPE_GTIMER , .wId = ShutpageTimerwID , .wVscope = WIDGET_VS_PRIVATE},
 .timeOut = 300,
 .enable = 0,                         //定时器开关位，0关，1开
 .timeValue = 0,                     //定时器当前计数值（周期是1MS）低16位是当前计数器值，高16位是定时器溢出标志，或者其他标识
 .timeoutEvent = GUI_NULL
};
#define ShutpagePic2wID      223
static const PICTUREBOX_INFO  ShutpagePic2={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = ShutpagePic2wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 186 , .y = 153 , .width = 110 , .height = 110},    
 .picId = 117,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};















gui_Err ShutPageEnterEvent(gui_int32 argc , const char **argv);
gui_Err ShutPageLeaveEvent(gui_int32 argc , const char **argv);

static gui_Err TouchToShut_pressEvent(gui_int32 argc , const char **argv);
static gui_Err TouchToShut_releaseEvent(gui_int32 argc , const char **argv);
static gui_Err TouchToShut_timeoutEvent(gui_int32 argc , const char **argv);
static gui_Err BackToHome_releaseEvent(gui_int32 argc , const char **argv);




#endif



