#include "DemoApp.h"
#include "guiFunc.h"
#include "stdio.h"
#include "string.h"
#include "m1130_utility_rtc.h"

extern Date SystemDateTime;
static gui_uint32 Year,Mon,Date1,Hour,Min,Sec;
gui_Err DialogPageEnterEvent(gui_int32 argc , const char **argv)
{
	guiCreateWidget((const void *)&DialogPage);//创建的第一个控件一定是page
	guiCreateWidget((const void *)&DialogPagePicBox);
	guiCreateWidget((const void *)&DialogPageTextBox);
	guiCreateWidget((const void *)&DialogPageButtonYES);
	guiCreateWidget((const void *)&DialogPageButtonNO);
	guiCreateWidget((const void *)&DialogPageNumberBox1);//年
	guiCreateWidget((const void *)&DialogPageNumberBox2);//月
	guiCreateWidget((const void *)&DialogPageNumberBox3);//日
	guiCreateWidget((const void *)&DialogPageNumberBox4);//时
	guiCreateWidget((const void *)&DialogPageNumberBox5);//分
	guiCreateWidget((const void *)&DialogPageNumberBox6);//秒
	guiAppendWidgetText(DPTextBoxwID,2,"20");
	guiAppendWidgetText(DPButtonYESwID,3,"YES");
	guiAppendWidgetText(DPButtonNOwID,2,"NO");	

	guiRegisterEvent(EVENT_REGISTER_TYPE_RELEASE , DPButtonYESwID , ModifyTime_releaseEvent);
	guiRegisterEvent(EVENT_REGISTER_TYPE_RELEASE , DPButtonNOwID , BackToHome_releaseEvent);
	//读取现在日期时间给对话框
	GetDate(&SystemDateTime);
	guiSetWidgetValue(DPNumBox1wID,SystemDateTime.year);
	guiSetWidgetValue(DPNumBox2wID,SystemDateTime.month);
	guiSetWidgetValue(DPNumBox3wID,SystemDateTime.day);
	guiSetWidgetValue(DPNumBox4wID,SystemDateTime.hour);
	guiSetWidgetValue(DPNumBox5wID,SystemDateTime.min);
	guiSetWidgetValue(DPNumBox6wID,SystemDateTime.sec);
	return GUI_EOK;
}
gui_Err DialogPageLeaveEvent(gui_int32 argc , const char **argv)
{
	
	return GUI_EOK;
}
//返回主界面
static gui_Err BackToHome_releaseEvent(gui_int32 argc , const char **argv)
{
	guiJumpPage(MPwID,MainPageLeaveEvent,MainPageEnterEvent);
	return GUI_EOK;
}
//修改日期时间并返回
static gui_Err ModifyTime_releaseEvent(gui_int32 argc , const char **argv)
{
	guiGetWidgetValue(DPNumBox1wID,&Year);
	guiGetWidgetValue(DPNumBox2wID,&Mon);
	guiGetWidgetValue(DPNumBox3wID,&Date1);
	guiGetWidgetValue(DPNumBox4wID,&Hour);
	guiGetWidgetValue(DPNumBox5wID,&Min);
	guiGetWidgetValue(DPNumBox6wID,&Sec);
	SystemDateTime.year=2000+(uint16_t) Year;
	if(1<=Mon&&Mon<=12)	SystemDateTime.month=(uint8_t)Mon;
	if(1<=Date1&&Date1<=31) SystemDateTime.day=(uint8_t)Date1;
	if(Hour<=24) SystemDateTime.hour=(uint8_t)Hour;
	if(Min<=59)	SystemDateTime.min=(uint8_t)Min;
	if(Sec<=59)	SystemDateTime.sec=(uint8_t)Sec;
	SetDate(&SystemDateTime);
	guiJumpPage(MPwID,MainPageLeaveEvent,MainPageEnterEvent);
	return GUI_EOK;
}



