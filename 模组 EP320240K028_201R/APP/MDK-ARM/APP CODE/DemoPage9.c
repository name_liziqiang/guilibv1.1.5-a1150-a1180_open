#include "DemoApp.h"
#include "guiFunc.h"
#include "stdio.h"
#include "string.h"
	gui_uint16 wcolor = 0;
gui_Err OtherPageEnterEvent(gui_int32 argc , const char **argv)
{
	guiCreateWidget((const void *)&OtherPage);//创建的第一个控件一定是page
	guiCreateWidget((const void *)&OtherPageCutPic1);
	guiCreateWidget((const void *)&OtherPageCutPic2);
  guiCreateWidget((const void *)&OtherPageTouchArea);
	guiCreateWidget((const void *)&OtherPageTextBox1);
	guiCreateWidget((const void *)&OtherPageTextBox2);
	guiCreateWidget((const void *)&OtherPageTextBox3);
  guiCreateWidget((const void *)&OtherPageBinaryButton);
	guiCreateWidget((const void *)&OtherPageOptionBox1);
	guiCreateWidget((const void *)&OtherPageOptionBox2);
	guiCreateWidget((const void *)&OtherPageOptionBox3);
	guiCreateWidget((const void *)&OtherPageQRCodeBox);	
	guiAppendWidgetText(OPTextBox1wID,6,"复选A");
	guiAppendWidgetText(OPTextBox2wID,6,"复选B");
	guiAppendWidgetText(OPTextBox3wID,6,"复选C");
	guiAppendWidgetText(OPBinaryButtonwID,strlen("双态按钮 触摸底部退出"),"双态按钮 触摸底部退出");
	guiAppendWidgetText(OPQRCodewID,26,"http://www.alphascale.com");
	
	guiRegisterEvent(EVENT_REGISTER_TYPE_RELEASE , OPTouchAreawID , BackToHome_releaseEvent);
	guiRegisterEvent(EVENT_REGISTER_TYPE_RELEASE , OPOptionBox1wID , OtherPage_OptionA_releaseEvent);
	guiRegisterEvent(EVENT_REGISTER_TYPE_RELEASE , OPOptionBox2wID , OtherPage_OptionB_releaseEvent);
	guiRegisterEvent(EVENT_REGISTER_TYPE_RELEASE , OPOptionBox3wID , OtherPage_OptionC_releaseEvent);
	guiRegisterEvent(EVENT_REGISTER_TYPE_RELEASE , OPBinaryButtonwID , OtherPage_BinaryButton_releaseEvent);

	return GUI_EOK;
}
gui_Err OtherPageLeaveEvent(gui_int32 argc , const char **argv)
{
	
	return GUI_EOK;
}

static gui_Err BackToHome_releaseEvent(gui_int32 argc , const char **argv)
{
	guiJumpPage(MPwID,MainPageLeaveEvent,MainPageEnterEvent);
	return GUI_EOK;
}
static gui_Err OtherPage_OptionA_releaseEvent(gui_int32 argc , const char **argv)
{
	guiGetWidgetValue(OPOptionBox1wID,&OptionA_value);
	printf("OptionA:%d\r\n",OptionA_value);
	return GUI_EOK;
}
static gui_Err OtherPage_OptionB_releaseEvent(gui_int32 argc , const char **argv)
{
	guiGetWidgetValue(OPOptionBox2wID,&OptionB_value);
	printf("OptionB:%d\r\n",OptionB_value);
	return GUI_EOK;
}
static gui_Err OtherPage_OptionC_releaseEvent(gui_int32 argc , const char **argv)
{
	guiGetWidgetValue(OPOptionBox3wID,&OptionC_value);
	printf("OptionC:%d\r\n",OptionC_value);
	return GUI_EOK;
}
static gui_Err OtherPage_BinaryButton_releaseEvent(gui_int32 argc , const char **argv)
{
	guiGetWidgetValue(OPBinaryButtonwID,&BinaryButton_value);
	printf("BinaryButton:%d\r\n",BinaryButton_value);
	return GUI_EOK;
}


