/**
  ******************************************************************************
  * @file    system_m1130.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    12/12/2013
  * @brief   CMSIS Cortex-M0 Device Peripheral Access Layer System Source File.
  ******************************************************************************  
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */

/** @addtogroup CMSIS
  * @{
  */

/** @addtogroup m1130_system
  * @{
  */  
  
/** @addtogroup m1130_System_Private_Includes
  * @{
  */

#include "m1130_rcc.h"
#include "m1130_wdg.h"

/*******************************************************************************
*  Clock Definitions
*******************************************************************************/


/**
  * @brief  Setup the microcontroller system
  *         Initialize the Embedded Flash Interface, the PLL and update the 
  *         SystemCoreClock variable.
  * @note   This function should be used only after reset.
  * @param  None
  * @retval None
  */
#if 1
void SystemInit (void)
{    
	
//	uint32_t	timeout = 0;
//	
//	RCC->AHBCLKCTRL0_SET = (1<<AHBCLK_BIT_GPIO) | (1<<AHBCLK_BIT_DMA) | (1<<AHBCLK_BIT_IOCON);
//	RCC->SYSAHBCLKDIV = 4;//FPGA��  96M/2=48M, ASIC�� 384M/4=96M

//	/* open sys_pll clk */
//	RCC->PDRUNCFG = RCC->PDRUNCFG & 0xFFFFFEEF;		//bit4=SYSPLL bit0=EXT12M

//	/*set sys_pll clk to 480MHz*/
//	RCC->SYSPLLCTRL = RCC_SYSPLLCTRL_FORCELOCK | (1 << 30) | (22 << 0);	//12M*(22+10)=384M		OSC:bit30=0

//	while((RCC->SYSPLLSTAT) != RCC_SYSPLLSTAT_LOCK)
//	{
//		if((timeout++) >= 0x8000)
//			break;
//	}

// 	commonDelay(1000);
//	
//	/*switch main clk source to syspll_out*/
//	RCC->MAINCLKSEL = 1;
//	/*make switch available*/
//	RCC->MAINCLKUEN = 0;
//	RCC->MAINCLKUEN = 1;	
	uint32_t	timeout = 0;
	
	RCC->AHBCLKCTRL0_SET = (1<<AHBCLK_BIT_GPIO) | (1<<AHBCLK_BIT_DMA) | (1<<AHBCLK_BIT_IOCON);
	RCC->SYSAHBCLKDIV = 4;							//FPGA��  96M/2=48M, ASIC�� 384M/4=96M

	/* open sys_pll clk */
	RCC->PDRUNCFG = RCC->PDRUNCFG & 0xFFFFFEEE;		//bit4=SYSPLL bit0=EXT12M

	/*set sys_pll clk to 480MHz*/
	RCC->SYSPLLCTRL = RCC_SYSPLLCTRL_FORCELOCK | (22 << 0);	//12M*(22+10)=480M		OSC:bit30=0

	while((RCC->SYSPLLSTAT) != RCC_SYSPLLSTAT_LOCK)
	{
		if((timeout++) >= 0x8000)
			break;
	}

 	commonDelay(1000);
	
	/*switch main clk source to syspll_out*/
	RCC->MAINCLKSEL = 1;
	/*make switch available*/
	RCC->MAINCLKUEN = 0;
	RCC->MAINCLKUEN = 1;	
}
#endif

/******************* (C) COPYRIGHT 2013 Alphascale *****END OF FILE****/
