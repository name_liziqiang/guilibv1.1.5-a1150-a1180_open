#ifndef _memoryInterface_h
#define _memoryInterface_h
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：涉及的内存操作  接口
 *实现功能：
 *
 *撰 写 人：Alpscale LCD Application development team
 *撰写时间：2019-7-22
 *测 试 人：Alpscale LCD Application development team
 *测试时间：2019-7-22
 *版 本 号：V1.01_2019-7-22
 *版本说明：
 *                              
 *          函数清单
 *
 *V1.01：   功能实现
 *
 *V1.02：
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "gStdint.h"

#ifdef __cplusplus
 extern "C" {
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *内存操作接口 
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef struct _MEMORY_DESCRIPTOR{
	//内存申请
	void *(*malloc)(gui_uint32 len);
	//内存释放
	void  (*free)(void *ptr);
	//重新申请内存
	void *(*realloc)(void *ptr , gui_uint32 len);
	//内存赋值
	void *(*memset)(void *ptr , int c , gui_uint32 len);
	//内存拷贝
	void *(*memcpy)(void *des , const void *src , gui_uint32 len);
}MEMORY_DESCRIPTOR;

extern const MEMORY_DESCRIPTOR Memory_Des;

#ifdef __cplusplus
}
#endif

#endif
