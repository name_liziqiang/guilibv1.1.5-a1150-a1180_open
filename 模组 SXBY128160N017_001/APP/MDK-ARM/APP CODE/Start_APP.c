#include "Start_APP.h"
#include "guiFunc.h"
#include "stdio.h"
#include "string.h"
#include "comInterface.h"
#include "lcddriver.h"

/*
*********************************************************************************************************************
*函数功能：初始化主页面
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/ 

static gui_uint8 battery=0;
static gui_uint16 NUM=100;

/*********************************************************************************************************************
*函数功能：初始化主页面
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/ 
void guiMainPageInit(void)
{ 
	guiJumpPage(GUI_NULL,GUI_NULL,logoEnterEvent);
}
/*********************************************************************************************************************
*函数功能：创建主页面控件
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/ 
gui_Err logoEnterEvent(gui_int32 argc , const char **argv)
{
	guiCreateWidget((const void *)&StartPage);//创建的第一个控件一定是page
	guiCreateWidget((const void *)&StartPageGTimer);
	guiCreateWidget((const void *)&LBatteryPic);
	guiCreateWidget((const void *)&RBatteryPic);
	guiCreateWidget((const void *)&LPointPic);
	guiCreateWidget((const void *)&RPointPic);
	guiCreateWidget((const void *)&NumGePic);
	guiCreateWidget((const void *)&NumShiPic);
	guiCreateWidget((const void *)&NumBaiPic);
	guiCreateWidget((const void *)&NumPointPic);
	guiCreateWidget((const void *)&IGePic);
	guiCreateWidget((const void *)&IShiPic);
	guiCreateWidget((const void *)&RGePic);
	guiCreateWidget((const void *)&RShiPic);
	guiCreateWidget((const void *)&UGePic);
	guiCreateWidget((const void *)&UShiPic);
	guiRegisterEvent(EVENT_REGISTER_TYPE_TIMEOUT , StartGTimerwID , DemoGTimer_timeoutEvent);
	return GUI_EOK;
}
/*********************************************************************************************************************
*函数功能：创建主页面控件
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/ 
static gui_Err DemoGTimer_timeoutEvent(gui_int32 argc , const char **argv)
{
	NUM++;
	if(NUM%2==0)
	{
		switch(battery)
		{
			case 0:{
				guiSetWidgetPictureID(LBatteryPicwID,12);guiSetWidgetPictureID(RBatteryPicwID,6);
				guiSetWidgetPictureID(LPointPicwID,13);guiSetWidgetPictureID(RPointPicwID,16);
				battery++;}	break;
			case 1:{
				guiSetWidgetPictureID(LBatteryPicwID,11);guiSetWidgetPictureID(RBatteryPicwID,5);
				battery++;}	break;
			case 2:{
				guiSetWidgetPictureID(LBatteryPicwID,10);guiSetWidgetPictureID(RBatteryPicwID,4);
				guiSetWidgetPictureID(LPointPicwID,14);guiSetWidgetPictureID(RPointPicwID,17);
				battery++;}	break;
			case 3:{
				guiSetWidgetPictureID(LBatteryPicwID,9);guiSetWidgetPictureID(RBatteryPicwID,3);
				battery++;}	break;
			case 4:{
				guiSetWidgetPictureID(LBatteryPicwID,8);guiSetWidgetPictureID(RBatteryPicwID,2);
				guiSetWidgetPictureID(LPointPicwID,15);guiSetWidgetPictureID(RPointPicwID,18);
				battery++;}	break;
			case 5:{
				guiSetWidgetPictureID(LBatteryPicwID,7);guiSetWidgetPictureID(RBatteryPicwID,1);
				battery=0;}	break;
			default:break;
		}	
	}
	switch(NUM%10)
	{
		case 0:guiSetWidgetPictureID(NumBaiPicwID,19);break;
		case 1:guiSetWidgetPictureID(NumBaiPicwID,20);break;
		case 2:guiSetWidgetPictureID(NumBaiPicwID,21);break;
		case 3:guiSetWidgetPictureID(NumBaiPicwID,22);break;
		case 4:guiSetWidgetPictureID(NumBaiPicwID,23);break;
		case 5:guiSetWidgetPictureID(NumBaiPicwID,24);break;
		case 6:guiSetWidgetPictureID(NumBaiPicwID,25);break;
		case 7:guiSetWidgetPictureID(NumBaiPicwID,26);break;
		case 8:guiSetWidgetPictureID(NumBaiPicwID,27);break;
		case 9:guiSetWidgetPictureID(NumBaiPicwID,28);break;
		default:break;
	}
	switch(NUM%100/10)
	{
		case 0:{
			guiSetWidgetPictureID(NumGePicwID,19);guiSetWidgetPictureID(IShiPicwID,40);
			guiSetWidgetPictureID(RShiPicwID,30);guiSetWidgetPictureID(UShiPicwID,30);
		}	break;
		case 1:{
			guiSetWidgetPictureID(NumGePicwID,20);guiSetWidgetPictureID(IShiPicwID,41);
			guiSetWidgetPictureID(RShiPicwID,31);guiSetWidgetPictureID(UShiPicwID,31);
		}	break;
		case 2:{
			guiSetWidgetPictureID(NumGePicwID,21);guiSetWidgetPictureID(IShiPicwID,42);
			guiSetWidgetPictureID(RShiPicwID,32);guiSetWidgetPictureID(UShiPicwID,32);
		}	break;
		case 3:{
			guiSetWidgetPictureID(NumGePicwID,22);guiSetWidgetPictureID(IShiPicwID,43);
			guiSetWidgetPictureID(RShiPicwID,33);guiSetWidgetPictureID(UShiPicwID,33);
		}	break;
		case 4:{
			guiSetWidgetPictureID(NumGePicwID,23);guiSetWidgetPictureID(IShiPicwID,44);
			guiSetWidgetPictureID(RShiPicwID,34);guiSetWidgetPictureID(UShiPicwID,34);
		}	break;
		case 5:{
			guiSetWidgetPictureID(NumGePicwID,24);guiSetWidgetPictureID(IShiPicwID,45);
			guiSetWidgetPictureID(RShiPicwID,35);guiSetWidgetPictureID(UShiPicwID,35);
		}	break;
		case 6:{
			guiSetWidgetPictureID(NumGePicwID,25);guiSetWidgetPictureID(IShiPicwID,46);
			guiSetWidgetPictureID(RShiPicwID,36);guiSetWidgetPictureID(UShiPicwID,36);
		}	break;
		case 7:{
			guiSetWidgetPictureID(NumGePicwID,26);guiSetWidgetPictureID(IShiPicwID,47);
			guiSetWidgetPictureID(RShiPicwID,37);guiSetWidgetPictureID(UShiPicwID,37);
		}	break;
		case 8:{
			guiSetWidgetPictureID(NumGePicwID,27);guiSetWidgetPictureID(IShiPicwID,48);
			guiSetWidgetPictureID(RShiPicwID,38);guiSetWidgetPictureID(UShiPicwID,38);
		}	break;
		case 9:{
			guiSetWidgetPictureID(NumGePicwID,28);guiSetWidgetPictureID(IShiPicwID,49);
			guiSetWidgetPictureID(RShiPicwID,39);guiSetWidgetPictureID(UShiPicwID,39);
		}	break;
		default:break;
	}
	switch(NUM/100)
	{
		case 0:{
			guiSetWidgetPictureID(NumShiPicwID,19);guiSetWidgetPictureID(IGePicwID,40);
			guiSetWidgetPictureID(RGePicwID,30);guiSetWidgetPictureID(UGePicwID,30);
		}	break;
		case 1:{
			guiSetWidgetPictureID(NumShiPicwID,20);guiSetWidgetPictureID(IGePicwID,41);
			guiSetWidgetPictureID(RGePicwID,31);guiSetWidgetPictureID(UGePicwID,31);
		}	break;
		case 2:{
			guiSetWidgetPictureID(NumShiPicwID,21);guiSetWidgetPictureID(IGePicwID,42);
			guiSetWidgetPictureID(RGePicwID,32);guiSetWidgetPictureID(UGePicwID,32);
		}	break;
		case 3:{
			guiSetWidgetPictureID(NumShiPicwID,22);guiSetWidgetPictureID(IGePicwID,43);
			guiSetWidgetPictureID(RGePicwID,33);guiSetWidgetPictureID(UGePicwID,33);
		}	break;
		case 4:{
			guiSetWidgetPictureID(NumShiPicwID,23);guiSetWidgetPictureID(IGePicwID,44);
			guiSetWidgetPictureID(RGePicwID,34);guiSetWidgetPictureID(UGePicwID,34);
		}	break;
		case 5:{
			guiSetWidgetPictureID(NumShiPicwID,24);guiSetWidgetPictureID(IGePicwID,45);
			guiSetWidgetPictureID(RGePicwID,35);guiSetWidgetPictureID(UGePicwID,35);
		}	break;
		case 6:{
			guiSetWidgetPictureID(NumShiPicwID,25);guiSetWidgetPictureID(IGePicwID,46);
			guiSetWidgetPictureID(RGePicwID,36);guiSetWidgetPictureID(UGePicwID,36);
		}	break;
		case 7:{
			guiSetWidgetPictureID(NumShiPicwID,26);guiSetWidgetPictureID(IGePicwID,47);
			guiSetWidgetPictureID(RGePicwID,37);guiSetWidgetPictureID(UGePicwID,37);
		}	break;
		case 8:{
			guiSetWidgetPictureID(NumShiPicwID,27);guiSetWidgetPictureID(IGePicwID,48);
			guiSetWidgetPictureID(RGePicwID,38);guiSetWidgetPictureID(UGePicwID,38);
		}	break;
		case 9:{
			guiSetWidgetPictureID(NumShiPicwID,28);guiSetWidgetPictureID(IGePicwID,49);
			guiSetWidgetPictureID(RGePicwID,39);guiSetWidgetPictureID(UGePicwID,39);
		}	break;
		default:break;
	}
	if(NUM==1000)	NUM=100;
	return GUI_EOK;
}

