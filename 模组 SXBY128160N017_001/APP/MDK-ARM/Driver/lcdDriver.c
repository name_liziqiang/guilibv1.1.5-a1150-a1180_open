#include "lcddriver.h"
#include "systemheart.h"
#include "stdio.h"
#include "lcdinterface.h"
#include "m1130_gpio.h"
#include "QspiDMA.h"
#include "lcdInit.h"


static __align(4) uint8_t  lcdSendBuff1[4];
#define LCDDRIVER_DELAY_NOP()     //__nop();__nop();__nop();__nop()
static __align(4) gui_uint16  lcdSendBuff[2][Lcd_SendBuf_HalfWord_Max+2];

/*
*********************************************************************************************************************
*函数功能：LCD 常用参数定义
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/

LCD_PARAMETER lcdPara={
	LCD_WIDTH,                          //LCD 宽度
	LCD_HEIGHT,                         //LCD 高度
	BLACK,                              //LCD 使用的背景颜色
	RED,                                //LCD 使用的字体颜色
	(gui_uint8 *)lcdSendBuff,
	Flash_Read_ByteMax*2	
};
/*
*********************************************************************************************************************
*函数功能：LCD_Read_DATA，从LCD读取一个数据
*入口参数：无
*出口参数：uint16_t，读取到的数据
*********************************************************************************************************************
*/
static uint16_t LCD_Read_DATA(void)
{
	return 0;
}
/*
*********************************************************************************************************************
*函数功能：LCD_Write_DATA  写一个数据到LCD
*入口参数：data 写入的数据
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_Write_DATA(uint16_t data)
{
	LCD_RS_G();
	LCD_CS_D();
	lcdSendBuff1[0] = data;
	QSPIDMA_SendBytes(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL , lcdSendBuff1 , 1);
	QSPIDMA_WaitEnd(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL);
	LCD_CS_G()
}
/*
*********************************************************************************************************************
*函数功能：LCD_Write_CMD  写一个命令到LCD
*入口参数：cmd 写入的命令
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_Write_CMD(uint16_t cmd)
{
	LCD_RS_D();
	LCD_CS_D();
	lcdSendBuff1[0] = cmd;
	QSPIDMA_SendBytes(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL , lcdSendBuff1 , 1);
	QSPIDMA_WaitEnd(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL);
	LCD_RS_G();
	LCD_CS_G()
}
/*
*********************************************************************************************************************
*函数功能：LCD_WriteReg，LCD写一个寄存器的值
*入口参数：LCD_Reg 寄存器地址
*          LCD_RegValue 需要的寄存器的值
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_WriteReg(uint16_t LCD_Reg,uint16_t LCD_RegValue)
{	
	LCD_Write_CMD(LCD_Reg);  
	LCD_Write_DATA(LCD_RegValue);	    		 
}
/*
*********************************************************************************************************************
*函数功能：LCD_SetCursor，LCD设置光标位置,开始位置和结束位置
*入口参数：Xpos:横坐标
*          Xepos:结束横坐标
*          Ypos:纵坐标
*          Yepos:结束纵坐标
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_SetCursor(uint16_t Xpos, uint16_t Ypos, uint16_t Xepos, uint16_t Yepos)
{	    
	LCD_Write_CMD(0X2A); 
	LCD_Write_DATA(Xpos>>8);LCD_Write_DATA(Xpos&0XFF);
	LCD_Write_DATA(Xepos>>8);LCD_Write_DATA(Xepos&0XFF); 		
	LCD_Write_CMD(0X2B); 
	LCD_Write_DATA(Ypos>>8);LCD_Write_DATA(Ypos&0XFF); 		
	LCD_Write_DATA(Yepos>>8);LCD_Write_DATA(Yepos&0XFF); 	
} 
/*
*********************************************************************************************************************
*函数功能：LCD_DisplayOn LCD开启显示
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_DisplayOn(void)
{					   
	LCD_Write_CMD(0x29);
}	
/*
*********************************************************************************************************************
*函数功能：LCD_DisplayOff，LCD关闭显示
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_DisplayOff(void)
{	   
	LCD_Write_CMD(0x28);
}  
/*
*********************************************************************************************************************
*函数功能：LCD_Scan_Dir，设置扫描方向
*入口参数：dir，新的扫描方向
*出口参数：LCD_SCAN，返回新的扫描方向
*********************************************************************************************************************
*/  
static void LCD_Scan_Dir(gui_uint8 dir)
{
	if(dir==DIR_HORIZONTAL){
		//横屏
		LCD_WriteReg(0x36,0xb8);//0x36寄存器控制着LCD的扫描方向
		uint16_t width_x=lcdPara.width;
		lcdPara.width=lcdPara.height;
		lcdPara.height=width_x;
	}
	else{
		LCD_WriteReg(0x36,0xc8);//0x36寄存器控制着LCD的扫描方向，写入的都是RGB，bit3为0的时候 显示屏显示RGB  1的时候显示屏显示BGR，我们应该是BGR
	}



} 
/*
*********************************************************************************************************************
*函数功能：LCD_Scroll，设置滚动区域（该驱动待测）
*入口参数：sx sy起点坐标
*          ex ey终点坐标
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_Scroll(uint16_t sx,uint16_t sy,uint16_t ex,uint16_t ey)
{
	uint16_t y_offset=sx-ex;
	LCD_Write_CMD(0x33);
	LCD_Write_DATA(sx>>8);LCD_Write_DATA(sx&0XFF);//顶部行
	LCD_Write_DATA(y_offset>>8);LCD_Write_DATA(y_offset&0XFF);
	LCD_Write_DATA(ey>>8);LCD_Write_DATA(ey&0XFF);//底部行
	LCD_Write_CMD(0x37);
	LCD_Write_DATA(ex>>8);LCD_Write_DATA(ex&0XFF);//底部行
}

const LCD_DRIVER Lcd_Driver={   
	LCD_Write_DATA,
	LCD_Read_DATA,
	LCD_Write_CMD, 			 //写命令
	LCD_SetCursor,       //LCD 设置光标起始位置和结束位置
	LCD_Scan_Dir,        //LCD扫描方向
	LCD_DisplayOn,
	LCD_DisplayOff,
	LCD_Scroll,
	LcdSetDim
};//LCD描述符	 
