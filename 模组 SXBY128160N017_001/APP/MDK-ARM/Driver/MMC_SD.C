#include "mmc_sd.h"			   
#include "m1130.h"
#include "m1130_gpio.h"
#include "m1130_qspi.h"
#include "m1130_xip_write.h"
#include "stdio.h"

uint8_t  SD_Type=0;//SD卡的类型 
__align(4) static uint8_t SD_WriteReadBuf[32];

//SPI硬件层初始化
void SD_SPI_Init(void)
{
	//QSPI_InitTypeDef QSPI_InitStructure;
	GPIO_SetPinMux(GPIO0, GPIO_Pin_5,GPIO_FUNCTION_0);//QSPI1_TF_CS 
	GPIO_SetPinDir(GPIO0, GPIO_Pin_5,GPIO_Mode_OUT);  //QSPI1_TF_CS 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_28,GPIO_FUNCTION_0);//QSPI1_XPT_CS 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_28,GPIO_Mode_OUT);  //QSPI1_XPT_CS 
	GPIO_SetPin(GPIO0, GPIO_Pin_5);//QSPI1_TF_CS 
	GPIO_SetPin(GPIO1, GPIO_Pin_28);//QSPI1_XPT_CS 
	
	RCC->AHBCLKCTRL0_SET=1<<14;//打开QSPI时钟
	RCC->PRESETCTRL0_CLR=1<<14;
	RCC->PRESETCTRL0_SET=1<<14;//复位QSPI1
	
	RCC->QUADSPI1CLKDIV = 8;//384/8=48mhz QSPI1 CLK=48MHZ
	
	QSPI1->CTRL0_CLR=1<<29;//关闭SPI

	GPIO_SetPinMux(GPIO0, GPIO_Pin_13, GPIO_FUNCTION_3);//QSPI1_MOSI 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_7, GPIO_FUNCTION_3);//QSPI_CLK 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_6, GPIO_FUNCTION_3);//QSPI1_MISO 	
	
	QSPI1->CTRL1=0x00002038; //需要使能SPI DMA 借助DMA自动清除RUN的机制，
	QSPI1->TIMING=0x00000400;
	QSPI1->CTRL0_SET=1<<28;//全双工
}


void SDSpiCS_Low(void)
{
	GPIO0->DT_CLR=GPIO_Pin_5;//QSPI1_TF_CS 
}
void SDSpiCS_High(void)
{
	GPIO0->DT_SET=GPIO_Pin_5;//QSPI1_TF_CS 
}


 
//SD卡初始化的时候,需要低速
void SD_SPI_SpeedLow(void)
{
 	//SPI_CLK=48MHZ   CLK_RATE = 1  SD_SPI_Init 中 DIV = 128 那么SPI = 0.375MHZ 低速不超过400K
	QSPI1->CTRL0_CLR = 1<<29;
	QSPI1->TIMING &= 0xffff00ff;
	QSPI1->TIMING |= (128<<8);
}
//SD卡正常工作的时候,可以高速了
void SD_SPI_SpeedHigh(void)
{
	//SPI_CLK=48MHZ   CLK_RATE = 1  SD_SPI_Init 中 DIV = 4 那么SPI = 12MHZ
	QSPI1->CTRL0_CLR = 1<<29;
	QSPI1->TIMING &= 0xffff00ff;
	QSPI1->TIMING |= (4<<8);
}
void SD_ExternCLk(void)
{
	SD_WriteReadBuf[0]=0xff;
 	xipTX(QSPI1,SD_WriteReadBuf,1);
}
///////////////////////////////////////////////////////////////////////////////////
//取消选择,释放SPI总线
void SD_DisSelect(void)
{
	SDSpiCS_High();
	SD_ExternCLk();//提供额外的8个时钟
}
//选择sd卡,并且等待卡准备OK
//返回值:0,成功;1,失败;
uint8_t SD_Select(void)
{
	SDSpiCS_Low();
	if(SD_WaitReady()==0)return 0;//等待成功
	SD_DisSelect();
	return 1;//等待失败
}
//等待卡准备好
//返回值:0,准备好了;其他,错误代码
uint8_t SD_WaitReady(void)
{
	uint32_t t=0;
	do
	{
		xipRX(QSPI1,SD_WriteReadBuf,1);
		if(SD_WriteReadBuf[0]==0XFF)return 0;//OK
		t++;		  	
	}while(t<0XFFFFFF);//等待 
	return 1;
}
//等待SD卡回应
//Response:要得到的回应值
//返回值:0,成功得到了该回应值
//    其他,得到回应值失败
static uint8_t SD_GetResponse(uint8_t Response)
{
	uint16_t Count=0xFFFF;//等待次数	   						  
	while (Count){
		xipRX(QSPI1,SD_WriteReadBuf,1);
		if(SD_WriteReadBuf[0]==Response)break;
		Count--;//等待得到准确的回应  	  
	}
	if (Count==0)return MSD_RESPONSE_FAILURE;//得到回应失败   
	else return MSD_RESPONSE_NO_ERROR;//正确回应
}
//从sd卡读取一个数据包的内容
//buf:数据缓存区
//len:要读取的数据长度.
//返回值:0,成功;其他,失败;	
uint8_t SD_RecvData(uint8_t*buf,uint16_t len)
{			  	  
	if(SD_GetResponse(0xFE))return 1;//等待SD卡发回数据起始令牌0xFE
    xipRX(QSPI1,buf,len);
    //下面是2个伪CRC（dummy CRC）
		SD_WriteReadBuf[0]=0xff;
		SD_WriteReadBuf[1]=0xff;
		xipTX(QSPI1,SD_WriteReadBuf,2);							  					    
    return 0;//读取成功
}
//向sd卡写入一个数据包的内容 512字节
//buf:数据缓存区
//cmd:指令
//返回值:0,成功;其他,失败;	
uint8_t SD_SendBlock(uint8_t*buf,uint8_t cmd)
{		  	  
	if(SD_WaitReady())return 1;//等待准备失效
	SD_WriteReadBuf[0]=cmd;
	xipTX(QSPI1,SD_WriteReadBuf,1);
	if(cmd!=0XFD)//不是结束指令
	{
		xipTX(QSPI1,buf,SD_BLOCK_MAXLEN);
		SD_WriteReadBuf[0]=0xff;//忽略crc
		SD_WriteReadBuf[1]=0xff;
		xipTX(QSPI1,SD_WriteReadBuf,3);	
		xipRX(QSPI1,SD_WriteReadBuf,1);//接收响应

		if((SD_WriteReadBuf[0]&0x1F)!=0x05)return 2;//响应错误									  					    
	}						 									  					    
  return 0;//写入成功
}

//向SD卡发送一个命令
//输入: uint8_t cmd   命令 
//      uint32_t arg  命令参数
//      uint8_t crc   crc校验值	   
//返回值:SD卡返回的响应															  
uint8_t SD_SendCmd(uint8_t cmd, uint32_t arg, uint8_t crc)
{	
	uint8_t Retry=0; 
	//SD_DisSelect();//取消上次片选
	if(SD_Select())return 0XFF;//片选失效 
	//发送
	SD_WriteReadBuf[0]=cmd | 0x40;//分别写入命令
	SD_WriteReadBuf[1]=arg >> 24;
	SD_WriteReadBuf[2]=arg >> 16;
	SD_WriteReadBuf[3]=arg >> 8;
	SD_WriteReadBuf[4]=arg >> 0;
	SD_WriteReadBuf[5]=crc;
	xipTX(QSPI1,SD_WriteReadBuf,6);
	if(cmd==CMD12){
		SD_WriteReadBuf[1]=0xff;
		xipTX(QSPI1,SD_WriteReadBuf,1);//Skip a stuff byte when stop reading
	}
  //等待响应，或超时退出
	Retry=0X1F;
	do{
		xipRX(QSPI1,SD_WriteReadBuf,1);//接收响应
	}while((SD_WriteReadBuf[0]&0X80) && Retry--);	 
	//返回状态值
  return SD_WriteReadBuf[0];
}		    																			  
//获取SD卡的CID信息，包括制造商信息
//输入: uint8_t *cid_data(存放CID的内存，至少16Byte）	  
//返回值:0：NO_ERR
//		 1：错误														   
uint8_t SD_GetCID(uint8_t *cid_data)
{
	uint8_t r1;	   
	//发CMD10命令，读CID
	r1=SD_SendCmd(CMD10,0,0x01);
	if(r1==0x00){
		r1=SD_RecvData(cid_data,16);//接收16个字节的数据	 
  }
	SD_DisSelect();//取消片选
	if(r1)return 1;
	else return 0;
}																				  
//获取SD卡的CSD信息，包括容量和速度信息
//输入:uint8_t *cid_data(存放CID的内存，至少16Byte）	    
//返回值:0：NO_ERR
//		 1：错误														   
uint8_t SD_GetCSD(uint8_t *csd_data)
{
    uint8_t r1;	 
    r1=SD_SendCmd(CMD9,0,0x01);//发CMD9命令，读CSD
    if(r1==0)
	{
    	r1=SD_RecvData(csd_data, 16);//接收16个字节的数据 
    }
	SD_DisSelect();//取消片选
	if(r1)return 1;
	else return 0;
}  
//获取SD卡的总扇区数（扇区数）   
//返回值:0： 取容量出错 
//       其他:SD卡的容量(扇区数/512字节)
//每扇区的字节数必为512，因为如果不是512，则初始化不能通过.														  
uint32_t SD_GetSectorCount(void)
{
    uint8_t csd[16];
    uint32_t Capacity;  
    uint8_t n;
	uint16_t csize;  					    
	//取CSD信息，如果期间出错，返回0
    if(SD_GetCSD(csd)!=0) return 0;	    
    //如果为SDHC卡，按照下面方式计算
    if((csd[0]&0xC0)==0x40)	 //V2.00的卡
    {	
		csize = csd[9] + ((uint16_t)csd[8] << 8) + 1;
		Capacity = (uint32_t)csize << 10;//得到扇区数	 		   
    }else//V1.XX的卡
    {	
		n = (csd[5] & 15) + ((csd[10] & 128) >> 7) + ((csd[9] & 3) << 1) + 2;
		csize = (csd[8] >> 6) + ((uint16_t)csd[7] << 2) + ((uint16_t)(csd[6] & 3) << 10) + 1;
		Capacity= (uint32_t)csize << (n - 9);//得到扇区数   
    }
    return Capacity;
}
//初始化SD卡
uint8_t SD_Initialize(void)
{
	uint8_t r1;      // 存放SD卡的返回值
	uint16_t retry;  // 用来进行超时计数
	uint16_t i;
	
	SD_SPI_Init();		//初始化IO
 	SD_SPI_SpeedLow();	//设置到低速模式 
	
 	for(i=0;i<10;i++){
		SD_WriteReadBuf[i]=0xff;
	}
	xipTX(QSPI1,SD_WriteReadBuf,10);//发送最少74个脉冲
	retry=20;	
	do
	{
		//printf("set sd idle1\r\n");
		r1=SD_SendCmd(CMD0,0,0x95);//进入IDLE状态
		//printf("set sd idle2\r\n");
		//printf("SD_SendCmd result:%02x\r\n",r1);
	}while((r1!=0X01) && retry--);
 	SD_Type=0;//默认无卡
	if(r1==0X01)
	{
		if(SD_SendCmd(CMD8,0x1AA,0x87)==1)//SD V2.0
		{
			xipRX(QSPI1,SD_WriteReadBuf,4);//Get trailing return value of R7 resp
			if(SD_WriteReadBuf[2]==0X01&&SD_WriteReadBuf[3]==0XAA)//卡是否支持2.7~3.6V
			{
				retry=0XFFFE;
				do
				{
					SD_SendCmd(CMD55,0,0X01);	//发送CMD55
					r1=SD_SendCmd(CMD41,0x40000000,0X01);//发送CMD41
				}while(r1&&retry--);
				if(retry&&SD_SendCmd(CMD58,0,0X01)==0)//鉴别SD2.0卡版本开始
				{
					xipRX(QSPI1,SD_WriteReadBuf,4);//得到OCR值
					if(SD_WriteReadBuf[0]&0x40)SD_Type=SD_TYPE_V2HC;//检查CCS
					else SD_Type=SD_TYPE_V2;   
				}
			}
		}
		else//SD V1.x/ MMC	V3
		{
			SD_SendCmd(CMD55,0,0X01);		//发送CMD55
			r1=SD_SendCmd(CMD41,0,0X01);	//发送CMD41
			if(r1<=1)
			{		
				SD_Type=SD_TYPE_V1;
				retry=0XFFFE;
				do //等待退出IDLE模式
				{
					SD_SendCmd(CMD55,0,0X01);	//发送CMD55
					r1=SD_SendCmd(CMD41,0,0X01);//发送CMD41
				}while(r1&&retry--);
			}else//MMC卡不支持CMD55+CMD41识别
			{
				SD_Type=SD_TYPE_MMC;//MMC V3
				retry=0XFFFE;
				do //等待退出IDLE模式
				{											    
					r1=SD_SendCmd(CMD1,0,0X01);//发送CMD1
				}while(r1&&retry--);  
			}
			if(retry==0||SD_SendCmd(CMD16,512,0X01)!=0)SD_Type=SD_TYPE_ERR;//错误的卡
		}
	}
	SD_DisSelect();//取消片选
	SD_SPI_SpeedHigh();//高速
	//printf("SD_Type is %02x\r\n",SD_Type);
	if(SD_Type)return 0;
	else if(r1)return r1; 	   
	return 0xaa;//其他错误
}
//读SD卡
//buf:数据缓存区
//sector:扇区
//cnt:扇区数
//返回值:0,ok;其他,失败.
uint8_t SD_ReadDisk(uint8_t*buf,uint32_t sector,uint8_t cnt)
{
	uint8_t r1;
	if(SD_Type!=SD_TYPE_V2HC)sector<<= 9;//转换为字节地址
	if(cnt==1)
	{
		r1=SD_SendCmd(CMD17,sector,0X01);//读命令
		if(r1==0)//指令发送成功
		{
			r1=SD_RecvData(buf,SD_BLOCK_MAXLEN);//接收512个字节	   
		}
	}else
	{
		r1=SD_SendCmd(CMD18,sector,0X01);//连续读命令
		do
		{
			r1=SD_RecvData(buf,SD_BLOCK_MAXLEN);//接收512个字节	 
			buf+=512;  
		}while(--cnt && r1==0); 	
		SD_SendCmd(CMD12,0,0X01);	//发送停止命令
	}   
	SD_DisSelect();//取消片选
	return r1;//
}
//写SD卡
//buf:数据缓存区
//sector:起始扇区
//cnt:扇区数
//返回值:0,ok;其他,失败.
uint8_t SD_WriteDisk(uint8_t*buf,uint32_t sector,uint8_t cnt)
{
	uint8_t r1;
	if(SD_Type!=SD_TYPE_V2HC)sector *= SD_BLOCK_MAXLEN;//转换为字节地址
	if(cnt==1)
	{
		r1=SD_SendCmd(CMD24,sector,0X01);//写 sector
		if(r1==0)//指令发送成功
		{
			r1=SD_SendBlock(buf,0xFE);//写512个字节	   
		}
	}else
	{
		if(SD_Type!=SD_TYPE_MMC)
		{
			SD_SendCmd(CMD55,0,0X01);	
			SD_SendCmd(CMD23,cnt,0X01);//发送指令	
		}
 		r1=SD_SendCmd(CMD25,sector,0X01);//连续读命令
		if(r1==0)
		{
			do
			{
				r1=SD_SendBlock(buf,0xFC);//接收512个字节	 
				buf+=SD_BLOCK_MAXLEN;  
			}while(--cnt && r1==0);
			r1=SD_SendBlock(0,0xFD);//接收512个字节 
		}
	}   
	SD_DisSelect();//取消片选
	return r1;//
}	

//__align(4) uint8_t SD_WriteReadSectorBuf[SD_BLOCK_MAXLEN];
//void sd_spi_test(void)
//{
//	uint8_t sd_check;
//	sd_check = SD_Initialize();
//	if(sd_check == 0){
//		//检测到SD
//		uint32_t sd_size=SD_GetSectorCount();//获取SD卡扇区数
//		printf("SD Sector Count:%d\r\n",sd_size);
//		for(int i=0;i<SD_BLOCK_MAXLEN;i++){
//			SD_WriteReadSectorBuf[i]=i;
//		}
//		SD_WriteDisk(SD_WriteReadSectorBuf,0,1);
//		for(int i=0;i<SD_BLOCK_MAXLEN;i++){
//			SD_WriteReadSectorBuf[i]=0;
//		}
//		SD_ReadDisk(SD_WriteReadSectorBuf,0,1);//从0扇区读取一个扇区的数据到SD_WriteReadSectorBuf
//		for(int i=0;i<SD_BLOCK_MAXLEN;i++){
//			printf("%x ",SD_WriteReadSectorBuf[i]);
//		}
//		printf("\r\n");
//	}
//	else printf("SD check failed\r\n");
//}



