#include "SystemHeart.h"
#include <stdio.h>
#include <string.h>
#include "m1130.h"
#include "m1130_tim.h"

volatile uint32_t systemTick=0;

void SystemHeart_Init(void)
{
	//初始化定时器16，用着系统心跳
	RCC->AHBCLKCTRL0_SET|=(1<<19);//开启定时器时钟
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Prescaler=95;//96Mhz/(95+1)=1Mhz
	TIM_TimeBaseStructure.TIM_Period=1000;//计数到1000刚好是1ms 中断
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	//SYSAHBCLKDIV=4 在SystemInit已经初始化了 AHB时钟96Mhz
	TIM_TimeBaseInit(TIM16, &TIM_TimeBaseStructure);
	TIM_SelectOutputTrigger(TIM16, TIM_TRGOSource_Enable);//选择TIM16的update事件更新为触发源
	TIM_ClearITPendingBit(TIM16, TIM_IT_Update);     			//清除update事件中断标志
	TIM_ITConfig(TIM16, TIM_IT_Update, ENABLE);       			//使能TIM16中断 
	//中断配置
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel=TIM16_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPriority=0;//2bit位宽
	NVIC_Init(&NVIC_InitStruct);
	TIM_Cmd(TIM16,ENABLE);
}
	
uint32_t Get_SystemTick(void)
{
	return systemTick;
}

#ifndef USE_HAL_DELAY
void Delay_ms(unsigned int delay)
{
	uint32_t tickstart = systemTick;
  uint32_t wait = delay;

  /* Add a freq to guarantee minimum wait */
  if (wait < SYSTEM_MAX_DELAY){
    wait += (uint32_t)(1);
  }
	//这样处理的原因在于，避免systemTick溢出导致一个很大很大的延时
	while(wait!=0){
		if(systemTick!=tickstart){
			wait--;
			tickstart=systemTick;
		}
	}
}
#endif
