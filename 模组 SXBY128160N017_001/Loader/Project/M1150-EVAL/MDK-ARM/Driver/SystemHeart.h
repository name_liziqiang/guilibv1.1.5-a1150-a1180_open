#ifndef _systemhear_h
#define _systemhear_h
#include "typedefine.h"


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：系统心跳（为后续所有事件的消息循环提供时基）
 *实现功能：1、系统心跳计数递增实现（1KHZ）
 *          2、获取系统当前心跳值
 *          3、MS级别系统延时函数（通过USE_HAL_DELAY宏可以定义兼容HAL库是否使用HAL库的延时函数）
 *
 *撰 写 人：Alpscale LCD Application development team
 *撰写时间：2019-7-1
 *测 试 人：Alpscale LCD Application development team
 *测试时间：2019-7-1
 *版 本 号：V1.01_2019-7-1
 *版本说明：
 *                              
 *          心跳计数递增                           获取系统心跳                              MS级别系统延时
 *
 *V1.01：   功能实现                               功能实现                                  功能实现
 *
 *V1.02：
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/*心跳最大计数值*/
#define  SYSTEMTICK_MAX      0xFFFFFFFFU
/*
 *ms级别的系统延时函数，依据systemTick计数器的实现，us系统不提供，需要使用的时候自己内部实现
 *拒绝使用delay、delay_us等函数名称，该延时函数会导致系统阻塞，请谨慎使用 
 */
//#define USE_HAL_DELAY//使用HAL库的延时函数
#ifdef USE_HAL_DELAY
	#include "stm32f1xx_hal.h"
	#define Delay_ms(delay)   HAL_Delay(delay)
#else 
	#define SYSTEM_MAX_DELAY      0xFFFFFFFFU
	void Delay_ms(unsigned int delay);
#endif


/*系统心跳计数递增实现，移植的时候创建1MS中断，然后调用下面接口函数*/
extern volatile uint32_t systemTick;
#define  SystemTick_Handle() systemTick++
/*获取系统当前心跳值*/
uint32_t Get_SystemTick(void);
/*初始化*/
void SystemHeart_Init(void);

void TIM6_IRQHandler(void);
#endif
