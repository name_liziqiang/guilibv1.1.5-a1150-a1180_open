#include "stdio.h"
#include <stdint.h>
#include <m1130_qspi.h>
#include "mmc_sd.h"
#include "ff.h"
#include "m1130_eval_qspi.h"
#include "lcddriver.h"
#include "lcdinit.h"
#include "LcdSpi.h"
#include "guiConfig.h"
#include "command.h"
#include "comInterface.h"
#include "mallocInterface.h"
#include "memoryInterface.h"
//#define DISP_DOWNLOAD_TIPS         1
#define FLASHRES_START_ADDR        ((FLASH_RES_START_ADDR) + (FLASH_SECTOR_SIZE))
#define FLASHAPP_START_ADDR        0x8000

#define WRITEPAGE_MAX    256
static __align(4) uint8_t FlashWriteBuff[WRITEPAGE_MAX];//4字节对齐的缓存区，便于flash数据的写入
//static FATFS SD_Fatfs;
//static FIL   ResourceFile;

extern int qspi_flash_erase_block_4k(QSPI_TypeDef *QSPIptr, uint32_t faddr);
extern int qspi_flash_write_page(QSPI_TypeDef *QSPIptr, uint8_t *buf, uint32_t faddr, int count);
extern void qspi_flash_global_unprotect(QSPI_TypeDef* QSPIptr);

#define UPDATE_LCD_TIPS_ON     //定义该宏表示打开LCD提示下载,
#define TIPS_DISP_X         25//图片显示的X坐标
#define TIPS_DISP_Y         50//图片显示的Y坐标
#define PIC_SIZE_WIDTH      72 //图片宽度
#define PIC_SIZE_HEIGHT     72 //图片高度

#ifdef UPDATE_LCD_TIPS_ON
extern const unsigned char gImage_updateLogo[];
static void disp_Tip_Logo()
{
	//uint16_t *data=(uint16_t *)gImage_updateLogo;
	uint32_t totalpoint=PIC_SIZE_WIDTH;
	totalpoint*=PIC_SIZE_HEIGHT; 			  //得到总点数
	
	Lcd_Driver.setcursor(TIPS_DISP_X,TIPS_DISP_Y,TIPS_DISP_X+PIC_SIZE_WIDTH-1,TIPS_DISP_Y+PIC_SIZE_HEIGHT-1);
	Lcd_Driver.writeCMD(0x2c);
	
	LCD_RS_G();
	uint32_t sendBytes = 0;
	uint8_t *data = (uint8_t *)gImage_updateLogo;
	while(1){
		if(totalpoint >= LCD_SENDBUF_MAX)sendBytes = LCD_SENDBUF_MAX;
		else sendBytes = totalpoint;
		Lcd_WaitSendEnd();
		Lcd_SendBytes((uint8_t *)data , sendBytes*2);
		totalpoint -= sendBytes;
		data += sendBytes<<1;
		if(totalpoint == 0){
			Lcd_WaitSendEnd();
			break;
		}
	}
}
#endif

void CheckUpade(void)
{
//	uint32_t readBytes=0; //读取文件字节数
//	uint32_t resBytes=0;	//文件字节数
//	uint32_t writeAddr=0;
//	uint32_t filePointer=0;
//	uint32_t start_Sector=0;
//	uint32_t count_Sector=0;//擦除flash的起始扇区和扇区个数
	
	//不管能不能挂载成功  先把LCD下载提示logo显示出来
	#ifdef UPDATE_LCD_TIPS_ON
		LCD_Init();
		Lcd_Driver.clear();
		disp_Tip_Logo();
	#endif
//	while(1);
	QSPI0->BUSY_DLY &= 0xFFFFF0FF;	//clean [11:8]
	QSPI0->BUSY_DLY |= (1<<8);
	qspi_flash_global_unprotect(QSPI0);//以防万一Flash默认值不是可写入	
//	while(1);
//	FRESULT res=f_mount(&SD_Fatfs,"0:",1);//挂在SD 1挂载 0移除
//	if(res==FR_OK){
//		//挂载SD卡成功 
//		printf("mount sd ok\r\n");
//		
//		res = f_open(&ResourceFile,"0:/App.bin",FA_READ);//尝试读取资源文件 固定文件名 GuiRes.bin
//		if(res==FR_OK){
//			//发现一个新的资源文件
//			resBytes=(uint32_t)ResourceFile.fsize;//得到文件大小
//			printf("find app code file ,size:%dByte\r\n",resBytes);
//			//开始擦除扇区
//			writeAddr=FLASHAPP_START_ADDR;
//			start_Sector = writeAddr/4096;
//			count_Sector = resBytes/4096;
//			if(resBytes%4096)count_Sector++;
//			printf("erase app code flash\r\n");
//			for(uint32_t i=start_Sector;i<start_Sector+count_Sector;i++){
//				qspi_flash_erase_block_4k(QSPI0,i*4096);
//			}
//			printf("write new app code to flash\r\n");
//			//开始写入数据
//			filePointer=0;
//			while(1){
//				//printf("filePointer: %d\r\n",filePointer);
//				readBytes=0;
//				f_read(&ResourceFile,FlashWriteBuff,WRITEPAGE_MAX,&readBytes);
//				if(readBytes!=0){
//					qspi_flash_write_page(QSPI0,FlashWriteBuff,writeAddr,readBytes);//这个函数位于 m1130_xip_write.c中，被连接到flash前8K 
//					writeAddr+=readBytes;
//					filePointer+=readBytes;
//					f_lseek(&ResourceFile,filePointer);//移动文件指针
//				}
//				else{
//					break;
//				}
//			}
//			f_close(&ResourceFile);
//		}
//		res = f_open(&ResourceFile,"0:/ResGui.bin",FA_READ);//尝试读取资源文件 固定文件名 GuiRes.bin
//		if(res==FR_OK){
//			//发现一个新的资源文件
//			resBytes=(uint32_t)ResourceFile.fsize;//得到文件大小
//			printf("find resource file ,size:%dByte\r\n",resBytes);
//			//开始擦除扇区
//			writeAddr=FLASHRES_START_ADDR;
//			start_Sector = writeAddr/4096;
//			count_Sector = resBytes/4096;
//			if(resBytes%4096)count_Sector++;
//			printf("erase resource flash\r\n");
//			for(uint32_t i=start_Sector;i<start_Sector+count_Sector;i++){
//				qspi_flash_erase_block_4k(QSPI0,i*4096);
//			}
//			printf("write new resource to flash\r\n");
//			//开始写入数据
//			filePointer=0;
//			while(1){
//				//printf("filePointer: %d\r\n",filePointer);
//				readBytes=0;
//				f_read(&ResourceFile,FlashWriteBuff,WRITEPAGE_MAX,&readBytes);
//				if(readBytes!=0){
//					qspi_flash_write_page(QSPI0,FlashWriteBuff,writeAddr,readBytes);//这个函数位于 m1130_xip_write.c中，被连接到flash前8K
//					writeAddr+=readBytes;
//					filePointer+=readBytes;
//					f_lseek(&ResourceFile,filePointer);//移动文件指针
//				}
//				else{
//					break;
//				}
//			}
//			f_close(&ResourceFile);
//		}
//		f_mount(&SD_Fatfs,"0:",0);
//	}
//	else printf("mount sd error\r\n");
}



CMD_State COMMAND_writeFlash(void *result,const void *charc,uint32_t c_len)
{
	/////////////////////////////////////////////////////////////////////////////////
	//写flash数据
	uint8_t *rdata=(uint8_t *)result;
	uint8_t *data=(uint8_t *)charc;
	if(*(data+0)!=0x47)return FAILED_CMD;
	if(*(data+1)!=0x47)return FAILED_CMD;
	if(*(data+2)!=0x4b)return FAILED_CMD;
	if(*(data+3)!=0x4a)return FAILED_CMD;//校验不过
	uint32_t addr=0;//写入的flash地址
	addr|=(*(data+4)<<24);
	addr|=(*(data+5)<<16);
	addr|=(*(data+6)<<8);
	addr|=(*(data+7)<<0);
	uint8_t count=(*(data+8));
//#if  USE_PRINTF_DEBUG
  //printf("COMMAND_writeFlash write addr:%d,count: %d\r\n",addr,count);
//#endif	
	for(int i=0;i<count;i++){
		FlashWriteBuff[i]=*(data+9+i);//将数据拷贝到一个4字节对齐的缓存区
	}
	if(count!=0)
  qspi_flash_write(QSPI0,FlashWriteBuff,addr,count);//这个函数位于 m1130_xip_write.c中，被连接到flash前8K
	rdata[0]=0x80;//功能码0003响应
	rdata[1]=0x03;
	rdata[2]=0x47;//校验码
	rdata[3]=0x47;
	rdata[4]=0x4b;
	rdata[5]=0x4a;
	rdata[6]=6;//数据长度
	rdata[7]=1;//操作成功
	Malloc_Des.memcpy(rdata+8,data+4,5);
	rdata[13]=0;
	for(int i=0;i<13;i++)
	{
		rdata[13]+=rdata[i];
	}
	Com_Des.outputString(rdata,14);//返回结果
	return OK_CMD;
}

CMD_State COMMAND_eraseFlash(void *result,const void *charc,uint32_t c_len)
{
	/////////////////////////////////////////////////////////////////////////////////
	//擦除flash扇区
	uint8_t *data=(uint8_t *)charc;
	if(*(data+0)!=0x47)return FAILED_CMD;
	if(*(data+1)!=0x47)return FAILED_CMD;
	if(*(data+2)!=0x4b)return FAILED_CMD;
	if(*(data+3)!=0x4a)return FAILED_CMD;//校验不过
	uint32_t start_Sector=0;//擦除的起始扇区
	start_Sector|=(*(data+4)<<24);
	start_Sector|=(*(data+5)<<16);
	start_Sector|=(*(data+6)<<8);
	start_Sector|=(*(data+7)<<0);
	uint32_t count_Sector=0;//擦除的扇区个数
	count_Sector|=(*(data+8)<<24);
	count_Sector|=(*(data+9)<<16);
	count_Sector|=(*(data+10)<<8);
	count_Sector|=(*(data+11)<<0);
for(uint32_t i=start_Sector;i<start_Sector+count_Sector;i++){
		qspi_flash_erase_block_4k(QSPI0,i*4096);
}
	uint8_t *rdata=(uint8_t *)result;
	rdata[0]=0x80;//功能码0004响应
	rdata[1]=0x04;
	rdata[2]=0x47;//校验码
	rdata[3]=0x47;
	rdata[4]=0x4b;
	rdata[5]=0x4a;
	rdata[6]=9;//数据长度
	rdata[7]=1;//操作成功
	Malloc_Des.memcpy(rdata+8,data+4,8);
	rdata[16]=0;
	for(int i=0;i<16;i++)
	{
		rdata[16]+=rdata[i];
	}
	Com_Des.outputString(rdata,17);//返回结果
	return OK_CMD;
}

CMD_State COMMAND_readCOM(void *result,const void *charc,uint32_t c_len)
{
	uint8_t *rdata=(uint8_t *)result;
	rdata[0]=0x80;
	rdata[1]=0x01;
	rdata[2]=0x05;//数据长度
	rdata[3]=0x00;//通讯类型 0usart
	rdata[4]=0x00;
	rdata[5]=0x01;
	rdata[6]=0xC2;
	rdata[7]=0x00;//通讯波特率，固定为115200，后期根据实际波特率调整
	rdata[8]=0;//校验和
	for(int i=0;i<8;i++)rdata[8]+=rdata[i];	
	
	Com_Des.outputString(rdata,9);//返回结果
	return OK_CMD;
}

CMD_State COMMAND_writeCOM(void *result,const void *charc,uint32_t c_len)
{
	return OK_CMD;
}

extern uint8_t Updata_state;
CMD_State COMMAND_askconnect(void *result,const void *charc,uint32_t c_len)
{
	uint8_t *rdata=(uint8_t *)result;
	uint8_t *data=(uint8_t *)charc;
	if(*(data+0)!=0x47)return FAILED_CMD;
	if(*(data+1)!=0x47)return FAILED_CMD;
	if(*(data+2)!=0x4b)return FAILED_CMD;
	if(*(data+3)!=0x4a)return FAILED_CMD;//校验不过
	uint8_t cmd =0;
	 cmd=*(data+5);
	if(cmd==0x03){
	  Updata_state=1;
		rdata[0]=0x80;//
		rdata[1]=0x06;
		rdata[2]=0x47;//校验码
		rdata[3]=0x47;
		rdata[4]=0x4b;
		rdata[5]=0x4a;
		rdata[6]=1;//数据长度
		rdata[7]=4;//操作成功
		for(int i=0;i<8;i++)
		{
			rdata[8]+=rdata[i];
		}
		Com_Des.outputString(rdata,9);//返回结果  
	}
	
	else	if(cmd==0x05){
	  Updata_state=0;
		rdata[0]=0x80;//
		rdata[1]=0x06;
		rdata[2]=0x47;//校验码
		rdata[3]=0x47;
		rdata[4]=0x4b;
		rdata[5]=0x4a;
		rdata[6]=1;//数据长度
		rdata[7]=6;//操作成功
		for(int i=0;i<8;i++)
		{
			rdata[8]+=rdata[i];
		}
		Com_Des.outputString(rdata,9);//返回结果  
	}
	else 
	{
		Updata_state=0;
		rdata[0]=0x80;//
		rdata[1]=0x06;
		rdata[2]=0x47;//校验码
		rdata[3]=0x47;
		rdata[4]=0x4b;
		rdata[5]=0x4a;
		rdata[6]=1;//数据长度
		rdata[7]=0xff;//操作成功
		for(int i=0;i<8;i++)
		{
			rdata[8]+=rdata[i];
		}
		Com_Des.outputString(rdata,9);//返回结果  
	}
	return OK_CMD;

}




