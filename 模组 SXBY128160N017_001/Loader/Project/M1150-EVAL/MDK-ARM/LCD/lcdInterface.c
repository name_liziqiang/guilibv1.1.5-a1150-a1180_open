#include "lcdInterface.h"
#include "m1130_gpio.h"
#include "resLoad.h"
#include "lcdDriver.h"
#include "QspiDMA.h"
#include <stdio.h>
#include <string.h>


#define  LCD_SENDBUF_BYTEMAX         508 //DMA最大传输 为511 块   这个值小于511 并且 小于LCD_SENDBUF_HALFWORD_MAX*2
#define  LCD_SENDBUF_HALFWORD_MAX    320 //大小 应该和LCD的宽度保持一致；便于驱动利用行扫描
static __align(4) uint16_t  lcdSendBuff[2][LCD_SENDBUF_HALFWORD_MAX];
/*
*********************************************************************************************************************
*函数功能：TransferData_FlashToLcd  将指定位置的数据 从flash搬运到LCD
*入口参数：flashAddr       flash数据地址
*         transferCount   需要传输的字节数 以Byte为单位
*出口参数：无
*********************************************************************************************************************
*/
static int TransferData_FlashToLcd(uint32_t flashAddr, uint32_t transferCount)
{
	uint32_t operationCount = 0;
	uint32_t readBytes = 0;
	DATA_QSPI->CTRL0_SET = QSPI_CTRL0_LOCK_CS;
	flash_ConfigReadFlashMsg(DATA_QSPI, FLASH_QSPIDMA_WRITE_CHANNEL, flashAddr);//配置flash
	if(transferCount >= LCD_SENDBUF_BYTEMAX)readBytes = LCD_SENDBUF_BYTEMAX;
	else                            				readBytes = transferCount;
	QSPIDMA_ReadBytes(DATA_QSPI, FLASH_QSPIDMA_READ_CHANNEL, (uint8_t *)(&lcdSendBuff[operationCount][0]),readBytes);
	transferCount -= readBytes;
	while(1){
			QSPIDMA_WaitEnd(DATA_QSPI , FLASH_QSPIDMA_READ_CHANNEL);//等待 DMA 传输完成
			operationCount++;
			if(operationCount>1)operationCount=0;
			QSPIDMA_ReadBytes(DATA_QSPI, FLASH_QSPIDMA_READ_CHANNEL, (uint8_t *)(&lcdSendBuff[operationCount][0]),    readBytes);
			QSPIDMA_SendBytes(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL  , (uint8_t *)(&lcdSendBuff[1-operationCount][0]) , readBytes);
			QSPIDMA_WaitEnd(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL);
			if(transferCount == 0)break;
			else{
				if(transferCount >= LCD_SENDBUF_BYTEMAX){
					readBytes = LCD_SENDBUF_BYTEMAX;
					transferCount -= readBytes;
				}
				else{
					readBytes = transferCount;
					transferCount=0;
				}
			}
	}
	QSPIDMA_WaitEnd(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL);//等待最后一批数据传输完成
	QSPIDMA_WaitEnd(DATA_QSPI , FLASH_QSPIDMA_READ_CHANNEL);
	DATA_QSPI->CTRL0_CLR = QSPI_CTRL0_LOCK_CS;
	return 0;
}
/*
*********************************************************************************************************************
*函数功能：TransferData_SramToLcd 将指定位置的数据 从SRAM搬运到LCD
*入口参数：flashAddr       flash数据地址
*         transferCount   需要传输的字节数 以Byte为单位
*出口参数：无
*********************************************************************************************************************
*/
static int TransferData_SramToLcd(const uint8_t *dataAddr, uint32_t transferCount)
{
	uint8_t *data = (uint8_t *)dataAddr;
	uint32_t sendBytes = 0;
	while(1){
		if(transferCount == 0)break;
		if(transferCount >= LCD_SENDBUF_BYTEMAX)sendBytes = LCD_SENDBUF_BYTEMAX;
		else                                    sendBytes = transferCount;
		QSPIDMA_SendBytes(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL , data , sendBytes);
		QSPIDMA_WaitEnd(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL);
		transferCount -= sendBytes;
		data += sendBytes;
	}
	return 0;
}
/*
*********************************************************************************************************************
*函数功能：LCD_Fill_UseOneData 将一个数据写N次到LCD显存
*          常用于清屏或者区域填充。开窗不由本函数完成，由调用函数完成，本函数只负责写数据
*入口参数：sPos       起点坐标
*          ePos       终点坐标
*          Data       写入的数据
*          WriteCount 写入的次数
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_Fill_UseOneData(LCD_POSITION sPos , LCD_POSITION ePos , uint16_t Data , uint32_t WriteCount)
{
	Lcd_Driver.setcursor(sPos.x,sPos.y,ePos.x,ePos.y);
	Lcd_Driver.writeCMD(0x2c);//发送写GRAM指令
	//初始化 待发送 缓存区，QSPI 数据流 SRAM->LCD  后面不停的刷这一片缓存区即可
	for(uint32_t i=0;i<LCD_SENDBUF_HALFWORD_MAX;i++){
		lcdSendBuff[0][i] = Data;
	}
	LCD_CS_D();
	//写入数据
	uint32_t sendBytes = 0;
	uint32_t writeTotal = WriteCount*2;//RGB565 一个点占用2字节，SPI需要发送的字节数是他的2倍
	while(1){
		if(writeTotal == 0)break;
		if(writeTotal >= LCD_SENDBUF_BYTEMAX)sendBytes = LCD_SENDBUF_BYTEMAX;
		else                                 sendBytes = writeTotal;
		QSPIDMA_SendBytes(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL , (uint8_t *)(&lcdSendBuff[0][0]) , sendBytes);
		QSPIDMA_WaitEnd(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL);
		writeTotal -= sendBytes;
	}
		printf("shuayanse:%02d",Data);

	LCD_CS_G();
}
/*
*********************************************************************************************************************
*函数功能：LCD_Fill_UseContinuousData 使用一块连续的数据的进行填充
*          常用于显示一张完整的图像
*入口参数：sPos       写入的LCD起点坐标
*          Data       写入的数据
*          dataWidth  数据宽度
*          dataHeight 数据高度
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_Fill_UseContinuousData(LCD_POSITION sPos,const uint16_t* Data,uint16_t dataWidth,uint16_t dataHeight)
{
	uint32_t WriteCount=dataWidth*dataHeight;
	Lcd_Driver.setcursor(sPos.x,sPos.y,sPos.x+dataWidth-1,sPos.y+dataHeight-1);
	Lcd_Driver.writeCMD(0x2c);//发送写GRAM指令
	LCD_CS_D();
	//开始读取flash数据 写入到LCD
	if(((uint32_t)Data) & 0x20000000){
		//数据在SRAM
		TransferData_SramToLcd((uint8_t *)Data, WriteCount<<1);
	}
	else{
		//数据在flash
		uint32_t flashAddr = (uint32_t)Data + READFLASH_ADDR_OFFSET;//XIP模式下得到的数据地址 需要还原成flash物理地址
		enter_XIPCritical();
		TransferData_FlashToLcd(flashAddr, WriteCount<<1);
		quit_XIPCritical();
	}
	LCD_CS_G();
}
/*
*********************************************************************************************************************
*函数功能：LCD_Fill_UsePartContinuousData  使用一块连续数据指定的一部分对LCD进行填充
*          常用于切图显示
*入口参数：DataBase     数据的基地址，注意它是连续数据块的起始地址，不是需要显示的区域的起始地址
*          WidthOffset  数据每一行的数据量（以uint16为基准进行的统计）
*          dispLoc      显示的地点位置，显示的宽度高度
*          dataPos      数据的起点位置
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_Fill_UsePartContinuousData(const uint16_t* DataBase,uint16_t WidthOffset,WIDGET_LOCATION dispLoc,LCD_POSITION dataPos)
{
	uint16_t (* Data)[WidthOffset];
	Data=(uint16_t(*)[WidthOffset])DataBase;//将一块连续的内存 转换成二位数组去访问
	WidthOffset = WidthOffset*2;//按Bytes算
	uint32_t flashAddr = (uint32_t)DataBase + READFLASH_ADDR_OFFSET;//XIP模式下得到的数据地址 需要还原成flash物理地址
	flashAddr += (dataPos.y*WidthOffset);
	flashAddr += (dataPos.x<<1);

	Lcd_Driver.setcursor(dispLoc.x,dispLoc.y,dispLoc.x+dispLoc.width-1,dispLoc.y+dispLoc.height-1);
	Lcd_Driver.writeCMD(0x2c);//发送写GRAM指令
	LCD_CS_D();
	uint32_t x = 0;
	uint32_t y = 0;
	//开始读取flash数据 写入到LCD
	if(((uint32_t)DataBase) & 0x20000000){
		//数据在SRAM
		for(y=1;y<dispLoc.height;y++){
			TransferData_SramToLcd((uint8_t *)(&Data[dataPos.y+y][dataPos.x]) , dispLoc.width*2);
		}
	}
	else{
		//数据在flash中不连续而且每行数据量并不大，使用DMA进行数据拷贝反而导致性能下降，故不采用DMA进行数据拷
		if(dispLoc.width <= LCD_SENDBUF_HALFWORD_MAX){
			for (x=0;x<dispLoc.width;x++)lcdSendBuff[0][x] = Data[dataPos.y+y][dataPos.x+x];//先把第一行的数据读取出来
			for(y=1;y<dispLoc.height;y++){
				QSPIDMA_WaitEnd(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL);
				QSPIDMA_SendBytes(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL , (uint8_t *)(&lcdSendBuff[1-(y%2)][0]) , dispLoc.width*2);
				for (x=0;x<dispLoc.width;x++)lcdSendBuff[y%2][x] = Data[dataPos.y+y][dataPos.x+x];//先把第一行的数据读取出来
			}
		}
		QSPIDMA_WaitEnd(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL);
		QSPIDMA_SendBytes(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL , (uint8_t *)(&lcdSendBuff[1-(y%2)][0]) , dispLoc.width*2);
		QSPIDMA_WaitEnd(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL);//把最后一次数据送出去
	}
	LCD_CS_G();
}
/*
*********************************************************************************************************************
*函数功能：GET_MaxFill_UseContinuousDataSplitBit 将一块连续的数据拆分从bit，得到每一行最大的需要填充的列数
*          常用于字符显示的时候获取最大宽度
*入口参数：DataBase         数据的基地址，注意它是连续数据块的起始地址，不是需要显示的区域的起始地址
*          CharWidthOffset  数据每一行的数据量（以uint16为基准进行的统计）
*出口参数：uint16_t 返回每一行 最后一个有效像素点的坐标，用于指示下一个字符的位置偏移
*********************************************************************************************************************
*/
static uint16_t GET_MaxFill_UseContinuousDataSplitBit(const uint8_t* DataBase,uint16_t widthOffset,uint16_t heightOffset)
{
	uint8_t (*charData)[widthOffset];
	charData=(uint8_t(*)[widthOffset])DataBase;//将一块连续的内存 转换成二位数组去访问
	uint16_t bytePos;
	uint16_t byteRemove;
	uint8_t  data;
	uint16_t maxWidth=0;
	for(int i=0;i<heightOffset;i++){
		for(int j=0;j<widthOffset*8;j++){
			bytePos=j/8;//该bit的数据处于那个字节
			byteRemove=7-j%8;//数据需要移动的位数
			data=charData[i][bytePos];
			data>>=byteRemove;
			data&=0x01;
			if(data){
				if(maxWidth<j)maxWidth=j;
			}
		}
	}
	return maxWidth;
}

/*
*********************************************************************************************************************
*函数功能：LCD_Fill_UseContinuousDataSplitBit 将一块连续的数据拆分从bit，分别以背景色或者前景色对LCD进行填充
*          常用于字符的显示
*入口参数：DataBase         数据的基地址，注意它是连续数据块的起始地址，不是需要显示的区域的起始地址
*          CharWidthOffset  数据每一行的数据量（以uint16为基准进行的统计）
*          dispLoc          显示的起点位置和高度宽度
*出口参数：uint16_t 返回每一行 最后一个有效像素点的坐标，用于指示下一个字符的位置偏移
*********************************************************************************************************************
*/
static uint16_t LCD_Fill_UseContinuousDataSplitBit(const uint8_t* DataBase,uint16_t CharWidthOffset,WIDGET_LOCATION dispLoc)
{
	Lcd_Driver.setcursor(dispLoc.x,dispLoc.y,dispLoc.x+dispLoc.width-1,dispLoc.y+dispLoc.height-1);
	Lcd_Driver.writeCMD(0x2c);//发送写GRAM指令
	uint8_t (*charData)[CharWidthOffset];
	charData=(uint8_t(*)[CharWidthOffset])DataBase;//将一块连续的内存 转换成二位数组去访问
	uint16_t WriteData[2];
	uint16_t bytePos;
	uint16_t byteRemove;
	uint8_t  data;
	WriteData[0]=Lcd_Driver.para->backcolor;
	WriteData[1]=Lcd_Driver.para->fontcolor;
	LCD_CS_D();	
	uint16_t maxWidth=0;
	//待优化
	// for (uint32_t i=0;i<dispLoc.height;i++){
	// 	if(dispLoc.width <= LCD_SENDBUF_HALFWORD_MAX){
	// 		for (uint32_t j=0;j<dispLoc.width;j++){
	// 			bytePos=j/8;//该bit的数据处于那个字节
	// 			byteRemove=7-j%8;//数据需要移动的位数
	// 			data=charData[i][bytePos];
	// 			data>>=byteRemove;
	// 			data&=0x01;
	// 			if(data){
	// 				if(maxWidth<j)maxWidth=j;
	// 			}
	// 			lcdSendBuff[0][j] = WriteData[data];
	// 		}
	// 		TransferData_SramToLcd((uint8_t *)(&lcdSendBuff[0][0]) , dispLoc.width*2);
	// 	}
	// }
	uint32_t y = 0;
	uint32_t x = 0;
	if(dispLoc.width <= LCD_SENDBUF_HALFWORD_MAX){
		for (x=0;x<dispLoc.width;x++){
			bytePos=x/8;//该bit的数据处于那个字节
			byteRemove=7-x%8;//数据需要移动的位数
			data=charData[y][bytePos];
			data>>=byteRemove;
			data&=0x01;
			if(data)if(maxWidth<x)maxWidth=x;
			lcdSendBuff[0][x] = WriteData[data];//先把第一行的数据读取出来
		}
		for(y=1;y<dispLoc.height;y++){
			QSPIDMA_WaitEnd(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL);
			QSPIDMA_SendBytes(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL , (uint8_t *)(&lcdSendBuff[1-(y%2)][0]) , dispLoc.width*2);
			for (x=0;x<dispLoc.width;x++){
				bytePos=x/8;//该bit的数据处于那个字节
				byteRemove=7-x%8;//数据需要移动的位数
				data=charData[y][bytePos];
				data>>=byteRemove;
				data&=0x01;
				if(data)if(maxWidth<x)maxWidth=x;
				lcdSendBuff[y%2][x] = WriteData[data];
			}
		}
	}
	QSPIDMA_WaitEnd(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL);
	QSPIDMA_SendBytes(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL , (uint8_t *)(&lcdSendBuff[1-(y%2)][0]) , dispLoc.width*2);
	QSPIDMA_WaitEnd(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL);//把最后一次数据送出去
	
	LCD_CS_G();
	return maxWidth;
}
/*
*********************************************************************************************************************
*函数功能：LCD_Fill_UseContinuousDataSplitBit_UseBackCutPic 将一块连续的数据拆分从bit，分别以图片背景和前景色对LCD进行填充
*          常用于透明字符字符的显示
*入口参数：DataBase         数据的基地址，注意它是连续数据块的起始地址，不是需要显示的区域的起始地址
*          CharWidthOffset  数据每一行的数据量（以uint16为基准进行的统计）
*          dispLoc          显示的起点位置和高度宽度
*          picId            图片ID
*出口参数：uint16_t 返回每一行 最后一个有效像素点的坐标，用于指示下一个字符的位置偏移
*********************************************************************************************************************
*/
static uint16_t LCD_Fill_UseContinuousDataSplitBit_UseBackCutPic(const uint8_t* DataBase,uint16_t CharWidthOffset,WIDGET_LOCATION dispLoc,uint16_t picId)
{
	uint8_t (*charData)[CharWidthOffset];
	charData=(uint8_t(*)[CharWidthOffset])DataBase;//将一块连续的内存 转换成二位数组去访问
	uint16_t WriteData[2];
	uint16_t bytePos;
	uint16_t byteRemove;
	uint8_t  data;
	uint16_t PicOffset=0;
	WriteData[0]=Lcd_Driver.para->backcolor;
	WriteData[1]=Lcd_Driver.para->fontcolor;
	Lcd_Driver.setcursor(dispLoc.x,dispLoc.y,dispLoc.x+dispLoc.width-1,dispLoc.y+dispLoc.height-1);
	Lcd_Driver.writeCMD(0x2c);//发送写GRAM指令
	//得到图片信息
	const uint16_t *picData=NULL;
	uint32_t addr=Res_GetPicture(picId);
	if(addr>FLASHRES_START_ADDR){
		//图片有效
		const uint8_t *imgx=(uint8_t *)(addr-READFLASH_ADDR_OFFSET);//给出的是实际物理地址，软件访问的时候需要-READFLASH_ADDR_OFFSET
		RES_PIC_MEG *imginfo=(RES_PIC_MEG*)imgx;//图片头部信息
		PicOffset=imginfo->width;
		picData = (uint16_t *)(imgx+sizeof(RES_PIC_MEG));
	}
	LCD_CS_D();	
	uint16_t maxWidth=0;
	uint32_t y = 0;
	uint32_t x = 0;
	if(dispLoc.width <= LCD_SENDBUF_HALFWORD_MAX){
		for (x=0;x<dispLoc.width;x++){
			if(picData!=NULL){
				WriteData[0]=picData[(dispLoc.y+y)*PicOffset+(dispLoc.x+x)];
			}
			bytePos=x/8;//该bit的数据处于那个字节
			byteRemove=7-x%8;//数据需要移动的位数
			data=charData[y][bytePos];
			data>>=byteRemove;
			data&=0x01;
			if(data)if(maxWidth<x)maxWidth=x;
			lcdSendBuff[0][x] = WriteData[data];//先把第一行的数据读取出来
		}
		for(y=1;y<dispLoc.height;y++){
			QSPIDMA_WaitEnd(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL);
			QSPIDMA_SendBytes(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL , (uint8_t *)(&lcdSendBuff[1-(y%2)][0]) , dispLoc.width*2);
			for (x=0;x<dispLoc.width;x++){
				if(picData!=NULL){
					WriteData[0]=picData[(dispLoc.y+y)*PicOffset+(dispLoc.x+x)];
				}
				bytePos=x/8;//该bit的数据处于那个字节
				byteRemove=7-x%8;//数据需要移动的位数
				data=charData[y][bytePos];
				data>>=byteRemove;
				data&=0x01;
				if(data)if(maxWidth<x)maxWidth=x;
				lcdSendBuff[y%2][x] = WriteData[data];
			}
		}
	}
	QSPIDMA_WaitEnd(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL);
	QSPIDMA_SendBytes(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL , (uint8_t *)(&lcdSendBuff[1-(y%2)][0]) , dispLoc.width*2);
	QSPIDMA_WaitEnd(LCD_QSPI , LCD_QSPIDMA_SEND_CHANNEL);//把最后一次数据送出去
	LCD_CS_G();
	return maxWidth;
}
/*
*********************************************************************************************************************
*函数功能：LCD_DrawPoint，LCD画一个点，使用LCD描述符前景色
*入口参数：x,y:坐标
*出口参数：无
*********************************************************************************************************************
*/ 
static void LCD_DrawPoint(uint16_t x,uint16_t y)
{
	LCD_POSITION sPos={x,y};
	LCD_POSITION ePos={x,y};
	LCD_Fill_UseOneData(sPos,ePos,Lcd_Driver.para->fontcolor,1);	
}
/*
*********************************************************************************************************************
*函数功能：LCD_ReadPoint，LCD读一个点
*入口参数：x,y:坐标
*出口参数：uint32_t，该点的颜色RGB值
*********************************************************************************************************************
*/
static uint32_t LCD_ReadPoint(uint16_t x,uint16_t y)
{
	uint16_t t=0;
	return t;
}

/*
*********************************************************************************************************************
*函数功能：LCD_Clear，lcd清屏显示，单色，LCD描述符的背景色，全屏
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_Clear(void)
{ 
	uint32_t totalpoint=Lcd_Driver.para->width;
	totalpoint*=Lcd_Driver.para->height; 			  //得到总点数
	LCD_POSITION sPos={0,0};
	LCD_POSITION ePos={Lcd_Driver.para->width-1,Lcd_Driver.para->height-1};
	LCD_Fill_UseOneData(sPos,ePos,Lcd_Driver.para->backcolor,totalpoint);
}
/*
*********************************************************************************************************************
*函数功能：LCD_Fill，使用LCD描述符的前景色填充LCD指定的一部分
*入口参数：sx sy起点坐标
*          ex ey终点坐标
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_Fill(uint16_t sx,uint16_t sy,uint16_t ex,uint16_t ey)
{          
	uint32_t xlen=ex-sx+1;
	uint32_t ylen=ey-sy+1; 
	LCD_POSITION sPos={sx,sy};
	LCD_POSITION ePos={ex,ey};
	LCD_Fill_UseOneData(sPos,ePos,Lcd_Driver.para->fontcolor,xlen*ylen);
}

const LCD_DESCRIPTOR Lcd_Des={
	&lcdPara,
	&FontLib_ASCII_1616,
	LCD_DrawPoint,
	LCD_ReadPoint,
	LCD_Clear,
	LCD_Fill,
	LCD_Fill_UseOneData,
	LCD_Fill_UseContinuousData,
	LCD_Fill_UsePartContinuousData,
	GET_MaxFill_UseContinuousDataSplitBit,
	LCD_Fill_UseContinuousDataSplitBit,
	LCD_Fill_UseContinuousDataSplitBit_UseBackCutPic
};



