#ifndef _mallocInterface_H
#define _mallocInterface_H
#include "typedefine.h"
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：内存管理接口文件
 *实现功能：
 *
 *撰 写 人：Alpscale LCD Application development team
 *撰写时间：2019-7-8
 *测 试 人：Alpscale LCD Application development team
 *测试时间：2019-7-8
 *版 本 号：V1.01_2019-7-8
 *版本说明：
 *                              
 *          
 *
 *V1.01：   
 *
 *V1.02：
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef struct _MALLOC_DESCRIPTOR{
	void *(*malloc)(uint32_t len);
	void  (*free)(void *ptr);
	void *(*realloc)(void *ptr , uint32_t len);
	void *(*memset)(void *ptr , int c , uint32_t len);
	void *(*memcpy)(void *des , const void *src , uint32_t len);
}MALLOC_DESCRIPTOR;

extern const MALLOC_DESCRIPTOR Malloc_Des;

#endif
