#ifndef _command_h
#define _command_h
#include "typedefine.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：系统指令集（信息交互时，指令系统的实现）
 *实现功能：1、指令集定义
 *          2、定义指令集索引表
 *          3、实现systemuart的消息循环
 *
 *撰 写 人：Alpscale LCD Application development team
 *撰写时间：2019-7-2
 *测 试 人：Alpscale LCD Application development team
 *测试时间：2019-7-2
 *版 本 号：V1.01_2019-7-2
 *版本说明：
 *                              
 *          指令集          指令集索引表        实现systemuart的消息循环
 *
 *V1.01：   实现            实现                实现
 *
 *V1.02：
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
 *所有函数实体均依照，下面的函数原型
 *函数出口参数，uint32_t，不需要函数返回值的请返回0
 *函数入口参数，result存放处理结果指针，类型为void，具体类型请各函数实体强制类型转换
 *函数入口参数，charc 处理对象指针，类型为void，具体类型请各函数实体强制类型转换
 *函数入口参数，c_len 处理对象长度，类型为uint32_t，
 *函数入口参数，不需要的，指针类型以空指针补齐，长度以0补齐
 */
 
typedef enum
{
  NO_CMD=(int)-3,      //不支持的命令
  NULL_CMD=(int)-2,    //空命令
	FAILED_CMD=(int)-1,  //命令执行失败
	OK_CMD               //正常执行命令，其他>=0的值，表示执行结果需要回复
}CMD_State;

typedef CMD_State (*FUNC)(void *result,const void *charc,uint32_t c_len);

#define FUNCSTRNAME_MAXLEN   16
typedef struct __FUNCSTR
{
	char Name[FUNCSTRNAME_MAXLEN];//函数名称
	FUNC Func;//函数指针
}FUNCSTR;

#define COMMAND_MAXCOUNT     100//指令集暂时定100个指令
extern const FUNCSTR SystemCommand_Tab[COMMAND_MAXCOUNT];
void Communication_Loop(void);//SystemUart消息循环
void Checkcomupdata(void);

#endif
