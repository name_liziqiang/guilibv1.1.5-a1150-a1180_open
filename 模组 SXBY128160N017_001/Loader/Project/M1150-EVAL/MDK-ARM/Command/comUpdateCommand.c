#include "command.h"
#include "comInterface.h"
#include "mallocInterface.h"
#include "memoryInterface.h"
#include "lcdInterface.h"
#include "drawChar.h"
#include "stdio.h"


#define DISP_DOWNLOAD_TIPS         1

#define WRITEPAGE_MAX    256
static __align(4) uint8_t FlashWriteBuff[WRITEPAGE_MAX];//4字节对齐的缓存区，便于flash数据的写入


CMD_State COMMAND_writeFlash(void *result,const void *charc,uint32_t c_len)
{
	/////////////////////////////////////////////////////////////////////////////////
	//写flash数据
	uint8_t *rdata=(uint8_t *)result;
	uint8_t *data=(uint8_t *)charc;
	if(*(data+0)!=0x47)return FAILED_CMD;
	if(*(data+1)!=0x47)return FAILED_CMD;
	if(*(data+2)!=0x4b)return FAILED_CMD;
	if(*(data+3)!=0x4a)return FAILED_CMD;//校验不过
	uint32_t addr=0;//写入的flash地址
	addr|=(*(data+4)<<24);
	addr|=(*(data+5)<<16);
	addr|=(*(data+6)<<8);
	addr|=(*(data+7)<<0);
	uint8_t count=(*(data+8));
#if  USE_PRINTF_DEBUG
	//printf("COMMAND_writeFlash write addr:%d,count: %d\r\n",addr,count);
#endif	
	for(int i=0;i<count;i++){
		FlashWriteBuff[i]=*(data+9+i);//将数据拷贝到一个4字节对齐的缓存区
	}
	Memory_Des.write(FlashWriteBuff,addr,count);//这个函数位于 m1130_xip_write.c中，被连接到flash前8K
#if DISP_DOWNLOAD_TIPS
	if(count<240){
		char downLoadTips[50];
		Malloc_Des.memset(downLoadTips,0,50);
		sprintf(downLoadTips,"DownLoad Flash Done\rPlease Reset Equipment...");
		gui_dispCharUseSystemFontLib(0,48,downLoadTips);
	}
#endif
	rdata[0]=0x80;//功能码0003响应
	rdata[1]=0x03;
	rdata[2]=0x47;//校验码
	rdata[3]=0x47;
	rdata[4]=0x4b;
	rdata[5]=0x4a;
	rdata[6]=6;//数据长度
	rdata[7]=1;//操作成功
	Malloc_Des.memcpy(rdata+8,data+4,5);
	rdata[13]=0;
	for(int i=0;i<13;i++)
	{
		rdata[13]+=rdata[i];
	}
	Com_Des.outputString(rdata,14);//返回结果
	return OK_CMD;
}

CMD_State COMMAND_eraseFlash(void *result,const void *charc,uint32_t c_len)
{
	/////////////////////////////////////////////////////////////////////////////////
	//擦除flash扇区
	uint8_t *data=(uint8_t *)charc;
	if(*(data+0)!=0x47)return FAILED_CMD;
	if(*(data+1)!=0x47)return FAILED_CMD;
	if(*(data+2)!=0x4b)return FAILED_CMD;
	if(*(data+3)!=0x4a)return FAILED_CMD;//校验不过
	uint32_t start_Sector=0;//擦除的起始扇区
	start_Sector|=(*(data+4)<<24);
	start_Sector|=(*(data+5)<<16);
	start_Sector|=(*(data+6)<<8);
	start_Sector|=(*(data+7)<<0);
	uint32_t count_Sector=0;//擦除的扇区个数
	count_Sector|=(*(data+8)<<24);
	count_Sector|=(*(data+9)<<16);
	count_Sector|=(*(data+10)<<8);
	count_Sector|=(*(data+11)<<0);
	
	ClearList(&ResList);
	ClearList(&CurrentPageList);
	//printf("COMMAND_eraseFlash sector start:%d,count: %d\r\n",start_Sector,count_Sector);
#if DISP_DOWNLOAD_TIPS
	//开始擦除扇区
	Lcd_Des.para->backcolor=BLACK;
	Lcd_Des.para->fontcolor=RED;
	Lcd_Des.clear();
	char downLoadTips[50];
	Malloc_Des.memset(downLoadTips,0,50);
	sprintf(downLoadTips,"Erase Sector:%06d,count:%04d",start_Sector,count_Sector);
	gui_dispCharUseSystemFontLib(0,0,downLoadTips);
#endif
	Memory_Des.erase(start_Sector*4096,count_Sector);//这个函数位于 m1130_xip_write.c中，被连接到flash前8K
#if DISP_DOWNLOAD_TIPS
	Malloc_Des.memset(downLoadTips,0,50);
	sprintf(downLoadTips,"Erase Flash Done...");
	gui_dispCharUseSystemFontLib(0,16,downLoadTips);
	Malloc_Des.memset(downLoadTips,0,50);
	sprintf(downLoadTips,"Resource DownLoading......");
	gui_dispCharUseSystemFontLib(0,32,downLoadTips);
#endif
	uint8_t *rdata=(uint8_t *)result;
	rdata[0]=0x80;//功能码0004响应
	rdata[1]=0x04;
	rdata[2]=0x47;//校验码
	rdata[3]=0x47;
	rdata[4]=0x4b;
	rdata[5]=0x4a;
	rdata[6]=9;//数据长度
	rdata[7]=1;//操作成功
	Malloc_Des.memcpy(rdata+8,data+4,8);
	rdata[16]=0;
	for(int i=0;i<16;i++)
	{
		rdata[16]+=rdata[i];
	}
	Com_Des.outputString(rdata,17);//返回结果
	return OK_CMD;
}

#include "mmc_sd.h"
#include "ff.h"
FATFS SD_Fatfs;
FIL   ResourceFile;
void CheckExternMemoryAndUpadeResource(void)
{
	uint32_t readBytes; //读取文件字节数
	uint32_t resBytes;	//文件字节数
	uint32_t writeAddr;
	uint32_t filePointer;
	uint32_t start_Sector;
	uint32_t count_Sector;//擦除flash的起始扇区和扇区个数
	FRESULT res=f_mount(&SD_Fatfs,"0:",1);//挂在SD 1挂载 0移除
	if(res==FR_OK){
		//挂载SD卡成功 
		printf("mount sd ok\r\n");
		res = f_open(&ResourceFile,"0:/ResGui.bin",FA_READ);//尝试读取资源文件 固定文件名 GuiRes.bin
		if(res==FR_OK){
			//发现一个新的资源文件
			resBytes=(uint32_t)ResourceFile.fsize;//得到文件大小
			//开始擦除扇区
			writeAddr=FLASHRES_START_ADDR;
			start_Sector = writeAddr/4096;
			count_Sector = resBytes/4096;
			if(resBytes%4096)count_Sector++;
#if DISP_DOWNLOAD_TIPS
			ClearList(&ResList);
			ClearList(&CurrentPageList);//清空资源列表，清空页面列表
			Lcd_Des.para->backcolor=BLACK;
			Lcd_Des.para->fontcolor=RED;
			Lcd_Des.clear();
			char downLoadTips[50];
			Malloc_Des.memset(downLoadTips,0,50);
			sprintf(downLoadTips,"Erase Sector:%06d,count:%04d",start_Sector,count_Sector);
			gui_dispCharUseSystemFontLib(0,0,downLoadTips);
#endif
			Memory_Des.erase(start_Sector*4096,count_Sector);//这个函数位于 m1130_xip_write.c中，被连接到flash前8K
#if DISP_DOWNLOAD_TIPS
			Malloc_Des.memset(downLoadTips,0,50);
			sprintf(downLoadTips,"Erase Flash Done...");
			gui_dispCharUseSystemFontLib(0,16,downLoadTips);
			Malloc_Des.memset(downLoadTips,0,50);
			sprintf(downLoadTips,"Resource DownLoading......");
			gui_dispCharUseSystemFontLib(0,32,downLoadTips);
#endif
			//开始写入数据
			filePointer=0;
			while(1){
				f_read(&ResourceFile,FlashWriteBuff,WRITEPAGE_MAX,&readBytes);
				if(readBytes!=0){
					Memory_Des.write(FlashWriteBuff,writeAddr,readBytes);//这个函数位于 m1130_xip_write.c中，被连接到flash前8K
					writeAddr+=readBytes;
					filePointer+=readBytes;
					f_lseek(&ResourceFile,filePointer);//移动文件指针
				}
				else{
#if DISP_DOWNLOAD_TIPS
					Malloc_Des.memset(downLoadTips,0,50);
					sprintf(downLoadTips,"DownLoad Flash Done\rPlease remove sd card \rReset Equipment...");
					gui_dispCharUseSystemFontLib(0,48,downLoadTips);
#endif
					break;
				}
			}
			f_close(&ResourceFile);
			f_mount(&SD_Fatfs,"0:",0);
			while(1);
		}
		f_mount(&SD_Fatfs,"0:",0);
	}
	else printf("mount sd error\r\n");
}
