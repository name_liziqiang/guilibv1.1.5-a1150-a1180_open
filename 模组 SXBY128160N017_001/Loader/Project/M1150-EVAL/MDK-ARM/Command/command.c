#include "command.h"
#include <stdio.h>
#include <string.h>
#include "mallocInterface.h"
#include "comInterface.h"

static CMD_State command_Handle(uint16_t cmd,void *result,const void *charc,uint32_t c_len);
extern void delay_ms(uint16_t ms);
uint8_t Updata_state;

void Communication_Loop(void)
{
	uint8_t *receiveBuff=Com_Des.getInputBuff();
	uint8_t *resultBuff =Com_Des.getOutputBuff();

	//询问是否接收完成
	if(Com_Des.getInputStatus()==COM_RECEIVED_NO)return;
	//得到接收到长度
	uint32_t receiveLen=Com_Des.getInputLen();
	//开始处理接收到的数据
	uint8_t checksum=0;
	uint16_t cmd;
	for(int i=0;i<receiveLen-1;i++)
	{
		checksum+=receiveBuff[i];
	}
	if(checksum==receiveBuff[receiveLen-1])
	{
		cmd=receiveBuff[0];
		cmd<<=8;
		cmd+=receiveBuff[1];//得到命令参数
		command_Handle(cmd,&resultBuff[0],&receiveBuff[2],receiveLen-3);
	}
	Com_Des.clearInputBuff();
}

static CMD_State command_Handle(uint16_t cmd,void *result,const void *charc,uint32_t c_len)
{
	if(cmd>=COMMAND_MAXCOUNT){
		//printf("no cmd\r\n");
		return NO_CMD;
	}
	return SystemCommand_Tab[cmd].Func(result,charc,c_len);
}

static CMD_State COMMAND_NULL_HANDLE(void *result,const void *charc,uint32_t c_len)
{
	//printf("null cmd\r\n");
	return NULL_CMD;
}

void Checkcomupdata(void)
{
	uint8_t *rdata;
	rdata[0]=0x80;//功能码0004响应
	rdata[1]=0x06;
	rdata[2]=0x47;//校验码
	rdata[3]=0x47;
	rdata[4]=0x4b;
	rdata[5]=0x4a;
	rdata[6]=1;//数据长度
	rdata[7]=0x02;//连接成功
	rdata[8]=0;
	for(int i=0;i<8;i++)
	{
		rdata[8]+=rdata[i];
	}
	Com_Des.outputString(rdata,9);//返回结果
	delay_ms(1000);

	uint8_t *receiveBuff=Com_Des.getInputBuff();
	uint8_t *resultBuff =Com_Des.getOutputBuff();

	//询问是否接收完成
	if(Com_Des.getInputStatus()==COM_RECEIVED_NO)return;
	//得到接收到长度
	uint32_t receiveLen=Com_Des.getInputLen();
	//开始处理接收到的数据
	uint8_t checksum=0;
	uint16_t cmd;
	for(int i=0;i<receiveLen-1;i++)
	{
		checksum+=receiveBuff[i];
	}
	if(checksum==receiveBuff[receiveLen-1])
	{
		
		cmd=receiveBuff[0];
		cmd<<=8;
		cmd+=receiveBuff[1];//得到命令参数
		command_Handle(cmd,&resultBuff[0],&receiveBuff[2],receiveLen-3);
//		Updata_state=1;
		Com_Des.clearInputBuff();
     while(1)
		 {
			 if(Updata_state==1){
				 	Communication_Loop();
			 }
	     else{
					break;
			}
		 }
	}
	
}

//系统指令集索引表
extern CMD_State COMMAND_readCOM(void *result,const void *charc,uint32_t c_len);
extern CMD_State COMMAND_writeCOM(void *result,const void *charc,uint32_t c_len);
extern CMD_State COMMAND_writeFlash(void *result,const void *charc,uint32_t c_len);
extern CMD_State COMMAND_eraseFlash(void *result,const void *charc,uint32_t c_len);
extern CMD_State COMMAND_askconnect(void *result,const void *charc,uint32_t c_len);

const FUNCSTR SystemCommand_Tab[COMMAND_MAXCOUNT]=
{
	{"NULL",COMMAND_NULL_HANDLE},                        //0
	{"readCOM",COMMAND_readCOM},                         //1
	{"writeCOM",COMMAND_writeCOM},                       //2
	{"writeFlash",COMMAND_writeFlash},                   //3
	{"eraseFlash",COMMAND_eraseFlash},                   //4
#if USE_LUA
	{"runLua",COMMAND_runLua},                           //5
#else
	{"NULL",COMMAND_NULL_HANDLE},                        //5
#endif
	{"askconnect",COMMAND_askconnect},            //6
	{"NULL",COMMAND_NULL_HANDLE},            //7
	{"NULL",COMMAND_NULL_HANDLE},            //8
	{"NULL",COMMAND_NULL_HANDLE},            //9
	{"NULL",COMMAND_NULL_HANDLE},            //10
	{"NULL",COMMAND_NULL_HANDLE},            //11
	{"NULL",COMMAND_NULL_HANDLE},            //12
	{"NULL",COMMAND_NULL_HANDLE},            //13
	{"NULL",COMMAND_NULL_HANDLE},            //14
	{"NULL",COMMAND_NULL_HANDLE},            //15
	{"NULL",COMMAND_NULL_HANDLE},            //16
	{"NULL",COMMAND_NULL_HANDLE},            //17
	{"NULL",COMMAND_NULL_HANDLE},            //18
	{"NULL",COMMAND_NULL_HANDLE},            //19
	{"NULL",COMMAND_NULL_HANDLE},            //20
	{"NULL",COMMAND_NULL_HANDLE},            //21
	{"NULL",COMMAND_NULL_HANDLE},            //22
	{"NULL",COMMAND_NULL_HANDLE},            //23
	{"NULL",COMMAND_NULL_HANDLE},            //24
	{"NULL",COMMAND_NULL_HANDLE},            //25
	{"NULL",COMMAND_NULL_HANDLE},            //26
	{"NULL",COMMAND_NULL_HANDLE},            //27
	{"NULL",COMMAND_NULL_HANDLE},            //28
	{"NULL",COMMAND_NULL_HANDLE},            //29
	{"NULL",COMMAND_NULL_HANDLE},            //30
	{"NULL",COMMAND_NULL_HANDLE},            //31
	{"NULL",COMMAND_NULL_HANDLE},            //32
	{"NULL",COMMAND_NULL_HANDLE},            //33
	{"NULL",COMMAND_NULL_HANDLE},            //34
	{"NULL",COMMAND_NULL_HANDLE},            //35
	{"NULL",COMMAND_NULL_HANDLE},            //36
	{"NULL",COMMAND_NULL_HANDLE},            //37
	{"NULL",COMMAND_NULL_HANDLE},            //38
	{"NULL",COMMAND_NULL_HANDLE},            //39
	{"NULL",COMMAND_NULL_HANDLE},            //40
	{"NULL",COMMAND_NULL_HANDLE},            //41
	{"NULL",COMMAND_NULL_HANDLE},            //42
	{"NULL",COMMAND_NULL_HANDLE},            //43
	{"NULL",COMMAND_NULL_HANDLE},            //44
	{"NULL",COMMAND_NULL_HANDLE},            //45
	{"NULL",COMMAND_NULL_HANDLE},            //46
	{"NULL",COMMAND_NULL_HANDLE},            //47
	{"NULL",COMMAND_NULL_HANDLE},            //48
	{"NULL",COMMAND_NULL_HANDLE},            //49
	{"NULL",COMMAND_NULL_HANDLE},            //50
	{"NULL",COMMAND_NULL_HANDLE},            //51
	{"NULL",COMMAND_NULL_HANDLE},            //52
	{"NULL",COMMAND_NULL_HANDLE},            //53
	{"NULL",COMMAND_NULL_HANDLE},            //54
	{"NULL",COMMAND_NULL_HANDLE},            //55
	{"NULL",COMMAND_NULL_HANDLE},            //56
	{"NULL",COMMAND_NULL_HANDLE},            //57
	{"NULL",COMMAND_NULL_HANDLE},            //58
	{"NULL",COMMAND_NULL_HANDLE},            //59
	{"NULL",COMMAND_NULL_HANDLE},            //60
	{"NULL",COMMAND_NULL_HANDLE},            //61
	{"NULL",COMMAND_NULL_HANDLE},            //62
	{"NULL",COMMAND_NULL_HANDLE},            //63
	{"NULL",COMMAND_NULL_HANDLE},            //64
	{"NULL",COMMAND_NULL_HANDLE},            //65
	{"NULL",COMMAND_NULL_HANDLE},            //66
	{"NULL",COMMAND_NULL_HANDLE},            //67
	{"NULL",COMMAND_NULL_HANDLE},            //68
	{"NULL",COMMAND_NULL_HANDLE},            //69
	{"NULL",COMMAND_NULL_HANDLE},            //70
	{"NULL",COMMAND_NULL_HANDLE},            //71
	{"NULL",COMMAND_NULL_HANDLE},            //72
	{"NULL",COMMAND_NULL_HANDLE},            //73
	{"NULL",COMMAND_NULL_HANDLE},            //74
	{"NULL",COMMAND_NULL_HANDLE},            //75
	{"NULL",COMMAND_NULL_HANDLE},            //76
	{"NULL",COMMAND_NULL_HANDLE},            //77
	{"NULL",COMMAND_NULL_HANDLE},            //78
	{"NULL",COMMAND_NULL_HANDLE},            //79
	{"NULL",COMMAND_NULL_HANDLE},            //80
	{"NULL",COMMAND_NULL_HANDLE},            //81
	{"NULL",COMMAND_NULL_HANDLE},            //82
	{"NULL",COMMAND_NULL_HANDLE},            //83
	{"NULL",COMMAND_NULL_HANDLE},            //84
	{"NULL",COMMAND_NULL_HANDLE},            //85
	{"NULL",COMMAND_NULL_HANDLE},            //86
	{"NULL",COMMAND_NULL_HANDLE},            //87
	{"NULL",COMMAND_NULL_HANDLE},            //88
	{"NULL",COMMAND_NULL_HANDLE},            //89
	{"NULL",COMMAND_NULL_HANDLE},            //90
	{"NULL",COMMAND_NULL_HANDLE},            //91
	{"NULL",COMMAND_NULL_HANDLE},            //92
	{"NULL",COMMAND_NULL_HANDLE},            //93
	{"NULL",COMMAND_NULL_HANDLE},            //94
	{"NULL",COMMAND_NULL_HANDLE},            //95
	{"NULL",COMMAND_NULL_HANDLE},            //96
	{"NULL",COMMAND_NULL_HANDLE},            //97
	{"NULL",COMMAND_NULL_HANDLE},            //98
	{"NULL",COMMAND_NULL_HANDLE}             //99
};
