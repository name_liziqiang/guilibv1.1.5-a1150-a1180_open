#ifndef _gWidgetInfo_h
#define _gWidgetInfo_h
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：gui 控件相关信息定义
 *实现功能：
 *
 *撰 写 人：Alpscale LCD Application development team
 *撰写时间：2019-7-22
 *测 试 人：Alpscale LCD Application development team
 *测试时间：2019-7-22
 *版 本 号：V1.01_2019-7-22
 *版本说明：
 *                              
 *          函数清单
 *
 *V1.01：   功能实现
 *
 *V1.02：
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "gStdint.h"
#include "gReturn.h"

#ifdef __cplusplus
 extern "C" {
#endif

	 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  控件类型定义
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define WIDGET_TYPE_MASK               0x00ff//控件类型uint16_t wType中低八位表示控件类型，高八位表示控件基本属性，有没有loc等信息
                                             //0为一个无效控件，所有控件类型最大值255（1-255）
#define WIDGET_TYPE_HASLOC_MASK        0x8000//bit15,为1表示有控件位置信息，
                                             //0表示没有
#define WIDGET_TYPE_HASSLIDE_MASX      0x4000//bit14,为1表示该控件支持滑动，
                                             //0表示不支持
#define WIDGET_TYPE_HASTIMING_MAX      0x2000//bit13,为1表示该控件支持定时动作
																						 //0表示不支持
#define WIDGET_TYPE_NULL               0x0000//无效控件，用户遍历flash的时候指示一个界面已经提取完所有控件了
#define WIDGET_TYPE_PAGE               0x8001//PAGE页面控件，一个界面资源原则上都是以该控件开始的
#define WIDGET_TYPE_TEXTBOX            0x8002//文本显示框控件
#define WIDGET_TYPE_ROLLTEXTBOX        0xa003//滚动文本显示框控件
#define WIDGET_TYPE_NUMBERBOX          0x8004//数字显示框控件
#define WIDGET_TYPE_FLOATNUMBERBOX     0x8005//模拟的浮点数显示控件
#define WIDGET_TYPE_BUTTON             0x8006//按钮控件
#define WIDGET_TYPE_PROGRESSBAR        0x8007//进度条控件
#define WIDGET_TYPE_PICTUREBOX         0x8008//图片显示框控件
#define WIDGET_TYPE_CUTPICTURE         0x8009//切图显示控件
#define WIDGET_TYPE_TOUCHAREA          0x800a//触摸块控件，只处理了一些事情，不会显示任何东西出来
#define WIDGET_TYPE_POINTER            0x800b//仪表指针控件，只显示了一个指针，不会绘制仪表盘
#define WIDGET_TYPE_GRAPH              0x800c//曲线图控件
#define WIDGET_TYPE_SLIDER             0xC00d//滑动块控件
#define WIDGET_TYPE_GTIMER             0x200e//定时器控件，没有位置信息
#define WIDGET_TYPE_GVARIATE           0x000f//变量控件，没有位置信息
#define WIDGET_TYPE_BINARYBUTTON       0x8010//双态按钮控件
#define WIDGET_TYPE_OPTIONBOX          0x8011//多选框或者单选框控件
#define WIDGET_TYPE_QRCODEBOX          0x8012//二维码显示框

#define WIDGET_TYPE_MAX                WIDGET_TYPE_QRCODEBOX

#define ASSERT_WIDGET_TYPE(type) 			(((type)&(WIDGET_TYPE_MASK))>((WIDGET_TYPE_MAX)&(WIDGET_TYPE_MASK)))?(0):(1)//判断一个type是不是有效控件																																																					 //有效为1  无效为0
#define GET_WIDGET_TYPE(type)    			((type)&(WIDGET_TYPE_MASK))//得到一个控件类型 去掉type的高八位，得到低八位,常用于控件事件处理的时候快速查表定位
#define ADJUST_WIDGET_TYPE(type,needtype) 	(type)==(needtype)?(0):(1)//判断一个控件的类型是不是我需要的类型，是返回0，不是返回1，用于异常处理
#define ADJUST_WIDGET_NOLOC(type) 			(((type)&WIDGET_TYPE_HASLOC_MASK)?(0):(1))//判断一个控件是否含有位置信息，是返回0，不是返回1
#define ADJUST_WIDGET_NOSLIDE(type) 		(((type)&WIDGET_TYPE_HASSLIDE_MASX)?(0):(1))//判断一个控件是否支持滑动，是返回0，不是返回1
#define ADJUST_WIDGET_NOTIMING(type) 		(((type)&WIDGET_TYPE_HASTIMING_MAX)?(0):(1))//判断一个控件是否支持定时，是返回0，不是返回1

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *LCD坐标系 坐标信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _LCD_POSITION{
	gui_uint16 x;
	gui_uint16 y;
}LCD_POSITION;
typedef LCD_POSITION*  pLCD_POSITION;

__packed typedef struct _LCD_RECTANGLE{
	LCD_POSITION startPos;
	LCD_POSITION endPos;
}LCD_RECTANGLEN;
typedef LCD_RECTANGLEN*  pLCD_RECTANGLEN;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  窗体位置信息
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _WIDGET_LOCATION{
	//控件位置信息
  gui_uint16 x;                                //显示的起始坐标
	gui_uint16 y;                                //显示的起始坐标
  gui_uint16 width;                            //显示的宽度
	gui_uint16 height;                           //显示的高度
}WIDGET_LOCATION; 
typedef WIDGET_LOCATION*  pWIDGET_LOCATION;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  控件字符绘制所支持的 背景方式
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define WIDGET_BACKMODE_NULL                0//没有背景
#define WIDGET_BACKMODE_COLOR               1//显示一种颜色值
#define WIDGET_BACKMODE_PIC                 2//显示一个图片
#define WIDGET_BACKMODE_CUTPIC              3//切图，从一个全幅图片中切取出来一部分显示
#define IS_WIDGET_BACKMODE(x)               ((x>=WIDGET_BACKMODE_COLOR)&(x<=WIDGET_BACKMODE_CUTPIC))

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  一个控件的基本信息描述------所有控件都具有的信息，，处于控件描述信息的最开始
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define WIDGET_NAME_MAX_LEN                11//一个控件的名称最大字符串长度
__packed typedef struct _WIDGET_BASE_INFO{
	//控件基本类型定义，所有控件都应该具备的，而且是在控件描述符的最前面
  gui_uint16 wType;                            //控件类型
  gui_uint16 wId;                              //控件ID
	//gui_uint8 wName[WIDGET_NAME_MAX_LEN];        //控件名称
  gui_uint8  wVscope;                          //控件的私有属性
}WIDGET_BASE_INFO; 
typedef WIDGET_BASE_INFO*  pWIDGET_BASE_INFO;

#define WIDGET_VS_PRIVATE                  0x00//控件属性私有
#define WIDGET_VS_GLOBAL                   0x01//控件属性全局

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  控件事件所使用的原型以及其他内容
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef gui_Err (*WIDGET_EVENT)(gui_int32 argc , const char **argv);//所有控件的显示控件原型
//argc :传入进来的命令参数
//argv :传入进来的所有命令首地址，二维数据  ASCII

#define EVENT_REGISTER_TYPE_RELEASE         0x0001//指示注册一个控件松开事件
#define EVENT_REGISTER_TYPE_PRESS           0x0002//指示注册一个控件按下事件
#define EVENT_REGISTER_TYPE_TIMEOUT         0x0003//指示注册一个控件定时溢出事件
#define EVENT_REGISTER_TYPE_UPDATE          0x0004//指示注册一个控件更新事件
#define EVENT_REGISTER_TYPE_SLIDE           0x0005//指示注册一个控件滑动事件

#define EVENT_REGISTER_TYPE_ENTER           0x0101//指示注册一个进入界面事件
#define EVENT_REGISTER_TYPE_LEAVE           0x0102//指示注册一个离开界面事件

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  页面信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _PAGE_INFO{
	//page控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
  gui_uint8  backMode;                       //背景
	gui_color backColor;                       //背景颜色，当背景模式为显示一种颜色的时候调用
	gui_uint16 picId;                          //图片ID，当背景模式为显示图片的时候调用
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT enterEvent;                   //进入页面
	WIDGET_EVENT leaveEvent;                   //离开页面
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}PAGE_INFO; 
typedef PAGE_INFO*  pPAGE_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  双态按钮信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define BINARYBUTTON_STATE_DISENABLE        0//双态按钮，不使能状态
#define BINARYBUTTON_STATE_ENABLE           1//双态按钮，使能状态
__packed typedef struct _BINARYBUTTON_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8 backMode;                        //背景模式
	gui_uint16 fontlibId;                      //字库ID
	gui_uint8 borderWidth;                     //边框线宽，像素单位
	gui_color borderColor;                     //边框颜色
	gui_uint16 enablePicId;                    //使能的背景图片
	gui_uint16 disenablePicId;                 //不使能的背景图片
	gui_color enableBackColor;                 //使能的背景颜色
	gui_color disenableBackColor;              //不使能的背景颜色
	gui_color enableFontColor;                 //使能的前景颜色
	gui_color disenableFontColor;              //不使能的前景颜色
	gui_uint8  xAlign;                         //水平对齐方式
	gui_uint8  yAlign;                         //垂直对齐方式
	gui_uint8  isDr;                           //显示不下的时候，是否换行
	gui_uint8  value;                          //双态按钮，0松开状态，1按下状态
	char  *text;                               //文本显示的数据指针，创建的时候从flash拷贝过来，
	                                           //---上位机传下来的应该是用户HMI输入的文本长度，占4个字节，目的是为了和MCU保存结构体长度一致
	                                           //---用户输入的文本信息直接跟在TEXTBOX_INFO结构体后面，MCU会偏移sizeof(BUTTON_INFO)之后去读
	gui_uint16 maxLen;                         //文本框显示的最大长度，按字节算，也是MCU需要申请的内存长度，ASCII占一个字节，中文占两个字节
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}BINARYBUTTON_INFO;
typedef BINARYBUTTON_INFO*  pBINARYBUTTON_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  按钮信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _BUTTON_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8 backMode;                        //背景模式
	gui_uint16 fontlibId;                      //字库ID
	gui_uint8 borderWidth;                     //边框线宽，像素单位
	gui_color borderColor;                     //边框颜色
	gui_uint16 pressPicId;                     //按下的背景图片
	gui_uint16 releasePicId;                   //松开的背景图片
	gui_color pressBackColor;                  //按下的背景颜色
	gui_color releaseBackColor;                //松开的背景颜色
	gui_color pressFontColor;                  //按下的前景颜色
	gui_color releaseFontColor;                //松开的前景颜色
	gui_uint8  xAlign;                         //水平对齐方式
	gui_uint8  yAlign;                         //垂直对齐方式
	gui_uint8  isDr;                           //显示不下的时候，是否换行
	char  *text;                               //文本显示的数据指针，创建的时候从flash拷贝过来，
	                                           //---上位机传下来的应该是用户HMI输入的文本长度，占4个字节，目的是为了和MCU保存结构体长度一致
	                                           //---用户输入的文本信息直接跟在TEXTBOX_INFO结构体后面，MCU会偏移sizeof(BUTTON_INFO)之后去读
	gui_uint16 maxLen;                         //文本框显示的最大长度，按字节算，也是MCU需要申请的内存长度，ASCII占一个字节，中文占两个字节
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}BUTTON_INFO; 
typedef BUTTON_INFO*  pBUTTON_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  切图信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _CUTPICTURE_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint16 picId;                          //图片ID，当背景模式为显示图片的时候调用
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}CUTPICTURE_INFO; 
typedef CUTPICTURE_INFO*  pCUTPICTURE_INFO;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  虚拟浮点数信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _FLOATNUMBERBOX_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8 backMode;                        //背景模式
	gui_uint8 keyId;                           //输入键盘ID -1表示没有键盘
	gui_uint16 fontlibId;                      //字库ID
	gui_uint8 borderWidth;                     //边框线宽，像素单位
	gui_color borderColor;                     //边框颜色
	gui_uint16 picId;                          //背景图片ID
	gui_color backColor;                       //背景色
	gui_color frontColor;                       //前景色
	gui_uint8  xAlign;                         //水平对齐方式
	gui_uint8  yAlign;                         //垂直对齐方式
	gui_uint8  fdcount;                        //小数部分长度，maxlen-fdcount就是整数部分长度，
	gui_uint8  isDr;                           //显示不下的时候，是否换行
	gui_uint32 data;                           //显示的值
	gui_uint8 maxLen;                          //显示值的时候显示的最大长度，0自动，前端去0，其他按指定显示，从各位开始算
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}FLOATNUMBERBOX_INFO; 
typedef FLOATNUMBERBOX_INFO*  pFLOATNUMBERBOX_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  曲线框信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "guiConfig.h"

//#define GRAPH_USE_AXIS                       //使用坐标轴
#define GRAPH_SCAN_LR                       0//左到右推进
#define GRAPH_SCAN_RL                       1
#define GRAPH_XAXIS_D                       0//X轴位置，底部、中部、顶部
#define GRAPH_XAXIS_C                       1
#define GRAPH_XAXIS_T                       2
#define GRAPH_YAXIS_L                       0//y轴位置，居左，居中，居右
#define GRAPH_YAXIS_C                       1
#define GRAPH_YAXIS_R                       2

#define GRAPH_CHANNEL_MAX                     4//曲线控件最大的通道数

__packed typedef struct _GRAPH_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8 backMode;                        //背景模式
	gui_uint8 dir;                             //bit0-bit3控制波形推荐方式，0000左到右推进,0001右到左推进
	gui_uint8 xAxis_loc;                       //轴位置，
	gui_uint8 yAxis_loc;                       //轴位置，
	gui_uint8 channelCount;                    //通道数量，1-4，共计4个通道
	gui_color axisColor;                       //显示出来的轴颜色
	gui_color backColor;                       //背景颜色
	gui_color gridColor;                       //网格颜色
	gui_uint8  gridWidth;                      //网格宽度0表示没有网格
	gui_uint8  gridHeight;                     //网格高度0表示没有网格
	gui_uint16 picId;                          //切图模式或者图片模式下，图片ID
	gui_color graphColor[GRAPH_CHANNEL_MAX];   //4个通道的颜色值
	gui_uint16 dis;                            //波形Y轴上缩放系数
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//上位机下传下来的配置信息到这个地方就结束，不管是从外部flash读取数据还是从内部flash拷贝数据，sizeof(GRAPH_INFO)均应该-16
	//以下16个字节的数据，是MCU端才有的，存放的4个指针，分别指向4个通道的数据地址，数据类型均为uint8_t，0-255
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	gui_uint8 *graphData[GRAPH_CHANNEL_MAX];   
	gui_uint16 dataLen[GRAPH_CHANNEL_MAX];
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}GRAPH_INFO; 
typedef GRAPH_INFO*  pGRAPH_INFO;

#define GRAPH_INFO_LEN_OFFSET     GRAPH_CHANNEL_MAX*4+GRAPH_CHANNEL_MAX*2+12 //曲线控件里面 除了控件参数外 还有这么多字节的额外数据  


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  定时器信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _GTIMER_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	gui_uint16 timeOut;                        //定时器超时时间
	gui_uint8  enable;                         //定时器开关位，0关，1开
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//上位机下传下来的配置信息到这个地方就结束，不管是从外部flash读取数据还是从内部flash拷贝数据，sizeof(GTIMER_INFO)均应该-4
	//以下4个字节的数据，是MCU端才有的，
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	gui_uint32 timeValue;                      //定时器当前计数值（周期是1MS）低16位是当前计数器值，高16位是定时器溢出标志，或者其他标识
	WIDGET_EVENT timeoutEvent;                 //定时器溢出，用户自定义，注册进来的
}GTIMER_INFO; 
typedef GTIMER_INFO*  pGTIMER_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  变量控件信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _GVARIATE_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	gui_uint8 vaType;                          //变量类型，0值，1字符串
	gui_uint32 value;                          //变量值
	char  *text;                               //文本显示的数据指针，创建的时候从flash拷贝过来，
	                                           //---上位机传下来的应该是用户HMI输入的文本长度，占4个字节，目的是为了和MCU保存结构体长度一致
	                                           //---用户输入的文本信息直接跟在TEXTBOX_INFO结构体后面，MCU会偏移sizeof(BUTTON_INFO)之后去读
	gui_uint16 maxLen;                         //文本框显示的最大长度，按字节算，也是MCU需要申请的内存长度，ASCII占一个字节，中文占两个字节
}GVARIATE_INFO; 
typedef GVARIATE_INFO*  pGVARIATE_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  数字框信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define NUMBERBOX_DISPMODE_NUMBER           0//数字方式显示
#define NUMBERBOX_DISPMODE_MONEY            1//货币方式
#define NUMBERBOX_DISPMODE_HEX              2//十六进制方式显示
__packed typedef struct _NUMBERBOX_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8 backMode;                        //背景模式
	gui_uint8 keyId;                           //输入键盘ID -1表示没有键盘
	gui_uint16 fontlibId;                      //字库ID
	gui_uint8 borderWidth;                     //边框线宽，像素单位
	gui_color borderColor;                     //边框颜色
	gui_uint16 picId;                          //背景图片ID
	gui_color backColor;                       //背景色
	gui_color frontColor;                       //前景色
	gui_uint8  xAlign;                         //水平对齐方式
	gui_uint8  yAlign;                         //垂直对齐方式
	gui_uint8  formatType;                     //数字显示模式，0数字，1货币，2HEX
	gui_uint8  isDr;                           //显示不下的时候，是否换行
	gui_uint32 data;                           //显示的值
	gui_uint8  maxLen;                         //显示值的时候显示的最大长度，0自动，前端去0，其他按指定显示，从各位开始算
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}NUMBERBOX_INFO; 
typedef NUMBERBOX_INFO*  pNUMBERBOX_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  选项框信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define OPTIONBOX_STATE_NOCHOOSE            0//不选择状态
#define OPTIONBOX_STATE_CHOOSE              1//选择状态
#define OPTIONBOX_TYPE_RECT                 0//矩形样式
#define OPTIONBOX_TYPE_CIRCLE               1//圆形样式
__packed typedef struct _OPTIONBOX_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8  backMode;                       //背景模式，切图、单色、图片
	gui_uint8  optionType;                     //选项样式，0中心显示方形，为复选框，1中心显示圆形，为单选框
	gui_color backColor;                       //背景颜色
	gui_color frontColor;                       //前景颜色，就是中间显示的框的颜色
	gui_uint16 cPicId;                         //选择下的背景图
	gui_uint16 nocPicId;                       //不选择下的背景图
	gui_uint8  value;                          //值，0表示没选择，1表示选择
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的        
}OPTIONBOX_INFO; 
typedef OPTIONBOX_INFO*  pOPTIONBOX_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  图片框信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _PICTUREBOX_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint16 picId;                          //图片ID，当背景模式为显示图片的时候调用
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}PICTUREBOX_INFO; 
typedef PICTUREBOX_INFO*  pPICTUREBOX_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  指针信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _POINTER_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8 backMode;                        //背景模式,支持单色和图片
	gui_uint16 picId;                          //图片或者切图模式下：图片ID
	gui_color backColor;                       //单色模式下：背景颜色
	gui_color frontColor;                      //单色模式下：前景颜色
	gui_uint8  frontWidth;                     //指针线宽
	gui_uint8  circleRadius;                   //指针的圆心半径，像素单位
	gui_uint16 AngleValue;                     //弧度值0-360，标识着指针显示的位置
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//上位机下传下来的配置信息到这个地方就结束，不管是从外部flash读取数据还是从内部flash拷贝数据，均应该-4
	//以下4个字节的数据，是MCU端才有的，
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	gui_uint16 last_endx;                      //绘制指针的时候上一次的终点坐标，起点坐标始终是仪表盘的圆心，始终不变的
	gui_uint16 last_endy;
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}POINTER_INFO; 
typedef POINTER_INFO*  pPOINTER_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  进度条信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define PROGRESSBAR_DIR_H                   0//进度条方向横向
#define PROGRESSBAR_DIR_V                   1//进度条方向纵向
__packed typedef struct _PROGRESSBAR_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8 backMode;                        //背景模式,支持单色和图片
	gui_uint8 dir;                             //方向
	gui_uint8 value;                           //进度条的值，0-100%
	gui_uint16 backPicId;                      //图片模式下，背景图片
	gui_uint16 frontPicId;                     //图片模式下，前端图片
	gui_color backColor;                       //单色模式下，背景颜色
	gui_color frontColor;                      //单色模式下，前端颜色
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}PROGRESSBAR_INFO; 
typedef PROGRESSBAR_INFO*  pPROGRESSBAR_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  二维码信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _QRCODEBOX_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8  dispLogo;                       //是否在中心位置显示LOGO,0不显示，1显示
	gui_color backColor;                       //背景颜色
	gui_color frontColor;                       //前景颜色
	gui_uint16 logoPicId;                      //logo图片ID
	char  *text;                               //文本显示的数据指针，创建的时候从flash拷贝过来，
	                                           //---上位机传下来的应该是用户HMI输入的文本长度，占4个字节，目的是为了和MCU保存结构体长度一致
	                                           //---用户输入的文本信息直接跟在TEXTBOX_INFO结构体后面，MCU会偏移sizeof(BUTTON_INFO)之后去读
	gui_uint16 maxLen;                         //文本框显示的最大长度，按字节算，也是MCU需要申请的内存长度，ASCII占一个字节，中文占两个字节
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//上位机下传下来的配置信息到这个地方就结束，不管是从外部flash读取数据还是从内部flash拷贝数据，均应该-2
	//以下2个字节的数据，是MCU端才有的，
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	gui_uint16 textLen;                        //只是当前文本实际长度
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}QRCODEBOX_INFO; 
typedef QRCODEBOX_INFO*  pQRCODEBOX_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  滚动文本信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _ROLLTEXTBOX_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;               //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                      //控件位置信息
	gui_uint8 backMode;                       //背景模式
	gui_uint8 keyId;                          //输入键盘ID -1表示没有键盘
	gui_uint16 fontlibId;                     //字库ID
	gui_uint8 borderWidth;                    //边框线宽，像素单位
	gui_color borderColor;                    //边框颜色
	gui_uint16 picId;                         //背景图片ID
	gui_color backColor;                      //背景色
	gui_color frontColor;                      //前景色
	gui_uint8  xAlign;                        //水平对齐方式
	gui_uint8  yAlign;                        //垂直对齐方式
	gui_uint8  isPassword;                    //是否显示密码，0不是 1是
	gui_uint8  isDr;                          //显示不下的时候，是否换行
	char  *text;                              //文本显示的数据指针，创建的时候从flash拷贝过来，
	                                          //---上位机传下来的应该是用户HMI输入的文本长度，占4个字节，目的是为了和MCU保存结构体长度一致
	                                          //---用户输入的文本信息直接跟在TEXTBOX_INFO结构体后面，MCU会偏移sizeof(TEXTBOX_INFO)之后去读
	gui_uint16 maxLen;                        //文本框显示的最大长度，按字节算，也是MCU需要申请的内存长度，ASCII占一个字节，中文占两个字节
	gui_uint8  dir;                           //文本移动方向
	gui_uint8  dis;                           //文本移动幅值，每次移动多少像素
	gui_uint16 timeOut;                          //文本移动周期
	gui_uint8  rollenble;                     //文本移动使能，0不移动，1移动
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//上位机下传下来的配置信息到这个地方就结束，不管是从外部flash读取数据还是从内部flash拷贝数据，均应该-2
	//以下2个字节的数据，是MCU端才有的，
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	gui_uint16 textLen;                       //只是当前文本实际长度
	char *count_st;							  						//文本显示起始指针 指针占4个字节 
	gui_uint16 start_x;						 					  //文本框显示的起始x坐标 
	WIDGET_EVENT pressEvent;                  //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}ROLLTEXTBOX_INFO; 
typedef ROLLTEXTBOX_INFO*  pROLLTEXTBOX_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  滑动块信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define SLIDER_DIR_H                        0//滑动块横向滑动
#define SLIDER_DIR_V                        1//滑动块纵向滑动
__packed typedef struct _SLIDER_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8 dir;                             //方向定义，横向还是纵向
	gui_uint8 backMode;                        //背景方式，切图、单色、图片
	gui_color backColor;                       //背景颜色
	gui_uint16 backPicId;                      //背景图片
	gui_uint8 vernierMode;                     //游标方式，单色、图片
	gui_color vernierColor;                    //游标颜色
	gui_uint16 vernierPicId;                   //游标图片
	gui_uint16 vernierWidth;                   //游标宽度
	gui_uint16 vernierHeight;                  //游标高度
	gui_uint16 value;                          //游标值
//	gui_uint16 maxValue;                       //游标最大值
//	gui_uint16 minValue;                       //游标最小值  
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//上位机下传的配置参数到这里就结束了
	//下面的数据是MCU专有的，在创建控件的时候应该减去下面的数据长度
	gui_uint16 v_last_x;                       //滑动块上一次的位置
	gui_uint16 v_last_y;
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT slidEvent;
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}SLIDER_INFO; 
typedef SLIDER_INFO*  pSLIDER_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  文本框信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _TEXTBOX_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8 backMode;                        //背景模式
	gui_uint8 keyId;                           //输入键盘ID -1表示没有键盘
	gui_uint16 fontlibId;                      //字库ID
	gui_uint8 borderWidth;                     //边框线宽，像素单位
	gui_color borderColor;                     //边框颜色
	gui_uint16 picId;                          //背景图片ID
	gui_color backColor;                       //背景色
	gui_color frontColor;                       //前景色
	gui_uint8  xAlign;                         //水平对齐方式
	gui_uint8  yAlign;                         //垂直对齐方式
	gui_uint8  isPassword;                     //是否显示密码，0不是 1是
	gui_uint8  isDr;                           //显示不下的时候，是否换行
	char  *text;                               //文本显示的数据指针，创建的时候从flash拷贝过来，
	                                           //---上位机传下来的应该是用户HMI输入的文本长度，占4个字节，目的是为了和MCU保存结构体长度一致
	                                           //---用户输入的文本信息直接跟在TEXTBOX_INFO结构体后面，MCU会偏移sizeof(TEXTBOX_INFO)之后去读
	gui_uint16 maxLen;                           //文本框显示的最大长度，按字节算，也是MCU需要申请的内存长度，ASCII占一个字节，中文占两个字节
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//上位机下传下来的配置信息到这个地方就结束，不管是从外部flash读取数据还是从内部flash拷贝数据，均应该-2
	//以下2个字节的数据，是MCU端才有的，
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	gui_uint16 textLen;                       //只是当前文本实际长度
	WIDGET_EVENT pressEvent;                  //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}TEXTBOX_INFO; 
typedef TEXTBOX_INFO*  pTEXTBOX_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  触摸热区信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _TOUCHAREA_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}TOUCHAREA_INFO; 
typedef TOUCHAREA_INFO*  pTOUCHAREA_INFO;

#ifdef __cplusplus
}
#endif
#endif
