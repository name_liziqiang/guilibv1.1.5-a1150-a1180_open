#ifndef _lcdInterface_h
#define _lcdInterface_h
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：lcd接口文件
 *实现功能：
 *
 *撰 写 人：Alpscale LCD Application development team
 *撰写时间：2019-7-22
 *测 试 人：Alpscale LCD Application development team
 *测试时间：2019-7-22
 *版 本 号：V1.01_2019-7-22
 *版本说明：
 *                              
 *          函数清单
 *
 *V1.01：   功能实现
 *
 *V1.02：
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "gStdint.h"
#include "gWidgetInfo.h"

#ifdef __cplusplus
 extern "C" {
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *LCD使用到的基本参数
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef struct _LCD_PARAMETER{
	gui_uint16 width;						//LCD 宽度
	gui_uint16 height;					//LCD 高度
	gui_color  backColor;				//LCD 使用的背景颜色
	gui_color  fontcolor;				//LCD 使用的字体颜色
}LCD_PARAMETER;
typedef LCD_PARAMETER* pLCD_PARAMETER;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *LCD使用到的基本接口
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef struct _LCD_DESCRIPTOR{
	void (*drawPoint)(gui_uint16 x,gui_uint16 y);
	gui_color (*readPoint)(gui_uint16 x,gui_uint16 y);
	void (*clear)(void);
	void (*fill)(gui_uint16 sx,gui_uint16 sy,gui_uint16 ex,gui_uint16 ey);
	void (*fillUseOneData)(LCD_POSITION sPos,LCD_POSITION ePos,const gui_color Data,gui_uint32 WriteCount);
	void (*fillUseContinuousData)(LCD_POSITION sPos,const gui_color* Data,gui_uint16 dataWidth,gui_uint16 dataHeight);
	void (*fillUsePartContinuousData)(const gui_color* DataBase,gui_uint16 WidthOffset,WIDGET_LOCATION dispLoc,LCD_POSITION dataPos);
	gui_uint16 (*getMaxWidthUseContinuousDataSplitBit)(const gui_uint8* DataBase,gui_uint16 widthOffset,gui_uint16 heightOffset);
	gui_uint16 (*fillUseContinuousDataSplitBit)(const gui_uint8* DataBase,gui_uint16 CharWidthOffset,WIDGET_LOCATION dispLoc);
	gui_uint16 (*fillUseContinuousDataSplitBitHasBack)(const gui_uint8* DataBase,gui_uint16 CharWidthOffset,WIDGET_LOCATION dispLoc,gui_uint16 imageWidth,gui_uint32 imageAddr);
}LCD_DESCRIPTOR;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *库会使用到的LCD接口声明-------该部分的东西的定义用户需要自己去实现它
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
extern 	      LCD_PARAMETER lcdPara;
extern const  LCD_DESCRIPTOR Lcd_Des;

extern void test(void);
#ifdef __cplusplus
}
#endif

#endif
