#include "DemoApp.h"
#include "guiFunc.h"
#include "stdio.h"
#include "string.h"
#include "comInterface.h"

static gui_uint8 UP=0,STOP=1,DOWN=0;
static gui_uint8 time=0,t=0;
static gui_int8 floor=0,once=0;
void guiMainPageInit(void)
{
guiJumpPage(GUI_NULL,GUI_NULL,StartPageEnterEvent);

}
gui_Err StartPageEnterEvent(gui_int32 argc , const char **argv)
{
	guiCreateWidget((const void *)&StartPage);//创建的第一个控件一定是page
	guiCreateWidget((const void *)&StartPageGTimer);
	guiCreateWidget((const void *)&StartPicJT);//箭头图标
	guiCreateWidget((const void *)&StartPicFLOOR);//层数图标

	guiRegisterEvent(EVENT_REGISTER_TYPE_TIMEOUT , StartGTimerwID , Cartoon_timeoutEvent);
	return GUI_EOK;
}
gui_Err StartPageLeaveEvent(gui_int32 argc , const char **argv)
{
	
	return GUI_EOK;
}
static gui_Err Cartoon_timeoutEvent(gui_int32 argc , const char **argv)
{
	
	if(STOP==1){
			once=0;
			if(t==0){	STOP=0;UP=1;}
			if(t==1){	STOP=0;DOWN=1;}
	}
	if(UP==1||DOWN==1){
		if(once==0)	{guiSetWidgetOvertime(StartGTimerwID,50);once=1;}
		time++;
		if(time>=31)	time=1;	
		if(UP==1)	guiSetWidgetPictureID(StartPicJTwID,time+9);
		if(DOWN==1)		guiSetWidgetPictureID(StartPicJTwID,time+39);
		if(time%15==0) 	
		{
			if(UP){
				floor++;
				if(floor<=9)
				guiSetWidgetPictureID(StartPicFLOORwID,floor);
				if(floor==9)	{	UP=0;DOWN=0;STOP=1;t=1;guiSetWidgetOvertime(StartGTimerwID,3000);guiSetWidgetPictureID(StartPicJTwID,40);time=0;}
			}
			if(DOWN){
				floor--;
				if(floor>=1)
				guiSetWidgetPictureID(StartPicFLOORwID,floor);
				if(floor==1)	{	UP=0;DOWN=0;STOP=1;t=0;guiSetWidgetOvertime(StartGTimerwID,3000);guiSetWidgetPictureID(StartPicJTwID,10);time=0;	}					
			}			
		}
	}
	return GUI_EOK;
}


