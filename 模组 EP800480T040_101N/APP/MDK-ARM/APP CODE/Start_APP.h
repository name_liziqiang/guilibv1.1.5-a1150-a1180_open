#ifndef _Start_APP_h
#define _Start_APP_h
#include "gWidgetInfo.h"
#include "gStdint.h"
#include "gReturn.h"

#define StartwID 0x0101
static const PAGE_INFO StartPage={
	{.wType = WIDGET_TYPE_PAGE , .wId = StartwID , .wVscope = WIDGET_VS_PRIVATE},
	{.x = 0 , .y = 0 , .width = 480 , .height = 800},
	.backMode = WIDGET_BACKMODE_PIC , 
	.backColor = 0x00FF,
	.picId = 0,

};

#define StartGTimerwID 0x0103
static const GTIMER_INFO StartPageGTimer={
 {.wType = WIDGET_TYPE_GTIMER , .wId = StartGTimerwID , .wVscope = WIDGET_VS_PRIVATE},
 .timeOut = 80,
 .enable = 1,                         //定时器开关位，0关，1开
 .timeValue = 0,                     //定时器当前计数值（周期是1MS）低16位是当前计数器值，高16位是定时器溢出标志，或者其他标识
 .timeoutEvent = GUI_NULL
};
#define StartPicJTwID 0x0104
static const PICTUREBOX_INFO StartPicJT={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = StartPicJTwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 100 , .y = 63 , .width = 280 , .height = 280},    
 .picId = 70,

};
#define StartPicFLOORwID 0x0105
static const PICTUREBOX_INFO StartPicFLOOR={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = StartPicFLOORwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 110 , .y = 433 , .width = 260 , .height = 320},    
 .picId = 1,

};


void guiMainPageInit(void);
gui_Err StartPageEnterEvent(gui_int32 argc , const char **argv);
gui_Err StartPageLeaveEvent(gui_int32 argc , const char **argv);
static gui_Err Cartoon_timeoutEvent(gui_int32 argc , const char **argv);



#endif

