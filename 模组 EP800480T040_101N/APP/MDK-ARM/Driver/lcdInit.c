#include "lcdInit.h"
#include "lcdDriver.h"
#include "lcdInterface.h"
#include "m1130_gpio.h"
#include "m1130_tim.h"
#include "stdio.h"

#define WriteComm Lcd_Driver.writeCMD
#define WriteData Lcd_Driver.writeData


void LCD_DIM_init(uint16_t arr,uint16_t psc,uint16_t pulse)
{
	//TIM15  CH1 输出PWM
	RCC->AHBCLKCTRL0_SET|=(1<<24);//开启定时器时钟
	RCC->AHBCLKCTRL0_SET|=(1<<3);
	RCC->PWMCLKDIV = 0X01;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	GPIO_InitTypeDef GPIO_InitStructure; 
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Function = GPIO_FUNCTION_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_Init(GPIO1, &GPIO_InitStructure);
	
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Prescaler=psc;//96Mhz/(95+1)=1Mhz
	TIM_TimeBaseStructure.TIM_Period=arr ;//fpwm=9600/(1199+1)=80Khz
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);
	
	//SYSAHBCLKDIV=4 在SystemInit已经初始化了 AHB时钟96Mhz	
	TIM_OCStructInit(&TIM_OCInitStructure);
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; //选择定时器模式:TIM脉冲宽度调制模式2
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; //比较输出使能
	TIM_OCInitStructure.TIM_Pulse = pulse; //设置待装入捕获比较寄存器的脉冲值
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; //输出极性:TIM输出比较极性高
	TIM_OC1Init(TIM4, &TIM_OCInitStructure);  //根据TIM_OCInitStruct中指定的参数初始化外设TIMx     //TIM4_CH1

	TIM_OC1PreloadConfig(TIM4,TIM_OCPreload_Enable);
	TIM_ARRPreloadConfig(TIM4,ENABLE);
	TIM_Cmd(TIM4,ENABLE);
}

void LcdSetDim(uint16_t pulse)
{
	TIM_SetCompare1(TIM4 , pulse);
}
#define LCD_WR_REG Lcd_Driver.writeCMD
#define LCD_WR_DATA Lcd_Driver.writeData
/*
*********************************************************************************************************************
*函数功能：LCDinitDelay LCD初始化模块延时函数
*入口参数：dly  延时时间
*出口参数：无
*********************************************************************************************************************
*/
void LCDinitDelay(unsigned int dly)
{
    unsigned int i,j;

    for(i=0;i<dly;i++)
    	for(j=0;j<4096;j++);
} 
/*
*********************************************************************************************************************
*函数功能：LCD_Init，LCD初始化，初始化LCD的硬件
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
void BlockWrite(unsigned int Xstart,unsigned int Ystart,unsigned int Xend,unsigned int Yend) //reentrant
{
	//ILI9481

	WriteComm(0x2a00);   
	WriteData(Xstart>>8);
	WriteComm(0x2a01); 
	WriteData(Xstart&0xff);
	WriteComm(0x2a02); 
	WriteData(Xend>>8);
	WriteComm(0x2a03); 
	WriteData(Xend&0xff);

	WriteComm(0x2b00);   
	WriteData(Ystart>>8);
	WriteComm(0x2b01); 
	WriteData(Ystart&0xff);
	WriteComm(0x2b02); 
	WriteData(Yend>>8);
	WriteComm(0x2b03); 
	WriteData(Yend&0xff);

	WriteComm(0x2c00);
}


void LCD_Init(void)
{
		GPIO0_IT->DIR_SET=0xffff0000;//DB0~15输出 
		GPIO_SetPinMux(GPIO1, GPIO_Pin_11,GPIO_FUNCTION_0);//LCD_BG 
		GPIO_SetPinMux(GPIO1, GPIO_Pin_10,GPIO_FUNCTION_0);//LCD_RD 
		GPIO_SetPinMux(GPIO0, GPIO_Pin_4,GPIO_FUNCTION_0);//LCD_WR 
		GPIO_SetPinMux(GPIO0, GPIO_Pin_3,GPIO_FUNCTION_0);//LCD_RS 
		GPIO_SetPinMux(GPIO1, GPIO_Pin_31,GPIO_FUNCTION_0);//LCD_CS 
    GPIO_SetPinMux(GPIO0, GPIO_Pin_2,GPIO_FUNCTION_0);//LCD_RST 
		
		GPIO_SetPinDir(GPIO0, GPIO_Pin_4,GPIO_Mode_OUT);//LCD_WR 
		GPIO_SetPinDir(GPIO1, GPIO_Pin_11,GPIO_Mode_OUT);//LCD_BG 
		GPIO_SetPinDir(GPIO1, GPIO_Pin_10,GPIO_Mode_OUT);//LCD_RD 	
		GPIO_SetPinDir(GPIO0, GPIO_Pin_3,GPIO_Mode_OUT);//LCD_RS 
		GPIO_SetPinDir(GPIO1, GPIO_Pin_31,GPIO_Mode_OUT);//LCD_CS 	
	  GPIO_SetPinDir(GPIO0, GPIO_Pin_2,GPIO_Mode_OUT);//LCD_RST

		LCD_NRST();
		LCDinitDelay(1000);
		LCD_RST();
		LCDinitDelay(1000);
		LCD_NRST();
		LCDinitDelay(1000);
//		
//PAGE1
		WriteComm(0xF000);    WriteData(0x0055);
		WriteComm(0xF001);    WriteData(0x00AA);
		WriteComm(0xF002);    WriteData(0x0052);
		WriteComm(0xF003);    WriteData(0x0008);
		WriteComm(0xF004);    WriteData(0x0001);
		
		//Set AVDD 5.2V
		WriteComm(0xB000);    WriteData(0x000D);
		WriteComm(0xB001);    WriteData(0x000D);
		WriteComm(0xB002);    WriteData(0x000D);
		
		//Set AVEE 5.2V
		WriteComm(0xB100);    WriteData(0x000D);
		WriteComm(0xB101);    WriteData(0x000D);
		WriteComm(0xB102);    WriteData(0x000D);
		
		//Set VCL -2.5V
		WriteComm(0xB200);    WriteData(0x0000);
		WriteComm(0xB201);    WriteData(0x0000);
		WriteComm(0xB202);    WriteData(0x0000);				
		
		//Set AVDD Ratio
		WriteComm(0xB600);    WriteData(0x0044);
		WriteComm(0xB601);    WriteData(0x0044);
		WriteComm(0xB602);    WriteData(0x0044);
		
		//Set AVEE Ratio
		WriteComm(0xB700);    WriteData(0x0034);
		WriteComm(0xB701);    WriteData(0x0034);
		WriteComm(0xB702);    WriteData(0x0034);
		
		//Set VCL -2.5V
		WriteComm(0xB800);    WriteData(0x0034);
		WriteComm(0xB801);    WriteData(0x0034);
		WriteComm(0xB802);    WriteData(0x0034);
				
		//Control VGH booster voltage rang
		WriteComm(0xBF00);    WriteData(0x0001); //VGH:7~18V	
		
		//VGH=15V(1V/step)	Free pump
		WriteComm(0xB300);    WriteData(0x000f);		//08
		WriteComm(0xB301);    WriteData(0x000f);		//08
		WriteComm(0xB302);    WriteData(0x000f);		//08
		
		//VGH Ratio
		WriteComm(0xB900);    WriteData(0x0034);
		WriteComm(0xB901);    WriteData(0x0034);
		WriteComm(0xB902);    WriteData(0x0034);
		
		//VGL_REG=-10(1V/step)
		WriteComm(0xB500);    WriteData(0x0008);
		WriteComm(0xB501);    WriteData(0x0008);
		WriteComm(0xB502);    WriteData(0x0008);
		
		WriteComm(0xC200);    WriteData(0x0003);
		
		//VGLX Ratio
		WriteComm(0xBA00);    WriteData(0x0034);
		WriteComm(0xBA01);    WriteData(0x0034);
		WriteComm(0xBA02);    WriteData(0x0034);
		
		//VGMP/VGSP=4.5V/0V
		WriteComm(0xBC00);    WriteData(0x0000);		//00
		WriteComm(0xBC01);    WriteData(0x0078);		//C8 =5.5V/90=4.8V
		WriteComm(0xBC02);    WriteData(0x0000);		//01
		
		//VGMN/VGSN=-4.5V/0V
		WriteComm(0xBD00);    WriteData(0x0000); //00
		WriteComm(0xBD01);    WriteData(0x0078); //90
		WriteComm(0xBD02);    WriteData(0x0000);
		
		//Vcom=-1.4V(12.5mV/step)
		WriteComm(0xBE00);    WriteData(0x0000);
		WriteComm(0xBE01);    WriteData(0x0064); //HSD:64;Novatek:50=-1.0V, 80  5f
		
//		//Gamma (R+)
//		WriteComm(0xD100);    WriteData(0x0000);
//		WriteComm(0xD101);    WriteData(0x0033);
//		WriteComm(0xD102);    WriteData(0x0000);
//		WriteComm(0xD103);    WriteData(0x0034);
//		WriteComm(0xD104);    WriteData(0x0000);
//		WriteComm(0xD105);    WriteData(0x003A);
//		WriteComm(0xD106);    WriteData(0x0000);
//		WriteComm(0xD107);    WriteData(0x004A);
//		WriteComm(0xD108);    WriteData(0x0000);
//		WriteComm(0xD109);    WriteData(0x005C);
//		WriteComm(0xD10A);    WriteData(0x0000);
//		WriteComm(0xD10B);    WriteData(0x0081);
//		WriteComm(0xD10C);    WriteData(0x0000);
//		WriteComm(0xD10D);    WriteData(0x00A6);
//		WriteComm(0xD10E);    WriteData(0x0000);
//		WriteComm(0xD10F);    WriteData(0x00E5);
//		WriteComm(0xD110);    WriteData(0x0001);
//		WriteComm(0xD111);    WriteData(0x0013);
//		WriteComm(0xD112);    WriteData(0x0001);
//		WriteComm(0xD113);    WriteData(0x0054);
//		WriteComm(0xD114);    WriteData(0x0001);
//		WriteComm(0xD115);    WriteData(0x0082);
//		WriteComm(0xD116);    WriteData(0x0001);
//		WriteComm(0xD117);    WriteData(0x00CA);
//		WriteComm(0xD118);    WriteData(0x0002);
//		WriteComm(0xD119);    WriteData(0x0000);
//		WriteComm(0xD11A);    WriteData(0x0002);
//		WriteComm(0xD11B);    WriteData(0x0001);
//		WriteComm(0xD11C);    WriteData(0x0002);
//		WriteComm(0xD11D);    WriteData(0x0034);
//		WriteComm(0xD11E);    WriteData(0x0002);
//		WriteComm(0xD11F);    WriteData(0x0067);
//		WriteComm(0xD120);    WriteData(0x0002);
//		WriteComm(0xD121);    WriteData(0x0084);
//		WriteComm(0xD122);    WriteData(0x0002);
//		WriteComm(0xD123);    WriteData(0x00A4);
//		WriteComm(0xD124);    WriteData(0x0002);
//		WriteComm(0xD125);    WriteData(0x00B7);
//		WriteComm(0xD126);    WriteData(0x0002);
//		WriteComm(0xD127);    WriteData(0x00CF);
//		WriteComm(0xD128);    WriteData(0x0002);
//		WriteComm(0xD129);    WriteData(0x00DE);
//		WriteComm(0xD12A);    WriteData(0x0002);
//		WriteComm(0xD12B);    WriteData(0x00F2);
//		WriteComm(0xD12C);    WriteData(0x0002);
//		WriteComm(0xD12D);    WriteData(0x00FE);
//		WriteComm(0xD12E);    WriteData(0x0003);
//		WriteComm(0xD12F);    WriteData(0x0010);
//		WriteComm(0xD130);    WriteData(0x0003);
//		WriteComm(0xD131);    WriteData(0x0033);
//		WriteComm(0xD132);    WriteData(0x0003);
//		WriteComm(0xD133);    WriteData(0x006D);
//		
//		//Gamma (G+)
//		WriteComm(0xD200);    WriteData(0x0000);
//		WriteComm(0xD201);    WriteData(0x0033);
//		WriteComm(0xD202);    WriteData(0x0000);
//		WriteComm(0xD203);    WriteData(0x0034);
//		WriteComm(0xD204);    WriteData(0x0000);
//		WriteComm(0xD205);    WriteData(0x003A);
//		WriteComm(0xD206);    WriteData(0x0000);
//		WriteComm(0xD207);    WriteData(0x004A);
//		WriteComm(0xD208);    WriteData(0x0000);
//		WriteComm(0xD209);    WriteData(0x005C);
//		WriteComm(0xD20A);    WriteData(0x0000);
//		WriteComm(0xD20B);    WriteData(0x0081);
//		WriteComm(0xD20C);    WriteData(0x0000);
//		WriteComm(0xD20D);    WriteData(0x00A6);
//		WriteComm(0xD20E);    WriteData(0x0000);
//		WriteComm(0xD20F);    WriteData(0x00E5);
//		WriteComm(0xD210);    WriteData(0x0001);
//		WriteComm(0xD211);    WriteData(0x0013);
//		WriteComm(0xD212);    WriteData(0x0001);
//		WriteComm(0xD213);    WriteData(0x0054);
//		WriteComm(0xD214);    WriteData(0x0001);
//		WriteComm(0xD215);    WriteData(0x0082);
//		WriteComm(0xD216);    WriteData(0x0001);
//		WriteComm(0xD217);    WriteData(0x00CA);
//		WriteComm(0xD218);    WriteData(0x0002);
//		WriteComm(0xD219);    WriteData(0x0000);
//		WriteComm(0xD21A);    WriteData(0x0002);
//		WriteComm(0xD21B);    WriteData(0x0001);
//		WriteComm(0xD21C);    WriteData(0x0002);
//		WriteComm(0xD21D);    WriteData(0x0034);
//		WriteComm(0xD21E);    WriteData(0x0002);
//		WriteComm(0xD21F);    WriteData(0x0067);
//		WriteComm(0xD220);    WriteData(0x0002);
//		WriteComm(0xD221);    WriteData(0x0084);
//		WriteComm(0xD222);    WriteData(0x0002);
//		WriteComm(0xD223);    WriteData(0x00A4);
//		WriteComm(0xD224);    WriteData(0x0002);
//		WriteComm(0xD225);    WriteData(0x00B7);
//		WriteComm(0xD226);    WriteData(0x0002);
//		WriteComm(0xD227);    WriteData(0x00CF);
//		WriteComm(0xD228);    WriteData(0x0002);
//		WriteComm(0xD229);    WriteData(0x00DE);
//		WriteComm(0xD22A);    WriteData(0x0002);
//		WriteComm(0xD22B);    WriteData(0x00F2);
//		WriteComm(0xD22C);    WriteData(0x0002);
//		WriteComm(0xD22D);    WriteData(0x00FE);
//		WriteComm(0xD22E);    WriteData(0x0003);
//		WriteComm(0xD22F);    WriteData(0x0010);
//		WriteComm(0xD230);    WriteData(0x0003);
//		WriteComm(0xD231);    WriteData(0x0033);
//		WriteComm(0xD232);    WriteData(0x0003);
//		WriteComm(0xD233);    WriteData(0x006D);
//		
//		//Gamma (B+)
//		WriteComm(0xD300);    WriteData(0x0000);
//		WriteComm(0xD301);    WriteData(0x0033);
//		WriteComm(0xD302);    WriteData(0x0000);
//		WriteComm(0xD303);    WriteData(0x0034);
//		WriteComm(0xD304);    WriteData(0x0000);
//		WriteComm(0xD305);    WriteData(0x003A);
//		WriteComm(0xD306);    WriteData(0x0000);
//		WriteComm(0xD307);    WriteData(0x004A);
//		WriteComm(0xD308);    WriteData(0x0000);
//		WriteComm(0xD309);    WriteData(0x005C);
//		WriteComm(0xD30A);    WriteData(0x0000);
//		WriteComm(0xD30B);    WriteData(0x0081);
//		WriteComm(0xD30C);    WriteData(0x0000);
//		WriteComm(0xD30D);    WriteData(0x00A6);
//		WriteComm(0xD30E);    WriteData(0x0000);
//		WriteComm(0xD30F);    WriteData(0x00E5);
//		WriteComm(0xD310);    WriteData(0x0001);
//		WriteComm(0xD311);    WriteData(0x0013);
//		WriteComm(0xD312);    WriteData(0x0001);
//		WriteComm(0xD313);    WriteData(0x0054);
//		WriteComm(0xD314);    WriteData(0x0001);
//		WriteComm(0xD315);    WriteData(0x0082);
//		WriteComm(0xD316);    WriteData(0x0001);
//		WriteComm(0xD317);    WriteData(0x00CA);
//		WriteComm(0xD318);    WriteData(0x0002);
//		WriteComm(0xD319);    WriteData(0x0000);
//		WriteComm(0xD31A);    WriteData(0x0002);
//		WriteComm(0xD31B);    WriteData(0x0001);
//		WriteComm(0xD31C);    WriteData(0x0002);
//		WriteComm(0xD31D);    WriteData(0x0034);
//		WriteComm(0xD31E);    WriteData(0x0002);
//		WriteComm(0xD31F);    WriteData(0x0067);
//		WriteComm(0xD320);    WriteData(0x0002);
//		WriteComm(0xD321);    WriteData(0x0084);
//		WriteComm(0xD322);    WriteData(0x0002);
//		WriteComm(0xD323);    WriteData(0x00A4);
//		WriteComm(0xD324);    WriteData(0x0002);
//		WriteComm(0xD325);    WriteData(0x00B7);
//		WriteComm(0xD326);    WriteData(0x0002);
//		WriteComm(0xD327);    WriteData(0x00CF);
//		WriteComm(0xD328);    WriteData(0x0002);
//		WriteComm(0xD329);    WriteData(0x00DE);
//		WriteComm(0xD32A);    WriteData(0x0002);
//		WriteComm(0xD32B);    WriteData(0x00F2);
//		WriteComm(0xD32C);    WriteData(0x0002);
//		WriteComm(0xD32D);    WriteData(0x00FE);
//		WriteComm(0xD32E);    WriteData(0x0003);
//		WriteComm(0xD32F);    WriteData(0x0010);
//		WriteComm(0xD330);    WriteData(0x0003);
//		WriteComm(0xD331);    WriteData(0x0033);
//		WriteComm(0xD332);    WriteData(0x0003);
//		WriteComm(0xD333);    WriteData(0x006D);
//		
//		//Gamma (R-)
//		WriteComm(0xD400);    WriteData(0x0000);
//		WriteComm(0xD401);    WriteData(0x0033);
//		WriteComm(0xD402);    WriteData(0x0000);
//		WriteComm(0xD403);    WriteData(0x0034);
//		WriteComm(0xD404);    WriteData(0x0000);
//		WriteComm(0xD405);    WriteData(0x003A);
//		WriteComm(0xD406);    WriteData(0x0000);
//		WriteComm(0xD407);    WriteData(0x004A);
//		WriteComm(0xD408);    WriteData(0x0000);
//		WriteComm(0xD409);    WriteData(0x005C);
//		WriteComm(0xD40A);    WriteData(0x0000);
//		WriteComm(0xD40B);    WriteData(0x0081);
//		WriteComm(0xD40C);    WriteData(0x0000);
//		WriteComm(0xD40D);    WriteData(0x00A6);
//		WriteComm(0xD40E);    WriteData(0x0000);
//		WriteComm(0xD40F);    WriteData(0x00E5);
//		WriteComm(0xD410);    WriteData(0x0001);
//		WriteComm(0xD411);    WriteData(0x0013);
//		WriteComm(0xD412);    WriteData(0x0001);
//		WriteComm(0xD413);    WriteData(0x0054);
//		WriteComm(0xD414);    WriteData(0x0001);
//		WriteComm(0xD415);    WriteData(0x0082);
//		WriteComm(0xD416);    WriteData(0x0001);
//		WriteComm(0xD417);    WriteData(0x00CA);
//		WriteComm(0xD418);    WriteData(0x0002);
//		WriteComm(0xD419);    WriteData(0x0000);
//		WriteComm(0xD41A);    WriteData(0x0002);
//		WriteComm(0xD41B);    WriteData(0x0001);
//		WriteComm(0xD41C);    WriteData(0x0002);
//		WriteComm(0xD41D);    WriteData(0x0034);
//		WriteComm(0xD41E);    WriteData(0x0002);
//		WriteComm(0xD41F);    WriteData(0x0067);
//		WriteComm(0xD420);    WriteData(0x0002);
//		WriteComm(0xD421);    WriteData(0x0084);
//		WriteComm(0xD422);    WriteData(0x0002);
//		WriteComm(0xD423);    WriteData(0x00A4);
//		WriteComm(0xD424);    WriteData(0x0002);
//		WriteComm(0xD425);    WriteData(0x00B7);
//		WriteComm(0xD426);    WriteData(0x0002);
//		WriteComm(0xD427);    WriteData(0x00CF);
//		WriteComm(0xD428);    WriteData(0x0002);
//		WriteComm(0xD429);    WriteData(0x00DE);
//		WriteComm(0xD42A);    WriteData(0x0002);
//		WriteComm(0xD42B);    WriteData(0x00F2);
//		WriteComm(0xD42C);    WriteData(0x0002);
//		WriteComm(0xD42D);    WriteData(0x00FE);
//		WriteComm(0xD42E);    WriteData(0x0003);
//		WriteComm(0xD42F);    WriteData(0x0010);
//		WriteComm(0xD430);    WriteData(0x0003);
//		WriteComm(0xD431);    WriteData(0x0033);
//		WriteComm(0xD432);    WriteData(0x0003);
//		WriteComm(0xD433);    WriteData(0x006D);
//		
//		//Gamma (G-)
//		WriteComm(0xD500);    WriteData(0x0000);
//		WriteComm(0xD501);    WriteData(0x0033);
//		WriteComm(0xD502);    WriteData(0x0000);
//		WriteComm(0xD503);    WriteData(0x0034);
//		WriteComm(0xD504);    WriteData(0x0000);
//		WriteComm(0xD505);    WriteData(0x003A);
//		WriteComm(0xD506);    WriteData(0x0000);
//		WriteComm(0xD507);    WriteData(0x004A);
//		WriteComm(0xD508);    WriteData(0x0000);
//		WriteComm(0xD509);    WriteData(0x005C);
//		WriteComm(0xD50A);    WriteData(0x0000);
//		WriteComm(0xD50B);    WriteData(0x0081);
//		WriteComm(0xD50C);    WriteData(0x0000);
//		WriteComm(0xD50D);    WriteData(0x00A6);
//		WriteComm(0xD50E);    WriteData(0x0000);
//		WriteComm(0xD50F);    WriteData(0x00E5);
//		WriteComm(0xD510);    WriteData(0x0001);
//		WriteComm(0xD511);    WriteData(0x0013);
//		WriteComm(0xD512);    WriteData(0x0001);
//		WriteComm(0xD513);    WriteData(0x0054);
//		WriteComm(0xD514);    WriteData(0x0001);
//		WriteComm(0xD515);    WriteData(0x0082);
//		WriteComm(0xD516);    WriteData(0x0001);
//		WriteComm(0xD517);    WriteData(0x00CA);
//		WriteComm(0xD518);    WriteData(0x0002);
//		WriteComm(0xD519);    WriteData(0x0000);
//		WriteComm(0xD51A);    WriteData(0x0002);
//		WriteComm(0xD51B);    WriteData(0x0001);
//		WriteComm(0xD51C);    WriteData(0x0002);
//		WriteComm(0xD51D);    WriteData(0x0034);
//		WriteComm(0xD51E);    WriteData(0x0002);
//		WriteComm(0xD51F);    WriteData(0x0067);
//		WriteComm(0xD520);    WriteData(0x0002);
//		WriteComm(0xD521);    WriteData(0x0084);
//		WriteComm(0xD522);    WriteData(0x0002);
//		WriteComm(0xD523);    WriteData(0x00A4);
//		WriteComm(0xD524);    WriteData(0x0002);
//		WriteComm(0xD525);    WriteData(0x00B7);
//		WriteComm(0xD526);    WriteData(0x0002);
//		WriteComm(0xD527);    WriteData(0x00CF);
//		WriteComm(0xD528);    WriteData(0x0002);
//		WriteComm(0xD529);    WriteData(0x00DE);
//		WriteComm(0xD52A);    WriteData(0x0002);
//		WriteComm(0xD52B);    WriteData(0x00F2);
//		WriteComm(0xD52C);    WriteData(0x0002);
//		WriteComm(0xD52D);    WriteData(0x00FE);
//		WriteComm(0xD52E);    WriteData(0x0003);
//		WriteComm(0xD52F);    WriteData(0x0010);
//		WriteComm(0xD530);    WriteData(0x0003);
//		WriteComm(0xD531);    WriteData(0x0033);
//		WriteComm(0xD532);    WriteData(0x0003);
//		WriteComm(0xD533);    WriteData(0x006D);
//		
//		//Gamma (B-)
//		WriteComm(0xD600);    WriteData(0x0000);
//		WriteComm(0xD601);    WriteData(0x0033);
//		WriteComm(0xD602);    WriteData(0x0000);
//		WriteComm(0xD603);    WriteData(0x0034);
//		WriteComm(0xD604);    WriteData(0x0000);
//		WriteComm(0xD605);    WriteData(0x003A);
//		WriteComm(0xD606);    WriteData(0x0000);
//		WriteComm(0xD607);    WriteData(0x004A);
//		WriteComm(0xD608);    WriteData(0x0000);
//		WriteComm(0xD609);    WriteData(0x005C);
//		WriteComm(0xD60A);    WriteData(0x0000);
//		WriteComm(0xD60B);    WriteData(0x0081);
//		WriteComm(0xD60C);    WriteData(0x0000);
//		WriteComm(0xD60D);    WriteData(0x00A6);
//		WriteComm(0xD60E);    WriteData(0x0000);
//		WriteComm(0xD60F);    WriteData(0x00E5);
//		WriteComm(0xD610);    WriteData(0x0001);
//		WriteComm(0xD611);    WriteData(0x0013);
//		WriteComm(0xD612);    WriteData(0x0001);
//		WriteComm(0xD613);    WriteData(0x0054);
//		WriteComm(0xD614);    WriteData(0x0001);
//		WriteComm(0xD615);    WriteData(0x0082);
//		WriteComm(0xD616);    WriteData(0x0001);
//		WriteComm(0xD617);    WriteData(0x00CA);
//		WriteComm(0xD618);    WriteData(0x0002);
//		WriteComm(0xD619);    WriteData(0x0000);
//		WriteComm(0xD61A);    WriteData(0x0002);
//		WriteComm(0xD61B);    WriteData(0x0001);
//		WriteComm(0xD61C);    WriteData(0x0002);
//		WriteComm(0xD61D);    WriteData(0x0034);
//		WriteComm(0xD61E);    WriteData(0x0002);
//		WriteComm(0xD61F);    WriteData(0x0067);
//		WriteComm(0xD620);    WriteData(0x0002);
//		WriteComm(0xD621);    WriteData(0x0084);
//		WriteComm(0xD622);    WriteData(0x0002);
//		WriteComm(0xD623);    WriteData(0x00A4);
//		WriteComm(0xD624);    WriteData(0x0002);
//		WriteComm(0xD625);    WriteData(0x00B7);
//		WriteComm(0xD626);    WriteData(0x0002);
//		WriteComm(0xD627);    WriteData(0x00CF);
//		WriteComm(0xD628);    WriteData(0x0002);
//		WriteComm(0xD629);    WriteData(0x00DE);
//		WriteComm(0xD62A);    WriteData(0x0002);
//		WriteComm(0xD62B);    WriteData(0x00F2);
//		WriteComm(0xD62C);    WriteData(0x0002);
//		WriteComm(0xD62D);    WriteData(0x00FE);
//		WriteComm(0xD62E);    WriteData(0x0003);
//		WriteComm(0xD62F);    WriteData(0x0010);
//		WriteComm(0xD630);    WriteData(0x0003);
//		WriteComm(0xD631);    WriteData(0x0033);
//		WriteComm(0xD632);    WriteData(0x0003);
//		WriteComm(0xD633);    WriteData(0x006D);
		
		//PAGE0
		WriteComm(0xF000);    WriteData(0x0055);
		WriteComm(0xF001);    WriteData(0x00AA);
		WriteComm(0xF002);    WriteData(0x0052);
		WriteComm(0xF003);    WriteData(0x0008);	
		WriteComm(0xF004);    WriteData(0x0000); 
		
		//480x800
		WriteComm(0xB500);    WriteData(0x0050);
		
		//WriteComm(0x2C00);    WriteData(0x0006); //8BIT 6-6-6?
		
		//Dispay control
		WriteComm(0xB100);    WriteData(0x00CC);	
		WriteComm(0xB101);    WriteData(0x0008); // S1->S1440:00;S1440->S1:02
		
		//Source hold time (Nova non-used)
		WriteComm(0xB600);    WriteData(0x0005);
		
		//Gate EQ control	 (Nova non-used)
		WriteComm(0xB700);    WriteData(0x0077);  //HSD:70;Nova:77	 
		WriteComm(0xB701);    WriteData(0x0077);	//HSD:70;Nova:77
		
		//Source EQ control (Nova non-used)
		WriteComm(0xB800);    WriteData(0x0001);  
		WriteComm(0xB801);    WriteData(0x0003);	//HSD:05;Nova:07
		WriteComm(0xB802);    WriteData(0x0003);	//HSD:05;Nova:07
		WriteComm(0xB803);    WriteData(0x0003);	//HSD:05;Nova:07
		
		//Inversion mode: column
		WriteComm(0xBC00);    WriteData(0x0002);	//00: column
		WriteComm(0xBC01);    WriteData(0x0000);	//01:1dot
		WriteComm(0xBC02);    WriteData(0x0000); 
		
		//Frame rate	(Nova non-used)
		WriteComm(0xBD00);    WriteData(0x0001);
		WriteComm(0xBD01);    WriteData(0x0084);
		WriteComm(0xBD02);    WriteData(0x001c); //HSD:06;Nova:1C
		WriteComm(0xBD03);    WriteData(0x001c); //HSD:04;Nova:1C
		WriteComm(0xBD04);    WriteData(0x0000);
		
		//LGD timing control(4H/4-Delay)
		WriteComm(0xC900);    WriteData(0x00D0);//3H:0x50;4H:0xD0//D
		WriteComm(0xC901);    WriteData(0x0002);//HSD:05;Nova:02
		WriteComm(0xC902);    WriteData(0x0050);//HSD:05;Nova:50
		WriteComm(0xC903);    WriteData(0x0050);//HSD:05;Nova:50;STV delay time
		WriteComm(0xC904);    WriteData(0x0050);//HSD:05;Nova:50;CLK delay time
		
		WriteComm(0x3600);    WriteData(0x0000);
		WriteComm(0x3500);    WriteData(0x0000);
		
		WriteComm(0xFF00);    WriteData(0x00AA);
		WriteComm(0xFF01);    WriteData(0x0055);
		WriteComm(0xFF02);    WriteData(0x0025);
		WriteComm(0xFF03);    WriteData(0x0001);
		
		WriteComm(0xFC00);    WriteData(0x0016);
		WriteComm(0xFC01);    WriteData(0x00A2);
		WriteComm(0xFC02);    WriteData(0x0026);
		
		WriteComm(0x1300);
		Lcd_Driver.scan_dir(DIR_VERTICAL);
//		WriteComm(0x3600);    WriteData(0x0068);//竖屏		
		WriteComm(0x3A00);    WriteData(0x0055);
		
		WriteComm(0x1100);//Sleep out
		LCDinitDelay(160);
		
		WriteComm(0x2900); //Display on  
    Lcd_Des.clear();
		LCD_BG_CLOSE();

}



