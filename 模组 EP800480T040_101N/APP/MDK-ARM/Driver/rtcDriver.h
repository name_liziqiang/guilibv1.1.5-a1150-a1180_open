#ifndef _rtcDriver_h
#define _rtcDriver_h
#include "gStdint.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：RTC实时时钟模块
 *实现功能：1、时间类型定义
 *          2、日期类型定义
 *          3、日期时间类型定义
 *          4、RTC 设备描述符定义
 *          
 *
 *撰 写 人：Alpscale LCD Application development team
 *撰写时间：2019-11-05
 *测 试 人：Alpscale LCD Application development team
 *测试时间：2019-11-05
 *版 本 号：V1.01_2019-11-05
 *版本说明：
 *                              
 *          函数清单
 *
 *V1.01：   功能实现
 *
 *V1.02：
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//1、时间类型定义
typedef struct _RTC_TIME{
	gui_uint8 mSecond;
	gui_uint8 Second;
	gui_uint8 Minute;
	gui_uint8 Hour;
}RTC_TIME;
typedef RTC_TIME*  pRTC_TIME;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//2、日期类型定义
typedef struct _RTC_DATE{
	gui_uint8 Day;
	gui_uint8 Weekday;//0-6 Sunday---Saturday
	gui_uint8 Month;
	gui_uint8 Year;   //00-99
}RTC_DATE;
typedef RTC_DATE*  pRTC_DATE;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//3、日期时间类型定义
typedef struct _RTC_TIMEDATE{
	RTC_TIME time;
	RTC_DATE date;
}RTC_TIMEDATE;
typedef RTC_TIMEDATE*  pRTC_TIMEDATE;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//4、RTC设备描述符定义
typedef struct _RTC_DES{
	gui_int8 (*writeTime)(pRTC_TIME rtcTime);
	gui_int8 (*writeDate)(pRTC_DATE rtcDate);
	gui_int8 (*writeDateTime)(pRTC_TIMEDATE dateTime);
	gui_int8 (*readTime)(pRTC_TIME rtcTime);
	gui_int8 (*readDate)(pRTC_DATE rtcDate);
	gui_int8 (*readDateTime)(pRTC_TIMEDATE dateTime);
}RTC_DES;
extern const RTC_DES Rtc_Des;
gui_int8 RTC_Init(void);
#endif
