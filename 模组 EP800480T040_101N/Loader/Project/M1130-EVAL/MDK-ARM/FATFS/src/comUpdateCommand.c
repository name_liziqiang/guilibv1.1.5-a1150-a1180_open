#include "stdio.h"
#include <stdint.h>
#include <m1130_qspi.h>
#include "mmc_sd.h"
#include "ff.h"
#include "m1130_eval_qspi.h"
#include "lcddriver.h"
#include "lcdinit.h"
#include "guiConfig.h"

#define DISP_DOWNLOAD_TIPS         1
#define FLASHRES_START_ADDR        ((FLASH_RES_START_ADDR) + (FLASH_SECTOR_SIZE))
#define FLASHAPP_START_ADDR        0x8000

#define WRITEPAGE_MAX    256
static __align(4) uint8_t FlashWriteBuff[WRITEPAGE_MAX];//4字节对齐的缓存区，便于flash数据的写入
static FATFS SD_Fatfs;
static FIL   ResourceFile;

extern int qspi_flash_erase_block_4k(QSPI_TypeDef *QSPIptr, uint32_t faddr);
extern int qspi_flash_write_page(QSPI_TypeDef *QSPIptr, uint8_t *buf, uint32_t faddr, int count);
extern void qspi_flash_global_unprotect(QSPI_TypeDef* QSPIptr);


#define UPDATE_LCD_TIPS_ON     //定义该宏表示打开LCD提示下载,
#define TIPS_DISP_X         204//图片显示的X坐标
#define TIPS_DISP_Y         364//图片显示的Y坐标
#define PIC_SIZE_WIDTH      72 //图片宽度
#define PIC_SIZE_HEIGHT     72 //图片高度

#ifdef UPDATE_LCD_TIPS_ON
extern const unsigned char gImage_updateLogo[];
static void disp_Tip_Logo()
{
	uint16_t *data=(uint16_t *)gImage_updateLogo;
	uint32_t totalpoint=PIC_SIZE_WIDTH;
	totalpoint*=PIC_SIZE_HEIGHT; 			  //得到总点数
	
	Lcd_Driver.setcursor(TIPS_DISP_X,TIPS_DISP_Y,TIPS_DISP_X+PIC_SIZE_WIDTH-1,TIPS_DISP_Y+PIC_SIZE_HEIGHT-1);
	Lcd_Driver.writeCMD(0x2c00);
	
	LCD_RS_G();
	LCD_CS_D();	

	LCD_LOCK_OTHER_PORT();//锁住无关端口
	//写入数据
	uint32_t i;
	for(i=0;i<totalpoint;i++){
		GPIO0->DT=(uint32_t)(*data++)<<16;
		LCD_WR_G();		
	}
	LCD_UNLOCK_OTHER_PORT();//释放无关端口
	LCD_CS_G();
}
#endif

void CheckUpade(void)
{
	uint32_t readBytes=0; //读取文件字节数
	uint32_t resBytes=0;	//文件字节数
	uint32_t writeAddr=0;
	uint32_t filePointer=0;
	uint32_t start_Sector=0;
	uint32_t count_Sector=0;//擦除flash的起始扇区和扇区个数

	#ifdef UPDATE_LCD_TIPS_ON
	uint8_t tips_flag=0;
	#endif

	QSPI0->BUSY_DLY &= 0xFFFFF0FF;	//clean [11:8]
	QSPI0->BUSY_DLY |= (1<<8);
	qspi_flash_global_unprotect(QSPI0);//以防万一Flash默认值不是可写入	
	
	FRESULT res=f_mount(&SD_Fatfs,"0:",1);//挂在SD 1挂载 0移除
	if(res==FR_OK){
		//挂载SD卡成功 
		printf("mount sd ok\r\n");
		
		res = f_open(&ResourceFile,"0:/App.bin",FA_READ);//尝试读取资源文件 固定文件名 GuiRes.bin
		if(res==FR_OK){

			#ifdef UPDATE_LCD_TIPS_ON
			LCD_Init();
			Lcd_Driver.clear();
			disp_Tip_Logo();
			tips_flag=1;
			#endif

			//发现一个新的资源文件
			resBytes=(uint32_t)ResourceFile.fsize;//得到文件大小
			printf("find app code file ,size:%dByte\r\n",resBytes);
			//开始擦除扇区
			writeAddr=FLASHAPP_START_ADDR;
			start_Sector = writeAddr/4096;
			count_Sector = resBytes/4096;
			if(resBytes%4096)count_Sector++;
			printf("erase app code flash\r\n");
			for(uint32_t i=start_Sector;i<start_Sector+count_Sector;i++){
				qspi_flash_erase_block_4k(QSPI0,i*4096);
			}
			printf("write new app code to flash\r\n");
			//开始写入数据
			filePointer=0;
			while(1){
				//printf("filePointer: %d\r\n",filePointer);
				readBytes=0;
				f_read(&ResourceFile,FlashWriteBuff,WRITEPAGE_MAX,&readBytes);
				if(readBytes!=0){
					qspi_flash_write_page(QSPI0,FlashWriteBuff,writeAddr,readBytes);//这个函数位于 m1130_xip_write.c中，被连接到flash前8K 
					writeAddr+=readBytes;
					filePointer+=readBytes;
					f_lseek(&ResourceFile,filePointer);//移动文件指针
				}
				else{
					break;
				}
			}
			f_close(&ResourceFile);
		}
		res = f_open(&ResourceFile,"0:/ResGui.bin",FA_READ);//尝试读取资源文件 固定文件名 GuiRes.bin
		if(res==FR_OK){

			#ifdef UPDATE_LCD_TIPS_ON
			if(tips_flag==0){
				LCD_Init();
				Lcd_Driver.clear();
				disp_Tip_Logo();
				tips_flag=1;
			}
			#endif

			//发现一个新的资源文件
			resBytes=(uint32_t)ResourceFile.fsize;//得到文件大小
			printf("find resource file ,size:%dByte\r\n",resBytes);
			//开始擦除扇区
			writeAddr=FLASHRES_START_ADDR;
			start_Sector = writeAddr/4096;
			count_Sector = resBytes/4096;
			if(resBytes%4096)count_Sector++;
			printf("erase resource flash\r\n");
			for(uint32_t i=start_Sector;i<start_Sector+count_Sector;i++){
				qspi_flash_erase_block_4k(QSPI0,i*4096);
			}
			printf("write new resource to flash\r\n");
			//开始写入数据
			filePointer=0;
			while(1){
				//printf("filePointer: %d\r\n",filePointer);
				readBytes=0;
				f_read(&ResourceFile,FlashWriteBuff,WRITEPAGE_MAX,&readBytes);
				if(readBytes!=0){
					qspi_flash_write_page(QSPI0,FlashWriteBuff,writeAddr,readBytes);//这个函数位于 m1130_xip_write.c中，被连接到flash前8K
					writeAddr+=readBytes;
					filePointer+=readBytes;
					f_lseek(&ResourceFile,filePointer);//移动文件指针
				}
				else{
					break;
				}
			}
			f_close(&ResourceFile);
		}
		f_mount(&SD_Fatfs,"0:",0);
	}
	else printf("mount sd error\r\n");
}
