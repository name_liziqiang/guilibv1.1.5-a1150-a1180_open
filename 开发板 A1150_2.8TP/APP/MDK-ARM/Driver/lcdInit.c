#include "lcdInit.h"
#include "lcdDriver.h"
#include "m1130_gpio.h"
#include "m1130_tim.h"
#include "stdio.h"
void LCD_DIM_init(uint16_t arr,uint16_t psc,uint16_t pulse)
{
	//TIM15  CH1 输出PWM
	RCC->AHBCLKCTRL0_SET|=(1<<24);//开启定时器时钟
	RCC->AHBCLKCTRL0_SET|=(1<<3);
	RCC->PWMCLKDIV = 0X01;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	GPIO_InitTypeDef GPIO_InitStructure; 
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Function = GPIO_FUNCTION_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_Init(GPIO1, &GPIO_InitStructure);
	
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Prescaler=psc;//96Mhz/(95+1)=1Mhz
	TIM_TimeBaseStructure.TIM_Period=arr ;//fpwm=9600/(1199+1)=80Khz
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);
	
	//SYSAHBCLKDIV=4 在SystemInit已经初始化了 AHB时钟96Mhz	
	TIM_OCStructInit(&TIM_OCInitStructure);
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; //选择定时器模式:TIM脉冲宽度调制模式2
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; //比较输出使能
	TIM_OCInitStructure.TIM_Pulse = pulse; //设置待装入捕获比较寄存器的脉冲值
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; //输出极性:TIM输出比较极性高
	TIM_OC1Init(TIM4, &TIM_OCInitStructure);  //根据TIM_OCInitStruct中指定的参数初始化外设TIMx     //TIM4_CH1
	
	TIM_OC1PreloadConfig(TIM4,TIM_OCPreload_Enable);
	TIM_ARRPreloadConfig(TIM4,ENABLE);
	TIM_Cmd(TIM4,ENABLE);
}

void LcdSetDim(uint16_t pulse)
{
	TIM_SetCompare1(TIM4 , pulse);
}

/*
*********************************************************************************************************************
*函数功能：LCD_Init，LCD初始化，初始化LCD的硬件
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
void LCD_Init(void)
{
	//初始化GPIO
	GPIO0_IT->DIR_SET=0xff000000;//DB0~15输出 
//	GPIO_SetPinMux(GPIO1, GPIO_Pin_10,GPIO_FUNCTION_0);//LCD_BG 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_11,GPIO_FUNCTION_0);//LCD_RD 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_4,GPIO_FUNCTION_0);//LCD_WR 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_26,GPIO_FUNCTION_0);//LCD_RS 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_27,GPIO_FUNCTION_0);//LCD_CS 
	
	GPIO_SetPinDir(GPIO0, GPIO_Pin_4,GPIO_Mode_OUT);//LCD_WR 
//	GPIO_SetPinDir(GPIO1, GPIO_Pin_10,GPIO_Mode_OUT);//LCD_BG 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_11,GPIO_Mode_OUT);//LCD_RD 	
	GPIO_SetPinDir(GPIO1, GPIO_Pin_26,GPIO_Mode_OUT);//LCD_RS 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_27,GPIO_Mode_OUT);//LCD_CS 	
	
	GPIO_SetPinMux(GPIO0, GPIO_Pin_5,GPIO_FUNCTION_0);//QSPI1_TF_CS 
	GPIO_SetPinDir(GPIO0, GPIO_Pin_5,GPIO_Mode_OUT);  //QSPI1_TF_CS 
	GPIO_SetPin(GPIO0, GPIO_Pin_5);
//	LCD_BG_CLOSE();
	Lcd_Driver.writeCMD(0x01);//software reset

	
	//关闭显示
	Lcd_Driver.writeCMD(0x28); 
	//Set the gray scale voltage to adjust the gamma characteristics of the TFT panel
	//设置灰度电压，调整TFT面板的伽马特性
	Lcd_Driver.writeCMD(0xCF);  
	Lcd_Driver.writeData(0x00); 
	Lcd_Driver.writeData(0xC1); 
	Lcd_Driver.writeData(0X30);
	
	Lcd_Driver.writeCMD(0xED);  
	Lcd_Driver.writeData(0x64); 
	Lcd_Driver.writeData(0x03); 
	Lcd_Driver.writeData(0X12); 
	Lcd_Driver.writeData(0X81); 
	
	Lcd_Driver.writeCMD(0xE8);  
	Lcd_Driver.writeData(0x85); 
	Lcd_Driver.writeData(0x10); 
	Lcd_Driver.writeData(0x7A);
	
	Lcd_Driver.writeCMD(0xCB);  
	Lcd_Driver.writeData(0x39); 
	Lcd_Driver.writeData(0x2C); 
	Lcd_Driver.writeData(0x00); 
	Lcd_Driver.writeData(0x34); 
	Lcd_Driver.writeData(0x02); 
	
	Lcd_Driver.writeCMD(0xF7);  
	Lcd_Driver.writeData(0x20); 
	
	Lcd_Driver.writeCMD(0xEA);  
	Lcd_Driver.writeData(0x00); 
	Lcd_Driver.writeData(0x00);
	//设置VREG1OUT,VREG2OUT	
	Lcd_Driver.writeCMD(0xC0);    //Power control 
	Lcd_Driver.writeData(0x1B);   //VRH[5:0] 
	//设置升压电路中使用的系数
	Lcd_Driver.writeCMD(0xC1);    //Power control 
	Lcd_Driver.writeData(0x01);   //SAP[2:0];BT[3:0] 
	
	Lcd_Driver.writeCMD(0xC5);    //VCM control 
	Lcd_Driver.writeData(0x30); 	 //3F
	Lcd_Driver.writeData(0x30); 	 //3C
	
	Lcd_Driver.writeCMD(0xC7);    //VCM control2 
	Lcd_Driver.writeData(0XB7); 
	//此命令定义帧内存的读/写扫描方向
	Lcd_Driver.writeCMD(0x36);    // Memory Access Control 
	Lcd_Driver.writeData(0x28);
	//接口的像素格式	
	Lcd_Driver.writeCMD(0x3A);   
	Lcd_Driver.writeData(0x55); 
	//Frame Rate Control帧速率控制
	Lcd_Driver.writeCMD(0xB1);   
	Lcd_Driver.writeData(0x00);   
	Lcd_Driver.writeData(0x1A);
	//Display Function Control显示功能控制
	Lcd_Driver.writeCMD(0xB6);    // Display Function Control 
	Lcd_Driver.writeData(0x82); //0x0A 
	Lcd_Driver.writeData(0x02); //0xA2 
	Lcd_Driver.writeCMD(0xF2);    // 3Gamma Function Disable 
	Lcd_Driver.writeData(0x00); 
	Lcd_Driver.writeCMD(0x26);    //Gamma curve selected 
	Lcd_Driver.writeData(0x01); 
	//设置灰度电压，调整TFT面板的伽马特性
	Lcd_Driver.writeCMD(0xE0);    //Set Gamma 
	Lcd_Driver.writeData(0x0F); 
	Lcd_Driver.writeData(0x2A); 
	Lcd_Driver.writeData(0x28); 
	Lcd_Driver.writeData(0x08); 
	Lcd_Driver.writeData(0x0E); 
	Lcd_Driver.writeData(0x08); 
	Lcd_Driver.writeData(0x54); 
	Lcd_Driver.writeData(0XA9); 
	Lcd_Driver.writeData(0x43); 
	Lcd_Driver.writeData(0x0A); 
	Lcd_Driver.writeData(0x0F); 
	Lcd_Driver.writeData(0x00); 
	Lcd_Driver.writeData(0x00); 
	Lcd_Driver.writeData(0x00); 
	Lcd_Driver.writeData(0x00); 
	//设置灰度电压，调整TFT面板的伽马特性 	
	Lcd_Driver.writeCMD(0XE1);    //Set Gamma 
	Lcd_Driver.writeData(0x00); 
	Lcd_Driver.writeData(0x15); 
	Lcd_Driver.writeData(0x17); 
	Lcd_Driver.writeData(0x07); 
	Lcd_Driver.writeData(0x11); 
	Lcd_Driver.writeData(0x06); 
	Lcd_Driver.writeData(0x2B); 
	Lcd_Driver.writeData(0x56); 
	Lcd_Driver.writeData(0x3C); 
	Lcd_Driver.writeData(0x05); 
	Lcd_Driver.writeData(0x10); 
	Lcd_Driver.writeData(0x0F); 
	Lcd_Driver.writeData(0x3F); 
	Lcd_Driver.writeData(0x3F); 
	Lcd_Driver.writeData(0x0F); 
	Lcd_Driver.writeData(0x2B); 
	Lcd_Driver.writeData(0x00);
	Lcd_Driver.writeData(0x00);
	Lcd_Driver.writeData(0x00);
	Lcd_Driver.writeData(0xef);	
	Lcd_Driver.writeData(0x2A); 
	Lcd_Driver.writeData(0x00);
	Lcd_Driver.writeData(0x00);
	Lcd_Driver.writeData(0x01);
	Lcd_Driver.writeData(0x3f);	 
	//此命令关闭睡眠模式
	Lcd_Driver.writeCMD(0x11); 
	//开启反显，正常0000显示黑色，ffff显示白色，如果不发这个命令，会显示反的
	Lcd_Driver.writeCMD(0x21); 
	//开启显示
	Lcd_Driver.writeCMD(0x29); 

	Lcd_Driver.scan_dir(DIR_HORIZONTAL);//测试程序切换到横屏
//	LCD_BG_OPEN();	
	LCD_DIM_init(100,95,LCD_LuminanceMax);  //10Khz	 
	printf("LCD_DIM_init OK\r\n");

}
