#include "TouchInit.h"
#include "m1130_gpio.h"
#include "m1130_qspi.h"
#include "m1130_tim.h"

/*
*********************************************************************************************************************
*函数功能：TouchInit，触摸控制器启动初始化
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/ 

void TouchInit(void)
{
#ifdef TOUCH_USE_SPI
	QSPI_InitTypeDef QSPI_InitStructure;
	GPIO_SetPinMux(GPIO0, GPIO_Pin_5,GPIO_FUNCTION_0);//QSPI1_TF_CS 
	GPIO_SetPinDir(GPIO0, GPIO_Pin_5,GPIO_Mode_OUT);  //QSPI1_TF_CS 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_28,GPIO_FUNCTION_0);//QSPI1_XPT_CS 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_28,GPIO_Mode_OUT);  //QSPI1_XPT_CS 
	GPIO_SetPin(GPIO0, GPIO_Pin_5);//QSPI1_TF_CS 
	GPIO_SetPin(GPIO1, GPIO_Pin_28);//QSPI1_XPT_CS 
	
	QSPI_InitStructure.QSPI_SlaveMode=QSPI_MASTER_MODE;
	QSPI_InitStructure.QSPI_FrameLength=QSPI_FRAME_LENGTH_8Bit;
	QSPI_InitStructure.QSPI_ModeSelect=QSPI_STD;
	QSPI_InitStructure.QSPI_CPOL = QSPI_CPOL_Low;
	QSPI_InitStructure.QSPI_CPHA = QSPI_CPHA_1Edge;
	QSPI_InitStructure.QSPI_FirstBit = QSPI_FirstBit_MSB;
	QSPI_InitStructure.QSPI_ClockDiv = (96<<8);
	QSPI_InitStructure.QSPI_ClockRate = 0;//48/(48*(1+0));//1mhz
	RCC->QUADSPI1CLKDIV = 8;//384/8=48mhz
	
	GPIO_SetPinMux(GPIO0, GPIO_Pin_13, GPIO_FUNCTION_3);//QSPI1_MOSI 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_7, GPIO_FUNCTION_3);//QSPI_CLK 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_6, GPIO_FUNCTION_3);//QSPI1_MISO 	
	
	QSPI_HwInit(QSPI1, &QSPI_InitStructure);            //初始化QSPI1
	
	QSPI1->BUSY_DLY &= 0XFFFFFF00;
	QSPI1->BUSY_DLY |= (8<<0);
	QSPI1->BUSY_DLY &= 0xFFFFF0FF;	//clean [11:8]
	QSPI1->BUSY_DLY |= (1<<8);      //设置一个合适的CAP_DLY
	
#else	
	GPIO_SetPinMux(GPIO1, GPIO_Pin_28,GPIO_FUNCTION_0);//QSPI1_XPT_CS 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_28,GPIO_Mode_OUT);  //QSPI1_XPT_CS 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_5,GPIO_FUNCTION_0);//QSPI1_TF_CS 
	GPIO_SetPinDir(GPIO0, GPIO_Pin_5,GPIO_Mode_OUT);  //QSPI1_TF_CS 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_6,GPIO_FUNCTION_0);//QSPI1_MISO 
	GPIO_SetPinDir(GPIO0, GPIO_Pin_6,GPIO_Mode_IN);   //QSPI1_MISO 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_13,GPIO_FUNCTION_0);//QSPI1_MOSI 
	GPIO_SetPinDir(GPIO0, GPIO_Pin_13,GPIO_Mode_OUT);  //QSPI1_MOSI 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_7,GPIO_FUNCTION_0);//QSPI_CLK 
	GPIO_SetPinDir(GPIO0, GPIO_Pin_7,GPIO_Mode_OUT);  //QSPI_CLK 	
#endif
}

void TouchScan_Init(void)//初始化一个定时器17作为触摸扫描周期
{
	
	RCC->AHBCLKCTRL0_SET|=(1<<20);//开启定时器时钟
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Prescaler=95;//96Mhz/(95+1)=1Mhz
	TIM_TimeBaseStructure.TIM_Period=1000;//计数到1000刚好是1ms 中断
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	//SYSAHBCLKDIV=4 在SystemInit已经初始化了 AHB时钟96Mhz
	TIM_TimeBaseInit(TIM17, &TIM_TimeBaseStructure);
	TIM_SelectOutputTrigger(TIM17, TIM_TRGOSource_Enable);//选择TIM17的update事件更新为触发源
	TIM_ClearITPendingBit(TIM17, TIM_IT_Update);     			//清除update事件中断标志
	TIM_ITConfig(TIM17, TIM_IT_Update, ENABLE);       			//使能TIM17中断 
	//中断配置
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel=TIM17_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPriority=0;//2bit位宽
	NVIC_Init(&NVIC_InitStruct);
	TIM_Cmd(TIM17,ENABLE);
}

