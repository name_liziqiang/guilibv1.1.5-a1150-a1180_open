#ifndef _DemoPage1_h
#define _DemoPage1_h
#include "gWidgetInfo.h"
#include "gStdint.h"
#include "gReturn.h"
#include "gResMsg.h"
#define FPwID 0x1001
static const PAGE_INFO FridgePage={
	{.wType = WIDGET_TYPE_PAGE , .wId = FPwID , .wVscope = WIDGET_VS_PRIVATE},
	{.x = 0 , .y = 0 , .width = 320 , .height = 240},
	.backMode = WIDGET_BACKMODE_PIC , 
	.backColor = 0x0000,
	.picId = 12,
	.enterEvent = GUI_NULL,
	.leaveEvent = GUI_NULL,
	.pressEvent = GUI_NULL,
	.releaseEvent = GUI_NULL,
};
#define FPNumBox1wID 0x1002
static const NUMBERBOX_INFO FridgePageNumberBox1={
 {.wType = WIDGET_TYPE_NUMBERBOX , .wId = FPNumBox1wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 85 , .y = 126 , .width = 35 , .height = 17},
 .backMode = WIDGET_BACKMODE_CUTPIC,              
 .keyId = 255,                          
 .fontlibId = 0,                      
 .borderWidth = 1,                   
 .borderColor = 0xFFDC,              
 .picId = 12,                         
 .backColor = 0x00FF,
 .frontColor = 0x0000,
 .xAlign = 1,                        
 .yAlign = 1, 
 .formatType = 0,                     
 .isDr = 0,                           
 .data = 05,
 .maxLen = 2,                      
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL 
};
#define FPNumBox2wID 0x1003
static const NUMBERBOX_INFO FridgePageNumberBox2={
 {.wType = WIDGET_TYPE_NUMBERBOX , .wId = FPNumBox2wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 85 , .y = 156 , .width = 35 , .height = 17},
 .backMode = WIDGET_BACKMODE_CUTPIC,              
 .keyId = 255,                          
 .fontlibId = 0,                      
 .borderWidth = 1,                   
 .borderColor = 0xFFDC,              
 .picId = 12,                         
 .backColor = 0x00FF,
 .frontColor = 0x0000,
 .xAlign = 1,                        
 .yAlign = 1, 
 .formatType = 0,                     
 .isDr = 0,                           
 .data = 00,
 .maxLen = 2,                      
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL 
};
#define FPNumBox3wID 0x1004
static const NUMBERBOX_INFO FridgePageNumberBox3={
 {.wType = WIDGET_TYPE_NUMBERBOX , .wId = FPNumBox3wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 85 , .y = 186 , .width = 35 , .height = 17},
 .backMode = WIDGET_BACKMODE_CUTPIC,              
 .keyId = 255,                          
 .fontlibId = 0,                      
 .borderWidth = 1,                   
 .borderColor = 0xFFDC,              
 .picId = 12,                         
 .backColor = 0x00FF,
 .frontColor = 0x0000,
 .xAlign = 1,                        
 .yAlign = 1, 
 .formatType = 0,                     
 .isDr = 0,                           
 .data = 18,
 .maxLen = 3,                      
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL 
};
#define FPSetButtonwID 0x1005
static const BUTTON_INFO FridgePageSetButton={
 {.wType = WIDGET_TYPE_BUTTON , .wId = FPSetButtonwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 142 , .y = 17 , .width = 69 , .height = 68},
 .backMode = WIDGET_BACKMODE_CUTPIC,
 .fontlibId = 1,              
 .borderWidth = 1,              
 .borderColor = 0xFFDC,        
 .pressPicId  = 13,             
 .releasePicId = 12,             
 .pressBackColor = 0x0020,
 .releaseBackColor = 0xBDF7,   
 .pressFontColor = 0xFFFF,        
 .releaseFontColor = 0x0000,      
 .xAlign = 1,
 .yAlign = 1,
 .isDr = 1,                   
 .text = (char *)0x04,
 .maxLen = 0x06,
 .pressEvent = GUI_NULL,
 .releaseEvent = GUI_NULL,
 .updateEvent = GUI_NULL,
};
#define FPNewsButtonwID 0x1006
static const BUTTON_INFO FridgePageNewsButton={
 {.wType = WIDGET_TYPE_BUTTON , .wId = FPNewsButtonwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 213 , .y = 17 , .width = 69 , .height = 68},
 .backMode = WIDGET_BACKMODE_CUTPIC,
 .fontlibId = 1,              
 .borderWidth = 1,              
 .borderColor = 0xFFDC,        
 .pressPicId  = 13,             
 .releasePicId = 12,            
 .pressBackColor = 0x0020,
 .releaseBackColor = 0xBDF7,   
 .pressFontColor = 0xFFFF,        
 .releaseFontColor = 0x0000,      
 .xAlign = 1,
 .yAlign = 1,
 .isDr = 1,                   
 .text = (char *)0x04,
 .maxLen = 0x06,
 .pressEvent = GUI_NULL,
 .releaseEvent = GUI_NULL,
 .updateEvent = GUI_NULL,
};
#define FPMsgButtonwID 0x1007
static const BUTTON_INFO FridgePageMsgButton={
 {.wType = WIDGET_TYPE_BUTTON , .wId = FPMsgButtonwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 142 , .y = 88 , .width = 69 , .height = 68},
 .backMode = WIDGET_BACKMODE_CUTPIC,
 .fontlibId = 1,              
 .borderWidth = 1,              
 .borderColor = 0xFFDC,        
 .pressPicId  = 13,             
 .releasePicId = 12,             
 .pressBackColor = 0x0020,
 .releaseBackColor = 0xBDF7,   
 .pressFontColor = 0xFFFF,        
 .releaseFontColor = 0x0000,      
 .xAlign = 1,
 .yAlign = 1,
 .isDr = 1,                   
 .text = (char *)0x04,
 .maxLen = 0x06,
 .pressEvent = GUI_NULL,
 .releaseEvent = GUI_NULL,
 .updateEvent = GUI_NULL,
};
#define FPDateButtonwID 0x1008
static const BUTTON_INFO FridgePageDateButton={
 {.wType = WIDGET_TYPE_BUTTON , .wId = FPDateButtonwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 213 , .y = 88 , .width = 69 , .height = 68},
 .backMode = WIDGET_BACKMODE_CUTPIC,
 .fontlibId = 1,              
 .borderWidth = 1,              
 .borderColor = 0xFFDC,        
 .pressPicId  = 13,             
 .releasePicId = 12,              
 .pressBackColor = 0x0020,
 .releaseBackColor = 0xBDF7,   
 .pressFontColor = 0xFFFF,        
 .releaseFontColor = 0x0000,      
 .xAlign = 1,
 .yAlign = 1,
 .isDr = 1,                   
 .text = (char *)0x04,
 .maxLen = 0x06,
 .pressEvent = GUI_NULL,
 .releaseEvent = GUI_NULL,
 .updateEvent = GUI_NULL,
};
#define FPMenuButtonwID 0x1009
static const BUTTON_INFO FridgePageMenuButton={
 {.wType = WIDGET_TYPE_BUTTON , .wId = FPMenuButtonwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 213 , .y = 160 , .width = 140 , .height = 68},
 .backMode = WIDGET_BACKMODE_CUTPIC,
 .fontlibId = 1,              
 .borderWidth = 1,              
 .borderColor = 0xFFDC,        
 .pressPicId  = 13,             
 .releasePicId = 12,             
 .pressBackColor = 0x0020,
 .releaseBackColor = 0xBDF7,   
 .pressFontColor = 0xFFFF,        
 .releaseFontColor = 0x0000,      
 .xAlign = 1,
 .yAlign = 1,
 .isDr = 1,                   
 .text = (char *)0x04,
 .maxLen = 0x06,
 .pressEvent = GUI_NULL,
 .releaseEvent = GUI_NULL,
 .updateEvent = GUI_NULL,
};
#define FPModeButtonwID 0x100A
static const BUTTON_INFO FridgePageModeButton={
 {.wType = WIDGET_TYPE_BUTTON , .wId = FPModeButtonwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 75 , .y = 211 , .width = 50 , .height = 17},
 .backMode = WIDGET_BACKMODE_CUTPIC,
 .fontlibId = 0,              
 .borderWidth = 1,              
 .borderColor = 0xFFDC,        
 .pressPicId  = 13,             
 .releasePicId = 12,             
 .pressBackColor = 0x0020,
 .releaseBackColor = 0xBDF7,   
 .pressFontColor = 0xFFFF,        
 .releaseFontColor = 0x0000,      
 .xAlign = 1,
 .yAlign = 1,
 .isDr = 1,                   
 .text = (char *)0x04,
 .maxLen = 0x06,
 .pressEvent = GUI_NULL,
 .releaseEvent = GUI_NULL,
 .updateEvent = GUI_NULL,
};
#define FPTextBoxwID 0x100B
static const TEXTBOX_INFO FridgePageTextBox={
 {.wType = WIDGET_TYPE_TEXTBOX , .wId = FPTextBoxwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 100 , .y = 85 , .width = 30 , .height = 15},
 .backMode = WIDGET_BACKMODE_CUTPIC,                 
 .keyId = 255,                          
 .fontlibId = 0,                     
 .borderWidth = 0,                   
 .borderColor = 0x00FF,              
 .picId = 12,                         
 .backColor = 0x0000,
 .frontColor = 0xFFEF,
 .xAlign = 1,                        
 .yAlign = 1,                       
 .isPassword = 0,                  
 .isDr = 0,   
 .text = (char *)0x00,                 
 .maxLen = 0x00014,                   
 .textLen = 0x0005,            
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL                
};
#define FPTextBox2wID 0x100C
static const TEXTBOX_INFO FridgePageTextBox2={
 {.wType = WIDGET_TYPE_TEXTBOX , .wId = FPTextBox2wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 20 , .y = 50 , .width = 75 , .height = 30},
 .backMode = WIDGET_BACKMODE_CUTPIC,                 
 .keyId = 255,                          
 .fontlibId = 0,                     
 .borderWidth = 1,                   
 .borderColor = 0x00FF,              
 .picId = 12,                         
 .backColor = 0x0000,
 .frontColor = 0xFFEF,
 .xAlign = 1,                        
 .yAlign = 1,                       
 .isPassword = 1,                  
 .isDr = 1,   
 .text = (char *)0x00,                 
 .maxLen = 0x0014,                   
 .textLen = 0x0005,            
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL                
};
#define FPCutPicwID 0x100D
static const CUTPICTURE_INFO FridgePageCutPic= {
 {.wType = WIDGET_TYPE_CUTPICTURE , .wId = FPCutPicwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 20 , .y = 105 , .width = 41 , .height = 50},
 .picId = 16,                          
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL 
};
#define FPGTimerwID 0x100E
static const GTIMER_INFO FridgePageGTimer={
 {.wType = WIDGET_TYPE_GTIMER , .wId = FPGTimerwID , .wVscope = WIDGET_VS_PRIVATE},
 .timeOut = 500,
 .enable = 0,                         //定时器开关位，0关，1开
 .timeValue = 0,                     //定时器当前计数值（周期是1MS）低16位是当前计数器值，高16位是定时器溢出标志，或者其他标识
 .timeoutEvent = GUI_NULL
}; 
//#define FPOptionBox1wID 0x100F
//static const OPTIONBOX_INFO FridgePageOptionBox1={   
// {.wType = WIDGET_TYPE_OPTIONBOX , .wId = FPOptionBox1wID , .wVscope = WIDGET_VS_PRIVATE},
// {.x = 113 , .y = 168 , .width = 20 , .height = 20},
// .backMode = WIDGET_BACKMODE_COLOR ,  
// .optionType = OPTIONBOX_TYPE_CIRCLE,
// .backColor = 0xFFFF,
// .frontColor = 0x97F3,
// .cPicId = 0,                      
// .nocPicId = 1,                    
// .value = OPTIONBOX_STATE_NOCHOOSE,
// .pressEvent = GUI_NULL,               
// .releaseEvent = GUI_NULL,             
// .updateEvent = GUI_NULL       
//};
//#define FPOptionBox2wID 0x1010
//static const OPTIONBOX_INFO FridgePageOptionBox2={   
// {.wType = WIDGET_TYPE_OPTIONBOX , .wId = FPOptionBox2wID , .wVscope = WIDGET_VS_PRIVATE},
// {.x = 113 , .y = 208 , .width = 20 , .height = 20},
// .backMode = WIDGET_BACKMODE_COLOR ,  
// .optionType = OPTIONBOX_TYPE_CIRCLE,
// .backColor = 0xFFFF,
// .frontColor = 0xFFF0,
// .cPicId = 5,                      
// .nocPicId = 1,                    
// .value = OPTIONBOX_STATE_NOCHOOSE,
// .pressEvent = GUI_NULL,               
// .releaseEvent = GUI_NULL,             
// .updateEvent = GUI_NULL       
//};
//#define FPOptionBox3wID 0x1011
//static const OPTIONBOX_INFO FridgePageOptionBox3={   
// {.wType = WIDGET_TYPE_OPTIONBOX , .wId = FPOptionBox3wID , .wVscope = WIDGET_VS_PRIVATE},
// {.x = 113 , .y = 247 , .width = 20 , .height = 20},
// .backMode = WIDGET_BACKMODE_COLOR ,  
// .optionType = OPTIONBOX_TYPE_CIRCLE,
// .backColor = 0xFFFF,
// .frontColor = 0xDDFF,
// .cPicId = 0,                      
// .nocPicId = 1,                    
// .value = OPTIONBOX_STATE_NOCHOOSE,
// .pressEvent = GUI_NULL,               
// .releaseEvent = GUI_NULL,             
// .updateEvent = GUI_NULL       
//};
#define FPOptionBox1wID 0x100F
static const OPTIONBOX_INFO FridgePageOptionBox1={   
 {.wType = WIDGET_TYPE_OPTIONBOX , .wId = FPOptionBox1wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 80 , .y = 20 , .width = 15 , .height = 15},
 .backMode = WIDGET_BACKMODE_COLOR ,  
 .optionType = OPTIONBOX_TYPE_CIRCLE,
 .backColor = 0xFFFF,
 .frontColor = 0x0000,
 .cPicId = 0,                      
 .nocPicId = 1,                    
 .value = OPTIONBOX_STATE_NOCHOOSE,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL       
};
#define FPOptionBox2wID 0x1010
static const OPTIONBOX_INFO FridgePageOptionBox2={   
 {.wType = WIDGET_TYPE_OPTIONBOX , .wId = FPOptionBox2wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 80 , .y = 40 , .width = 15 , .height = 15},
 .backMode = WIDGET_BACKMODE_COLOR ,  
 .optionType = OPTIONBOX_TYPE_CIRCLE,
 .backColor = 0xFFFF,
 .frontColor = 0x0000,
 .cPicId = 5,                      
 .nocPicId = 1,                    
 .value = OPTIONBOX_STATE_NOCHOOSE,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL       
};
#define FPOptionBox3wID 0x1011
static const OPTIONBOX_INFO FridgePageOptionBox3={   
 {.wType = WIDGET_TYPE_OPTIONBOX , .wId = FPOptionBox3wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 80 , .y = 60 , .width = 15 , .height = 15},
 .backMode = WIDGET_BACKMODE_COLOR ,  
 .optionType = OPTIONBOX_TYPE_CIRCLE,
 .backColor = 0xFFFF,
 .frontColor = 0x0000,
 .cPicId = 0,                      
 .nocPicId = 1,                    
 .value = OPTIONBOX_STATE_NOCHOOSE,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL       
};
#define FPTextBox3wID 0x1012
static const TEXTBOX_INFO FridgePageTextBox3={
 {.wType = WIDGET_TYPE_TEXTBOX , .wId = FPTextBox3wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 100 , .y = 105 , .width = 35 , .height = 17},
 .backMode = WIDGET_BACKMODE_CUTPIC,                 
 .keyId = 255,                          
 .fontlibId = 0,                     
 .borderWidth = 0,                   
 .borderColor = 0x00FF,              
 .picId = 12,                         
 .backColor = 0x0000,
 .frontColor = 0xFFEF,
 .xAlign = 1,                        
 .yAlign = 1,                       
 .isPassword = 0,                  
 .isDr = 0,   
 .text = (char *)0x00,                 
 .maxLen = 9,                   
 .textLen = 0,            
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL                   
};
#define FPBackButtonwID 0x1015
static const BUTTON_INFO FridgePageBackButton={
 {.wType = WIDGET_TYPE_BUTTON , .wId = FPBackButtonwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 280 , .y = 195 , .width = 40 , .height = 45},
 .backMode = WIDGET_BACKMODE_CUTPIC,
 .fontlibId = 1,              
 .borderWidth = 1,              
 .borderColor = 0xFFDC,        
 .pressPicId  = 13,             
 .releasePicId = 12,             
 .pressBackColor = 0x0020,
 .releaseBackColor = 0xBDF7,   
 .pressFontColor = 0xFFFF,        
 .releaseFontColor = 0x0000,      
 .xAlign = 1,
 .yAlign = 1,
 .isDr = 1,                   
 .text = (char *)0x04,
 .maxLen = 0x06,
 .pressEvent = GUI_NULL,
 .releaseEvent = GUI_NULL,
 .updateEvent = GUI_NULL,
};
#define FPTextBoxAwID 0x1016
static const TEXTBOX_INFO FridgePageTextBoxA={
 {.wType = WIDGET_TYPE_TEXTBOX , .wId = FPTextBoxAwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 100 , .y = 18 , .width = 35 , .height = 17},
 .backMode = WIDGET_BACKMODE_CUTPIC,                 
 .keyId = 255,                          
 .fontlibId = 0,                     
 .borderWidth = 0,                   
 .borderColor = 0x00FF,              
 .picId = 12,                         
 .backColor = 0x0000,
 .frontColor = 0xFFEF,
 .xAlign = 1,                        
 .yAlign = 1,                       
 .isPassword = 0,                  
 .isDr = 0,   
 .text = (char *)0x00,                 
 .maxLen = 0x00014,                   
 .textLen = 0x0005,            
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL                   
};
#define FPTextBoxBwID 0x1017
static const TEXTBOX_INFO FridgePageTextBoxB={
 {.wType = WIDGET_TYPE_TEXTBOX , .wId = FPTextBoxBwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 100 , .y = 38 , .width = 35 , .height = 17},
 .backMode = WIDGET_BACKMODE_CUTPIC,                 
 .keyId = 255,                          
 .fontlibId = 0,                     
 .borderWidth = 0,                   
 .borderColor = 0x00FF,              
 .picId = 12,                         
 .backColor = 0x0000,
 .frontColor = 0xFFEF,
 .xAlign = 1,                        
 .yAlign = 1,                       
 .isPassword = 0,                  
 .isDr = 0,   
 .text = (char *)0x00,                 
 .maxLen = 0x00014,                   
 .textLen = 0x0005,            
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL                   
};
#define FPTextBoxCwID 0x1018
static const TEXTBOX_INFO FridgePageTextBoxC={
 {.wType = WIDGET_TYPE_TEXTBOX , .wId = FPTextBoxCwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 100 , .y = 58 , .width = 35 , .height = 17},
 .backMode = WIDGET_BACKMODE_CUTPIC,                 
 .keyId = 255,                          
 .fontlibId = 0,                     
 .borderWidth = 0,                   
 .borderColor = 0x00FF,              
 .picId = 12,                         
 .backColor = 0x0000,
 .frontColor = 0xFFEF,
 .xAlign = 1,                        
 .yAlign = 1,                       
 .isPassword = 0,                  
 .isDr = 0,   
 .text = (char *)0x00,                 
 .maxLen = 0x00014,                   
 .textLen = 0x0005,            
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL                   
};
#define FPPicBox1wID 0x1019
static const PICTUREBOX_INFO FridgePagePicBox1={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = FPPicBox1wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 20 , .y = 105 , .width = 41 , .height = 50},    
 .picId = 95,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
#define FPPicBox2wID 0x101A
static const PICTUREBOX_INFO FridgePagePicBox2={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = FPPicBox2wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 20 , .y = 155 , .width = 41 , .height = 19},    
 .picId = 97,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
#define FPPicBox3wID 0x101B
static const PICTUREBOX_INFO FridgePagePicBox3={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = FPPicBox3wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 20 , .y = 174 , .width = 41 , .height = 38},    
 .picId = 99,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
gui_Err FridgePageLeaveEvent(gui_int32 argc , const char **argv);
gui_Err FridgePageEnterEvent(gui_int32 argc , const char **argv);

static gui_Err FridgePageMode_releaseEvent(gui_int32 argc , const char **argv);
static gui_Err BackToHome_releaseEvent(gui_int32 argc , const char **argv);
static gui_Err FridgePage_TimeOutEvent(gui_int32 argc , const char **argv);
static gui_Err FridgePageFlash_releaseEvent(gui_int32 argc , const char **argv);
static gui_Err FridgePageOption_releaseEvent(gui_int32 argc , const char **argv);
static gui_Err FridgePageOption1_pressEvent(gui_int32 argc , const char **argv);
static gui_Err FridgePageOption2_pressEvent(gui_int32 argc , const char **argv);
static gui_Err FridgePageOption3_pressEvent(gui_int32 argc , const char **argv);












#endif
