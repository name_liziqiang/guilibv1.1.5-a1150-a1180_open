#ifndef _Start_APP_h
#define _Start_APP_h
#include "gWidgetInfo.h"
#include "gStdint.h"
#include "gReturn.h"
#include "gResMsg.h"
//#define StartwID 0x0101
//static const PAGE_INFO StartPage={
//	{.wType = WIDGET_TYPE_PAGE , .wId = StartwID , .wVscope = WIDGET_VS_PRIVATE},
//	{.x = 0 , .y = 0 , .width = 480 , .height = 320},
//	.backMode = WIDGET_BACKMODE_PIC , 
//	.backColor = 0x00FF,
//	.picId = 34,
//	.enterEvent = GUI_NULL,
//	.leaveEvent = GUI_NULL,
//	.pressEvent = GUI_NULL,
//	.releaseEvent = GUI_NULL,
//};

//#define StartGTimerwID 0x0103
//static const GTIMER_INFO StartPageGTimer={
// {.wType = WIDGET_TYPE_GTIMER , .wId = StartGTimerwID , .wVscope = WIDGET_VS_PRIVATE},
// .timeOut = 200,
// .enable = 1,                         //定时器开关位，0关，1开
// .timeValue = 0,                     //定时器当前计数值（周期是1MS）低16位是当前计数器值，高16位是定时器溢出标志，或者其他标识
// .timeoutEvent = GUI_NULL
//};
//#define StartPicwID 0x0104
//static const PICTUREBOX_INFO StartPagePicBox={
// {.wType = WIDGET_TYPE_PICTUREBOX , .wId = StartPicwID , .wVscope = WIDGET_VS_PRIVATE},
// {.x = 215 , .y = 122 , .width = 50 , .height = 50},    
// .picId = 26,
// .pressEvent = GUI_NULL,               
// .releaseEvent = GUI_NULL,             
// .updateEvent = GUI_NULL  
//};
























/*****************************************紫芯**************************************/
#define LOGOwID 0x0202
static const PAGE_INFO LOGOPage={
	{.wType = WIDGET_TYPE_PAGE , .wId = LOGOwID , .wVscope = WIDGET_VS_PRIVATE},
	{.x = 0 , .y = 0 , .width = 320 , .height = 240},
	.backMode = WIDGET_BACKMODE_PIC , 
	.backColor = 0x00FF,
	.picId = 255,
	.enterEvent = GUI_NULL,
	.leaveEvent = GUI_NULL,
	.pressEvent = GUI_NULL,
	.releaseEvent = GUI_NULL,
};
#define LOGOPICwID 0x0203	
static const PICTUREBOX_INFO LOGOPIC={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = LOGOPICwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 255 , .y = 190 , .width = 40 , .height = 12},    
 .picId = 74,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
#define LOGOVIRTUALGIFwID 0x0204
static const VIRTUALGIF_INFO LOGOPageVIRTUALGIF={
 {.wType = WIDGET_TYPE_VIRTUALGIF , .wId = LOGOVIRTUALGIFwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 50 , .y = 74 , .width = 230 , .height = 46},    
 .startPicId = 130,
 .endPicId = 254,
 .playTime=50,
 .enable=1,
 .replayEnable=1,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL ,
 .timeoutEvent = GUI_NULL ,
};
#define LOGOGTimerwID 0x0205
static const GTIMER_INFO LOGOPageGTimer={
 {.wType = WIDGET_TYPE_GTIMER , .wId = LOGOGTimerwID , .wVscope = WIDGET_VS_PRIVATE},
 .timeOut = 50,
 .enable = 1,                         //定时器开关位，0关，1开
 .timeValue = 0,                     //定时器当前计数值（周期是1MS）低16位是当前计数器值，高16位是定时器溢出标志，或者其他标识
 .timeoutEvent = GUI_NULL
};
#define LOGOQRCodewID 0x0206
static const QRCODEBOX_INFO LOGOQRCode={
 {.wType = WIDGET_TYPE_QRCODEBOX , .wId = LOGOQRCodewID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 240 , .y = 160 , .width = 70 , .height = 70},  //width有效  height无效
 .dispLogo = 0,
 .backColor = 0x79d3,
 .frontColor = 0x0000,                     
 .logoPicId = 0,
 .text = (char *)0x00, 	                                          
 .maxLen = 30,                     
 .textLen = 0x0000,                      
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
void guiMainPageInit(void);

gui_Err LOGOPageEnterEvent(gui_int32 argc , const char **argv);
gui_Err LOGOPageLeaveEvent(gui_int32 argc , const char **argv);
static gui_Err GetGIF_timeoutEvent(gui_int32 argc , const char **argv);

#endif








