/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
//#include "string.h"
#include "m1130_uart.h"
#include "misc.h"
#include "m1130_gpio.h"
#include "lcddriver.h"
#include "lcdInit.h"
#include "guiFunc.h"
#include "systemuart.h"
#include "systemHeart.h"
#include "rtcDriver.h"
#include "Key.h"

extern void guiMainPageInit(void);
void outclk_init(void)
{
  GPIO_SetPinMux(GPIO0, GPIO_Pin_25, GPIO_FUNCTION_3);
  RCC->OUTCLKSEL = 0;
  RCC->OUTCLKDIV = 1;
  RCC->OUTCLKUEN = 0;
  RCC->OUTCLKUEN = 1;
}

/**
  * @brief  Main program.asd
  * @param  None
  * @retval None
  */
#include "lcdInterface.h"
#include "touchInterface.h"
#include "TouchInit.h"
//extern void i2c_test(void);

int main()
{
	//outclk_init();//内部晶振输出，用于校准内部振荡器
	//SystemInit 中将系统时钟初始化为96Mhz

	/*******************初始化一系列MCU外设，然后启动GUI*******************/
	SystemHeart_Init();//初始化一个定时器16作为系统心跳
	SystemUart_Init(115200);//初始化系统UART
	printf("APP Start Init\r\n");
	LCD_Init();//初始化LCD，并加装资源
	TouchInit();
	TouchScan_Init();
	RTC_Init();
	Key_init();
	guiInit();//初始化GUI库
//  guiGetRefreshColorTime(BLUE);
//	guiGetRefreshPicTime(1);
//    while(1);
	guiMainPageInit();
	while(1){
		guiMainLoop();
	}
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  printf("Wrong parameters value: file %s on line %d\r\n", file, line);
  /* Infinite loop */
  while (1)
  {
  }
}
#endif
