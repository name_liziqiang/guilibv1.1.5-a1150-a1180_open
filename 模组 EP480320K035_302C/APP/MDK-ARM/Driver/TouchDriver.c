#include "touchdriver.h"
#include "m1130.h"
#include "m1130_qspi.h"
#include "m1130_xip_write.h"
#include "m1130_gpio.h"
#include "m1130_i2c.h"
#include "m1130_utility_i2c.h"

uint8_t touchBuf[0x40];
uint8_t TouchPosBuf[6];
extern uint8_t Press_Sta;
extern uint8_t Press_Ms;
/*
*********************************************************************************************************************
*函数功能：TP_Write_Byte,触摸控制器写入一个字节，内部调用
*入口参数：num，写入的值
*出口参数：无
*********************************************************************************************************************
*/
#ifndef TOUCH_USE_SPI
static void TP_Write_Byte(uint8_t num)    
{  
	uint8_t count=0;   
	for(count=0;count<8;count++)  
	{ 	  
		if((num&0x80)!=0)TDIN_G();  
		else TDIN_D();   
		__NOP();__NOP();__NOP();__NOP();
		__NOP();__NOP();__NOP();__NOP();
		TCLK_D(); 	 
		num<<=1;    
		TCLK_G();		//上升沿有效	        
	}		 			    
}
#endif 
/*
*********************************************************************************************************************
*函数功能：TP_Read_AD，触摸控制器读取一次AD，内部调用
*入口参数：CMD，命令
*出口参数：uint16_t，读取到的AD值
*********************************************************************************************************************
*/ 
#ifdef TOUCH_USE_SPI
static void TouchDriverCS_Low(void)
{
	GPIO1->DT_CLR=GPIO_Pin_28;//QSPI1_XPT_CS 
}
static void TouchDriverCS_High(void)
{
	GPIO1->DT_SET=GPIO_Pin_28;//QSPI1_XPT_CS 
}
__ALIGN4 uint8_t Read_Buff[4];
static uint16_t TP_Read_AD(uint8_t CMD)	  
{ 
	uint32_t rData;

	TouchDriverCS_Low();
	Read_Buff[0]=CMD;
	xipTX(QSPI1,Read_Buff,1);	
	__NOP();__NOP();__NOP();__NOP();
	__NOP();__NOP();__NOP();__NOP();
	__NOP();__NOP();__NOP();__NOP();
	__NOP();__NOP();__NOP();__NOP();
	__NOP();__NOP();__NOP();__NOP();
	__NOP();__NOP();__NOP();__NOP();
	__NOP();__NOP();__NOP();__NOP();
	__NOP();__NOP();__NOP();__NOP();
	xipRX(QSPI1,Read_Buff,2);	
	
	rData=(Read_Buff[1]<<8)+Read_Buff[0];
	rData>>=4;

	TouchDriverCS_High();
	return (uint16_t)rData;
}
#else 
static uint16_t TP_Read_AD(uint8_t CMD)	  
{ 	 
	uint8_t count=0; 	  
	uint16_t Num=0; 
	uint32_t data;
	TCLK_D();		//先拉低时钟 	 
	TDIN_D(); 	//拉低数据线
	TCS_D(); 		//选中触摸屏IC
	TP_Write_Byte(CMD);//发送命令字
	__NOP();__NOP();__NOP();__NOP();
	__NOP();__NOP();__NOP();__NOP();
	__NOP();__NOP();__NOP();__NOP();
	TCLK_D();
	__NOP();__NOP();__NOP();__NOP();
	__NOP();__NOP();__NOP();__NOP();
	__NOP();__NOP();__NOP();__NOP();	
	TCLK_G();		//给1个时钟，清除BUSY	    	    
	TCLK_D();  	    
	for(count=0;count<16;count++)//读出16位数据,只有高12位有效 
	{ 				  
		Num<<=1; 	 
		TCLK_G();  //下降沿有效  	
		__NOP();__NOP();__NOP();__NOP();	
		__NOP();__NOP();__NOP();__NOP();
		__NOP();__NOP();__NOP();__NOP();		
		data=DOUTPROT;
		data&=DOUTPIN;
		if(data!=0)Num++; 		 		
		TCLK_D();
	}  	
	Num>>=4;   	//只有高12位有效.
	TCS_G();		  //释放片选	 
	return(Num);   
}
#endif

uint16_t touchReadX(void)
{
	return TP_Read_AD(0xd0);
}

uint16_t touchReadY(void)
{
	return TP_Read_AD(0x90);
}

uint8_t touchReadXY(uint16_t *x,uint16_t *y)
{	
	unsigned char tmp=0;
//     		GT911_RD_Reg(0x8140, data, 0x40);
//	if(I2C_Read(0XBB >> 1, (unsigned int)0x8140, 2, touchBuf, 0x40)<0)
	if(I2C_Read(0XBB >> 1, (unsigned int)0x814E, 2, TouchPosBuf, 6)<0)
	{
//				printf("read touch xy error\r\n");
				return 0;//read failed
  }
//	tmp = touchBuf[0x0E];
	tmp = TouchPosBuf[0];
//    printf("tmp 1:%d\r\n",tmp);
	if(tmp&0x80){	
//		Press_Sta = 1;
//		Press_Ms = 0;
//		printf("tmp 1:%d\r\n",tmp);
//		for(int i=0;i<0x40;i++)
//		{
//     printf("data[%d]=  %d \r\n",i,touchBuf[i]);
//		}
//		*x=(uint16_t)(touchBuf[17]<<8)+touchBuf[16];
//		*y=(uint16_t) (touchBuf[19]<<8)+touchBuf[18];
		*x=(uint16_t)(TouchPosBuf[3]<<8)+TouchPosBuf[2];
		*y=(uint16_t) (TouchPosBuf[5]<<8)+TouchPosBuf[4];
//					printf("xPos:%04d\r\n",*x);
//					printf("yPos:%04d\r\n",*y);		
            tmp = 0;
            I2C_Write(0XBA >> 1, (unsigned int)0x814E, 2, &tmp, 1);
					return 1;
	}
  else 
	{
//		printf("tmp 2:%d\r\n",tmp);
//		*x=(uint16_t)(TouchPosBuf[3]<<8)+TouchPosBuf[2];
//		*y=(uint16_t) (TouchPosBuf[5]<<8)+TouchPosBuf[4];
		
		tmp = 0;
		I2C_Write(0XBA >> 1, (unsigned int)0x814E, 2, &tmp, 1);
    return 0;
	}

}

