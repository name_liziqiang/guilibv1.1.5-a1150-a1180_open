#include "TouchInit.h"
#include "m1130_gpio.h"
#include "m1130_qspi.h"
#include "m1130_tim.h"
#include "m1130_utility_i2c.h"
#include "stdio.h"
#include "string.h"
#include "delay.h"

//const unsigned char GT911_CFG_TBL[]=  
//{ 
//	0X42,0X10,0X01,0XE0,0X01,0X0A,0X05,0X00,0X01,0X08,
//	0X28,0X05,0X50,0X32,0X03,0X05,0X00,0X00,0X00,0X00,
//	0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X87,0X08,0X09,
//	0X17,0X15,0X31,0X0d,0X00,0X00,0X01,0X98,0X04,0X25,
//	0X00,0X00,0X00,0X00,0X00,0X03,0X64,0X32,0X00,0X00,
//	0X00,0X11,0X94,0X94,0Xc5,0X02,0X07,0X00,0X00,0X04,
//	0X70,0X15,0X00,0X69,0X21,0X00,0X63,0X33,0X00,0X5c,
//	0X4f,0X00,0X57,0X7a,0X00,0X57,0X00,0X00,0X00,0X00,
//	0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
//	0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
//	0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
//	0X00,0X00,0X06,0X10,0X08,0X04,0X02,0X12,0X0a,0X0c,
//	0X0e,0Xff,0Xff,0Xff,0Xff,0Xff,0X00,0X00,0X00,0X00,
//	0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
//	0X00,0X00,0X02,0X04,0X06,0X08,0X0a,0X1d,0X1e,0X20,
//	0X21,0X22,0X24,0X26,0X1f,0X0c,0X00,0Xff,0Xff,0Xff,
//	0Xff,0Xff,0Xff,0Xff,0Xff,0Xff,0Xff,0Xff,0X00,0X00,
//	0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
//	0X00,0X00,0X00,0X00
//};

void GT911_Interupt_Init(void)
{
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel=GPIO1_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPriority=1;//2bit位宽
	NVIC_Init(&NVIC_InitStruct);
//	GPIO_LevelITEnable(GPIO1_IT,GPIO_Pin_26,GPIO_IRQ_LEVEL_HIGH);//指定高电平中断方式    GPIO_Pin_31
	GPIO_EdgeITEnable(GPIO1_IT,GPIO_Pin_26,GPIO_IRQ_EDGE_FALLING);
//	GPIO_EdgeITClear(GPIO1_IT,GPIO_Pin_26); //GPIO_Pin_31
}

int GT911_RD_Reg(unsigned short int reg,unsigned char *buf,unsigned char len)
{     
	return I2C_Read(GT_CMD_RD>>1, (unsigned int)reg, 2, buf, len);
}

int GT911_WR_Reg(unsigned short int reg,unsigned char *buf,unsigned char len)
{     
	return I2C_Write(GT_CMD_WR>>1, (unsigned int)reg, 2, buf, len);
}

GT911_POINT_DATA GetPointData(unsigned char cnt, unsigned char data[])
{
	GT911_POINT_DATA  gt911_data;
     gt911_data.cnt = 0;
     switch(cnt){
         case 5:
             gt911_data.points[4].x = data[0x31]<<8 | data[0x30];
             gt911_data.points[4].y = data[0x33]<<8 | data[0x32];       
         case 4:
             gt911_data.points[3].x = data[0x29]<<8 | data[0x28];
             gt911_data.points[3].y = data[0x2B]<<8 | data[0x2A];       
         case 3:
             gt911_data.points[2].x = data[0x21]<<8 | data[0x20];
             gt911_data.points[2].y = data[0x23]<<8 | data[0x22];
         case 2:
             gt911_data.points[1].x = data[0x19]<<8 | data[0x18];
             gt911_data.points[1].y = data[0x1B]<<8 | data[0x1A];
         case 1:
             gt911_data.points[0].x = data[0x11]<<8 | data[0x10];
             gt911_data.points[0].y = data[0x13]<<8 | data[0x12];           
             break;
         default:
             break;
     }

    gt911_data.cnt = cnt;
	 return gt911_data;
}

/*********************************************************************************************************************
*函数功能：ShowPointData，打印坐标点信息
*入口参数：gt911_data-触摸坐标点信息
*出口参数：无
*********************************************************************************************************************/
void ShowPointData(GT911_POINT_DATA  gt911_data)
{
     unsigned char cnt = gt911_data.cnt;
     for(unsigned char i=0; i<cnt; i++){
         printf("Point%d(%d,%d)\t", i+1, gt911_data.points[i].x, gt911_data.points[i].y);
     }
     printf("\r\n");
     memset(&gt911_data, 0, sizeof(gt911_data));
}

/*******************************************************************************************************************
*函数功能：GT911_Send_Cfg，发送配置参数
*入口参数：mode-0,参数不保存到flash
*			     1,参数保存到flash
*出口参数：无
********************************************************************************************************************/
void GT911_Send_Cfg(unsigned char mode)
{
	unsigned char buf[2];
	unsigned char i=0;
	static unsigned char GT911_CFG_TBL[]=  
	{ 
		0X42,0X10,0X01,0XE0,0X01,0X0A,0X05,0X00,0X01,0X08,
		0X28,0X05,0X50,0X32,0X03,0X05,0X00,0X00,0X00,0X00,
		0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X87,0X08,0X09,
		0X17,0X15,0X31,0X0d,0X00,0X00,0X01,0X98,0X04,0X25,
		0X00,0X00,0X00,0X00,0X00,0X03,0X64,0X32,0X00,0X00,
		0X00,0X11,0X94,0X94,0Xc5,0X02,0X07,0X00,0X00,0X04,
		0X70,0X15,0X00,0X69,0X21,0X00,0X63,0X33,0X00,0X5c,
		0X4f,0X00,0X57,0X7a,0X00,0X57,0X00,0X00,0X00,0X00,
		0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
		0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
		0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
		0X00,0X00,0X06,0X10,0X08,0X04,0X02,0X12,0X0a,0X0c,
		0X0e,0Xff,0Xff,0Xff,0Xff,0Xff,0X00,0X00,0X00,0X00,
		0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
		0X00,0X00,0X02,0X04,0X06,0X08,0X0a,0X1d,0X1e,0X20,
		0X21,0X22,0X24,0X26,0X1f,0X0c,0X00,0Xff,0Xff,0Xff,
		0Xff,0Xff,0Xff,0Xff,0Xff,0Xff,0Xff,0Xff,0X00,0X00,
		0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,0X00,
		0X00,0X00,0X00,0X00
	};
		buf[0]=0;
	buf[1]=mode;	
	for(i=0;i<sizeof(GT911_CFG_TBL);i++)
		buf[0]+=GT911_CFG_TBL[i];
  buf[0]=(~buf[0])+1;//校验为字节和取补码
	GT911_WR_Reg(GT_CFGS_REG,(unsigned char *)GT911_CFG_TBL,sizeof(GT911_CFG_TBL));
	GT911_WR_Reg(GT_CHECK_REG,buf,2);
} 
/*
*********************************************************************************************************************
*函数功能：TouchInit，触摸控制器启动初始化
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/ 
void Clr_Touch_Flag(void)
{
	unsigned char temp = 0;
	GT911_WR_Reg(GT_GSTID_REG,&temp,1); //清坐标寄存器
}


void TouchInit(void)
{
	unsigned char temp[4]; 
	unsigned char i;
	
	GPIO_SetPinMux(GPIO1, GPIO_Pin_26,GPIO_FUNCTION_0);//CTP_INT
	GPIO_SetPinDir(GPIO1, GPIO_Pin_26,GPIO_Mode_OUT);  //CTP_INT
    
	GPIO_SetPinMux(GPIO1, GPIO_Pin_27,GPIO_FUNCTION_0); //CTP_RES   //GPIO_Pin_31
	GPIO_SetPinDir(GPIO1, GPIO_Pin_27,GPIO_Mode_OUT);  //CTP_RES    //GPIO_Pin_31
    
	GPIO_ClearPin(GPIO1,GPIO_Pin_26);   //GPIO_Pin_31
	GPIO_ClearPin(GPIO1,GPIO_Pin_27);
	delay_ms(10);
	GPIO_SetPin(GPIO1,GPIO_Pin_27);
	delay_ms(10);
	GPIO_ConfigPull(GPIO1, GPIO_Pin_26,GPIO_PULL_DISABLE);  //GPIO_Pin_31
	GPIO_SetPinDir(GPIO1, GPIO_Pin_26,GPIO_Mode_IN);  //INT	    GPIO_Pin_31
	delay_ms(5);
	
	I2C_Configuration(I2C_SPEED_STD,I2C_GP0_A);
//	GT911_Interupt_Init();
	
	GT911_RD_Reg(GT_PID_REG,temp,4);
	printf("%s\r\n",temp);	
	printf("TouchPad_ID:%d,%d,%d\r\n",temp[0],temp[1],temp[2]);
	if(strcmp((char*)temp,"911")==0)//若设备ID是'911'
	{
		GT911_RD_Reg(GT_CFGS_REG,temp,1);//读取版本信息
		printf("Default Ver:%x\r\n",temp[0]);
		if(temp[0]<0X42)//判断版本是否需要更新
		{ 			
			delay_ms(10);
			temp[0]=0X02;			
			GT911_WR_Reg(GT_CTRL_REG,temp,1);//软复位
		
			printf("版本更新中");
			GT911_Send_Cfg(1);//发送配置表，掉电保存
			delay_ms(10);			
		   for(i=0;i<186;i++)
		   {
				GT911_RD_Reg(0X8047+i,temp,1);
				printf("TEMP[%x]:%x\r\n",(0X8047+i),temp[0]);
		   }
			printf("版本更完成");
			delay_ms(10);
			temp[0]=0X00;	 
			GT911_WR_Reg(GT_CTRL_REG,temp,1);//退出软复位，恢复坐标读取
			GT911_WR_Reg(GT_GSTID_REG,temp,1); //清坐标寄存器		   
		}
	} 
	GT911_WR_Reg(GT_GSTID_REG,temp,1); //清坐标寄存器	
//#ifdef TOUCH_USE_SPI
//	QSPI_InitTypeDef QSPI_InitStructure;
//	GPIO_SetPinMux(GPIO0, GPIO_Pin_5,GPIO_FUNCTION_0);//QSPI1_TF_CS 
//	GPIO_SetPinDir(GPIO0, GPIO_Pin_5,GPIO_Mode_OUT);  //QSPI1_TF_CS 
//	GPIO_SetPinMux(GPIO1, GPIO_Pin_28,GPIO_FUNCTION_0);//QSPI1_XPT_CS 
//	GPIO_SetPinDir(GPIO1, GPIO_Pin_28,GPIO_Mode_OUT);  //QSPI1_XPT_CS 
//	GPIO_SetPin(GPIO0, GPIO_Pin_5);//QSPI1_TF_CS 
//	GPIO_SetPin(GPIO1, GPIO_Pin_28);//QSPI1_XPT_CS 
//	
//	QSPI_InitStructure.QSPI_SlaveMode=QSPI_MASTER_MODE;
//	QSPI_InitStructure.QSPI_FrameLength=QSPI_FRAME_LENGTH_8Bit;
//	QSPI_InitStructure.QSPI_ModeSelect=QSPI_STD;
//	QSPI_InitStructure.QSPI_CPOL = QSPI_CPOL_Low;
//	QSPI_InitStructure.QSPI_CPHA = QSPI_CPHA_1Edge;
//	QSPI_InitStructure.QSPI_FirstBit = QSPI_FirstBit_MSB;
//	QSPI_InitStructure.QSPI_ClockDiv = (96<<8);
//	QSPI_InitStructure.QSPI_ClockRate = 0;//48/(48*(1+0));//1mhz
//	RCC->QUADSPI1CLKDIV = 8;//384/8=48mhz
//	
//	GPIO_SetPinMux(GPIO0, GPIO_Pin_13, GPIO_FUNCTION_3);//QSPI1_MOSI 
//	GPIO_SetPinMux(GPIO0, GPIO_Pin_7, GPIO_FUNCTION_3);//QSPI_CLK 
//	GPIO_SetPinMux(GPIO0, GPIO_Pin_6, GPIO_FUNCTION_3);//QSPI1_MISO 	
//	
//	QSPI_HwInit(QSPI1, &QSPI_InitStructure);            //初始化QSPI1
//	
//	QSPI1->BUSY_DLY &= 0XFFFFFF00;
//	QSPI1->BUSY_DLY |= (8<<0);
//	QSPI1->BUSY_DLY &= 0xFFFFF0FF;	//clean [11:8]
//	QSPI1->BUSY_DLY |= (1<<8);      //设置一个合适的CAP_DLY
//	
//#else	
//	GPIO_SetPinMux(GPIO1, GPIO_Pin_28,GPIO_FUNCTION_0);//QSPI1_XPT_CS 
//	GPIO_SetPinDir(GPIO1, GPIO_Pin_28,GPIO_Mode_OUT);  //QSPI1_XPT_CS 
//	GPIO_SetPinMux(GPIO0, GPIO_Pin_5,GPIO_FUNCTION_0);//QSPI1_TF_CS 
//	GPIO_SetPinDir(GPIO0, GPIO_Pin_5,GPIO_Mode_OUT);  //QSPI1_TF_CS 
//	GPIO_SetPinMux(GPIO0, GPIO_Pin_6,GPIO_FUNCTION_0);//QSPI1_MISO 
//	GPIO_SetPinDir(GPIO0, GPIO_Pin_6,GPIO_Mode_IN);   //QSPI1_MISO 
//	GPIO_SetPinMux(GPIO0, GPIO_Pin_13,GPIO_FUNCTION_0);//QSPI1_MOSI 
//	GPIO_SetPinDir(GPIO0, GPIO_Pin_13,GPIO_Mode_OUT);  //QSPI1_MOSI 
//	GPIO_SetPinMux(GPIO0, GPIO_Pin_7,GPIO_FUNCTION_0);//QSPI_CLK 
//	GPIO_SetPinDir(GPIO0, GPIO_Pin_7,GPIO_Mode_OUT);  //QSPI_CLK 	
//#endif
}

//void Touch_Point_Read(void)
//{
//	unsigned char tmp;
//	unsigned char data[0x40];
//	GT911_POINT_DATA     gt911_data;
//	if(Point_Read){
//		Point_Read=0;
//		GT911_RD_Reg(0x8140, data, 0x40);
//		tmp = data[0x0E];
//		if((tmp&0x80) && ((tmp&0x0f)>0)){			
//			 gt911_data=GetPointData(tmp&0x0f, data);			
//			 ShowPointData(gt911_data);
//		}
//		tmp = 0;
//		GT911_WR_Reg(0x814E, &tmp, 1);
//	}
//}

void TouchScan_Init(void)//初始化一个定时器17作为触摸扫描周期
{
	
	RCC->AHBCLKCTRL0_SET|=(1<<20);//开启定时器时钟
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Prescaler=95;//96Mhz/(95+1)=1Mhz
	TIM_TimeBaseStructure.TIM_Period=1000;//计数到1000刚好是1ms 中断
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	//SYSAHBCLKDIV=4 在SystemInit已经初始化了 AHB时钟96Mhz
	TIM_TimeBaseInit(TIM17, &TIM_TimeBaseStructure);
	TIM_SelectOutputTrigger(TIM17, TIM_TRGOSource_Enable);//选择TIM17的update事件更新为触发源
	TIM_ClearITPendingBit(TIM17, TIM_IT_Update);     			//清除update事件中断标志
	TIM_ITConfig(TIM17, TIM_IT_Update, ENABLE);       			//使能TIM17中断 
	//中断配置
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel=TIM17_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPriority=0;//2bit位宽
	NVIC_Init(&NVIC_InitStruct);
	TIM_Cmd(TIM17,ENABLE);
}

