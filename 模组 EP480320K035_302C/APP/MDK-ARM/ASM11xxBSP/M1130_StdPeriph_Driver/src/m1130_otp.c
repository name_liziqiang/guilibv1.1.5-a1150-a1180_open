/**
  ******************************************************************************
  * @file    m1130_otp.c
  * @author  Alpscale Software Team
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   This file provides all the CMP firmware functions.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */

#include "misc.h"
#include "m1130_otp.h"
#include "m1130_rcc.h"

void OTP_Reset(void)
{
	RCC_ResetAHBCLK(1<<AHBCLK_BIT_OTP);
}

uint32_t OTP_ReadSN(void)
{
	uint32_t status;
	
	do{
		status  = readReg(0x40005100);
	} while ((status & 1) == 0);
	
	return readReg(0x40005004);
}

static uint32_t OTP_ReadADJ(void)
{
	uint32_t status,otp_ana_adj_val,temp;
	
	do{
		status  = readReg(0x40005100);
	} while ((status & 1) == 0);
	
	otp_ana_adj_val = readReg(0x40005000);
	temp = (~otp_ana_adj_val)&0x40480;
	return (otp_ana_adj_val&0x3FB7F)|temp;
}

//[7:0]
uint32_t OTP_ReadADJ_12M(void)
{
	uint32_t otp_ana_adj_val = OTP_ReadADJ();
	return otp_ana_adj_val&0xFF;
}

//[10:8]
uint32_t OTP_ReadADJ_10K(void)
{
	uint32_t otp_ana_adj_val = OTP_ReadADJ();
	return (otp_ana_adj_val>>8)&0x7;
}

//[13:11]
uint32_t OTP_ReadADJ_LDO(void)
{
	uint32_t otp_ana_adj_val = OTP_ReadADJ();
	return (otp_ana_adj_val>>11)&0x7;
}

//[18:14]
uint32_t OTP_ReadADJ_RTC_CAP(void)
{
	uint32_t otp_ana_adj_val = OTP_ReadADJ();
	return (otp_ana_adj_val>>14)&0x1F;
}

