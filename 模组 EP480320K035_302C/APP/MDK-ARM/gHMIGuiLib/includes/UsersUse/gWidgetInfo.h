#ifndef _gWidgetInfo_h
#define _gWidgetInfo_h
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：gui 控件相关信息定义
 *实现功能：
 *
 *撰 写 人：Epitome Design Systems   LCD Application development team
 *撰写时间：2019-7-22
 *测 试 人：Epitome Design Systems   LCD Application development team
 *测试时间：2019-7-22
 *版 本 号：V1.01_2019-7-22
 *版本说明：
 *                              
 *          函数清单
 *
 *V1.01：   功能实现
 *
 *V1.02：
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "gStdint.h"
#include "gReturn.h"

#ifdef __cplusplus
 extern "C" {
#endif
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *LCD坐标系 坐标信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _LCD_POSITION{
	gui_uint16 x;
	gui_uint16 y;
}LCD_POSITION;
typedef LCD_POSITION*  pLCD_POSITION;

__packed typedef struct _LCD_RECTANGLE{
	LCD_POSITION startPos;
	LCD_POSITION endPos;
}LCD_RECTANGLEN;
typedef LCD_RECTANGLEN*  pLCD_RECTANGLEN;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  窗体位置信息
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _WIDGET_LOCATION{
	//控件位置信息
  gui_uint16 x;                                //显示的起始坐标
	gui_uint16 y;                                //显示的起始坐标
  gui_uint16 width;                            //显示的宽度
	gui_uint16 height;                           //显示的高度
}WIDGET_LOCATION; 
typedef WIDGET_LOCATION*  pWIDGET_LOCATION;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  一个控件的基本信息描述------所有控件都具有的信息，，处于控件描述信息的最开始
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define WIDGET_NAME_MAX_LEN                11//一个控件的名称最大字符串长度
__packed typedef struct _WIDGET_BASE_INFO{
	//控件基本类型定义，所有控件都应该具备的，而且是在控件描述符的最前面
  gui_uint16 wType;                            //控件类型
  gui_uint16 wId;                              //控件ID
	//gui_uint8 wName[WIDGET_NAME_MAX_LEN];        //控件名称
  gui_uint8  wVscope;                          //控件的私有属性
}WIDGET_BASE_INFO; 
typedef WIDGET_BASE_INFO*  pWIDGET_BASE_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  控件事件所使用的原型以及其他内容
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
typedef gui_Err (*WIDGET_EVENT)(gui_int32 argc , const char **argv);//所有控件的显示控件原型
//argc :传入进来的命令参数
//argv :传入进来的所有命令首地址，二维数据  ASCII

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  页面信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _PAGE_INFO{
	//page控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
  gui_uint8  backMode;                       //背景
	gui_color backColor;                       //背景颜色，当背景模式为显示一种颜色的时候调用
	gui_uint16 picId;                          //图片ID，当背景模式为显示图片的时候调用
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT enterEvent;                   //进入页面
	WIDGET_EVENT leaveEvent;                   //离开页面
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}PAGE_INFO; 
typedef PAGE_INFO*  pPAGE_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  双态按钮信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _BINARYBUTTON_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8 backMode;                        //背景模式
	gui_uint16 fontlibId;                      //字库ID
	gui_uint8 borderWidth;                     //边框线宽，像素单位
	gui_color borderColor;                     //边框颜色
	gui_uint16 enablePicId;                    //使能的背景图片
	gui_uint16 disenablePicId;                 //不使能的背景图片
	gui_color enableBackColor;                 //使能的背景颜色
	gui_color disenableBackColor;              //不使能的背景颜色
	gui_color enableFontColor;                 //使能的前景颜色
	gui_color disenableFontColor;              //不使能的前景颜色
	gui_uint8  xAlign;                         //水平对齐方式
	gui_uint8  yAlign;                         //垂直对齐方式
	gui_uint8  isDr;                           //显示不下的时候，是否换行
	gui_uint8  value;                          //双态按钮，0松开状态，1按下状态
	char  *text;                               //文本显示的数据指针，创建的时候从flash拷贝过来，
	                                           //---上位机传下来的应该是用户HMI输入的文本长度，占4个字节，目的是为了和MCU保存结构体长度一致
	                                           //---用户输入的文本信息直接跟在TEXTBOX_INFO结构体后面，MCU会偏移sizeof(BUTTON_INFO)之后去读
	gui_uint16 maxLen;                         //文本框显示的最大长度，按字节算，也是MCU需要申请的内存长度，ASCII占一个字节，中文占两个字节
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}BINARYBUTTON_INFO;
typedef BINARYBUTTON_INFO*  pBINARYBUTTON_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  按钮信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _BUTTON_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8 backMode;                        //背景模式
	gui_uint16 fontlibId;                      //字库ID
	gui_uint8 borderWidth;                     //边框线宽，像素单位
	gui_color borderColor;                     //边框颜色
	gui_uint16 pressPicId;                     //按下的背景图片
	gui_uint16 releasePicId;                   //松开的背景图片
	gui_color pressBackColor;                  //按下的背景颜色
	gui_color releaseBackColor;                //松开的背景颜色
	gui_color pressFontColor;                  //按下的前景颜色
	gui_color releaseFontColor;                //松开的前景颜色
	gui_uint8  xAlign;                         //水平对齐方式
	gui_uint8  yAlign;                         //垂直对齐方式
	gui_uint8  isDr;                           //显示不下的时候，是否换行
	char  *text;                               //文本显示的数据指针，创建的时候从flash拷贝过来，
	                                           //---上位机传下来的应该是用户HMI输入的文本长度，占4个字节，目的是为了和MCU保存结构体长度一致
	                                           //---用户输入的文本信息直接跟在TEXTBOX_INFO结构体后面，MCU会偏移sizeof(BUTTON_INFO)之后去读
	gui_uint16 maxLen;                         //文本框显示的最大长度，按字节算，也是MCU需要申请的内存长度，ASCII占一个字节，中文占两个字节
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}BUTTON_INFO; 
typedef BUTTON_INFO*  pBUTTON_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  切图信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _CUTPICTURE_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint16 picId;                          //图片ID，当背景模式为显示图片的时候调用
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}CUTPICTURE_INFO; 
typedef CUTPICTURE_INFO*  pCUTPICTURE_INFO;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  虚拟浮点数信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _FLOATNUMBERBOX_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8 backMode;                        //背景模式
	gui_uint8 keyId;                           //输入键盘ID -1表示没有键盘
	gui_uint16 fontlibId;                      //字库ID
	gui_uint8 borderWidth;                     //边框线宽，像素单位
	gui_color borderColor;                     //边框颜色
	gui_uint16 picId;                          //背景图片ID
	gui_color backColor;                       //背景色
	gui_color frontColor;                       //前景色
	gui_uint8  xAlign;                         //水平对齐方式
	gui_uint8  yAlign;                         //垂直对齐方式
	gui_uint8  fdcount;                        //小数部分长度，maxlen-fdcount就是整数部分长度，
	gui_uint8  isDr;                           //显示不下的时候，是否换行
	gui_uint32 data;                           //显示的值
	gui_uint8 maxLen;                          //显示值的时候显示的最大长度，0自动，前端去0，其他按指定显示，从各位开始算
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}FLOATNUMBERBOX_INFO; 
typedef FLOATNUMBERBOX_INFO*  pFLOATNUMBERBOX_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  曲线框信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define GRAPH_CHANNEL_MAX                     4//曲线控件最大的通道数
__packed typedef struct _GRAPH_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8 backMode;                        //背景模式
	gui_uint8 dir;                             //bit0-bit3控制波形推荐方式，0000左到右推进,0001右到左推进
	gui_uint8 xAxis_loc;                       //轴位置，
	gui_uint8 yAxis_loc;                       //轴位置，
	gui_uint8 channelCount;                    //通道数量，1-4，共计4个通道
	gui_color axisColor;                       //显示出来的轴颜色
	gui_color backColor;                       //背景颜色
	gui_color gridColor;                       //网格颜色
	gui_uint8  gridWidth;                      //网格宽度0表示没有网格
	gui_uint8  gridHeight;                     //网格高度0表示没有网格
	gui_uint16 picId;                          //切图模式或者图片模式下，图片ID
	gui_color graphColor[GRAPH_CHANNEL_MAX];   //4个通道的颜色值
	gui_uint16 dis;                            //波形Y轴上缩放系数
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//上位机下传下来的配置信息到这个地方就结束，不管是从外部flash读取数据还是从内部flash拷贝数据，sizeof(GRAPH_INFO)均应该-16
	//以下16个字节的数据，是MCU端才有的，存放的4个指针，分别指向4个通道的数据地址，数据类型均为uint8_t，0-255
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	gui_uint8 *graphData[GRAPH_CHANNEL_MAX];   
	gui_uint16 dataLen[GRAPH_CHANNEL_MAX];
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}GRAPH_INFO; 
typedef GRAPH_INFO*  pGRAPH_INFO;

#define GRAPH_INFO_LEN_OFFSET     (GRAPH_CHANNEL_MAX*4+GRAPH_CHANNEL_MAX*2+12) //曲线控件里面 除了控件参数外 还有这么多字节的额外数据  


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  定时器信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _GTIMER_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	gui_uint16 timeOut;                        //定时器超时时间
	gui_uint8  enable;                         //定时器开关位，0关，1开
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//上位机下传下来的配置信息到这个地方就结束，不管是从外部flash读取数据还是从内部flash拷贝数据，sizeof(GTIMER_INFO)均应该-4
	//以下4个字节的数据，是MCU端才有的，
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	gui_uint32 timeValue;                      //定时器当前计数值（周期是1MS）低16位是当前计数器值，高16位是定时器溢出标志，或者其他标识
	WIDGET_EVENT timeoutEvent;                 //定时器溢出，用户自定义，注册进来的
}GTIMER_INFO; 
typedef GTIMER_INFO*  pGTIMER_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  变量控件信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _GVARIATE_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	gui_uint8 vaType;                          //变量类型，0值，1字符串
	gui_uint32 value;                          //变量值
	char  *text;                               //文本显示的数据指针，创建的时候从flash拷贝过来，
	                                           //---上位机传下来的应该是用户HMI输入的文本长度，占4个字节，目的是为了和MCU保存结构体长度一致
	                                           //---用户输入的文本信息直接跟在TEXTBOX_INFO结构体后面，MCU会偏移sizeof(BUTTON_INFO)之后去读
	gui_uint16 maxLen;                         //文本框显示的最大长度，按字节算，也是MCU需要申请的内存长度，ASCII占一个字节，中文占两个字节
}GVARIATE_INFO; 
typedef GVARIATE_INFO*  pGVARIATE_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  数字框信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _NUMBERBOX_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8 backMode;                        //背景模式
	gui_uint8 keyId;                           //输入键盘ID -1表示没有键盘
	gui_uint16 fontlibId;                      //字库ID
	gui_uint8 borderWidth;                     //边框线宽，像素单位
	gui_color borderColor;                     //边框颜色
	gui_uint16 picId;                          //背景图片ID
	gui_color backColor;                       //背景色
	gui_color frontColor;                       //前景色
	gui_uint8  xAlign;                         //水平对齐方式
	gui_uint8  yAlign;                         //垂直对齐方式
	gui_uint8  formatType;                     //数字显示模式，0数字，1货币，2HEX
	gui_uint8  isDr;                           //显示不下的时候，是否换行
	gui_uint32 data;                           //显示的值
	gui_uint8  maxLen;                         //显示值的时候显示的最大长度，0自动，前端去0，其他按指定显示，从各位开始算
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}NUMBERBOX_INFO; 
typedef NUMBERBOX_INFO*  pNUMBERBOX_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  选项框信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _OPTIONBOX_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8  backMode;                       //背景模式，切图、单色、图片
	gui_uint8  optionType;                     //选项样式，0中心显示方形，为复选框，1中心显示圆形，为单选框
	gui_color backColor;                       //背景颜色
	gui_color frontColor;                       //前景颜色，就是中间显示的框的颜色
	gui_uint16 cPicId;                         //选择下的背景图
	gui_uint16 nocPicId;                       //不选择下的背景图
	gui_uint8  value;                          //值，0表示没选择，1表示选择
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的        
}OPTIONBOX_INFO; 
typedef OPTIONBOX_INFO*  pOPTIONBOX_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  图片框信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _PICTUREBOX_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint16 picId;                          //图片ID，当背景模式为显示图片的时候调用
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}PICTUREBOX_INFO; 
typedef PICTUREBOX_INFO*  pPICTUREBOX_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  指针信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _POINTER_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8 backMode;                        //背景模式,支持单色和图片
	gui_uint16 picId;                          //图片或者切图模式下：图片ID
	gui_color backColor;                       //单色模式下：背景颜色
	gui_color frontColor;                      //单色模式下：前景颜色
	gui_uint8  frontWidth;                     //指针线宽
	gui_uint8  circleRadius;                   //指针的圆心半径，像素单位
	gui_uint16 AngleValue;                     //弧度值0-360，标识着指针显示的位置
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//上位机下传下来的配置信息到这个地方就结束，不管是从外部flash读取数据还是从内部flash拷贝数据，均应该-4
	//以下4个字节的数据，是MCU端才有的，
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	gui_uint16 last_endx;                      //绘制指针的时候上一次的终点坐标，起点坐标始终是仪表盘的圆心，始终不变的
	gui_uint16 last_endy;
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}POINTER_INFO; 
typedef POINTER_INFO*  pPOINTER_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  进度条信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _PROGRESSBAR_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8 backMode;                        //背景模式,支持单色和图片
	gui_uint8 dir;                             //方向
	gui_uint8 value;                           //进度条的值，0-100%
	gui_uint16 backPicId;                      //图片模式下，背景图片
	gui_uint16 frontPicId;                     //图片模式下，前端图片
	gui_color backColor;                       //单色模式下，背景颜色
	gui_color frontColor;                      //单色模式下，前端颜色
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}PROGRESSBAR_INFO; 
typedef PROGRESSBAR_INFO*  pPROGRESSBAR_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  二维码信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _QRCODEBOX_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8  dispLogo;                       //是否在中心位置显示LOGO,0不显示，1显示
	gui_color backColor;                       //背景颜色
	gui_color frontColor;                       //前景颜色
	gui_uint16 logoPicId;                      //logo图片ID
	char  *text;                               //文本显示的数据指针，创建的时候从flash拷贝过来，
	                                           //---上位机传下来的应该是用户HMI输入的文本长度，占4个字节，目的是为了和MCU保存结构体长度一致
	                                           //---用户输入的文本信息直接跟在TEXTBOX_INFO结构体后面，MCU会偏移sizeof(BUTTON_INFO)之后去读
	gui_uint16 maxLen;                         //文本框显示的最大长度，按字节算，也是MCU需要申请的内存长度，ASCII占一个字节，中文占两个字节
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//上位机下传下来的配置信息到这个地方就结束，不管是从外部flash读取数据还是从内部flash拷贝数据，均应该-2
	//以下2个字节的数据，是MCU端才有的，
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	gui_uint16 textLen;                        //只是当前文本实际长度
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}QRCODEBOX_INFO; 
typedef QRCODEBOX_INFO*  pQRCODEBOX_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  滚动文本信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _ROLLTEXTBOX_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;               //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                      //控件位置信息
	gui_uint8 backMode;                       //背景模式
	gui_uint8 keyId;                          //输入键盘ID -1表示没有键盘
	gui_uint16 fontlibId;                     //字库ID
	gui_uint8 borderWidth;                    //边框线宽，像素单位
	gui_color borderColor;                    //边框颜色
	gui_uint16 picId;                         //背景图片ID
	gui_color backColor;                      //背景色
	gui_color frontColor;                      //前景色
	gui_uint8  xAlign;                        //水平对齐方式
	gui_uint8  yAlign;                        //垂直对齐方式
	gui_uint8  isPassword;                    //是否显示密码，0不是 1是
	gui_uint8  isDr;                          //显示不下的时候，是否换行
	char  *text;                              //文本显示的数据指针，创建的时候从flash拷贝过来，
	                                          //---上位机传下来的应该是用户HMI输入的文本长度，占4个字节，目的是为了和MCU保存结构体长度一致
	                                          //---用户输入的文本信息直接跟在TEXTBOX_INFO结构体后面，MCU会偏移sizeof(TEXTBOX_INFO)之后去读
	gui_uint16 maxLen;                        //文本框显示的最大长度，按字节算，也是MCU需要申请的内存长度，ASCII占一个字节，中文占两个字节
	gui_uint8  dir;                           //文本移动方向
	gui_uint8  dis;                           //文本移动幅值，每次移动多少像素
	gui_uint16 timeOut;                       //文本移动周期
	gui_uint8  enable;                     		//文本移动使能，0不移动，1移动
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//上位机下传下来的配置信息到这个地方就结束，不管是从外部flash读取数据还是从内部flash拷贝数据，均应该-2
	//以下2个字节的数据，是MCU端才有的，
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	gui_uint32 timeValue; 
	gui_uint16 textLen;                       //只是当前文本实际长度
	char *count_st;							  						//文本显示起始指针 指针占4个字节 
	gui_uint16 start_x;						 					  //文本框显示的起始x坐标 
	WIDGET_EVENT pressEvent;                  //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT updateEvent;                 //控件更新事件，用户自定义，注册过来的
	WIDGET_EVENT timeoutEvent;                //控件超时事件
}ROLLTEXTBOX_INFO; 
typedef ROLLTEXTBOX_INFO*  pROLLTEXTBOX_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  滑动块信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _SLIDER_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8 dir;                             //方向定义，横向还是纵向
	gui_uint8 backMode;                        //背景方式，切图、单色、图片
	gui_color backColor;                       //背景颜色
	gui_uint16 backPicId;                      //背景图片
	gui_uint8 vernierMode;                     //游标方式，单色、图片
	gui_color vernierColor;                    //游标颜色
	gui_uint16 vernierPicId;                   //游标图片
	gui_uint16 vernierWidth;                   //游标宽度
	gui_uint16 vernierHeight;                  //游标高度
	gui_uint16 value;                          //游标值
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//上位机下传的配置参数到这里就结束了
	//下面的数据是MCU专有的，在创建控件的时候应该减去下面的数据长度
	gui_uint16 v_last_x;                       //滑动块上一次的位置
	gui_uint16 v_last_y;
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT slidEvent;
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}SLIDER_INFO; 
typedef SLIDER_INFO*  pSLIDER_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  文本框信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _TEXTBOX_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint8 backMode;                        //背景模式
	gui_uint8 keyId;                           //输入键盘ID -1表示没有键盘
	gui_uint16 fontlibId;                      //字库ID
	gui_uint8 borderWidth;                     //边框线宽，像素单位
	gui_color borderColor;                     //边框颜色
	gui_uint16 picId;                          //背景图片ID
	gui_color backColor;                       //背景色
	gui_color frontColor;                      //前景色
	gui_uint8  xAlign;                         //水平对齐方式
	gui_uint8  yAlign;                         //垂直对齐方式
	gui_uint8  isPassword;                     //是否显示密码，0不是 1是
	gui_uint8  isDr;                           //显示不下的时候，是否换行
	char  *text;                               //文本显示的数据指针，创建的时候从flash拷贝过来，
	                                           //---上位机传下来的应该是用户HMI输入的文本长度，占4个字节，目的是为了和MCU保存结构体长度一致
	                                           //---用户输入的文本信息直接跟在TEXTBOX_INFO结构体后面，MCU会偏移sizeof(TEXTBOX_INFO)之后去读
	gui_uint16 maxLen;                         //文本框显示的最大长度，按字节算，也是MCU需要申请的内存长度，ASCII占一个字节，中文占两个字节
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//上位机下传下来的配置信息到这个地方就结束，不管是从外部flash读取数据还是从内部flash拷贝数据，均应该-2
	//以下2个字节的数据，是MCU端才有的，
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	gui_uint16 textLen;                       //只是当前文本实际长度
	WIDGET_EVENT pressEvent;                  //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}TEXTBOX_INFO; 
typedef TEXTBOX_INFO*  pTEXTBOX_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中控件  触摸热区信息描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _TOUCHAREA_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//每个控件的私有属性
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}TOUCHAREA_INFO; 
typedef TOUCHAREA_INFO*  pTOUCHAREA_INFO;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库控件  虚拟GIF控件描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _VIRTUALGIF_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint16 startPicId;                     //Gif首张图片ID
	gui_uint16 endPicId;                       //Gif末张图片ID
	gui_uint16 playTime;                       //帧时间间隔
	gui_uint8  enable;                         //播放开关位，0关，1开
	gui_uint8  replayEnable;                   //重复播放开关位，0关，1开
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//每个控件的私有属性
	gui_uint32 timeValue;                      // 
  gui_uint16   currentPicId;                 //当前的图片ID
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT updateEvent;                 //控件更新事件，用户自定义，注册进来的 
	WIDGET_EVENT timeoutEvent;                //控件超时事件

}VIRTUALGIF_INFO; 
typedef VIRTUALGIF_INFO*  pVIRTUALGIF_INFO;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 **GUI库控件  圆形滑动块控件描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _ROUNDSLIDER_INFO{
	//控件描述。。。。。
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息，但此控件上位机下位机都必须保证width=height，控件等比缩放，保持正方形，预览显示保持正圆，上位机可以把height留着，但无法设置，只显示跟width一样的数值
	gui_uint8 backMode;                        //背景方式，单色、图片；单色模式下滑轨为圆/圆环（根据insideRadius）
	gui_color backColor;                       //背景颜色（insideRadius=0时为大圆的颜色，insideRadius>0时为圆环的颜色）
	gui_uint16 outerRadius;                    //背景外半径（单色模式有效）（0<=内半径<外半径<=width/2）
	gui_uint16 insideRadius;                   //背景内半径（单色模式有效）（0<=内半径<外半径<=width/2）
	gui_uint16 backPicId;                      //背景图片
	gui_uint8 vernierMode;                     //游标方式，单色、图片；单色模式下游标为小实心圆，内切于圆滑轨，其圆心在r=(width/2-vernierWidth/2)的虚拟圆周上运动
	gui_uint16 vernierWidth;                   //游标宽度，无论单色和图片，游标必须为正方形，只传vernierWidth，高度自动锁定等于vernierWidth，若上位机用户设置图片宽高不等则弹提示框
	gui_color vernierColor;                    //游标颜色
	gui_uint16 vernierPicId;                   //游标图片
	gui_uint16 value;                          //游标值0-360
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//上位机下传的配置参数到这里就结束了
	//下面的数据是MCU专有的，在创建控件的时候应该减去下面的数据长度
	gui_uint16 v_last_x;                       //滑动块上一次的位置
	gui_uint16 v_last_y;
  gui_uint16 value_min;
  gui_uint16 value_max;
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT slidEvent;
	WIDGET_EVENT updateEvent;                  //控件更新事件，用户自定义，注册过来的
}ROUNDSLIDER_INFO; 
typedef ROUNDSLIDER_INFO*  pROUNDSLIDER_INFO;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 **GUI库控件  图片动画控件描述
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
__packed typedef struct _PICANIMATION_INFO{
	//控件描述
	WIDGET_BASE_INFO base_info;                //所有控件都具有的基本信息
	WIDGET_LOCATION loc;                       //控件位置信息
	gui_uint16 beforePicId;                    //动画过渡前的图片ID
	gui_uint16 afterPicId;                     //动画过渡后的图片ID
	gui_uint16 playTime;                       //帧时间间隔
	gui_uint8  pattern;                        //动画样式
	gui_uint8  dis;				                     //单位时间变换多少像素			
  gui_uint8  enable;                        //播放开关位，0关，1开
	gui_uint8  replayEnable;                   //重复播放开关位，0关，1开
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//每个控件的私有属性 	
  gui_uint32   timeValue;                      // 
  gui_uint16   flagover;                 //当前的图片ID
  gui_uint16   currentplay;                  //当前的图片帧数
	WIDGET_EVENT pressEvent;                   //控件按下事件，用户自定义，注册进来的
	WIDGET_EVENT releaseEvent;                 //控件松开事件，用户自定义，注册进来的 
	WIDGET_EVENT updateEvent;                 //控件松开事件，用户自定义，注册进来的 
  WIDGET_EVENT timeoutEvent;                //控件超时事件

}PICANIMATION_INFO; 
typedef PICANIMATION_INFO*  pPICANIMATION_INFO;

#ifdef __cplusplus
}
#endif
#endif
