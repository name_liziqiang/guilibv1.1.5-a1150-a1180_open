#ifndef _gResMsg_h
#define _gResMsg_h
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：gui 一些操作描述信息定义
 *实现功能：
 *
 *撰 写 人：Epitome Design Systems   LCD Application development team
 *撰写时间：2019-7-22
 *测 试 人：Epitome Design Systems   LCD Application development team
 *测试时间：2019-7-22
 *版 本 号：V1.01_2019-7-22
 *版本说明：
 *                              
 *          函数清单
 *
 *V1.01：   功能实现
 *
 *V1.02：
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "gStdint.h"

#ifdef __cplusplus
 extern "C" {
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *资源类型定义
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define RTYPE_NULL               0//无效资源类型，MCU检测到该类型就可以说明资源读取结束了
#define RTYPE_GUICONFIG          2//HMI开发出来的GUI界面资源
#define RTYPE_PICTURE            3//图片资源
#define RTYPE_TRUETYPE           4//字库资源
#define RTYPE_CARTOON            5//动画资源
#define RTYPE_VIDEO              6//视频资源
#define RTYPE_VOICE              7//音频资源
#define RTYPE_EVENT              8//GUI运行过程中执行的事件，

#define RTYPE_ERRER              9//错误的资源类型

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  控件类型定义
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define WIDGET_TYPE_MASK               0x00ff//控件类型uint16_t wType中低八位表示控件类型，高八位表示控件基本属性，有没有loc等信息
                                             //0为一个无效控件，所有控件类型最大值255（1-255）
#define WIDGET_TYPE_HASLOC_MASK        0x8000//bit15,为1表示有控件位置信息，
                                             //0表示没有
#define WIDGET_TYPE_HASSLIDE_MASX      0x4000//bit14,为1表示该控件支持滑动，
                                             //0表示不支持
#define WIDGET_TYPE_HASTIMING_MAX      0x2000//bit13,为1表示该控件支持定时动作
																						 //0表示不支持
#define WIDGET_TYPE_NULL               0x0000//无效控件，用户遍历flash的时候指示一个界面已经提取完所有控件了
#define WIDGET_TYPE_PAGE               0x8001//PAGE页面控件，一个界面资源原则上都是以该控件开始的
#define WIDGET_TYPE_TEXTBOX            0x8002//文本显示框控件
#define WIDGET_TYPE_ROLLTEXTBOX        0xa003//滚动文本显示框控件
#define WIDGET_TYPE_NUMBERBOX          0x8004//数字显示框控件
#define WIDGET_TYPE_FLOATNUMBERBOX     0x8005//模拟的浮点数显示控件
#define WIDGET_TYPE_BUTTON             0x8006//按钮控件
#define WIDGET_TYPE_PROGRESSBAR        0x8007//进度条控件
#define WIDGET_TYPE_PICTUREBOX         0x8008//图片显示框控件
#define WIDGET_TYPE_CUTPICTURE         0x8009//切图显示控件
#define WIDGET_TYPE_TOUCHAREA          0x800a//触摸块控件，只处理了一些事情，不会显示任何东西出来
#define WIDGET_TYPE_POINTER            0x800b//仪表指针控件，只显示了一个指针，不会绘制仪表盘
#define WIDGET_TYPE_GRAPH              0x800c//曲线图控件
#define WIDGET_TYPE_SLIDER             0xC00d//滑动块控件
#define WIDGET_TYPE_GTIMER             0x200e//定时器控件，没有位置信息
#define WIDGET_TYPE_GVARIATE           0x000f//变量控件，没有位置信息
#define WIDGET_TYPE_BINARYBUTTON       0x8010//双态按钮控件
#define WIDGET_TYPE_OPTIONBOX          0x8011//多选框或者单选框控件
#define WIDGET_TYPE_QRCODEBOX          0x8012//二维码显示框
#define WIDGET_TYPE_VIRTUALGIF         0xA013//虚拟gif控件，具有位置信息，支持定时动作
#define WIDGET_TYPE_ROUNDSLIDER  			 0xC014//圆形滑动块
#define WIDGET_TYPE_PICANIMATION       0xA015//动画图片框

#define WIFGET_TYPE_MIN                WIDGET_TYPE_PAGE
#define WIDGET_TYPE_MAX                WIDGET_TYPE_PICANIMATION

//#define ASSERT_WIDGET_TYPE(type) 			(((type)&(WIDGET_TYPE_MASK))>((WIDGET_TYPE_MAX)&(WIDGET_TYPE_MASK)))?(0):(1)//判断一个type是不是有效控件																																																					 //有效为1  无效为0
#define ASSERT_WIDGET_TYPE(type)    (((type&WIDGET_TYPE_MASK)<=(WIDGET_TYPE_MAX&WIDGET_TYPE_MASK))&((type&WIDGET_TYPE_MASK)>=(WIFGET_TYPE_MIN&WIDGET_TYPE_MASK)))
#define GET_WIDGET_TYPE(type)    			((type)&(WIDGET_TYPE_MASK))//得到一个控件类型 去掉type的高八位，得到低八位,常用于控件事件处理的时候快速查表定位
#define ADJUST_WIDGET_TYPE(type,needtype) 	(type)==(needtype)?(0):(1)//判断一个控件的类型是不是我需要的类型，是返回0，不是返回1，用于异常处理
#define ADJUST_WIDGET_NOLOC(type) 			(((type)&WIDGET_TYPE_HASLOC_MASK)?(0):(1))//判断一个控件是否含有位置信息，是返回0，不是返回1
#define ADJUST_WIDGET_NOSLIDE(type) 		(((type)&WIDGET_TYPE_HASSLIDE_MASX)?(0):(1))//判断一个控件是否支持滑动，是返回0，不是返回1
#define ADJUST_WIDGET_NOTIMING(type) 		(((type)&WIDGET_TYPE_HASTIMING_MAX)?(0):(1))//判断一个控件是否支持定时，是返回0，不是返回1

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  控件支持的背景方式
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define WIDGET_BACKMODE_NULL                0//没有背景
#define WIDGET_BACKMODE_COLOR               1//显示一种颜色值
#define WIDGET_BACKMODE_PIC                 2//显示一个图片
#define WIDGET_BACKMODE_CUTPIC              3//切图，从一个全幅图片中切取出来一部分显示
#define IS_WIDGET_BACKMODE(x)               ((x>=WIDGET_BACKMODE_COLOR)&(x<=WIDGET_BACKMODE_CUTPIC))

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  控件支持的文本对齐方式
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define CHARALIGN_X_L                        0//左对齐
#define CHARALIGN_X_C                        1//中对齐
#define CHARALIGN_X_R                        2//右对齐
#define CHARALIGN_Y_T                        0//顶对齐
#define CHARALIGN_Y_C                        1//中对齐
#define CHARALIGN_Y_B                        2//底对齐

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  控件的私有属性定义
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define WIDGET_VS_PRIVATE                  0x00//控件属性私有
#define WIDGET_VS_GLOBAL                   0x01//控件属性全局

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  控件注册支持的事件类型
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define EVENT_REGISTER_TYPE_RELEASE         0x0001//指示注册一个控件松开事件
#define EVENT_REGISTER_TYPE_PRESS           0x0002//指示注册一个控件按下事件
#define EVENT_REGISTER_TYPE_TIMEOUT         0x0003//指示注册一个控件定时溢出事件
#define EVENT_REGISTER_TYPE_UPDATE          0x0004//指示注册一个控件更新事件
#define EVENT_REGISTER_TYPE_SLIDE           0x0005//指示注册一个控件滑动事件

#define EVENT_REGISTER_TYPE_ENTER           0x0101//指示注册一个进入界面事件
#define EVENT_REGISTER_TYPE_LEAVE           0x0102//指示注册一个离开界面事件

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  数字框显示方式定义
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define NUMBERBOX_DISPMODE_NUMBER           0//数字方式显示
#define NUMBERBOX_DISPMODE_MONEY            1//货币方式
#define NUMBERBOX_DISPMODE_HEX              2//十六进制方式显示

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  双态按钮状态定义
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define BINARYBUTTON_STATE_DISENABLE        0//双态按钮，不使能状态
#define BINARYBUTTON_STATE_ENABLE           1//双态按钮，使能状态

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  进度条方向定义
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define PROGRESSBAR_DIR_H                   0//进度条方向横向
#define PROGRESSBAR_DIR_V                   1//进度条方向纵向

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  滑动块方向定义
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define SLIDER_DIR_H                        0//滑动块横向滑动
#define SLIDER_DIR_V                        1//滑动块纵向滑动

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  选项框状态和样式定义
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define OPTIONBOX_STATE_NOCHOOSE            0//不选择状态
#define OPTIONBOX_STATE_CHOOSE              1//选择状态
#define OPTIONBOX_TYPE_RECT                 0//矩形样式
#define OPTIONBOX_TYPE_CIRCLE               1//圆形样式

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  曲线框扫描方向和极坐标位置定义
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define GRAPH_SCAN_LR                       0//左到右推进
#define GRAPH_SCAN_RL                       1
#define GRAPH_XAXIS_D                       0//X轴位置，底部、中部、顶部
#define GRAPH_XAXIS_C                       1
#define GRAPH_XAXIS_T                       2
#define GRAPH_YAXIS_L                       0//y轴位置，居左，居中，居右
#define GRAPH_YAXIS_C                       1
#define GRAPH_YAXIS_R                       2

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *GUI库中  动画图片框的动画样式
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define PICANIMATION_TYPE_lSLIDEINTO   0           //从左滑入
#define PICANIMATION_TYPE_rSLIDEINTO   1           //从右滑入
#define PICANIMATION_TYPE_uSLIDEINTO   2           //从上滑入
#define PICANIMATION_TYPE_dSLIDEINTO   3           //从下滑入
#define PICANIMATION_TYPE_luSLIDEINTO  4           //左上滑入
#define PICANIMATION_TYPE_ruSLIDEINTO  5           //右上滑入
#define PICANIMATION_TYPE_ldSLIDEINTO  6           //左下滑入
#define PICANIMATION_TYPE_rdSLIDEINTO  7           //右下滑入
#define PICANIMATION_TYPE_lSLIDEOUT    8           //向左滑出
#define PICANIMATION_TYPE_rSLIDEOUT    9           //向右滑出
#define PICANIMATION_TYPE_uSLIDEOUT    10          //向上滑出
#define PICANIMATION_TYPE_dSLIDEOUT    11          //向下滑出
#define PICANIMATION_TYPE_luSLIDEOUT   12          //左上滑出
#define PICANIMATION_TYPE_ruSLIDEOUT   13          //右上滑出
#define PICANIMATION_TYPE_ldSLIDEOUT   14          //左下滑出
#define PICANIMATION_TYPE_rdSLIDEOUT   15          //右下滑出
#define PICANIMATION_TYPE_hOPEN        16          //横向开幕  （左右滑出）
#define PICANIMATION_TYPE_vOPEN        17          //纵向开幕   (上下滑出)
#define PICANIMATION_TYPE_hCLOSE       18          //横向闭幕   （左右滑入）
#define PICANIMATION_TYPE_vCLOSE       19          //纵向闭幕(上下滑入)
#define PICANIMATION_TYPE_CORNERINTO   20           //纵向闭幕(四角滑入)
#define PICANIMATION_TYPE_CORNEROUT    21           //纵向闭幕(四角滑出)
#define PICANIMATION_TYPE_SHUTTER      22          //百叶窗
//#define PICANIMATION_TYPE_PLAID        23          //格栅效果
#ifdef __cplusplus
}
#endif

#endif
