#ifndef _ADC_R__H
#define _ADC_R__H
#include "M1130_adc.h"

void myADC_Init(ADC_TypeDef *ADCx);
void ADC_Test(void);
float AD_Conv(uint32_t ad_raw);


#endif
