/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_tim.h"
#include <string.h>



void delay_ms(uint16_t ms)
{
	uint16_t i;
	for(; ms>0; ms--)
		for(i=0; i<4000; i++);
}


uint16_t rx_buf[256];
uint16_t rxlen;
uint16_t rx_flag;
uint16_t rx_ptr;
void UART1_IRQHandler(void)
{

    if((UART1->INTR & UART_IT_RXIS) != RESET)
    {
     rx_buf[rx_ptr++] = UART1->DATA;
     UART1->INTR_CLR = UART_INTR_RXIS;
    }
    if((UART1->INTR & UART_IT_TFEIS) != RESET)
    {
       UART1->INTR_CLR = UART_IT_TFEIS; 
        
    }

      if((UART1->INTR & UART_IT_RTIS) != RESET)
    {
    while (!UART_GetFlagStatus(UART1, UART_FLAG_RXFE))
            rx_buf[rx_ptr++] = UART1->DATA;
            rxlen =rx_ptr;
            rx_flag = 1;
            rx_ptr = 0;
        UART1->INTR_CLR = UART_INTR_RTIS;
        }
}
void UART1_Configuration(void)
{
  UART_InitTypeDef UART_InitStructure;
  NVIC_InitTypeDef NVIC_InitStruct;


    RCC_ResetAHBCLK(1 << AHBCLK_BIT_UART1);
    RCC_SETCLKDivider(RCC_CLOCKFREQ_UART1CLK, 4);
    GPIO_SetPinMux(GPIO0, GPIO_Pin_18, GPIO_FUNCTION_2);
    GPIO_SetPinMux(GPIO0, GPIO_Pin_19, GPIO_FUNCTION_2);

  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE; 
  NVIC_InitStruct.NVIC_IRQChannel = UART1_IRQn;
  NVIC_InitStruct.NVIC_IRQChannelPriority = 2;
  NVIC_Init(&NVIC_InitStruct);

  UART_Reset(UART1);
  UART_StructInit(&UART_InitStructure);
  UART_InitStructure.UART_BaudRate = 115200;
  UART_InitStructure.UART_FEN=UART_FEN_Disable;//关闭fifo 一字节接收中断
  UART_Init(UART1, &UART_InitStructure);
  /* set timeout to 8 uart clk cycle len */
  UART1->CTRL0_CLR = UART_CTRL0_RXTIMEOUT;
  UART1->CTRL0_SET = 8 << 16;
  UART1->CTRL3_SET = 0x00000001;//配置为9位数据位
  UART_ITConfig(UART1, UART_IT_RXIEN | UART_IT_RTIEN, ENABLE);
  UART_Cmd(UART1, ENABLE);
}

void UART0_Configuration(void)
{
  UART_InitTypeDef UART_InitStructure;

  RCC_ResetAHBCLK(1 << AHBCLK_BIT_UART0);
  RCC_SETCLKDivider(RCC_CLOCKFREQ_UART0CLK, 4);
    
   GPIO_SetPinMux(GPIO0, GPIO_Pin_16, GPIO_FUNCTION_3);
   GPIO_SetPinMux(GPIO0, GPIO_Pin_17, GPIO_FUNCTION_3);

  UART_Reset(UART0);
  UART_StructInit(&UART_InitStructure);
  UART_InitStructure.UART_BaudRate = 115200;
  UART_InitStructure.UART_FEN=UART_FEN_Disable;//关闭fifo 一字节接收中断
  UART_Init(UART0, &UART_InitStructure);
  /* set timeout to 8 uart clk cycle len */
  UART0->CTRL0_CLR = UART_CTRL0_RXTIMEOUT;
  UART0->CTRL0_SET = 8 << 16;
  UART0->CTRL3_SET = 0x00000001;//配置为9位数据位
  UART_Cmd(UART0, ENABLE);
}



void UART0_SendData( uint16_t Data,uint16_t len)
{
   for(int i=0; i<len; i++)
    {
        UART0->DATA = Data;
    	while (UART_GetFlagStatus(UART0, UART_FLAG_TXFE) == RESET)
	    {}
    }
}


uint16_t senddata;
int main()
{
    UART0_Configuration();
    UART1_Configuration();
    senddata=0x131;
	while(1){
        UART0_SendData(senddata,1);
        delay_ms(1000);
        if(senddata==0x131)senddata=0x031;
        else               senddata=0x131;
        
    }
}

/* Private function prototypes -----------------------------------------------*/
#ifdef __GNUC__
  /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
     set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Retargets the C library printf function to the UART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
	/* Place your implementation of fputc here */
	/* e.g. write a character to the UART */
	UART_SendData(UART1, (uint8_t) ch);
	
	/* Loop until the end of transmission */
	while (UART_GetFlagStatus(UART1, UART_FLAG_TXFE) == RESET)
	{}
	
	return ch;
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  printf("Wrong parameters value: file %s on line %d\r\n", file, line);
  /* Infinite loop */
  while (1)
  {
  }
}
#endif
