/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "m1130_uart.h"  
#include "misc.h"
#include "m1130_qspi.h"	 
#include "m1130_eval_qspi.h"
#include "m1130_utility_usart.h"
#include "ff.h"

void delay_ms(uint16_t ms)
{
	uint16_t i;
	for(; ms>0; ms--)
		for(i=0; i<4000; i++);
}
//注意，例程使用了DMA，buff需要四字节对齐，定义时加上修饰符__align(4)   
FATFS Fatfs;			/* FatFs文件系统对象 */
FIL file;		 		/* 文件对象 */
FRESULT f_res;  		/* 文件操作结果 */
UINT fnum;       		/* 文件成功读写数量 */
char fpath[100]; 		/* 保存当前扫描路径 */
char readbuffer[4096];             /*  */
int main()
{
    BYTE work[FF_MAX_SS]; 
	FATFS *pfs;
	DWORD fre_clust, fre_sect, tot_sect, textsize;
    UART_Configuration(UART1, 115200, UART1_GP2);
	printf("\n*************** 挂载设备 ***************\r\n");
	f_res = f_mount(&Fatfs, "0:",1);
  if (f_res != FR_OK) {
        printf("mount failed  %d\r\n",f_res);
		/* 格式化FLASH */
		f_res=f_mkfs("0:", 0, work,sizeof work); 
		if(f_res == 0){
			/* 设置Flash磁盘的名字 */
			f_setlabel((const TCHAR *)"0:");	//设置Flash磁盘的名字
			  printf("mount success\r\n");
		}
		else{  
			printf("mount failed  %d\r\n", f_res);
			while (1);	
		}
		delay_ms(1000);
  }
  else{printf("mount success\r\n");};
  
  printf("\n*************** 设备信息获取 ***************\r\n");
  /* 获取设备信息和空簇大小 */
  f_res = f_getfree("0:", &fre_clust, &pfs);

  /* 计算得到总的扇区个数和空扇区个数 */
  tot_sect = (pfs->n_fatent - 2) * pfs->csize;
  fre_sect = fre_clust * pfs->csize;

  /* 打印信息(4096 字节/扇区) */
    printf("》设备总空间：%d KB。\n》可用空间：  %d KB。\n", tot_sect *4, fre_sect *4);
    printf("\n******** 文件定位和格式化写入功能测试 ********\r\n");
    f_res = f_open(&file, "0:test.txt",
                          FA_CREATE_ALWAYS|FA_WRITE|FA_READ );
	if ( f_res == FR_OK )
	{
    /*  文件定位 */
    f_res = f_lseek(&file,0);
    if (f_res == FR_OK)
    {
      /* 格式化写入，参数格式类似printf函数 */
      f_printf(&file,"\nhello world！\n");
	  /* 关闭文件 */
	  f_close(&file);
	  /* 打开前面创建的文件 */
	  f_res = f_open(&file,"0:/test.txt",FA_READ);
	  /* 得到文件大小 */	
	  textsize=f_size(&file);
	  printf("find app code file ,size:%dByte\r\n",textsize);
      /*  文件定位到文件起始位置 */
      f_res = f_lseek(&file,0);
      /* 读取文件所有内容到缓存区 */
      f_res = f_read(&file,readbuffer,textsize,&fnum);
      if(f_res == FR_OK)
      {
        printf("》文件内容：\n%s\n",readbuffer);
      }
    }
	/* 关闭文件 */
    f_close(&file);    
	}
  else
  {
    printf("!! 打开文件失败：%d\n",f_res);

  }
 

	while(1);
}


