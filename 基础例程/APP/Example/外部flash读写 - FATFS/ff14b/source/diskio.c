/*-----------------------------------------------------------------------*/
/* Low level disk I/O module SKELETON for FatFs     (C)ChaN, 2019        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "ff.h"			/* Obtains integer types */
#include "diskio.h"		/* Declarations of disk functions */
#include "m1130_qspi.h"	 
#include "m1130_eval_qspi.h"
/* Definitions of physical drive number for each drive */

#define EX_FLASH	0	/* Example: Map SPI FLASH to physical drive 0 */


//对于W25Q128
#define FLASH_SECTOR_SIZE 	4096	
#define FLASH_SECTOR_COUNT 	1024    //W25Q128,前16M字节给FATFS占用	
#define FLASH_BLOCK_SIZE   	1     		//每个BLOCK有8个扇区	

/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
	BYTE pdrv		/* Physical drive nmuber to identify the drive */
)
{
	return RES_OK;

}



/*-----------------------------------------------------------------------*/
/* Inidialize succ a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE pdrv				/* Physical drive nmuber to identify the drive */
)
{
	switch (pdrv) {
	
	case EX_FLASH :
		qspi_flash_init(QSPI1, 4, QSPI_MASTER_MODE, 2, 0, QSPI1_GP3, 0);
		QSPI1->BUSY_DLY &= 0xFFFFF0FF;	//clean [11:8]
		QSPI1->BUSY_DLY |= (1<<8);
		qspi_flash_global_unprotect(QSPI1);//以防万一Flash默认值不是可写入
		return RES_OK;
		break;
	}
	return STA_NOINIT;
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE pdrv,		/* Physical drive nmuber to identify the drive */
	BYTE *buff,		/* Data buffer to store read data */
	LBA_t sector,	/* Start sector in LBA */
	UINT count		/* Number of sectors to read */
)
{
	switch (pdrv) {
		case EX_FLASH://外部flash
			for(;count>0;count--)
			{
//				qspi_flash_read(QSPI1, buff, sector*FLASH_SECTOR_SIZE, FLASH_SECTOR_SIZE);	
				qspi_flash_quad_io_read(QSPI1, buff, sector*FLASH_SECTOR_SIZE, FLASH_SECTOR_SIZE);	
				sector++;
				buff+=FLASH_SECTOR_SIZE;
			}

			return RES_OK;
			break;
	}

	return RES_PARERR;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if FF_FS_READONLY == 0

DRESULT disk_write (
	BYTE pdrv,			/* Physical drive nmuber to identify the drive */
	const BYTE *buff,	/* Data to be written */
	LBA_t sector,		/* Start sector in LBA */
	UINT count			/* Number of sectors to write */
)
{

	switch (pdrv) {
		
		case EX_FLASH://外部flash
			for(;count>0;count--)
			{		
                qspi_flash_erase_block_4k(QSPI1,sector*FLASH_SECTOR_SIZE );				
//				qspi_flash_write(QSPI1,(uint8_t*)buff,sector*FLASH_SECTOR_SIZE,FLASH_SECTOR_SIZE);
				qspi_flash_quad_io_write(QSPI1,(uint8_t*)buff,sector*FLASH_SECTOR_SIZE,FLASH_SECTOR_SIZE);
				sector++;
				buff+=FLASH_SECTOR_SIZE;
			}
			return RES_OK;
			break;
	}

	return RES_PARERR;
}

#endif


/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
	DRESULT res;
	switch (pdrv) {
		case EX_FLASH://外部flash
			switch(cmd)
					{
						case CTRL_SYNC:
							res = RES_OK; 
							break;	 
						case GET_SECTOR_SIZE:
							*(WORD*)buff = FLASH_SECTOR_SIZE;
							res = RES_OK;
							break;	 
						case GET_BLOCK_SIZE:
							*(WORD*)buff = FLASH_BLOCK_SIZE;
							res = RES_OK;
							break;	 
						case GET_SECTOR_COUNT:
							*(DWORD*)buff = FLASH_SECTOR_COUNT;
							res = RES_OK;
							break;
						default:
							res = RES_PARERR;
							break;
					}
					
			break;
	}

	return res;
}
//获得时间
//User defined function to give a current time to fatfs module      */
//31-25: Year(0-127 org.1980), 24-21: Month(1-12), 20-16: Day(1-31) */                                                                                                                                                                                                                                          
//15-11: Hour(0-23), 10-5: Minute(0-59), 4-0: Second(0-29 *2) */                                                                                                                                                                                                                                                
DWORD get_fattime (void)
{				 
	return 0;
}			 



