/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "m1130_uart.h"
#include "m1130_rcc.h" 
#include "m1130_gpio.h" 
//#include "m1130_eval_i2c.h"
#include "m1130_utility_usart.h"
#include "m1130_utility_i2c.h"
#include "m1130_i2c.h"
#include "string.h"
#include "stdio.h"


#define Master_IIC_Use_IRQ
#define IIC_Use_10bits_Addr
#define IIC_SLAVER_ADDRESS  0X55

#define IIC_MASTER       I2C0
#define IIC_MASTER_IRQn  I2C0_IRQn
#define IIC_MASTER_GPIO  I2C0_GP1

#define IIC_SLAVER       I2C0
#define IIC_SLAVER_IRQn  I2C1_IRQn
#define IIC_SLAVER_GPIO  I2C1_GP11_04

#define  IIC_TEST_BUFFER_LEN     32
static char Slaver_RxBuffer[IIC_TEST_BUFFER_LEN];
static char Master_RxBuffer[IIC_TEST_BUFFER_LEN];
static volatile int8_t Slaver_RxLen = 0;

static volatile uint8_t stopCount;
static volatile uint8_t startCount;//Used to count Start_DET 
static volatile uint8_t reqCount;
static uint8_t sendData = 0;

/**
 * @brief delay function
 * 
 * @param ms 
 */
static void IIC_TestDelay(uint32_t ms)
{
  uint32_t i, j;
  for(i=0; i < ms; i++){
    for(j=0; j<100; j++){
      __nop();
    }
  }
}

void I2C0_IRQHandler(void)
{
    __IO unsigned int stat = 0;
	stat = I2C_GetITFlag(IIC_SLAVER);
  if(I2C_CheckITFlag(IIC_SLAVER, stat, I2C_IC_RAW_INTR_STAT_RD_REQ)){
    //I2C Master Request data
    IIC_SLAVER->DATA_CMD = sendData & 0x00ff;
    // IIC_SLAVER->DATA_CMD = 0xaa;
    sendData++;
    reqCount++;
    I2C_ClearRawITPendingBit(IIC_SLAVER, I2C_IC_RAW_INTR_STAT_RD_REQ);
  }
  if(I2C_CheckITFlag(IIC_SLAVER, stat, I2C_IC_RAW_INTR_STAT_RX_FULL)){
    //RX BUF full RX_TL 1BYTES
    Slaver_RxBuffer[Slaver_RxLen] = IIC_SLAVER->DATA_CMD&0xff;
    Slaver_RxLen ++;
    if(Slaver_RxLen >= IIC_TEST_BUFFER_LEN)Slaver_RxLen = 0;
  }
}

void I2C_Slaver_Configuration(void)
{
    RCC_ResetAHBCLK(1<<AHBCLK_BIT_I2C0);
    GPIO_SetPinMux(GPIO0, GPIO_Pin_0, GPIO_FUNCTION_2);  /*SDA*/
    GPIO_SetPinMux(GPIO0, GPIO_Pin_1, GPIO_FUNCTION_2);  /*SCL*/
    
    I2C_InitTypeDef I2C_InitStruct;

    I2C_InitStruct.I2C_Mode = I2C_SLAVE_MODE; 
	I2C_InitStruct.I2C_ClockSpeed = I2C_Speedmode_FAST;
	I2C_InitStruct.I2C_10BITADDR_SLAVE = I2C_7BITADDR_SLAVE;
	I2C_InitStruct.I2C_OwnAddress = IIC_SLAVER_ADDRESS;    //   
	I2C_InitStruct.I2C_10BITADDR_MASTER = I2C_7BITADDR_MASTER;
	I2C_InitStruct.I2C_RESTART_EN = I2C_RESTART_ENABLED;
    I2C_Init(I2C0,&I2C_InitStruct);
    
    
    NVIC_InitTypeDef NVIC_InitStruct;
    NVIC_InitStruct.NVIC_IRQChannel = I2C0_IRQn;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
    NVIC_Init(&NVIC_InitStruct);
    I2C_ClearITPendingBit(I2C0); 
    
    I2C_ITConfig(I2C0, I2C_IT_RX_FULL | I2C_IT_RD_REQ, ENABLE);
    I2C0->SDA_SETUP=0;
    I2C_Cmd(I2C0, ENABLE, 1);//
}


/**
 * @brief Test function master read data from Slaver
 * 
 */
void I2C_TestReadFromSlaver(void)
{
  int i;

    I2C_Configuration(I2C_SPEED_STD,I2C_GP0_A);

  while(1){
    memset(Slaver_RxBuffer, 0, sizeof(Slaver_RxBuffer));
    startCount = 0;
    stopCount = 0;
    reqCount = 0;
    
    I2C_Read(IIC_SLAVER_ADDRESS, 0x55, 0, (uint8_t *)(&Master_RxBuffer[0]), IIC_TEST_BUFFER_LEN);
    printf("Master Rec Data:  \r\n");
    for (i = 0; i < IIC_TEST_BUFFER_LEN; i++){
      printf("0x%02x,  ", Master_RxBuffer[i]);
    }
    printf("\r\n");
    printf("startCount = %d,  stopCount = %d, reqCount = %d\r\n",startCount, stopCount, reqCount);
    startCount = 0;
    stopCount = 0;
    reqCount = 0;
    IIC_TestDelay(30000);
  }
}


/**
 * @brief Test function master write data to Slaver
 * 
 */
void I2C_TestWriteToSlaver(void)
{
  uint32_t i;
  I2C_Configuration(I2C_SPEED_STD,I2C_GP0_A);
  while(1){
      
    memset(Master_RxBuffer, 0, sizeof(Master_RxBuffer));
    // for(i = 0; i < IIC_TEST_BUFFER_LEN; i++)Master_RxBuffer[i] = sendData++;
    for(i = 0; i < IIC_TEST_BUFFER_LEN; i++)Master_RxBuffer[i] = i;

    I2C_Write(IIC_SLAVER_ADDRESS, 0x55, 0, (uint8_t *)(&Master_RxBuffer[0]), IIC_TEST_BUFFER_LEN);
    IIC_TestDelay(30000);
  }
}

void I2C_TestSlaver(void)
{
 
    I2C_Slaver_Configuration();

    
  while(1){
      
  }
}


//通过对接两块开发板的IIC接口测试
int main()
{   
    UART_Configuration(UART1, 115200, UART1_GP2);
	printf("\r\n-----------------|- M1130 Standard Peripheral Library Driver -|-----------------\r\n");
    I2C_TestSlaver(); //从机接收
//    I2C_TestWriteToSlaver();//主机发送
//    I2C_TestReadFromSlaver();//主机读取
	while (1)
    {
    
    IIC_TestDelay(30000);

    }
	
}


