/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_utility_usart.h"

#if 0
//64pin
	#define 	KEY_PIN		GPIO_Pin_17
	#define 	LED_PIN		GPIO_Pin_9
	#define 	LED_PORT	GPIO1
#else
//48pin
	#define 	KEY_PIN		GPIO_Pin_23
	#define 	LED_PIN		GPIO_Pin_20
	#define 	LED_PORT	GPIO0
#endif


void GPIO_IRQInit(void)
{
	NVIC_InitTypeDef NVIC_InitStruct;

	NVIC_InitStruct.NVIC_IRQChannel = GPIO0_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
	NVIC_Init(&NVIC_InitStruct);
}

uint32_t data;
uint32_t irs;
void GPIO0_IRQHandler(void)
{
	data = GPIO0_IT->MIS;
	irs  = GPIO0_IT->IRS;
	GPIO0_IT->IC = data;
	__asm("NOP");
	__asm("NOP");

	if(GPIO_ReadPin(GPIO0, KEY_PIN)){
		GPIO_SetPin(LED_PORT, LED_PIN);
    	printf("LED ON\r\n");
	}else{
		GPIO_ClearPin(LED_PORT, LED_PIN);
    	printf("LED OFF\r\n");
	}
}

int main()
{
    UART_Configuration(UART1, 115200, UART1_GP2);
    printf("\r\n-----------------|- LED&Key test -|-----------------\r\n");
    printf("Press Key: LED is ON\r\n");
 
	GPIO_SetPinMux(GPIO0, KEY_PIN, GPIO_FUNCTION_0);
	GPIO_SetPinDir(GPIO0, KEY_PIN, GPIO_Mode_IN);

	GPIO_ConfigPull(GPIO0, KEY_PIN, GPIO_PULL_DOWN);
	GPIO_EdgeITEnable(GPIO0_IT, KEY_PIN, GPIO_IRQ_EDGE_DOUBLE);

	GPIO_SetPinMux(LED_PORT, LED_PIN, GPIO_FUNCTION_0);
	GPIO_SetPinDir(LED_PORT, LED_PIN, GPIO_Mode_OUT);
	GPIO_ClearPin(LED_PORT, LED_PIN);

	//clean IRQ
	GPIO_IRQInit();
	while(1);
}

