/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_tim.h"
#include <string.h>


       

typedef enum
{
  UART0_GP2,
  UART0_GP7,
  UART1_GP0,
  UART1_GP2,
  UART2_GP0,
  UART2_GP7,
} UartPinGroup;

void delay_ms(uint16_t ms)
{
	uint16_t i;
	for(; ms>0; ms--)
		for(i=0; i<4000; i++);
}


uint8_t TIMES=0;
uint8_t rx_buf[256];
uint16_t rxlen;
uint16_t rx_flag;
uint16_t rx_ptr;
void UART1_IRQHandler(void)
{

    if((UART1->INTR & UART_IT_RXIS) != RESET)
    {

      rx_buf[rx_ptr++] = UART1->DATA;
      TIMES++;
        if( rx_buf[rx_ptr-1]==0x0A)
        {
        rxlen =rx_ptr;
        rx_flag = 1;
        rx_ptr = 0;
        }
     UART1->INTR_CLR = UART_INTR_RXIS;
    }
}

/**
  * @brief  configure uart
  * @param  UARTx: Select the UART peripheral.
  *   This parameter can be one of the following values:
  *   UARTx, x:[0:1:2].
  * @param  BaudRate: Set uart baudrate
  * @param  PinGroup: Select uart pin group to set pin mux
  *   This parameter can be one of the following values:
  *   UART0_GP2/7, UART1_GP0/2, UART2_GP0/7
  * @param  RxBuf: Set uart receive buffer
  * @retval:none
  */

void UART_Configuration(UART_TypeDef *UARTx, uint32_t BaudRate, UartPinGroup PinGroup)
{
  UART_InitTypeDef UART_InitStructure;
  NVIC_InitTypeDef NVIC_InitStruct;

  RCC_UARTCLKSel(RCC_UARTCLK_SOURCE_SYSPLL);
  if (UARTx == UART0)
  {
    RCC_ResetAHBCLK(1 << AHBCLK_BIT_UART0);
    RCC_SETCLKDivider(RCC_CLOCKFREQ_UART0CLK, 4);
    NVIC_InitStruct.NVIC_IRQChannel = UART0_IRQn;
  }
  else if (UARTx == UART1)
  {
    RCC_ResetAHBCLK(1 << AHBCLK_BIT_UART1);
    RCC_SETCLKDivider(RCC_CLOCKFREQ_UART1CLK, 4);
    NVIC_InitStruct.NVIC_IRQChannel = UART1_IRQn;
  }
  else if (UARTx == UART2)
  {
    RCC_ResetAHBCLK(1 << AHBCLK_BIT_UART2);
    RCC_SETCLKDivider(RCC_CLOCKFREQ_UART2CLK, 4);
    NVIC_InitStruct.NVIC_IRQChannel = UART2_IRQn;
  }

  switch (PinGroup)
  {
  case UART0_GP2:
    GPIO_SetPinMux(GPIO0, GPIO_Pin_16, GPIO_FUNCTION_3);
    GPIO_SetPinMux(GPIO0, GPIO_Pin_17, GPIO_FUNCTION_3);
    break;
  case UART0_GP7:
    GPIO_SetPinMux(GPIO1, GPIO_Pin_24, GPIO_FUNCTION_2);
    GPIO_SetPinMux(GPIO1, GPIO_Pin_25, GPIO_FUNCTION_2);
    break;
  case UART1_GP0:
    GPIO_SetPinMux(GPIO0, GPIO_Pin_4, GPIO_FUNCTION_2);
    GPIO_SetPinMux(GPIO0, GPIO_Pin_3, GPIO_FUNCTION_2);
    break;
  case UART1_GP2:
    GPIO_SetPinMux(GPIO0, GPIO_Pin_18, GPIO_FUNCTION_2);
    GPIO_SetPinMux(GPIO0, GPIO_Pin_19, GPIO_FUNCTION_2);
    break;
  case UART2_GP0:
    GPIO_SetPinMux(GPIO0, GPIO_Pin_0, GPIO_FUNCTION_3);
    GPIO_SetPinMux(GPIO0, GPIO_Pin_1, GPIO_FUNCTION_3);
    break;
  case UART2_GP7:
    GPIO_SetPinMux(GPIO1, GPIO_Pin_26, GPIO_FUNCTION_2);
    GPIO_SetPinMux(GPIO1, GPIO_Pin_27, GPIO_FUNCTION_2);
    break;
  default:
    break;
  }

  NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_InitStruct.NVIC_IRQChannelPriority = 2;
  NVIC_Init(&NVIC_InitStruct);

  UART_Reset(UARTx);
  UART_StructInit(&UART_InitStructure);
  UART_InitStructure.UART_BaudRate = BaudRate;
  UART_InitStructure.UART_FEN = UART_FEN_Disable;

  UART_Init(UARTx, &UART_InitStructure);
  /* set timeout to 8 uart clk cycle len */
//  UARTx->CTRL0_CLR = UART_CTRL0_RXTIMEOUT;
//  UARTx->CTRL0_SET = 16 << 16;

  UART_ITConfig(UARTx, UART_IT_RXIEN, ENABLE);
  UART_Cmd(UARTx, ENABLE);
}
	#define 	LED_PIN		GPIO_Pin_20
	#define 	LED_PORT	GPIO0
void LED_Init(void)
{
 
   GPIO_SetPinMux(LED_PORT, LED_PIN, GPIO_FUNCTION_0);
	GPIO_SetPinDir(LED_PORT, LED_PIN, GPIO_Mode_OUT);
	GPIO_ClearPin(LED_PORT, LED_PIN);
}

int main()
{

    uint16_t t;  
	uint16_t len;	
	uint16_t times=0;
    LED_Init();   
    UART_Configuration(UART1, 115200, UART1_GP2);
    printf("\r\n开发板 串口实验\r\n");
	while(1){
        
         if(rx_flag== 1)
		{					   
			len=rxlen;//得到此次接收到的数据长度
			printf("\r\n您发送的消息为:\r\n\r\n");
			for(t=0;t<len;t++)
			{
				UART_SendData(UART1, rx_buf[t]);//向串口1发送数据
				while((UART1->STAT&UART_FLAG_TXFE)==0) ;//等待发送结束
			}
			printf("\r\n\r\n");//插入换行
            printf("TIMES== %d",TIMES);
            TIMES=0;
			   rxlen=0;
            rx_flag=0;
		}else
		{
			times++;
			if(times%5000==0)
			{
				printf("\r\n开发板 串口实验\r\n");
			}
			if(times%200==0)printf("请输入数据,以回车键结束\n");  
            if(times%30==0)LED_PORT->DT_TOG=GPIO_Pin_20;//闪烁LED,提示系统正在运行.
			delay_ms(10);   
		}
    }
}

/* Private function prototypes -----------------------------------------------*/
#ifdef __GNUC__
  /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
     set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Retargets the C library printf function to the UART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
	/* Place your implementation of fputc here */
	/* e.g. write a character to the UART */
	UART_SendData(UART1, (uint8_t) ch);
	
	/* Loop until the end of transmission */
	while (UART_GetFlagStatus(UART1, UART_FLAG_TXFE) == RESET)
	{}
	
	return ch;
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  printf("Wrong parameters value: file %s on line %d\r\n", file, line);
  /* Infinite loop */
  while (1)
  {
  }
}
#endif
