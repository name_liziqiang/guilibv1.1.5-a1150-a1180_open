/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_wdg.h"
#include "m1130_rtc.h"
#include "m1130_utility_rtc.h"
#include "m1130_qspi.h"	 
#include "m1130_eval_qspi.h"
#include "m1130_utility_usart.h"

//#define FLASH_4BYTES_ADDR



#define TEST_START_ADDR		0x1000		//
#define TEST_DATA_SIZE		64
void delay_ms(uint16_t ms)
{
	uint16_t i;
	for(; ms>0; ms--)
		for(i=0; i<4000; i++);
}
//注意，例程使用了DMA，buff需要四字节对齐，定义时加上修饰符__align(4)
__align(4) uint8_t writeBuf[TEST_DATA_SIZE]= {0};
__align(4) uint8_t tempBuf[TEST_DATA_SIZE]= {0};
int main()
{
	int i;
    UART_Configuration(UART1, 115200, UART1_GP2);
	printf("Start Test YYY\r\n");

	qspi_flash_init(QSPI1, 4, QSPI_MASTER_MODE, 2, 0, QSPI1_GP3, 0);
	QSPI1->BUSY_DLY &= 0xFFFFF0FF;	//clean [11:8]
	QSPI1->BUSY_DLY |= (1<<8);
	qspi_flash_global_unprotect(QSPI1);//以防万一Flash默认值不是可写入	
	
	qspi_flash_erase_block_4k(QSPI1, TEST_START_ADDR);
	for(i = 0;i < TEST_DATA_SIZE; i++){
		writeBuf[i] = i;
	}
	qspi_flash_quad_io_write(QSPI1, writeBuf, TEST_START_ADDR, TEST_DATA_SIZE);
	
    delay_ms(10);
    
    qspi_flash_quad_io_read(QSPI1, tempBuf, TEST_START_ADDR, TEST_DATA_SIZE);	
	for(i = 0;i< TEST_DATA_SIZE; i++){
	printf("tempBuf[%d]= %02x", i, tempBuf[i]);
    printf("\r\n");
	}

	while(1);
}


