/**
  ******************************************************************************
  * @file    Project/M1130-EVAL/src/m1130_it.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    10-21-2013
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "m1130_it.h"
#include "m1130.h"
#include "m1130_uart.h"
#include "stdio.h"
#include "rthw.h"
#include "rtthread.h"
#include "m1130_gpio.h"
#include "rtdef.h"

void commonDelay(volatile unsigned long time)
{
	while ( time )time--; 
}

void TIM16_IRQHandler(void)
{
	TIM16->SR=0;//清除中断标志

}


//定义一个静态信号量
extern struct rt_semaphore  shell_rx_sem;
char uart_rx_data;
void UART0_IRQHandler(void)
{
	if((UART0->INTR&UART_IT_RXIS)!=0){
		//接收中断
		UART0->INTR_SET=UART_IT_RXIS;
		uart_rx_data=UART0->DATA;
		rt_sem_release(&shell_rx_sem);//释放信号量
	}
	else if((UART0->INTR&UART_IT_RTIS)!=0){
		UART0->INTR_SET=UART_IT_RTIS;
		uart_rx_data=UART0->DATA;
		rt_sem_release(&shell_rx_sem);//释放信号量
	}
}

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
// void NMI_Handler(void)
// {
// }

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
//void HardFault_Handler(void)
//{
//	printf("HardFault_Handler");
//  /* Go to infinite loop when Hard Fault exception occurs */
//  while (1)
//  {
//  }
//}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSV_Handler exception.
  * @param  None
  * @retval None
  */
//void PendSV_Handler(void)
//{
//}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */

void SysTick_Handler(void)
{
		
    /* enter interrupt */
    rt_interrupt_enter();

    rt_tick_increase();

    /* leave interrupt */
    rt_interrupt_leave();
}

/******************************************************************************/
/*            m1130 Peripherals Interrupt Handlers                        */
/******************************************************************************/

/******************************************************************************/
/*                 m1130 Peripherals Interrupt Handlers                        */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_m1130.s).                                                    */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/**
  * @}
  */ 

/**
  * @}
  */ 

//完成 RT Thread Finsh 接口支持

char rt_hw_console_getchar(void)
{
	char ch=(char)-1;
	rt_sem_take(&shell_rx_sem, RT_WAITING_FOREVER);  //接收信号量
	ch=uart_rx_data;
	return ch;
}

void rt_hw_console_output(const char *str)
{
	rt_size_t i = 0, size = 0;
	char a = '\r';

	size = rt_strlen(str);
	for (i = 0; i < size; i++){
		if (*(str + i) == '\n'){
			while((UART0->STAT&UART_FLAG_TXFE)==0);//等待上一次串口数据发送完成  
			UART0->DATA = a;//写DR,串口1将发送数据
		}
		while((UART0->STAT&UART_FLAG_TXFE)==0);//等待上一次串口数据发送完成  
		UART0->DATA = *(str+i);//写DR,串口1将发送数据
	}
}
/******************* (C) COPYRIGHT 2013 ALPHASCALE  *****END OF FILE****/
