/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "m1130_uart.h"
#include "misc.h"
#include "m1130_gpio.h"
#include "rtthread.h"


void outclk_init(void)
{
  GPIO_SetPinMux(GPIO0, GPIO_Pin_25, GPIO_FUNCTION_3);
  RCC->OUTCLKSEL = 0;
  RCC->OUTCLKDIV = 1;
  RCC->OUTCLKUEN = 0;
  RCC->OUTCLKUEN = 1;
}

/**
  * @brief  Main program.asd
  * @param  None
  * @retval None
  */

static void thread_test_entry(void *parameter)
{
	rt_kprintf("rt_test thread start\r\n");
	while(1){
		rt_thread_mdelay(500);
		GPIO0->DT_TOG=GPIO_Pin_20;
	}
}

int main()
{
	//outclk_init();//内部晶振输出，用于校准内部振荡器
	//SystemInit 中将系统时钟初始化为96Mhz
	/*******************初始化一系列MCU外设，然后启动GUI*******************/
	GPIO_SetPinDir(GPIO0, GPIO_Pin_20,GPIO_Mode_OUT);
	GPIO_SetPinMux(GPIO0, GPIO_Pin_20,GPIO_FUNCTION_0);
	GPIO0->DT_SET=GPIO_Pin_20;
	//动态创建一个测试线程
	rt_thread_t tid=RT_NULL;
	tid = rt_thread_create("rt_test",
						   thread_test_entry,RT_NULL,
                           256,
                           RT_THREAD_PRIORITY_MAX-1, 5);
	if(tid!=RT_NULL)rt_thread_startup(tid);
	rt_kprintf("main thread end\r\n");
	return 0;
//	while(1){
//		rt_thread_mdelay(500);
//		GPIO0->DT_TOG=GPIO_Pin_20;
//		//rt_kprintf("main thread run\n");
//	}
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  printf("Wrong parameters value: file %s on line %d\r\n", file, line);
  /* Infinite loop */
  while (1)
  {
  }
}
#endif
