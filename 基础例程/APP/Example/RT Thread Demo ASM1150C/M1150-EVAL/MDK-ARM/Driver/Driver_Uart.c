#include "driver_uart.h"
#include "rtthread.h"
#include "m1130_uart.h"


static rt_err_t uart_init(rt_device_t dev)
{
	return 0;
}
	
static rt_err_t uart_open(rt_device_t dev,rt_uint16_t oflag)
{
	return 0;
}

extern struct rt_semaphore  shell_rx_sem;
extern char uart_rx_data;
static rt_size_t uart_read(rt_device_t dev , rt_off_t pos , void *buffer , rt_size_t size)
{
	while(1){
		rt_sem_take(&shell_rx_sem, RT_WAITING_FOREVER);  //接收信号量
		*(uint8_t *)buffer=uart_rx_data;
		return 1;
	}
}

static rt_size_t uart_write(rt_device_t dev , rt_off_t pos , const void *buffer , rt_size_t size)
{
	char *str=(char *)buffer;
	rt_size_t i = 0;
	char a = '\r';

	for (i = 0; i < size; i++){
		if (*(str + i) == '\n'){
			while((UART0->STAT&UART_FLAG_TXFE)==0);//等待上一次串口数据发送完成  
			UART0->DATA = a;//写DR,串口1将发送数据
		}
		while((UART0->STAT&UART_FLAG_TXFE)==0);//等待上一次串口数据发送完成  
		UART0->DATA = *(str+i);//写DR,串口1将发送数据
	}
	return 1;
}


static struct rt_device uart_dev;
static int uart_register(void)
{
    uart_dev.init  = uart_init;
    uart_dev.open  = uart_open;
    uart_dev.read  = uart_read;
    uart_dev.write = uart_write;
    rt_device_register(&uart_dev, "uart0", 0);
    return 1;
}

