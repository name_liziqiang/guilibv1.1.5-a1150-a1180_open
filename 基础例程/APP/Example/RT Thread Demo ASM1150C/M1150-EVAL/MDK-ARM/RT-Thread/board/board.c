/*
 * Copyright (c) 2006-2019, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2017-07-24     Tanek        the first version
 * 2018-11-12     Ernest Chen  modify copyright
 */
 
#include <stdint.h>
#include <rthw.h>
#include <rtthread.h>


#include "m1130_uart.h"
#include "misc.h"
#include "m1130_gpio.h"



#define _SCB_BASE       (0xE000E010UL)
#define _SYSTICK_CTRL   (*(rt_uint32_t *)(_SCB_BASE + 0x0))
#define _SYSTICK_LOAD   (*(rt_uint32_t *)(_SCB_BASE + 0x4))
#define _SYSTICK_VAL    (*(rt_uint32_t *)(_SCB_BASE + 0x8))
#define _SYSTICK_CALIB  (*(rt_uint32_t *)(_SCB_BASE + 0xC))
#define _SYSTICK_PRI    (*(rt_uint8_t  *)(0xE000ED23UL))

// Updates the variable SystemCoreClock and must be called 
// whenever the core clock is changed during program execution.
extern void SystemInit(void);
void SystemCoreClockUpdate(void)
{
	SystemInit();
}

// Holds the system core clock, which is the system clock 
// frequency supplied to the SysTick timer and the processor 
// core clock.
uint32_t SystemCoreClock=96000000;

static uint32_t _SysTick_Config(rt_uint32_t ticks)
{
    if ((ticks - 1) > 0xFFFFFF)
    {
        return 1;
    }
    
    _SYSTICK_LOAD = ticks - 1; 
    _SYSTICK_PRI = 0xFF;
    _SYSTICK_VAL  = 0;
    _SYSTICK_CTRL = 0x07;  
    
    return 0;
}


#if defined(RT_USING_USER_MAIN) && defined(RT_USING_HEAP)
//#define RT_HEAP_SIZE 4096
//static uint32_t rt_heap[RT_HEAP_SIZE];     // heap default size: 4K(1024 * 4)


//这里不使用 动态数组，查连接文件末尾地址，把剩余空间全部搞成堆
#define M1130_SRAM0_START           0x20000000
#define M1130_SRAM0_END             (M1130_SRAM0_START+32*1024)  //合计32K

#if defined(__CC_ARM) || defined(__CLANG_ARM)
extern int Image$$RW_IRAM1$$ZI$$Limit;                   // RW_IRAM1，需与链接脚本中运行时域名相对应
#define HEAP_BEGIN      ((void *)&Image$$RW_IRAM1$$ZI$$Limit)
#endif 
#define HEAP_END                     M1130_SRAM0_END

RT_WEAK void *rt_heap_begin_get(void)
{
//    return rt_heap;
	return HEAP_BEGIN;
}

RT_WEAK void *rt_heap_end_get(void)
{
//    return rt_heap + RT_HEAP_SIZE;
	return (void *)HEAP_END;
}
#endif

struct rt_semaphore  shell_rx_sem;

static int SystemUart_Init()
{
	RCC_ResetAHBCLK(1 << AHBCLK_BIT_UART0);
	GPIO_SetPinMux(GPIO1, GPIO_Pin_24, GPIO_FUNCTION_2);
  GPIO_SetPinMux(GPIO1, GPIO_Pin_25, GPIO_FUNCTION_2);//设置引脚复用
	UART_InitTypeDef UART_InitStruct;
	UART_StructInit(&UART_InitStruct);
	UART_InitStruct.UART_BaudRate						 =115200;
	UART_InitStruct.UART_RXIFLSEL   				 =UART_RXIFLSEL_2;
	UART_InitStruct.UART_TXIFLSEL   				 =UART_TXIFLSEL_2;
	UART_InitStruct.UART_FEN        				 =UART_FEN_Enable;
	UART_InitStruct.UART_SoftwareFlowControl =UART_SoftwareFlowControl_None;
	UART_InitStruct.UART_HardwareFlowControl =UART_HardwareFlowControl_None;
	UART_InitStruct.UART_Mode                =UART_Mode_Rx|UART_Mode_Tx;
	UART_InitStruct.UART_Parity              =UART_Parity_No;
	UART_InitStruct.UART_StopBits 					 = UART_StopBits_1;
	UART_InitStruct.UART_WordLength 				 = UART_WordLength_8b;
	
	RCC_SetAHBCLK(1<<AHBCLK_BIT_UART0,ENABLE);
	RCC_UARTCLKSel(RCC_UARTCLK_SOURCE_SYSPLL);
	RCC_SETCLKDivider(RCC_CLOCKFREQ_UART0CLK, 32);
	UART_Reset(UART0);
	UART_Init(UART0, &UART_InitStruct);
	
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = UART0_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
  NVIC_InitStruct.NVIC_IRQChannelPriority = 3;
  NVIC_Init(&NVIC_InitStruct);
	
	/* set timeout to 8 uart clk cycle len */
  UART0->CTRL0_CLR = UART_CTRL0_RXTIMEOUT;
  UART0->CTRL0_SET = 16 << 16;

  UART_ITConfig(UART0, UART_IT_RXIEN | UART_IT_RTIEN, ENABLE);//开启接收中断和接收超时中断
  UART_Cmd(UART0, ENABLE);

  rt_sem_init(&shell_rx_sem,"senSRX",0,RT_IPC_FLAG_FIFO);
	rt_kprintf("---------------------------------------------------------------------------------------------\n");
	return 0;
}

INIT_BOARD_EXPORT(SystemUart_Init);
/**
 * This function will initial your board.
 */
void rt_hw_board_init()
{
    /* System Clock Update */
    SystemCoreClockUpdate();
    
    /* System Tick Configuration */
    _SysTick_Config(SystemCoreClock / RT_TICK_PER_SECOND);
		
    /* Call components board initial (use INIT_BOARD_EXPORT()) */
#ifdef RT_USING_COMPONENTS_INIT
    rt_components_board_init();
#endif

#if defined(RT_USING_USER_MAIN) && defined(RT_USING_HEAP)
    rt_system_heap_init(rt_heap_begin_get(), rt_heap_end_get());
#endif
}
