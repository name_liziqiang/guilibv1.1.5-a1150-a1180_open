 /**
   ******************************************************************************
   * @file    m1130_cmp.h
   * @author  Alpscale Software Team
   * @version V1.0.0
   * @date    19-December-2013
   * @brief   This file contains all the functions prototypes for the CMP firmware 
   *          library
   ******************************************************************************
   * @attention
   *
   * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
   * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
   * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
   * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
   * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
   * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
   *
   * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
   ******************************************************************************
   */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __M1130_CMP_H
#define __M1130_CMP_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "m1130.h"

/** @addtogroup M1130_StdPeriph_Driver
  * @{
  */  

/** @defgroup CMP 
  * @brief CMP driver modules
  * @{
  */
typedef struct
{
  uint8_t Channel;            /*!< Select CMP0/CMP1     
                                This parameter can be a value of @ref CMP_channels  */
	uint8_t SelectNegitave;     /*!< Select internel VBG(DISABLE) or external cmp_neg(ENABLE) as reference voltage */
	uint8_t Hysen;              /*!< Specifies whether add 100mV delay for CMP or not,set ENABLE/DISABLE */
}CMP_InitTypeDef;

/** @defgroup CMP_channels
  * @{
  */ 
#define CMP_Channel_0			((uint8_t) 0x01)
#define CMP_Channel_1			((uint8_t) 0x02)
/**
  * @}
  */


/** @defgroup CMP_PERIPH 
  * @{
  */
#define IS_CMP_CHANNEL(PERIPH)	((PERIPH & CMP_Channel_0) || \
																 (PERIPH & CMP_Channel_1))
/**
  * @}
  */


/*  Function used to set the CMP configuration to the default reset state *****/
void CMP_Reset(void);
void CMP_CLKSET(FunctionalState NewState);
void CMP_Init(CMP_InitTypeDef* CMP_InitStruct);
void CMP_ITConfig(uint8_t channel,FunctionalState NewState);
ITStatus CMP_GetITStatus(uint8_t channel);
void CMP_ClearITFlag(uint8_t channel);
uint8_t CMP_GetCompareResult(uint8_t channel);

#ifdef __cplusplus
}
#endif

#endif /*__M1130_CMP_H */

/**
  * @}
  */ 

/**
  * @}
  */ 

/************************ (C) COPYRIGHT Alpscale *****END OF FILE****/
