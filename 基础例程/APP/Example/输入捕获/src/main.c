/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_utility_usart.h"
#include "m1130_tim.h"

void delay_ms(uint16_t ms)
{
	uint16_t i;
	for(; ms>0; ms--)
		for(i=0; i<4000; i++);
}
void TIM16_IRQHandler(void)
{
	
	  static uint32_t tick1;
  static uint32_t oldCount1=0;
  uint32_t count1;
  if ((TIM_GetITStatus(TIM16, TIM_IT_CC1)) != RESET)
  {
		count1 = TIM16->CCR1;	
	if(count1 < oldCount1){
      //Counter overflow
      tick1 = 65535 - oldCount1;
      tick1 += count1;
    }
    else tick1 = count1 - oldCount1;
    oldCount1 = count1;   	
	printf("1:%d\r\n", tick1);
    TIM_ClearITPendingBit(TIM16, TIM_IT_CC1);
  }
}
/*
*********************************************************************************************************************
*函数功能：初始化
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
void TIM16_Init(void)
{
	//初始化定时器16，用着系统心跳
	RCC->AHBCLKCTRL0_SET|=(1<<AHBCLK_BIT_TIM16);//开启定时器16时钟
	
	
	 GPIO_SetPinMux(GPIO1, GPIO_Pin_9,GPIO_FUNCTION_3);

	
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Prescaler=95;//96Mhz/(95+1)=1Mhz
	TIM_TimeBaseStructure.TIM_Period=0xffff;//计数到最大
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_ICInitTypeDef TIM_ICInitStruct;

	//初始化TIM2输入捕获参数
	TIM_ICInitStruct.TIM_Channel             = TIM_Channel_1; //选择输入端 IC1映射到TI1上
	TIM_ICInitStruct.TIM_ICFilter            = 0x00;			//IC1F=0000 配置输入滤波器 不滤波
	TIM_ICInitStruct.TIM_ICPolarity          = TIM_ICPolarity_Rising; //上升沿捕获
	TIM_ICInitStruct.TIM_ICPrescaler         = TIM_ICPSC_DIV1;  //配置输入分频,不分频
	TIM_ICInitStruct.TIM_ICSelection         = TIM_ICSelection_DirectTI;  //映射到TI1上
	TIM_ICInit(TIM16, &TIM_ICInitStruct);      			//
	//中断配置
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel=TIM16_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPriority=0;//2bit位宽
	NVIC_Init(&NVIC_InitStruct);
	TIM_ClearITPendingBit(TIM16, TIM_IT_CC1);     		
    TIM_ITConfig(TIM16, TIM_IT_CC1, ENABLE); 	//Enable TIM interrupt
	TIM_Cmd(TIM16,ENABLE);
}

int main()
{
    UART_Configuration(UART1, 115200, UART1_GP2);
    printf("\r\n-----------------|- TIM INPUT test -|-----------------\r\n");
    
    TIM16_Init();
	while(1){
         
    }
}

