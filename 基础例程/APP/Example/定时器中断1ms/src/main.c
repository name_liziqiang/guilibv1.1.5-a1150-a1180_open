/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_utility_usart.h"
#include "m1130_tim.h"

void delay_ms(uint16_t ms)
{
	uint16_t i;
	for(; ms>0; ms--)
		for(i=0; i<4000; i++);
}
void TIM15_IRQHandler(void)
{
	TIM15->SR=0;//清除中断标志

   GPIO0->DT_TOG=GPIO_Pin_0;
}
/*
*********************************************************************************************************************
*函数功能：初始化一个定时器作为系统心跳-1ms
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
void SystemHeart_Init(void)
{
	//初始化定时器16，用着系统心跳
	RCC->AHBCLKCTRL0_SET|=(1<<AHBCLK_BIT_TIM15);//开启定时器时钟
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Prescaler=95;//96Mhz/(95+1)=1Mhz
	TIM_TimeBaseStructure.TIM_Period=999;//计数到1000刚好是1ms 中断
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	//SYSAHBCLKDIV=4 在SystemInit已经初始化了 AHB时钟96Mhz
	TIM_TimeBaseInit(TIM15, &TIM_TimeBaseStructure);
	TIM_SelectOutputTrigger(TIM15, TIM_TRGOSource_Enable);//选择TIM15的update事件更新为触发源
	TIM_ClearITPendingBit(TIM15, TIM_IT_Update);     			//清除update事件中断标志
	TIM_ITConfig(TIM15, TIM_IT_Update, ENABLE);       			//使能TIM15中断 
	//中断配置
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel=TIM15_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPriority=0;//2bit位宽
	NVIC_Init(&NVIC_InitStruct);
	TIM_Cmd(TIM15,ENABLE);
}

int main()
{
    GPIO_SetPinMux(GPIO0, GPIO_Pin_0,GPIO_FUNCTION_0);
	GPIO_SetPinDir(GPIO0, GPIO_Pin_0,GPIO_Mode_OUT);
    GPIO0->DT_CLR=GPIO_Pin_0;
    SystemHeart_Init();
	while(1){
         
    }
}

