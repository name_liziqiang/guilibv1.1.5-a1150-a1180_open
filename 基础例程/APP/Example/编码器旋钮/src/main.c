/**
  ******************************************************************************
  * @file    Project/m1130-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_dma.h"
#include "m1130_utility_usart.h"

#define 	ENCODE_A_PIN		GPIO_Pin_1
#define 	ENCODE_B_PIN		GPIO_Pin_0
#define 	ENCODE_PORT	GPIO0

#define 	CLOCKWISE     1
#define 	ANTICLOCKWISE 2

void GPIO_IRQInit(void)
{
	NVIC_InitTypeDef NVIC_InitStruct;

	NVIC_InitStruct.NVIC_IRQChannel = GPIO0_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
	NVIC_Init(&NVIC_InitStruct);
}

uint32_t data;
uint32_t irs;
uint8_t  rotation_dir;
uint16_t cnt;
uint8_t  A_FLAG=0;
uint8_t  B_FLAG=0;
void GPIO0_IRQHandler(void)
{
	data = GPIO0_IT->MIS;
	irs  = GPIO0_IT->IRS;
//	GPIO0_IT->IC = data;
//	__asm("NOP");
//	__asm("NOP");
	if((data & GPIO_Pin_1) && !(GPIO_ReadPin(GPIO0, ENCODE_A_PIN))){
		A_FLAG = 1;
		B_FLAG = 0;
		cnt = 0;
	}
	if((data & GPIO_Pin_0) && (GPIO_ReadPin(GPIO0, ENCODE_B_PIN))){
		B_FLAG = 1;
		A_FLAG = 0;
		cnt = 0;
	}
	GPIO0_IT->IC = data;
}

void Encode_Scan(void)
{
  if(A_FLAG == 1){
		cnt++;
		if(cnt>=200){
			if(!(GPIO_ReadPin(GPIO0, ENCODE_A_PIN))){
				if(GPIO_ReadPin(GPIO0, ENCODE_B_PIN)){
					rotation_dir=CLOCKWISE;
					printf("Clockwise rotation \r\n");
				}
				else if(!GPIO_ReadPin(GPIO0, ENCODE_B_PIN)){
					rotation_dir=ANTICLOCKWISE;
					printf("Counter-Clockwise rotation \r\n");
				}
			}
			cnt = 0;
			A_FLAG =0;
		}
	}
	if(B_FLAG == 1){
		cnt++;
		if(cnt>=200){
			if((GPIO_ReadPin(GPIO0, ENCODE_B_PIN))){
				if(GPIO_ReadPin(GPIO0, ENCODE_A_PIN)){
					rotation_dir=CLOCKWISE;
					printf("Clockwise rotation \r\n");
				}
				else if(!GPIO_ReadPin(GPIO0, ENCODE_A_PIN)){
					rotation_dir=ANTICLOCKWISE;
					printf("Counter-Clockwise rotation \r\n");
				}
			}
			cnt = 0;
			B_FLAG =0;
		}
	}
}

int main()
{
	UART_Configuration(UART1, 115200, UART1_GP2);
	printf("\r\n-----------------|- ENCODE test -|-----------------\r\n");
 
	GPIO_SetPinMux(ENCODE_PORT, ENCODE_A_PIN, GPIO_FUNCTION_0);
	GPIO_SetPinDir(ENCODE_PORT, ENCODE_A_PIN, GPIO_Mode_IN);
	GPIO_ConfigPull(ENCODE_PORT, ENCODE_A_PIN, GPIO_PULL_UP);
	GPIO_EdgeITEnable(GPIO0_IT, ENCODE_A_PIN, GPIO_IRQ_EDGE_FALLING);

	GPIO_SetPinMux(ENCODE_PORT, ENCODE_B_PIN, GPIO_FUNCTION_0);
	GPIO_SetPinDir(ENCODE_PORT, ENCODE_B_PIN, GPIO_Mode_IN);
	GPIO_ConfigPull(ENCODE_PORT, ENCODE_B_PIN, GPIO_PULL_DOWN);
	GPIO_EdgeITEnable(GPIO0_IT, ENCODE_B_PIN, GPIO_IRQ_EDGE_RISING);

	//clean IRQ
	GPIO_IRQInit();
	while(1){
		Encode_Scan();
		
	}
}

