/*----------------------------------------------------------------------*/
/* FatFs sample														    */
/*----------------------------------------------------------------------*/

#include <stdio.h>
#include "ff.h"
#include "stdlib.h"
#include "m1130_qspi.h"	 
#include "m1130_eval_qspi.h"
#include "part.h"
#include "m1130_rcc.h"

// #define USE_SECOND_QSPI_FLASH
#define LOADER_APP_FILENAME			"Loader_App.bin"
#define CONFIG_FILENAME			"config.bin"
#define INFO_FILENAME				"Infor.bin"
#define PIC_FILENAME				"pic.bin"
#define BUFF_SIZE						1024//2048

#define LOADER_BURN_ADDR    0
#define APP_BURN_ADDR			  0x8000		//32k

#define LOADER_SIZE					0x8000		//32K
#define CONFIG_SIZE					0x1000		
#define INFO_SIZE						0x1000

#ifndef USE_SECOND_QSPI_FLASH
	#define CONFIG_BURN_ADDR		0x20000		//128k
	#define INFO_BURN_ADDR			0x21000		//132k
	#define PIC_BURN_ADDR 			0x22000   //136k
#else 
	#define CONFIG_BURN_ADDR		0x0	
	#define INFO_BURN_ADDR			0x1000	
	#define PIC_BURN_ADDR 			0x2000
#endif

FATFS* pFatfs;		/* File system object */
FIL *pFil;				/* File object */
FATFS Fatfs;		/* File system object */
FIL Fil;			/* File object */
unsigned char Buff[BUFF_SIZE];		/* 2KB for all temp buffer */
void die (		/* Stop with dying message */
	FRESULT rc	/* FatFs return value */
)
{
	printf("Failed with rc=%u.\r\n", rc);
}

FRESULT upgrade(QSPI_TypeDef* QSPIptr,const char* filename, uint32_t faddr)
{
	FRESULT rc;			/* Result code */
	DIR dir;				/* Directory object */
	UINT br, i, shift;
	UINT readSize;
	DWORD size;

	printf("start to burn %s into flash\r\n",filename);
	rc = f_open(&Fil, filename, FA_READ);
	if(rc){
		printf("file:%s open fail\r\n",filename);
		return rc;
	}
		
	if(faddr == LOADER_BURN_ADDR){
    size = LOADER_SIZE;
  }else if(faddr == APP_BURN_ADDR){
    size = Fil.fsize - LOADER_SIZE;
  }else{
    size = Fil.fsize;
	}
	shift = 0;
	while (shift < size)
	{
		qspi_flash_erase_block_4k(QSPIptr, faddr + shift); //Must be done before write
		shift += 0x1000;
	}
	shift = 0;
	for (;;){
		if (shift + BUFF_SIZE <= size){
			readSize = BUFF_SIZE;
		}
		else{
			readSize = size - shift;
		}
		rc = f_read(&Fil, Buff, readSize, &br); /* Read a chunk of file */
		if (rc || !br)
			break; /* Error or end of file */
		qspi_flash_write(QSPIptr, Buff, faddr + shift, readSize);
		shift += readSize;
		if (shift >= size)
			break;
	}
	f_close(&Fil);
	if (rc){
		return rc;
	}

	return FR_OK;
}

int isUpgrade(QSPI_TypeDef *QSPIptr, const char *filename, int faddr, int size)
{
	int oldVersion = 0;
	int RoldVersion = 0;
	int newVersion = 0;
	FRESULT rc;			/* Result code */
	UINT br;
	//get old version
  if (faddr != APP_BURN_ADDR) {
    qspi_flash_read(QSPIptr, (uint8_t *)(&oldVersion), faddr + size - 4, 4);
	}else{
    while (1) {
			size += 0x8000; //32KB
      qspi_flash_read(QSPIptr, (uint8_t *)(&oldVersion), faddr + size - 8, 4);
      qspi_flash_read(QSPIptr, (uint8_t *)(&RoldVersion), faddr + size - 4, 4);
      if (size > 0x80000) return -1;
      if (oldVersion == ~RoldVersion) break;
    }
	}
	//get new version
	if (f_open(&Fil, filename, FA_READ)){
		printf("file:%s open fail\r\n", filename);
		return -1;
	}
  if (faddr != APP_BURN_ADDR) {
    f_lseek(&Fil, size - 4);
  }else{
    f_lseek(&Fil, pFil->fsize - 8);
	}
	rc = f_read(&Fil, &newVersion, 4, &br); /* Read a chunk of file */
	if (rc || !br){
		return -1;
	}
	f_close(&Fil);
	printf("oldversion=%d,newversion=%d\r\n", oldVersion & 0xffffffff, newVersion);
	if(newVersion > oldVersion){
		return 0;
	}
	return -1;
}

void flashUpgrade(QSPI_TypeDef *QSPIptr)
{
	//Loader.bin
	if (!isUpgrade(QSPIptr, LOADER_APP_FILENAME, LOADER_BURN_ADDR, LOADER_SIZE)){
		upgrade(QSPIptr, LOADER_APP_FILENAME, LOADER_BURN_ADDR);
	}
	//App.bin
	if (!isUpgrade(QSPIptr, LOADER_APP_FILENAME, APP_BURN_ADDR, 0)){
		upgrade(QSPIptr, LOADER_APP_FILENAME, APP_BURN_ADDR); 
	}
	//config.bin
	if(!isUpgrade(QSPIptr,CONFIG_FILENAME,CONFIG_BURN_ADDR,CONFIG_SIZE)){
		upgrade(QSPIptr, CONFIG_FILENAME, CONFIG_BURN_ADDR);
	}
	//Infor.bin and pic.bin
	if (!isUpgrade(QSPIptr, INFO_FILENAME, INFO_BURN_ADDR, INFO_SIZE)){
		upgrade(QSPIptr, INFO_FILENAME, INFO_BURN_ADDR);
		upgrade(QSPIptr, PIC_FILENAME, PIC_BURN_ADDR);
	}
	return;
}
/*-----------------------------------------------------------------------*/
/* Program Main                                                          */
/*-----------------------------------------------------------------------*/

int FatDemo (void)
{
	FRESULT rc;				/* Result code */
	DIR dir;				/* Directory object */
	FILINFO fno;			/* File information object */
	UINT br, i, shift;
	UINT readSize;
	/*注册磁盘卷 0表示 U盘*/
	f_mount(0, &Fatfs);		/* Register volume work area (never fails) */

#if 0

#ifndef USE_SECOND_QSPI_FLASH
	qspi_flash_init(QSPI0, 4, QSPI_MASTER_MODE, 4, 1, QSPI0_GP4, 1);
	flashUpgrade(QSPI0);
#else
	qspi_flash_init(QSPI1, 4, QSPI_MASTER_MODE, 4, 1, QSPI1_GP3, 1);
	flashUpgrade(QSPI1);
#endif

#endif
#if 0
	/**********************************************************/
	//读文件示例
	printf("Open an existing file (data.txt).\r\n");
	rc = f_open(&Fil, "data.txt", FA_READ);
	if (rc) die(rc);

	printf("read the file content.\r\n");
	for (;;) {
		rc = f_read(&Fil, Buff, BUFF_SIZE, &br);	/* Read a chunk of file */
		if (rc || !br) break;			/* Error or end of file */
		// for (i = 0; i < br; i++)		/* Type the data */
		// 	putchar(Buff[i]);
	}
	if (rc) die(rc);

	printf("Close the file.\r\n");
	rc = f_close(&Fil);
	if (rc) die(rc);
#endif

#if 0
	/**********************************************************/
	//创建和写文件示例
	printf("\r\nCreate a new file (hello.txt).\r\n");
	rc = f_open(&Fil, "HELLO.TXT", FA_WRITE | FA_CREATE_ALWAYS);
	if (rc) die(rc);

	printf("\r\nWrite a text data. (Hello world!)\r\n");
	rc = f_write(&Fil, "Hello world!\r\n", 14, &br);
	if (rc) die(rc);
	printf("%u bytes written.\r\n", br);

	printf("\r\nClose the file.\r\n");
	rc = f_close(&Fil);
	if (rc) die(rc);
	/**********************************************************/
#endif
#if 1
	/**********************************************************/
	//列目录示例
	printf("\r\nOpen root directory.\r\n");
	rc = f_opendir(&dir, "");
	if (rc) die(rc);

	printf("\r\nDirectory listing...\r\n");
	for (;;) {
		rc = f_readdir(&dir, &fno);		/* Read a directory item */
		if (rc || !fno.fname[0]) break;	/* Error or end of dir */
		if (fno.fattrib & AM_DIR)
			printf("   <dir>  %s\r\n", fno.fname);
		else
			printf("%8lu  %s\r\n", fno.fsize, fno.fname);
	}
	if (rc) die(rc);
	/**********************************************************/
#endif
#if 0	//read test.bin and write into flash
	rc = f_open(&Fil, "test.bin", FA_READ);
	if (rc) die(rc);

	RCC_SETCLKDivider(RCC_CLOCKFREQ_QSPI1CLK, 4);
	qspi_flash_init(QSPI1, 4, QSPI_MASTER_MODE, 4, 1, QSPI1_GP3, 1);
	qspi_flash_write_enable(QSPI1, 1);	
	qspi_flash_global_unprotect(QSPI1);
	
	qspi_flash_write_enable(QSPI1, 1);
	qspi_flash_erase_block_64k(QSPI1, 0xE0000);//Must be done before write

	shift = 0;
	for (;;) {
		if(shift+512<=Fil.fsize){
			readSize = 512;
		}else{
			readSize = Fil.fsize-shift;
		}
		rc = f_read(&Fil, Buff, readSize, &br);	/* Read a chunk of file */
		if (rc || !br) break;				/* Error or end of file */
		qspi_flash_write(QSPI1, Buff, 0xE0000+shift, readSize);
		shift += readSize;
		if(shift>=Fil.fsize)break;
	}
	if (rc) die(rc);
#endif

	printf("FatDemo completed.\r\n");
	return 0;
}


