/**
  ******************************************************************************
  * @file    m1130_eval_i2c.h
  * @author  Alpscale Application Team
  * @version V1.0.0
  * @date    16-October-2013
  * @brief   This file contains all the functions prototypes for the 
  *              m1130_eval_i2c.c driver.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alpscale</center></h2>
  ******************************************************************************  
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __M1130_EVAL_I2C_EE_H
#define __M1130_EVAL_I2C_EE_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "m1130_eval.h"
#include "m1130_i2c.h"
#include "m1130_gpio.h" 
#include "m1130_rcc.h"
#include "m1130_dma.h"
/** @addtogroup Utilities
  * @{
  */ 

/** @addtogroup M1130_EVAL
  * @{
  */
  
/** @addtogroup M1130_EVAL_I2C
  * @{
  */  

/** @defgroup M1130_EVAL_I2C_Exported_Types
  * @{
  */ 
typedef enum { I2C_GP7,I2C_GP0_0,I2C_GP0_3}I2C_GPIO;
/**
  * @}
  */  

/** @defgroup M1130_EVAL_I2C_Exported_Constants
  * @{
  */
#define TX_BUFFER_DEPTH  8	  //Depth of the transmit buffer
#define RX_BUFFER_DEPTH  8	  //Depth of receive buffer
// #define I2C_DMA_ENABLE   
// #define I2C_IRQ_ENABLE	 
/**
  * @}
  */  
  
/** @defgroup M1130_EVAL_I2C_Exported_Macros
  * @{
  */    
/**
  * @}
  */ 

/** @defgroup M1130_EVAL_I2C_Exported_Functions
  * @{
  */
int i2cOpen(char *dev_name, I2C_GPIO i2c_io);
void i2cClose(void);
int i2cWrite(char *wbuf, uint32_t i2cSlaveInterelAddr, int BYTENUMBER);
int i2cRead(char *rbuf, uint32_t i2cSlaveInterelAddr, int BYTENUMBER);
void I2C_RccInit(I2C_TypeDef* I2Cx);


#ifdef __cplusplus
}
#endif

#endif /* __M1130_EVAL_I2C_H */
/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2013 Alpscale *****END OF FILE****/


