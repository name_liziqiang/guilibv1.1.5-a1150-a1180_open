#ifndef __M1130_UTILITY_RTC_H
#define __M1130_UTILITY_RTC_H

#include <stdint.h>

typedef struct
{
  uint16_t year;
  uint8_t  month;
  uint8_t  day;
  int8_t  hour;
  uint8_t  min;
  uint8_t  sec;
  uint8_t  wday;
} Date;

void getDateByTimeStamp(Date *date, uint32_t seconds);
uint32_t getTimeStampByDate(Date *date);
void GetDate(Date *date);
void SetDate(Date *date);
void GetAlarm(Date *date);
void SetAlarm(Date *date);
void RTC_Configuration(void);

extern uint8_t alarmChanged;
extern uint8_t timeChanged;
extern Date gCurTime;
extern Date mydate;

#endif //__M1130_UTILITY_RTC_H
