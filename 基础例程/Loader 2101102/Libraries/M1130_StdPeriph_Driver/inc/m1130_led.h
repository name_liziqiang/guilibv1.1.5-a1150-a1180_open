#ifndef __M1130_LED_H
#define __M1130_LED_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "m1130.h"

//�Ĵ�����ַ
#define        LED_CTL_R_REG          (LED_BASE + 0x100)
#define        LED_CTL_PROG_MEM       (LED_BASE + 0x400)


#define PIN_INVALID	0xFF
#define PIN_COL 	0x00	//0~31
#define PIN_ROW 	0x20	//0~7
#define PIN_HS 		0x28
#define PIN_OE 		0x30
#define PIN_CLK 	0x38


void LedReset(void);
void LedSetPin(uint8_t pinIndex, uint8_t value);
void LedSetDelay(uint8_t base, uint16_t before_hs, uint8_t hs_valid, uint16_t after_hs, uint16_t before_row, uint16_t after_row);
void LedSetClock(uint8_t low, uint8_t high);
void LedSetRow(uint8_t with138, uint8_t rowscanCount);
void LedSetCol(uint8_t msb, uint8_t colCount, uint16_t dwCount);
void LedSetPwm(uint32_t clkInPeriod, uint16_t periodInBase);
void LedSetGrayPwm(uint8_t gray, uint32_t activeInPeriod);
void LedSetAddrBase(uint16_t baseAddr);
void LedSetAddrPic0(uint32_t foregroundAddr, uint32_t backgroundAddr, uint32_t maskAddr);
void LedSetAddrPic1(uint32_t foregroundAddr, uint32_t backgroundAddr, uint32_t maskAddr);
void pic0IsReady(uint16_t displayTimes);
void pic1IsReady(uint16_t displayTimes);
void LedSetPol(uint8_t clk_pol, uint8_t oe_pol, uint8_t hs_pol, uint8_t data_invert, uint8_t row_pol);
void LedEnableInt(uint8_t frameInt, uint8_t picInt);
int LedRun(void);
void LedStop(void);
uint8_t LedGetCurrentPic(void);
void LedEnableBitcopy(uint8_t enable);
void LedSetScanGrey(uint8_t scan, uint32_t grey);
void LedSetScanNumber(uint8_t totalScanNum, uint8_t scanNumInRow);
void LedSetLatchClkNum(uint8_t clkNum);
void LedSetClkIdle(uint8_t clk_idle);

uint8_t LedGetLoopCount(void);
uint8_t LedGetGreyArray(uint8_t loopIndex, uint8_t* greyArray);

#ifdef __cplusplus
}
#endif

#endif /* __M1130_LED_H */

