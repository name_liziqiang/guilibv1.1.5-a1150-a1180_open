#include "misc.h"
#include "usb.h"
#include "ASM1130_usb.h"
#include "musb_core.h"
#include "usbhost.h"
#include "part.h"
#include "M1130_usb.h"
#include "diskio.h"
#include "stdio.h"

#define USB_ASM1130_DEBUG	

#ifdef USB_ASM1130_DEBUG
	#define USBprint(s)					printf(s);							
	#define USBPrintHex(p, n, s)		{printf(p);printf("0x%x",n);printf(s);}
	#define USBPrintNum(p, n, s)	    {printf(p);printf("%d",n);printf(s);}
#else
	#define USBprint(s)								
	#define USBPrintHex(p, n, s)		
	#define USBPrintNum(p, n, s)
#endif

void delay(unsigned long time);
int _RegUSBVol(void);
int FatDemo (void);


/* MUSB platform configuration */
struct musb_config musb_cfg = {
	(struct musb_regs *)USB_BASE,
	 0x3FFFFFF,
	 0,
	 0
};


int musb_platform_init()
{
	return 0;
}


void musb_platform_deinit(void)
{

}


uint8_t USB_init(void)
{
	volatile uint8_t intrusb;
	
	//open usb0 clk
	RCC_ResetAHBCLK(1<<AHBCLK_BIT_USB);

    musb_cfg.regs->power = 0x0;
	delay(0x1000);
	
	musb_cfg.regs->intrtx1 = 0x0;
	musb_cfg.regs->intrtx2 = 0x0;
	musb_cfg.regs->intrrx1 = 0x0;
	musb_cfg.regs->intrrx2 = 0x0;
	intrusb = musb_cfg.regs->intrusb;	//intrusb是只读寄存器，读取之后自动清0
	if(intrusb){
		USBPrintHex("\r\nClean intrusb=",intrusb,"\r\n");	
	}
	musb_cfg.regs->intrusbe = 0x30;
	musb_cfg.regs->txcsr1 = 0x0;	//CSR0的bit10已经不存在了
	musb_cfg.regs->txcsr2 = 0x0;	//CSR0的bit10已经不存在了
	musb_cfg.regs->devctl = MUSB_DEVCTL_SESSION;

	return 0;
}

void DisableUsbInt(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Interrupt Disable  */
	NVIC_InitStructure.NVIC_IRQChannel = USB0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
	NVIC_Init(&NVIC_InitStructure);
}

void EnableUsbInt(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Interrupt Enable  */
	NVIC_InitStructure.NVIC_IRQChannel = USB0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

uint32_t g_forceReset = 0;
uint32_t g_usbconnect = 0;
uint32_t g_usbrunning = 0;
int USB0_IRQHandler(void)
{
	volatile uint8_t intrusb;

	intrusb = musb_cfg.regs->intrusb;
	DisableUsbInt();
	// USBPrintHex("\r\nUSB IRQ intrusb=",intrusb,"\r\n");	

	if(intrusb & (0x01<<4)) /* connect interrupt */
	{
		USBprint("Connect IRQ\r\n");	
		g_usbconnect = 1;

	}
	else if(intrusb & (0x01<<5)) /* disconnect interrupt */
	{
	
		USBprint("Disconnect IRQ\r\n");
		

		g_usbconnect = 0;

	}
	else if(intrusb & (0x01<<2)) /* reset interrupt (enter device error)*/
	{
		
		USBprint("Reset IRQ\r\n");
	}
		
	EnableUsbInt();

	return 1;
}

#define ID_SOFT_HOST		0x00001000		//bit12=1 bit13=0
#define VBUS_SOFT			0x00000170	//bit456+bit8

int ASM1130_UsbHostIrqInit()
{
	//open USB power up
	RCC->PDRUNCFG = (RCC->PDRUNCFG & (!RCC_PDRUNCFG_USB_PD));
	//1130
	RCC->USBCTRL = ID_SOFT_HOST+VBUS_SOFT;
	RCC->USBCLKDIV = 8;
 	delay(1000);
	
	USB_init();
	EnableUsbInt();
	return 0;
}

int ASM1130_UsbHostInit()
{
 	/***********************************
	*U盘初始化
	*/

	if(usb_scan_init()<0)
		return -1;
	//do_usb_inf();//显示U盘信息(可选)
	//do_usb_part();//显示分区信息(可选)
	if(_RegUSBVol()<0)//注册U盘卷(必选)
		return -1;

	FatDemo();//FAT文件系统演示程序
	//FatFileCopy();
	delayms(1000);
	return 0;
}


int ASM1130_UsbHostprocess()
{
	int ret = 0;
	if (g_forceReset)
	{
		DisableUsbInt();
		g_forceReset = 0;
		g_usbconnect = 0;
		g_usbrunning = 0;
		USB_init();
		EnableUsbInt();
		return 0;
	}

	if (g_usbconnect == 0 && g_usbrunning == 0)
	{
		;
	}
	else if (g_usbconnect == 0 && g_usbrunning == 1)
	{
		//ASM4130_UsbHostDeInit();
		g_usbrunning = 0;
	}
	else if (g_usbconnect == 1 && g_usbrunning == 0)
	{
		delay(10000);
		if (ASM1130_UsbHostInit() == 0 && g_forceReset == 0)
		{
			g_usbrunning = 1;
			ret = 1;
		}
	}
	else
	{
		;
	}
	return ret;
}

static int usb_stor_dev = -1; /* current device */
block_dev_desc_t *stor_dev;

int _UnRegUSBVol(void)
		{
		return 0;
}

int _RegUSBVol(void)
{
	
	usb_stor_dev = usb_stor_scan(1);
	if(usb_stor_dev<0)
		return -1;
	
	stor_dev = usb_stor_get_dev(usb_stor_dev);
	USBprint("\r\nUSB Vol U registered OK\r\n");
	return 0;
}
	



static int USBVolRead(uint32_t nSec,
						void* pBuff,
						uint32_t nSecCnt,
						uint32_t* pnRead)
{

	uint32_t nRealReadSec = 0;
	
	if (!pnRead)
	{
		return -1;
	}
	*pnRead = 0;
	
	nRealReadSec = stor_dev->block_read(usb_stor_dev, nSec, nSecCnt,
						 (uint32_t *)pBuff);
	
	*pnRead = nRealReadSec;
	
	return 0;
}


static int USBVolWrite(uint32_t nSec,void* pBuff,uint32_t nSecCnt,uint32_t* pnWrite)
{

	uint32_t nRealWriteSec = 0;

	if (!pnWrite)
	{
		return -1;
	}
	*pnWrite = 0;

	nRealWriteSec = stor_dev->block_write(usb_stor_dev, nSec, nSecCnt,
						 (uint32_t *)pBuff);
	
	*pnWrite = nRealWriteSec;
	return 0;
}

int U_ReadBlock(
	uint32_t sector,	/* Sector address (LBA) */
	uint8_t *buff,		/* Data buffer to store read data */
	uint8_t count		/* Number of sectors to read (1..255) */
	)
	{
	uint32_t nRead=0;
	int status = 0;

	if(g_forceReset)
		return -1;

	status = USBVolRead(sector,buff,count,&nRead);
	return status;
	}


int U_WriteBlock(	
	uint32_t sector,	/* Sector address (LBA) */
	uint8_t *buff,		/* Data to be written */
	uint8_t count		/* Number of sectors to read (1..255) */
	)
{
	uint32_t nWrite=0;
	int status = 0;
	
	if(g_forceReset)
		return -1;
	
	status = USBVolWrite(sector,buff,count,&nWrite);
	return status;
}
	


