#include "m1130_rcc.h"
#include "m1130_tim.h"

volatile uint32_t systemTick=0;

void SystemHeart_Init(void)
{
	//初始化定时器16，用着系统心跳
	RCC->AHBCLKCTRL0_SET|=(1<<19);//开启定时器时钟
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Prescaler=95;//96Mhz/(95+1)=1Mhz
	TIM_TimeBaseStructure.TIM_Period=1000;//计数到1000刚好是1ms 中断
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	//SYSAHBCLKDIV=4 在SystemInit已经初始化了 AHB时钟96Mhz
	TIM_TimeBaseInit(TIM16, &TIM_TimeBaseStructure);
	TIM_SelectOutputTrigger(TIM16, TIM_TRGOSource_Enable);//选择TIM16的update事件更新为触发源
	TIM_ClearITPendingBit(TIM16, TIM_IT_Update);     			//清除update事件中断标志
	TIM_ITConfig(TIM16, TIM_IT_Update, ENABLE);       			//使能TIM16中断 
	//中断配置
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel=TIM16_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPriority=0;//2bit位宽
	NVIC_Init(&NVIC_InitStruct);
	TIM_Cmd(TIM16,ENABLE);
}
	
uint32_t Get_SystemTick(void)
{
	return systemTick;
}
void delay(unsigned long time)
{
	time=time+time*2;

	while ( time )time--; 
}

void SystemInit (void)
{    
	uint32_t	timeout = 0;
	RCC->AHBCLKCTRL0_SET = (1<<AHBCLK_BIT_GPIO) | (1<<AHBCLK_BIT_DMA) | (1<<AHBCLK_BIT_IOCON);
	RCC->SYSAHBCLKDIV = 4;							//FPGA上  96M/4=48M , 1130上  384M/4=96M

	/* open sys_pll clk */
	RCC->PDRUNCFG = RCC->PDRUNCFG & 0xFFFFFEEF;		//bit4=SYSPLL bit0=EXT12M

	/*set sys_pll clk to 480MHz*/
	RCC->SYSPLLCTRL = RCC_SYSPLLCTRL_FORCELOCK | (1 << 30) | (22 << 0);	//12M*(22+10)=384M

	while((RCC->SYSPLLSTAT) != RCC_SYSPLLSTAT_LOCK)
	{
		if((timeout++) >= 0x8000)
			break;
	}

 	delay(1000);
	
	/*switch main clk source to syspll_out*/
	RCC->MAINCLKSEL = 1;
	/*make switch available*/
	RCC->MAINCLKUEN = 0;
	RCC->MAINCLKUEN = 1;	
}
/******************* (C) COPYRIGHT 2013 Alphascale *****END OF FILE****/
