#include "lcdInit.h"
#include "lcdDriver.h"
#include "m1130_gpio.h"
#include "delay.h"
#define LCD_WR_REG Lcd_Driver.writeCMD
#define LCD_WR_DATA Lcd_Driver.writeData
/*
*********************************************************************************************************************
*函数功能：LCDinitDelay LCD初始化模块延时函数
*入口参数：dly  延时时间
*出口参数：无
*********************************************************************************************************************
*/
void LCDDelay(unsigned int dly)
{
    unsigned int i,j;

    for(i=0;i<dly;i++)
    	for(j=0;j<255;j++);
}

void ILI9488_CTC35_Initial_Code(void) 
{
// VCI=2.8V
//************* Reset LCD Driver ****************// 
//LCD_nRESET = 1; 
//Delayms(1); // Delay 1ms 
//LCD_nRESET = 0; 
//Delayms(10); // Delay 10ms // This delay time is necessary 
//LCD_nRESET = 1; 
//Delayms(120); // Delay 120 ms 
 
//************* Start Initial Sequence **********// 
  LCD_NRST();
	delay_ms(10);
	LCD_RST();
	delay_ms(20);
	LCD_NRST();
	delay_ms(20);
	LCD_RD_G();

	//*************LCD Driver Initial **********//
	LCD_WR_REG(0x11); // Sleep Out
	delay_ms(120); // Delay 120ms
	LCD_WR_REG(0xf0) ; 
	LCD_WR_DATA(0xc3) ;
	
	LCD_WR_REG(0xf0) ;
	LCD_WR_DATA(0x96) ;
	
	LCD_WR_REG(0x36);
	LCD_WR_DATA(0x48);
	
	LCD_WR_REG(0xB4);
	LCD_WR_DATA(0x01);
	
	LCD_WR_REG(0xB7) ;
	LCD_WR_DATA(0xC6) ;
	
	LCD_WR_REG(0xe8);
	LCD_WR_DATA(0x40);
	LCD_WR_DATA(0x8a);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x29);
	LCD_WR_DATA(0x19);
	LCD_WR_DATA(0xa5);
	LCD_WR_DATA(0x33);
	
	LCD_WR_REG(0xc1);
	LCD_WR_DATA(0x06);
	LCD_WR_REG(0xc2);
	LCD_WR_DATA(0xa7);
	LCD_WR_REG(0xc5);
	LCD_WR_DATA(0x18);
	
	LCD_WR_REG(0xe0); //Positive Voltage Gamma Control
	LCD_WR_DATA(0xf0);
	LCD_WR_DATA(0x09);
	LCD_WR_DATA(0x0b);
	LCD_WR_DATA(0x06);
	LCD_WR_DATA(0x04);
	LCD_WR_DATA(0x15);
	LCD_WR_DATA(0x2f);
	LCD_WR_DATA(0x54);
	LCD_WR_DATA(0x42);
	LCD_WR_DATA(0x3c);
	LCD_WR_DATA(0x17);
	LCD_WR_DATA(0x14);
	LCD_WR_DATA(0x18);
	LCD_WR_DATA(0x1b); 
	
	LCD_WR_REG(0xe1); //Negative Voltage Gamma Control
	LCD_WR_DATA(0xf0);
	LCD_WR_DATA(0x09);
	LCD_WR_DATA(0x0b);
	LCD_WR_DATA(0x06);
	LCD_WR_DATA(0x04);
	LCD_WR_DATA(0x03);
	LCD_WR_DATA(0x2d);
	LCD_WR_DATA(0x43);
	LCD_WR_DATA(0x42);
	LCD_WR_DATA(0x3b);
	LCD_WR_DATA(0x16);
	LCD_WR_DATA(0x14);
	LCD_WR_DATA(0x17);
	LCD_WR_DATA(0x1b);
	
	LCD_WR_REG(0xf0);
	LCD_WR_DATA(0x3c);
	
	LCD_WR_REG(0xf0);
	LCD_WR_DATA(0x69);
	
  LCD_WR_REG(0x3A);
	LCD_WR_DATA(0x55);
	
	delay_ms(120); //Delay 120ms
	LCD_WR_REG(0x29); //Display ON
		
//	LCD_Display_Dir(1);		 	//默认为竖屏
//	LCD_BG_OPEN();					//点亮背光
//	LCD_Clear(WHITE);

} 
/*
*********************************************************************************************************************
*函数功能：LCD_Init，LCD初始化，初始化LCD的硬件
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
void LCD_Init(void)
{
	//初始化GPIO	
//初始化GPIO
	GPIO0_IT->DIR_SET=0xffff0000;//DB0~15输出 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_2,GPIO_FUNCTION_0);//LCD_BG 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_29,GPIO_FUNCTION_0);//LCD_RD 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_7,GPIO_FUNCTION_0);//LCD_WR 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_4,GPIO_FUNCTION_0);//LCD_RS 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_30,GPIO_FUNCTION_0);//LCD_CS 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_3,GPIO_FUNCTION_0);//LCD_RST 
	
	GPIO_SetPinDir(GPIO0, GPIO_Pin_7,GPIO_Mode_OUT);//LCD_WR 
	GPIO_SetPinDir(GPIO0, GPIO_Pin_2,GPIO_Mode_OUT);//LCD_BG 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_29,GPIO_Mode_OUT);//LCD_RD 	
	GPIO_SetPinDir(GPIO0, GPIO_Pin_4,GPIO_Mode_OUT);//LCD_RS 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_30,GPIO_Mode_OUT);//LCD_CS
	GPIO_SetPinDir(GPIO0, GPIO_Pin_3,GPIO_Mode_OUT);//LCD_RST  	
	
//	GPIO_SetPinMux(GPIO0, GPIO_Pin_5,GPIO_FUNCTION_0);//QSPI1_TF_CS 
//	GPIO_SetPinDir(GPIO0, GPIO_Pin_5,GPIO_Mode_OUT);  //QSPI1_TF_CS 
//	GPIO0->DT_SET = GPIO_Pin_5;
//		LCDDelay(30000);
	

	//关闭显示
	//Lcd_Driver.writeCMD(0x28); 
  ILI9488_CTC35_Initial_Code();

//	//此命令关闭睡眠模式
//	Lcd_Driver.writeCMD(0x11); 
//	//开启反显，正常0000显示黑色，ffff显示白色，如果不发这个命令，会显示反的
//	Lcd_Driver.writeCMD(0x20); 
//	//开启显示
//	Lcd_Driver.writeCMD(0x29); 
//	LCDDelay(500);
	Lcd_Driver.para->dir=Lcd_Driver.scan_dir(DIR_HORIZONTAL);//测试程序切换到横屏
////	LCDDelay(300);
	LCD_BG_OPEN();
}
