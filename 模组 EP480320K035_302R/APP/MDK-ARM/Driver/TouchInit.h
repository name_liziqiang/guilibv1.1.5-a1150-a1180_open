#ifndef _TouchInit_h
#define _TouchInit_h

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：通讯初始化模块，单独列出的原因在于，驱动里面有部分代码是需要高速执行的。
 *实现功能：1、
 *
 *撰 写 人：Alpscale LCD Application development team
 *撰写时间：2019-7-3
 *测 试 人：Alpscale LCD Application development team
 *测试时间：2019-7-3
 *版 本 号：V1.01_2019-7-3
 *版本说明：
 *                              
 *          接口支持          读写函数          命令支持 
 *
 *V1.01：   功能实现          功能实现          ok 2019/07/04
 *
 *V1.02：
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//IO操作函数
#define GT_RST_HIGH()    		GPIO1->DT_SET=GPIO_Pin_27	  //GT911复位引脚
#define GT_RST_LOW()     		GPIO1->DT_CLR=GPIO_Pin_27
#define GT_INT_HIGH()    		GPIO1->DT_SET=GPIO_Pin_26	  //GT911中断引脚
#define GT_INT_LOW()    		GPIO1->DT_CLR=GPIO_Pin_26

//I2C读写命令	有两组地址供选择：0xBA/0xBB和0x28/0x29
#define GT_CMD_WR 		0XBA     	//写命令
#define GT_CMD_RD 		0XBB		//读命令
  
/********************************GT911部分寄存器定义***************************/
#define GT_CTRL_REG 	0X8040   	//GT911控制寄存器
#define GT_CFGS_REG 	0X8047   	//配置版本
#define GT_X_MAX_LOW 	0X8048   	//X轴低字节
#define GT_X_MAX_HOW 	0X8049   	//X轴高字节
#define GT_Y_MAX_LOW 	0X804A   	//Y轴低字节
#define GT_Y_MAX_HOW 	0X804B   	//Y轴高字节
#define GT_TOUCH_NUM  0X804C      //输出触摸点数1--10


#define GT_CHECK_REG 	0X80FF   	//GT911校验和寄存器
#define GT_PID_REG 		0X8140   	//GT911产品ID寄存器

#define GT_GSTID_REG  0X814E       //GT911当前检测到的触摸状态

typedef struct _POINT{
     unsigned short int x;
     unsigned short int y;
}POINT, *PPOINT;
typedef struct _GT911_POINT_DATA{
     unsigned char cnt;
     POINT points[5];
}GT911_POINT_DATA, *PGT911_POINT_DATA;

extern unsigned char Point_Read;

void TouchInit(void);
void Touch_Point_Read(void);
void TouchScan_Init(void);//初始化一个定时器17作为触摸扫描周期

#endif

