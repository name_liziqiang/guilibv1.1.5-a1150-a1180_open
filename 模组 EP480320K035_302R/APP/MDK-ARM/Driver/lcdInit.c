#include "lcdInit.h"
#include "lcdDriver.h"
#include "m1130_gpio.h"
#include "m1130_tim.h"
#include "stdio.h"
#include "delay.h"

#define LCD_WR_REG Lcd_Driver.writeCMD
#define LCD_WR_DATA Lcd_Driver.writeData

void LCD_DIM_init(uint16_t arr,uint16_t psc,uint16_t pulse)
{
	//TIM15  CH1 输出PWM
	RCC->AHBCLKCTRL0_SET|=(1<<24);//开启定时器时钟
	RCC->AHBCLKCTRL0_SET|=(1<<3);
	RCC->PWMCLKDIV = 0X01;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	GPIO_InitTypeDef GPIO_InitStructure; 
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Function = GPIO_FUNCTION_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_Init(GPIO0, &GPIO_InitStructure);
	
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Prescaler=psc;//96Mhz/(95+1)=1Mhz
	TIM_TimeBaseStructure.TIM_Period=arr ;//fpwm=9600/(1199+1)=80Khz
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);
	
	//SYSAHBCLKDIV=4 在SystemInit已经初始化了 AHB时钟96Mhz	
	TIM_OCStructInit(&TIM_OCInitStructure);
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; //选择定时器模式:TIM脉冲宽度调制模式2
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; //比较输出使能
	TIM_OCInitStructure.TIM_Pulse = pulse; //设置待装入捕获比较寄存器的脉冲值
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; //输出极性:TIM输出比较极性高
	TIM_OC1Init(TIM4, &TIM_OCInitStructure);  //根据TIM_OCInitStruct中指定的参数初始化外设TIMx     //TIM4_CH1
	
	TIM_OC1PreloadConfig(TIM4,TIM_OCPreload_Enable);
	TIM_ARRPreloadConfig(TIM4,ENABLE);
	TIM_Cmd(TIM4,ENABLE);
}

void LcdSetDim(uint16_t pulse)
{
	TIM_SetCompare1(TIM4 , pulse);
}

/*
*********************************************************************************************************************
*函数功能：LCD_Init，LCD初始化，初始化LCD的硬件
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
void LCD_Init(void)
{
	//初始化GPIO
	GPIO0_IT->DIR_SET=0xffff0000;//DB0~15输出 
//	GPIO_SetPinMux(GPIO0, GPIO_Pin_2,GPIO_FUNCTION_0);//LCD_BG 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_29,GPIO_FUNCTION_0);//LCD_RD 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_7,GPIO_FUNCTION_0);//LCD_WR 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_4,GPIO_FUNCTION_0);//LCD_RS 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_30,GPIO_FUNCTION_0);//LCD_CS 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_3,GPIO_FUNCTION_0);//LCD_RST 
	
	GPIO_SetPinDir(GPIO0, GPIO_Pin_7,GPIO_Mode_OUT);//LCD_WR 
//	GPIO_SetPinDir(GPIO0, GPIO_Pin_2,GPIO_Mode_OUT);//LCD_BG 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_29,GPIO_Mode_OUT);//LCD_RD 	
	GPIO_SetPinDir(GPIO0, GPIO_Pin_4,GPIO_Mode_OUT);//LCD_RS 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_30,GPIO_Mode_OUT);//LCD_CS
	GPIO_SetPinDir(GPIO0, GPIO_Pin_3,GPIO_Mode_OUT);//LCD_RST  	
	
	GPIO_SetPinMux(GPIO0, GPIO_Pin_5,GPIO_FUNCTION_0);//QSPI1_TF_CS 
	GPIO_SetPinDir(GPIO0, GPIO_Pin_5,GPIO_Mode_OUT);  //QSPI1_TF_CS 
	GPIO_SetPin(GPIO0, GPIO_Pin_5);
	LCD_BG_CLOSE();
//	Lcd_Driver.writeCMD(0x01);//software reset
	LCD_NRST();
	delay_ms(100);
	LCD_RST();
	delay_ms(200);
	LCD_NRST();
	delay_ms(200);
	LCD_RD_G();

	//*************LCD Driver Initial **********//
	LCD_WR_REG(0x11); // Sleep Out
	delay_ms(120); // Delay 120ms
	LCD_WR_REG(0xf0) ; 
	LCD_WR_DATA(0xc3) ;
	LCD_WR_REG(0xf0) ;
	LCD_WR_DATA(0x96) ;
	LCD_WR_REG(0x36);
	LCD_WR_DATA(0x48);
	LCD_WR_REG(0xB4);
	LCD_WR_DATA(0x01);
	LCD_WR_REG(0xB7) ;
	LCD_WR_DATA(0xC6) ;
	LCD_WR_REG(0xe8);
	LCD_WR_DATA(0x40);
	LCD_WR_DATA(0x8a);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x00);
	LCD_WR_DATA(0x29);
	LCD_WR_DATA(0x19);
	LCD_WR_DATA(0xa5);
	LCD_WR_DATA(0x33);
	LCD_WR_REG(0xc1);
	LCD_WR_DATA(0x06);
	LCD_WR_REG(0xc2);
	LCD_WR_DATA(0xa7);
	LCD_WR_REG(0xc5);
	LCD_WR_DATA(0x18);
	LCD_WR_REG(0xe0); //Positive Voltage Gamma Control
	LCD_WR_DATA(0xf0);
	LCD_WR_DATA(0x09);
	LCD_WR_DATA(0x0b);
	LCD_WR_DATA(0x06);
	LCD_WR_DATA(0x04);
	LCD_WR_DATA(0x15);
	LCD_WR_DATA(0x2f);
	LCD_WR_DATA(0x54);
	LCD_WR_DATA(0x42);
	LCD_WR_DATA(0x3c);
	LCD_WR_DATA(0x17);
	LCD_WR_DATA(0x14);
	LCD_WR_DATA(0x18);
	LCD_WR_DATA(0x1b); 
	LCD_WR_REG(0xe1); //Negative Voltage Gamma Control
	LCD_WR_DATA(0xf0);
	LCD_WR_DATA(0x09);
	LCD_WR_DATA(0x0b);
	LCD_WR_DATA(0x06);
	LCD_WR_DATA(0x04);
	LCD_WR_DATA(0x03);
	LCD_WR_DATA(0x2d);
	LCD_WR_DATA(0x43);
	LCD_WR_DATA(0x42);
	LCD_WR_DATA(0x3b);
	LCD_WR_DATA(0x16);
	LCD_WR_DATA(0x14);
	LCD_WR_DATA(0x17);
	LCD_WR_DATA(0x1b);
	LCD_WR_REG(0xf0);
	LCD_WR_DATA(0x3c);
	LCD_WR_REG(0xf0);
	LCD_WR_DATA(0x69);
  LCD_WR_REG(0x3A);
	LCD_WR_DATA(0x55);
	delay_ms(120); //Delay 120ms
	LCD_WR_REG(0x29); //Display ON


	Lcd_Driver.scan_dir(DIR_HORIZONTAL);//测试程序切换到横屏
//	LCD_BG_OPEN();	
	LCD_DIM_init(100,95,100);  //10Khz	 
	printf("LCD_DIM_init OK\r\n");

}
