#include "lcdInterface.h"
#include "m1130_gpio.h"
#include "lcdDriver.h"
#include <stdio.h>
#include <string.h>


#define LCDINTERFACE_DELAY_NOP()     //__nop();__nop();__nop();__nop()
/*
*********************************************************************************************************************
*函数功能：LCD_Fill_UseOneData 将一个数据写N次到LCD显存
*          常用于清屏或者区域填充。开窗不由本函数完成，由调用函数完成，本函数只负责写数据
*入口参数：sPos       起点坐标
*          ePos       终点坐标
*          Data       写入的数据
*          WriteCount 写入的次数
*出口参数：无
*********************************************************************************************************************
*/
//static void LCD_Fill_UseOneData(LCD_POSITION sPos,LCD_POSITION ePos,const uint16_t Data,uint32_t WriteCount)
//{
//	Lcd_Driver.setcursor(sPos.x,sPos.y,ePos.x,ePos.y);
//	Lcd_Driver.writeCMD(0x2c);//发送写GRAM指令
//	LCD_RS_G();
//	LCD_CS_D();	

//	LCD_LOCK_OTHER_PORT();//锁住无关端口
//	//写入数据
//	uint32_t i;
//	for(i=0;i<WriteCount;i++){
//		GPIO0->DT=(uint32_t)Data<<16;
//		LCD_WR_G();
//	}
//	LCD_UNLOCK_OTHER_PORT();//释放无关端口
//	LCD_CS_G();
//}
extern void LCD_WriteOneData(uint32_t WriteCount);
static void LCD_Fill_UseOneData(LCD_POSITION sPos,LCD_POSITION ePos,const uint16_t Data,uint32_t WriteCount)
{
	Lcd_Driver.setcursor(sPos.x,sPos.y,ePos.x,ePos.y);
	Lcd_Driver.writeCMD(0x2c);//发送写GRAM指令
	LCD_RS_G();
	LCD_CS_D();	

	LCD_LOCK_OTHER_PORT();//锁住无关端口
	//写入数据
	GPIO0->DT=(uint32_t)Data<<16;
	LCD_WriteOneData(WriteCount);
	LCD_UNLOCK_OTHER_PORT();//释放无关端口
	LCD_CS_G();
}
/*
*********************************************************************************************************************
*函数功能：LCD_Fill_UseContinuousData 使用一块连续的数据的进行填充
*          常用于显示一张完整的图像
*入口参数：sPos       写入的LCD起点坐标
*          Data       写入的数据
*          dataWidth  数据宽度
*          dataHeight 数据高度
*出口参数：无
*********************************************************************************************************************
*/
//static void LCD_Fill_UseContinuousData(LCD_POSITION sPos,const uint16_t* Data,uint16_t dataWidth,uint16_t dataHeight)
//{
//	uint32_t WriteCount=dataWidth*dataHeight;
//	Lcd_Driver.setcursor(sPos.x,sPos.y,sPos.x+dataWidth-1,sPos.y+dataHeight-1);
//	Lcd_Driver.writeCMD(0x2c);//发送写GRAM指令
//	LCD_RS_G();
//	LCD_CS_D();	

//	LCD_LOCK_OTHER_PORT();//锁住无关端口
//	//写入数据
//	uint32_t i;
//	for(i=WriteCount;i>0;i--){
//		GPIO0->DT=(uint32_t)(*Data++)<<16;
//		LCD_WR_D();//
//		LCD_WR_G();
//	}
//	LCD_UNLOCK_OTHER_PORT();//释放无关端口
//	LCD_CS_G();
//}
extern void LCD_WriteDatas(const uint16_t* Data, uint32_t WriteCount);
//图片数据地址，图片像素数量均是4的整数倍的时候  效率最高
static void LCD_Fill_UseContinuousData(LCD_POSITION sPos,const uint16_t* Data,uint16_t dataWidth,uint16_t dataHeight)
{
	uint32_t count=dataWidth*dataHeight;
	const uint16_t* gData = Data;
	Lcd_Driver.setcursor(sPos.x,sPos.y,sPos.x+dataWidth-1,sPos.y+dataHeight-1);
	Lcd_Driver.writeCMD(0x2c);//发送写GRAM指令
	LCD_RS_G();
	LCD_CS_D();	

	LCD_LOCK_OTHER_PORT();//锁住无关端口
	//写入数据
	uint32_t addr = (uint32_t)Data;
//	printf("count1  =  %08d\r\n", count);
//	printf("addr  =  0x%08x\r\n", addr);
	if(addr % 2){
		//地址不是2的整数倍
		uint32_t i;
		for(i=count;i>0;i--){
			GPIO0->DT=(uint32_t)(*gData++)<<16;
			LCD_WR_G();
		}		
	}
	else{
		//地址是2的整数倍
		if(addr % 4){
			//地址不是4的整数倍
			GPIO0->DT=(uint32_t)(gData[0])<<16;
			LCD_WR_G();//先吧第一个像素数据写进去，保证地址是4的整数倍
			count--;
			gData++;
		}
		uint32_t WriteCount = count/4;
		WriteCount = WriteCount<<2;//保证 写入的次数是4的整数倍
		uint32_t otherCount = count%4;	
		LCD_WriteDatas(gData, WriteCount);
		if(otherCount != 0){
			gData+=WriteCount;
			//写入剩余的像素
			uint32_t i;
			for(i=0;i<otherCount;i++){
				GPIO0->DT=(uint32_t)(gData[i])<<16;
				LCD_WR_G();
			}		
		}
	}
	LCD_UNLOCK_OTHER_PORT();//释放无关端口
	LCD_CS_G();
}
/*
*********************************************************************************************************************
*函数功能：LCD_Fill_UsePartContinuousData  使用一块连续数据指定的一部分对LCD进行填充
*          常用于切图显示
*入口参数：DataBase     数据的基地址，注意它是连续数据块的起始地址，不是需要显示的区域的起始地址
*          WidthOffset  数据每一行的数据量（以uint16为基准进行的统计）
*          dispLoc      显示的地点位置，显示的宽度高度
*          dataPos      数据的起点位置
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_Fill_UsePartContinuousData(const uint16_t* DataBase,uint16_t WidthOffset,WIDGET_LOCATION dispLoc,LCD_POSITION dataPos)
{
	uint16_t (* Data)[WidthOffset];
	Data=(uint16_t(*)[WidthOffset])DataBase;//将一块连续的内存 转换成二位数组去访问
	Lcd_Driver.setcursor(dispLoc.x,dispLoc.y,dispLoc.x+dispLoc.width-1,dispLoc.y+dispLoc.height-1);
	Lcd_Driver.writeCMD(0x2c);//发送写GRAM指令
	LCD_RS_G();
	LCD_CS_D();	

	LCD_LOCK_OTHER_PORT();//锁住无关端口
	//写入数据
	uint32_t i,j;
	for(i=0;i<dispLoc.height;i++){
		for(j=0;j<dispLoc.width;j++){
			GPIO0->DT=(uint32_t)Data[dataPos.y+i][dataPos.x+j]<<16;
			LCD_WR_G();
		}
	}
	LCD_UNLOCK_OTHER_PORT();//释放无关端口
	LCD_CS_G();
}
/*
*********************************************************************************************************************
*函数功能：LCD_DrawPoint，LCD画一个点，使用LCD描述符前景色
*入口参数：x,y:坐标
*出口参数：无
*********************************************************************************************************************
*/ 
static void LCD_DrawPoint(uint16_t x,uint16_t y)
{
	LCD_POSITION sPos={x,y};
	LCD_POSITION ePos={x,y};
	LCD_Fill_UseOneData(sPos,ePos,lcdPara.fontcolor,1);	
}
/*
*********************************************************************************************************************
*函数功能：LCD_ReadPoint，LCD读一个点
*入口参数：x,y:坐标
*出口参数：uint32_t，该点的颜色RGB值
*********************************************************************************************************************
*/
__inline static void ReadBegin(void)
{
	LCD_CS_D();
	LCD_DATA_IN();
	LCD_RS_G();
	LCD_WR_G();
}
__inline static void ReadEnd(void)         
{                       
  LCD_CS_G();   
	LCD_DATA_OUT(); 
}
__inline static void LCD_ReadTrig(void)        
{    
  LCD_RD_D();       
  LCD_RD_G();     
}
static uint16_t LCD_ReadPoint(uint16_t x,uint16_t y)
{
	uint16_t r=0,g=0,b=0;
	LCD_POSITION sPos={x,y};
	LCD_POSITION ePos={x,y};	
	Lcd_Driver.setcursor(sPos.x,sPos.y,ePos.x,ePos.y);
	Lcd_Driver.writeCMD(0x2E);//发送写GRAM指令
	
   ReadBegin();       
	LCD_ReadTrig();     
	Lcd_Driver.readData();  //dummy Read    
				
	LCD_ReadTrig();         
	r = Lcd_Driver.readData()&0xf800;    

	LCD_ReadTrig();         
	b=Lcd_Driver.readData()&0xf800;     

	LCD_ReadTrig();         
	g=Lcd_Driver.readData()&0xfc00;   	
	
	ReadEnd();     

	return (((r>>11)<<11)|((g>>10)<<5)|(b>>11));
}
/*
*********************************************************************************************************************
*函数功能：LCD_Fill，使用LCD描述符的前景色填充LCD指定的一部分
*入口参数：sx sy起点坐标
*          ex ey终点坐标
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_Fill(uint16_t sx,uint16_t sy,uint16_t ex,uint16_t ey)
{          
	uint32_t xlen=ex-sx+1;
	uint32_t ylen=ey-sy+1; 
	LCD_POSITION sPos={sx,sy};
	LCD_POSITION ePos={ex,ey};
	LCD_Fill_UseOneData(sPos,ePos,lcdPara.fontcolor,xlen*ylen);
}

const LCD_DESCRIPTOR Lcd_Des={
	LCD_DrawPoint,
	LCD_ReadPoint,
	LCD_Fill,
	LCD_Fill_UseOneData,
	LCD_Fill_UseContinuousData,
	LCD_Fill_UsePartContinuousData,
};



