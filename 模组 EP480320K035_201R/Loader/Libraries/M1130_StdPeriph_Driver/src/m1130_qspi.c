/**
  ******************************************************************************
  * @file    m407_qspi.c
  * @author  Alpscale Application Team
  * @version V1.0.0
  * @date    27-November-2013
  * @brief   This file provides all the QSPI firmware functions.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, Alpscale SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alpscale</center></h2>
  */ 

/* Includes ------------------------------------------------------------------*/
#include "m1130_rcc.h"
#include "m1130_qspi.h"
#include "stdio.h"

void QSPI_SetPowerState(QSPI_TypeDef *QSPIptr, uint32_t QSPI_PowerState)
{
  if (QSPI_PowerState == QSPI_PowerState_ON)
  {
    if(QSPIptr == QSPI0){
      RCC_ResetAHBCLK(1 << AHBCLK_BIT_QSPI0);
    }else if(QSPIptr == QSPI1){
      RCC_ResetAHBCLK(1 << AHBCLK_BIT_QSPI1);
    }
  }
  else
  {
    if(QSPIptr == QSPI0){
      RCC_SetAHBCLK(1 << AHBCLK_BIT_QSPI0, DISABLE);
    }else if(QSPIptr == QSPI1){
      RCC_SetAHBCLK(1 << AHBCLK_BIT_QSPI1, DISABLE);
    }
  }
}


/**
  * @brief  Initializes the QSPI peripheral according to the specified 
  *   parameters in the QSPI_InitStruct.  
  * @param QSPIptr: the QUAD spi controller base address.
  * @param  QSPI_InitStruct : pointer to a QSPI_InitTypeDef structure 
  *   that contains the configuration information for the QSPI peripheral.
  * @retval None
  */
void QSPI_HwInit(QSPI_TypeDef *QSPIptr, QSPI_InitTypeDef *QSPI_InitStruct)
{	
	volatile int i;

  QSPI_SetPowerState(QSPIptr, QSPI_PowerState_ON);

#if 1
/**Vital notice, must delay for a while**/
 for(i=0; i< 0x1000; i++)
	;
#endif

  QSPIptr->CTRL1 = (QSPIptr->CTRL1&0xfffffe00)|0x38;
  QSPIptr->CTRL1_SET = QSPI_InitStruct->QSPI_CPOL|QSPI_InitStruct->QSPI_CPHA|QSPI_InitStruct->QSPI_FirstBit;
  QSPIptr->TIMING = 0xFFFF0000|QSPI_InitStruct->QSPI_ClockDiv|QSPI_InitStruct->QSPI_ClockRate;

  QSPIptr->CTRL1_SET = 0x0;
  QSPIptr->CTRL2 = 0;	
}

void QSPI_DMACmd(QSPI_TypeDef *QSPIptr, FunctionalState NewState)
{
	if(NewState == ENABLE)
  		QSPIptr->CTRL1_SET = QSPI_CTRL1_DMA_ENABLE;
	else
  		QSPIptr->CTRL1_CLR = QSPI_CTRL1_DMA_ENABLE;
}

void QSPI_ModeSet(QSPI_TypeDef *QSPIptr, QSPIModeSelect ModeSelect)
{
  if (ModeSelect == QSPI_STD)
  {
    QSPIptr->CTRL1 = (QSPIptr->CTRL1 & 0xfffffff8) | 0x00;
  }
  else if (ModeSelect == QSPI_DUAL)
  {
    QSPIptr->CTRL1 = (QSPIptr->CTRL1 & 0xfffffff8) | 0x01;
  }
  else
  {
    QSPIptr->CTRL1 = (QSPIptr->CTRL1 & 0xfffffff8) | 0x02;
  }
}

void QSPI_QpiSet(QSPI_TypeDef *QSPIptr, uint8_t enable)
{
  if (enable)
  {
    QSPIptr->CTRL1 = QSPIptr->CTRL1 | 0x00800000;
  }
  else
  {
    QSPIptr->CTRL1 = QSPIptr->CTRL1 & 0xFF7FFFFF;
  }
}

void QSPI_CS_Low(QSPI_TypeDef *QSPIptr)
{
	QSPIptr->CTRL0 |= QSPI_CTRL0_LOCK_CS;		
}

void QSPI_CS_High(QSPI_TypeDef* QSPIptr)
{
	QSPIptr->CTRL0 &= ~QSPI_CTRL0_LOCK_CS;		
}

void QSPI_DataConfig(QSPI_TypeDef *QSPIptr, QSPI_DataInitTypeDef *QSPI_DataInitStruct)
{
  uint32_t tmepReg;

  QSPIptr->XFER = QSPI_DataInitStruct->QSPI_DataLength;
  if (QSPIptr->CTRL0 & QSPI_CTRL0_LOCK_CS)
  {
    tmepReg = 0x20000000 | QSPI_DataInitStruct->QSPI_DUPLEX | QSPI_DataInitStruct->QSPI_TransferDir | QSPI_CTRL0_LOCK_CS;
  }
  else
  {
    tmepReg = 0x20000000 | QSPI_DataInitStruct->QSPI_DUPLEX | QSPI_DataInitStruct->QSPI_TransferDir;
  }
  QSPIptr->CTRL0 = tmepReg;
}

FlagStatus QSPI_GetFlagStatus(QSPI_TypeDef *QSPIptr, uint32_t QSPI_FLAG)
{
  FlagStatus bitstatus = RESET;

  if ((QSPIptr->STATUS & QSPI_FLAG) != (uint32_t)RESET)
  {
    bitstatus = SET;
  }
  else
  {
    bitstatus = RESET;
  }

  return bitstatus;
}

/******************* (C) COPYRIGHT 2013 Alpscale *****END OF FILE****/
