
#include "stdio.h"
#include "touchInterface.h"
#include "m1130_gpio.h"
#include "lcddriver.h"
#include "guiConfig.h"



#define VALID_AD_OFFSET   80  //有效触摸AD 的最大偏移量，两次ad的差值进行比较，超过这个值认为无效
#define VALID_AD_COUNT    4		 //连续5次采样
static uint16_t LastXad=0;
static uint16_t LastYad=0;
static uint16_t adValidCount=0;
extern uint16_t touchReadX(void);
extern uint16_t touchReadY(void);
/* 私有变量 ------------------------------------------------------------------*/
XPT2046_Calibration cal_value={0};
XPT2046_Calibration cal_value1={0};


extern uint8_t flag_caiji;
static uint16_t touch_read_rotation=0;
#define TSLIDE_MIN_OFFSET         8//判断滑动的最小偏移量

/**
  * 函数功能: us 级别延时，不是很精确
  * 输入参数: cnt：延时时间
  * 返 回 值: 无
  * 说    明：无
  */
static void XPT2046_DelayUS ( __IO uint32_t ulCount )
{
	uint32_t i;
	for ( i = 0; i < ulCount; i ++ )
	{
		uint8_t uc = 12;     //设置值为12，大约延1微秒  	      
		while ( uc -- );     //延1微秒	
	}	
}
/*
*********************************************************************************************************************
*函数功能：触摸控制器定时扫描接口
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/ 
void Touch_Scan(void)
{
}
/*
*********************************************************************************************************************
*函数功能：Touch_Adjust，触摸控制器校准函数
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/ 
void Touch_Adjust(void)
{

}
/**
  * 函数功能: 读取TP x y 的AD值(12bit，最大是4096)
  * 输入参数: x：读到AD值
  *           y：读到AD值
  * 返 回 值: 无
  * 说    明：无
  */
uint8_t XPT2046_ReadAdc_XY(int16_t *sX_Ad,int16_t *sY_Ad)  
{ 

	uint16_t count;
	static uint16_t xad=0;
	static uint16_t yad=0;
	//uint32_t tick=tick=Get_SystemTick();
	//不能一次读两个通道，会导致数据出问题，
	if((touch_read_rotation%2)==0){
		xad=touchReadX();
	}
	else {
		yad=touchReadY();
	}
	touch_read_rotation++;
	if(touch_read_rotation>=2){
		touch_read_rotation=0;
//		printf("xad:%05d   yad:%05d\r\n",xad,yad);
		if(adValidCount==0){
			adValidCount++;
			LastXad=xad;
			LastYad=yad;//第一次检测到
		}
		else{
			count=0;
			//检测X坐标是否在合理的误差范围内
			if(xad>=LastXad){
				if((xad-LastXad)>VALID_AD_OFFSET)count++;
			}
			else{
				if((LastXad-xad)>VALID_AD_OFFSET)count++;
			}
			//检测Y坐标是否在合理的误差范围内
			if(yad>=LastYad){
				if((yad-LastYad)>VALID_AD_OFFSET)count++;
			}
			else{
				if((LastYad-yad)>VALID_AD_OFFSET)count++;
			}
			if(count)adValidCount=0;//有一个不符合范围，重新来
			else {
				LastXad=xad;
				LastYad=yad;
				adValidCount++;
				if(adValidCount>=VALID_AD_COUNT)
				{
						* sY_Ad = xad; 
	          * sX_Ad = yad;	
	         return 1;
				}
			}
		}
		
		//printf("scan need time is %d\r\n",Get_SystemTick()-tick);
	}
      XPT2046_DelayUS(2000);    			

	return 0;
}

	
	/**
  * 函数功能: 得到简单滤波之后的X Y
  * 输入参数: pTouch_AD_x：保存测量X+对应的AD值
  *           pTouch_AD_y：保存测量Y+对应的AD值
  *           wait：松开等待使能
  *            参数 1：使能等待松开
  *                 0：无需等待松开
  * 返 回 值: 无
  * 说    明：画板应用实例专用,不是很精准，但是速度比较快
  */
static uint8_t XPT2046_ReadAdc_Smooth_XY(int32_t *pTouch_AD_x,int32_t *pTouch_AD_y,uint8_t wait)
{
	uint8_t ucCount = 0, i;

	int16_t sAD_X, sAD_Y;
	int16_t sBufferArray[2][10]={{0},{0}};  //坐标X和Y进行多次采样	
	int32_t lX_Min, lX_Max, lY_Min, lY_Max;
//					flag_caiji=0;
  while(XPT2046_EXTI_Read() != XPT2046_EXTI_ActiveLevel);
	
	XPT2046_DelayUS(50000);    
  while(ucCount<20)
  {
    if(XPT2046_EXTI_Read() == XPT2046_EXTI_ActiveLevel)
    {

			XPT2046_DelayUS(2000);    
			if(ucCount<10)
			{
      sAD_X=touchReadX();	     				
			sBufferArray[0][ucCount]=sAD_X;  

			}
			else
			{
	    sAD_Y=touchReadY();      
			sBufferArray[1][ucCount-10]=sAD_Y;

			}
      ucCount ++; 
//				}			
    }
    else if(wait==0)
      break;
  }
  while(wait)
  {
    if(XPT2046_EXTI_Read() != XPT2046_EXTI_ActiveLevel)break;
  }
  
	if(ucCount==20)
  {
    lX_Max=lX_Min=sBufferArray[0][0];
    lY_Max=lY_Min=sBufferArray[1][0];       
    for(i=1;i<10;i++)
    {
      if(sBufferArray[0][i]<lX_Min)
        lX_Min=sBufferArray[0][i];
      else if(sBufferArray[0][i]>lX_Max)
        lX_Max=sBufferArray[0][i];
    }
    for(i=1;i<10;i++)
    {
      if(sBufferArray[1][i]<lY_Min)
        lY_Min=sBufferArray[1][i];			
      else if(sBufferArray[1][i]>lY_Max)
        lY_Max=sBufferArray[1][i];
    }		
//    for(int j=0;j<10;j++)
//		{
//			printf("第 %d 个pTouch_AD_x的值：%d\r\n",j,sBufferArray[0][j]);
//		}
//		printf("最小值的X值：%d\r\n",lX_Min);
//		printf("最小值的X值：%d\r\n",lX_Max);
    /*去除最小值和最大值之后求平均值*/
    *pTouch_AD_x=(sBufferArray[0][0]+sBufferArray[0][1]+sBufferArray[0][2]+sBufferArray[0][3]+sBufferArray[0][4]+
               sBufferArray[0][5]+sBufferArray[0][6]+sBufferArray[0][7]+sBufferArray[0][8]+sBufferArray[0][9]-lX_Min-lX_Max)>>3;
//		printf("一共：%d\r\n",sBufferArray[0][0]+sBufferArray[0][1]+sBufferArray[0][2]+sBufferArray[0][3]+sBufferArray[0][4]+
//               sBufferArray[0][5]+sBufferArray[0][6]+sBufferArray[0][7]+sBufferArray[0][8]+sBufferArray[0][9]);
//			printf("平均pTouch_AD_x的值：%d\r\n",*pTouch_AD_x);
//		    for(int j=0;j<10;j++)
//		{
//			printf("第 %d 个pTouch_AD_y的值：%d\r\n",j,sBufferArray[1][j]);
//		}
    *pTouch_AD_y=(sBufferArray[1][0]+sBufferArray[1][1]+sBufferArray[1][2]+sBufferArray[1][3]+sBufferArray[1][4]+
               sBufferArray[1][5]+sBufferArray[1][6]+sBufferArray[1][7]+sBufferArray[1][8]+sBufferArray[1][9]-lY_Min-lY_Max)>>3;
//	      	printf("最小值的y值：%d\r\n",lY_Min);
//		      printf("最小值的y值：%d\r\n",lY_Max);
//		      printf("一共：%d\r\n",sBufferArray[1][0]+sBufferArray[1][1]+sBufferArray[1][2]+sBufferArray[1][3]+sBufferArray[1][4]+
//               sBufferArray[1][5]+sBufferArray[1][6]+sBufferArray[1][7]+sBufferArray[1][8]+sBufferArray[1][9]);
//					printf("平均pTouch_AD_y的值：%d\r\n",*pTouch_AD_y);

    return 0;
  }
  *pTouch_AD_x=-1;
  *pTouch_AD_y=-1;
  return 1;
}


/**
  * 函数功能: 电阻屏校准算法实现
  * 输入参数: XPT2046_Calibration结构体指针
  * 返 回 值: 0：计算成功，1：无法计算
  * 说    明：无
  */
static uint8_t perform_calibration(XPT2046_Calibration *cal) 
{
	int j;
	float n, x, y, x2, y2, xy, z, zx, zy;
	float det, a, b, c, e, f, i;
	float scaling = 65536.0;
//	float scaling = 4096.0;

// Get sums for matrix
	n = x = y = x2 = y2 = xy = 0;
	for(j=0;j<5;j++) {
		n += 1.0;
		x += (float)cal->touch_x[j];
		y += (float)cal->touch_y[j];
		x2 += (float)(cal->touch_x[j]*cal->touch_x[j]);
		y2 += (float)(cal->touch_y[j]*cal->touch_y[j]);
		xy += (float)(cal->touch_x[j]*cal->touch_y[j]);
	}

// Get determinant of matrix -- check if determinant is too small
	det = n*(x2*y2 - xy*xy) + x*(xy*y - x*y2) + y*(x*xy - y*x2);
	if(det < 0.1 && det > -0.1) {
//		printf("ts_calibrate: determinant is too small -- %f\n\r",det);
		return 1;
	}

// Get elements of inverse matrix
	a = (x2*y2 - xy*xy)/det;
	b = (xy*y - x*y2)/det;
	c = (x*xy - y*x2)/det;
	e = (n*y2 - y*y)/det;
	f = (x*y - n*xy)/det;
	i = (n*x2 - x*x)/det;

// Get sums for x calibration
	z = zx = zy = 0;
	for(j=0;j<5;j++) {
		z += (float)cal->lcd_x[j];
		zx += (float)(cal->lcd_x[j]*cal->touch_x[j]);
		zy += (float)(cal->lcd_x[j]*cal->touch_y[j]);
	}

// Now multiply out to get the calibration for framebuffer x coord
	cal->adjust[0] = (int32_t)((a*z + b*zx + c*zy)*(scaling));
	cal->adjust[1] = (int32_t)((b*z + e*zx + f*zy)*(scaling));
	cal->adjust[2] = (int32_t)((c*z + f*zx + i*zy)*(scaling));

//	printf("%f %f %f\n\r",(a*z + b*zx + c*zy),
//				(b*z + e*zx + f*zy),
//				(c*z + f*zx + i*zy));

// Get sums for y calibration
	z = zx = zy = 0;
	for(j=0;j<5;j++) {
		z += (float)cal->lcd_y[j];
		zx += (float)(cal->lcd_y[j]*cal->touch_x[j]);
		zy += (float)(cal->lcd_y[j]*cal->touch_y[j]);
	}

// Now multiply out to get the calibration for framebuffer y coord
	cal->adjust[3] = (int32_t)((a*z + b*zx + c*zy)*(scaling));
	cal->adjust[4] = (int32_t)((b*z + e*zx + f*zy)*(scaling));
	cal->adjust[5] = (int32_t)((c*z + f*zx + i*zy)*(scaling));

//	printf("%f %f %f\n\r",(a*z + b*zx + c*zy),
//				(b*z + e*zx + f*zy),
//				(c*z + f*zx + i*zy));

// If we got here, we're OK, so assign scaling to a[6] and return
	cal->adjust[6] = (int32_t)scaling;
	return 0;
}
 		char displaychar[50];

/**
  * 函数功能: 触摸屏校正函数
  * 输入参数: 无
  * 返 回 值: 0：校正成功
  *           1：校正失败
  * 说    明：无
  */
uint8_t XPT2046_Touch_Calibrate(void)
{  
  uint8_t i;  

  uint16_t usTest_x=0,usTest_y=0;  
  
  /* 设定“十”字交叉点的坐标 */ 
  cal_value.lcd_x[0]=20;
  cal_value.lcd_y[0]=20;
  
  cal_value.lcd_x[1]=20;
  cal_value.lcd_y[1]=LCD_WIDTH-20;
  
  cal_value.lcd_x[2]=LCD_HEIGHT-20;
  cal_value.lcd_y[2]=cal_value.lcd_y[1];
  
  cal_value.lcd_x[3]=cal_value.lcd_x[2];
  cal_value.lcd_y[3]=cal_value.lcd_y[0];	
  
  cal_value.lcd_x[4]=LCD_HEIGHT/2;
  cal_value.lcd_y[4]=LCD_WIDTH/2;	
  
  for(i=0; i<5; i++)
  {        

		LCD_showcolor(WHITE);
    /* 适当的延时很有必要 */        
    XPT2046_DelayUS(200000);    
    LCD_DrawCross(cal_value.lcd_x[i],cal_value.lcd_y[i]); //显示校正用的“十”字    
		sprintf(displaychar,"please touch  %d  Point",i+1);
		LCD_DispString_EN(150,100,displaychar,WHITE,RED);
    XPT2046_ReadAdc_Smooth_XY(&cal_value.touch_x[i],&cal_value.touch_y[i],1);
		printf("now Calibration x=(%d)-> %d y=(%d)-> %d\n\r",cal_value.lcd_x[i],cal_value.touch_x[i],cal_value.lcd_y[i],cal_value.touch_y[i]);

  }
  
  if(perform_calibration(&cal_value)==1) 
  {
		printf("Calibration failed.\n\r");
    return 1;
	}
	  printf("开始比对校准\n\r");
  /* 用原始参数计算出 原始参数与坐标的转换系数。 */
  for(i=0; i<2; i++)
  {    
    int xtemp,ytemp,usGap_x,usGap_y;       
    xtemp=cal_value.touch_x[2*i];
    ytemp=cal_value.touch_y[2*i];
    printf("before Calibration x=(%d)-> %d y=(%d)-> %d\n\r",cal_value.lcd_x[2*i],xtemp,cal_value.lcd_y[2*i],ytemp);
    
		usTest_x=(int)((cal_value.adjust[0]+cal_value.adjust[1]*xtemp+cal_value.adjust[2]*ytemp)/cal_value.adjust[6]);
		usTest_y=(int)((cal_value.adjust[3]+cal_value.adjust[4]*xtemp+cal_value.adjust[5]*ytemp)/cal_value.adjust[6]);
	  printf("after Calibration x = %d y=%d\n\r",usTest_x,usTest_y);
    
    usGap_x=(usTest_x>cal_value.lcd_x[2*i])?(usTest_x-cal_value.lcd_x[2*i]):(cal_value.lcd_x[2*i]-usTest_x);   //实际X坐标与计算坐标的绝对差
    usGap_y=(usTest_y>cal_value.lcd_y[2*i])?(usTest_y-cal_value.lcd_y[2*i]):(cal_value.lcd_y[2*i]-usTest_y);   //实际Y坐标与计算坐标的绝对差
  
    if((usGap_x>10)||(usGap_y>10))
    {
      LCD_showcolor(WHITE);		
      LCD_DispString_EN(80,100,"Calibrate fail",WHITE,RED);
      LCD_DispString_EN(110,130,"try again",WHITE,RED);
      XPT2046_DelayUS(1000000);   
      return 1;   
    }      
  }
  cal_value.cal_flag = 0xAA55;
	LCD_showcolor(WHITE); 
  LCD_DispString_EN(50,100,"Calibrate Succed",WHITE,BLUE);
  XPT2046_DelayUS(1000000); 
	LCD_showcolor(BLACK); 
  return 0;    
}
/**
  * 函数功能: 获取 XPT2046 触摸点（校准后）的坐标
  * 输入参数: pLCD_x：校准后x的坐标
  *           pLCD_y：校准后y的坐标
  * 返 回 值: 无
  * 说    明：无
  */
void XPT2046_Get_TouchedPoint(uint16_t *pLCD_x,uint16_t *pLCD_y)
{
  int xtemp,ytemp;
  
  if(XPT2046_ReadAdc_Smooth_XY(&xtemp,&ytemp,0)==0)
  {
    *pLCD_x=(uint16_t)((cal_value.adjust[0]+cal_value.adjust[1]*xtemp+cal_value.adjust[2]*ytemp)/cal_value.adjust[6]);
    *pLCD_y=(uint16_t)((cal_value.adjust[3]+cal_value.adjust[4]*xtemp+cal_value.adjust[5]*ytemp)/cal_value.adjust[6]);
  }
  else
  {
    *pLCD_x=0xFFFF;
    *pLCD_y=0xFFFF;
  }
  
} 
/*
*********************************************************************************************************************
*触摸控制器设备描述符
*********************************************************************************************************************
*/ 
TOUCH_DESCRIPTOR Touch_Des={   
	//LCD描述符	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//扫描触摸屏.0,屏幕扫描;1,物理坐标;	 	
	Touch_Scan,
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//触摸屏校准 
	Touch_Adjust,
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//两个方向上AD的最大值和最小值
	300,
	2663,
	
	348,
	2922,
//	258,
//	2775,
//	
//	285,
//	3210,
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//当前坐标
	//电容屏有最多5组坐标,电阻屏则用x[0],y[0]代表:此次扫描时,触屏的坐标,用
	0,0,0,0,0,
	0,0,0,0,0,	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////								
	//触摸状态、按下？松开？滑动？见TOUCH_STATE枚举
	TOUCH_RELEASE,					
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////								
	//触摸屏校准系数，用于AD到触摸像素点的转换，后期应该保存起来，上电读取
					
	0.13542,
	0.18648	,

	-41,  
	-65,	
//    0.12733,
//    0.16410,
//    33,
//    47,
//		float xfac;					
//	float yfac;
//	short xoff;
//	short yoff;	 
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////								
	//输入设备类型，电阻触摸、电容触摸、按钮触摸，前期只能电阻触摸，
	TOUCH_RES
};
