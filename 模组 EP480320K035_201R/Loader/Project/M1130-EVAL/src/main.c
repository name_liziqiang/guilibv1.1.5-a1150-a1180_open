/**
  ******************************************************************************
  * @file    Project/M1800-EVAL/main.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    19-December-2013
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */  

/* Includes ------------------------------------------------------------------*/
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "main.h"
#include "m1130_uart.h" 
#include "m1130_gpio.h" 
#include "m1130_rcc.h"	 
#include "misc.h"
#include "m1130_qspi.h"	 
#include "m1130_eval_qspi.h"
#include "guiConfig.h"
#include "flashInterface.h"
#include "TouchInit.h"
#include "touchInterface.h"
#include "lcddriver.h"
#include "lcdinit.h"


/* Private function prototypes -----------------------------------------------*/
#ifdef __GNUC__
  /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
     set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
/* Private functions ---------------------------------------------------------*/

void serial_init(void);

void delay(unsigned long time);
void delay_ms(uint16_t ms)
{
	uint16_t i;
	for(; ms>0; ms--)
		for(i=0; i<4000; i++);
}

//#define FLASH_4BYTES_ADDR

int switchQuad(uint32_t ctrl2Val)
{	
	GPIO_SetPinMux(GPIO1, GPIO_Pin_0, GPIO_FUNCTION_1);
	GPIO_SetPinMux(GPIO1, GPIO_Pin_3, GPIO_FUNCTION_1);
	//QSPI0->CMD = QUAD_SPI_CMD_FAST_READ_OCTAL_QUAD_IO;	//0xEB
	QSPI_ModeSet(QSPI0, QSPI_STD); //XIP自动切换到4线

#ifdef FLASH_4BYTES_ADDR
	QSPI0->XIP_READ_CMD = 0xEC;
	QSPI0->ADD_BYTE_LEN = 1;
#else
	QSPI0->XIP_READ_CMD = 0xEB;
	QSPI0->ADD_BYTE_LEN = 0;
#endif

	QSPI0->DUMMY = 2;					//
	QSPI0->START_ADDR = 0x8000;			//32KB
	QSPI0->CTRL2 = ctrl2Val;	
	//QSPI0->BUSY_DLY = 0;
	//bit13 DMA_ENABLE=0
	QSPI_DMACmd(QSPI0, DISABLE);
	//bit26 WRITE
	QSPI0->CTRL0_CLR = 1<<26;
	//bit28 HALF_DUPLEX=1
	QSPI0->CTRL0_SET = 1<<28;
	//bit25 MEMAP=1
	QSPI0->CTRL0_SET = 0x0a000000;

	return 0;
}
int switchQuad1(uint32_t ctrl2Val)
{	
//init XIP1
	GPIO_SetPinMux(GPIO0, GPIO_Pin_14, GPIO_FUNCTION_3); //gp1_6 d2
	GPIO_SetPinMux(GPIO0, GPIO_Pin_15, GPIO_FUNCTION_3); //gp1_7 d3
//	RCC_SETCLKDivider(RCC_CLOCKFREQ_QSPI1CLK, 2);
//	QSPI_PinSwitch(QSPI1, 1);
	QSPI_ModeSet(QSPI1, QSPI_STD);

#ifdef FLASH_4BYTES_ADDR
	QSPI1->XIP_READ_CMD = 0xEC;
	QSPI1->ADD_BYTE_LEN = 1;
#else
	QSPI1->XIP_READ_CMD = 0xEB;
	QSPI1->ADD_BYTE_LEN = 0;
#endif

	QSPI1->DUMMY = 2;		
	QSPI1->START_ADDR = 0;		//QSPI1 = 0;
  //QSPI1->CTRL2 = 0x2001;	
  QSPI1->CTRL2 = ctrl2Val;	
  //QSPI1->BUSY_DLY = 0x101;
	//bit13 DMA_ENABLE=0
	QSPI_DMACmd(QSPI1, DISABLE);
	//bit26 WRITE
	QSPI1->CTRL0_CLR = 1<<26;
	//bit28 HALF_DUPLEX=1
	QSPI1->CTRL0_SET = 1<<28;
	//bit25 MEMAP=1
	QSPI1->CTRL0_SET = 0x0a000000;

	return 0;
}
void startXIP(void)
{
/*
	(1)ISSI		M7-0=0xA0
	(2)GD		  M7-0=0xA0
	(3)ESMT		M7-0=0xA5
	(4)WB		  M7-0=0x20
	(5)MXIC		M7-0=0xA5
*/
	//RCC->XMAP_ADDR = 1; //flash 32KB = XIP 0  等价于  flash 40KB => XIP 8KB
	switchQuad(0x0000);
	RCC->ADDRESS_REMAP = 1;	//REBOOT to XIP		
}

void start_xip1(void)
{
  switchQuad1(0x0000);
	RCC->ADDRESS_REMAP1 = 1;	//REBOOT to XIP	
}
int ASM1130_UsbHostIrqInit(void);
int ASM1130_UsbHostprocess(void);

int checkCapDly(void)
{
	//检查flash  0x8000 地址是否有合法程序
	uint32_t spAddr,resetAddr;
	int8_t i;
	int8_t CapDlyMin=-1;
	int8_t CapDlyMax=-1;
	QSPI0->BUSY_DLY&=0XFFFFFF00;
	for(i=0;i<16;i++){
		QSPI0->BUSY_DLY &= 0xFFFFF0FF;	//clean [11:8]
		QSPI0->BUSY_DLY |= (i<<8);
		
		qspi_flash_read(QSPI0, (uint8_t *)0x20008000, 0x8000, 16);	//read 16 Bytes
		
		spAddr = inl(0x20008000)&0xFFFF0000;			//spAddr=0x2000XXXX
		resetAddr = inl(0x20008004);					//resetAddr=0x000000B1
		if((spAddr==0x20000000)&&(resetAddr==0x000000B1)){
			//printf("test capdly is %02d\r\n",i);
			if(CapDlyMin<0){
				CapDlyMin=i;
				CapDlyMax=i;
			}
			else CapDlyMax=i;
		}
	}
	if(CapDlyMin>=0){
		//有合适的 CapDly值
		uint32_t CapDly=0;
#if 1
		if((CapDlyMax-CapDlyMin)<=1){
			//最大值和最小值只差1个 甚至更小
			CapDly=CapDlyMax;
		}
		else CapDly=(CapDlyMax+CapDlyMin)>>1;//取最大和最小的中间值
#else
		CapDly=(CapDlyMax+CapDlyMin)>>1;//取最大和最小的中间值
#endif
		QSPI0->BUSY_DLY &= 0xFFFFF0FF;	//clean [11:8]
		QSPI0->BUSY_DLY |= (CapDly<<8);
		printf("CapDly is %02d\r\n",CapDly);
		return 0;
	}
	else return -1;		//fail
}

void outclk_init(void)
{
  GPIO_SetPinMux(GPIO0, GPIO_Pin_25, GPIO_FUNCTION_3);
  RCC->OUTCLKSEL = 0;
  RCC->OUTCLKDIV = 1;
  RCC->OUTCLKUEN = 0;
  RCC->OUTCLKUEN = 1;
}

extern void CheckUpade(void);
extern int qspi_flash_write_page(QSPI_TypeDef *QSPIptr, uint8_t *buf, uint32_t faddr, int count);

#define SYSTEMLOADER_DEBUG
//#define ADJUST_RC
#define  XPT_Calibration_ADDR 0

int main()
{
	//SystemInit里已经切换为外部晶振
#ifdef SYSTEMLOADER_DEBUG
	serial_init();
#endif
 printf("Start Loader\r\n");
 GPIO_SetPinMux(GPIO1, GPIO_Pin_18,GPIO_FUNCTION_0);//SW  用于下程序，按键按下上电，进入下载状态
 GPIO_SetPinDir(GPIO1, GPIO_Pin_18,GPIO_Mode_IN);//SW
 GPIO_ConfigPull(GPIO1, GPIO_Pin_18,GPIO_PULL_UP);
		
 GPIO_SetPinMux(GPIO1, GPIO_Pin_29,GPIO_FUNCTION_0);//模拟开关
 GPIO_SetPinDir(GPIO1, GPIO_Pin_29,GPIO_Mode_OUT);//模拟开关
 GPIO1->DT_SET=GPIO_Pin_29;//模拟开关打开

if(GPIO_ReadInputDataBit(GPIO1, GPIO_Pin_18)==RESET)
	{
		while(1)
		{
		 delay_ms(1000);
     printf("please update\r\n");
		}
	}			

	delay_ms(1000);

  quad_spi_flash_init(QSPI0, 2, 2, 0);//96Mhz
  quad_spi_flash_init(QSPI1, 2, 2, 0);//96Mhz
	
	QSPI1->BUSY_DLY &= 0xFFFFF0FF;	//clean [11:8]
	QSPI1->BUSY_DLY |= (1<<8);
	qspi_flash_global_unprotect(QSPI1);//以防万一Flash默认值不是可写入	
	
	if(XPT2046_EXTI_Read() == XPT2046_EXTI_ActiveLevel)
	{
//	  qspi_flash_erase_block_4k(QSPI1,XPT_Calibration_ADDR);
	}
//	 while(XPT2046_EXTI_Read() == XPT2046_EXTI_ActiveLevel);

	 qspi_flash_read(QSPI1,(uint8_t *)&cal_value,XPT_Calibration_ADDR,sizeof(XPT2046_Calibration));	
   if(cal_value.cal_flag!=0xAA55||XPT2046_EXTI_Read() == XPT2046_EXTI_ActiveLevel)
	 {	
 
	  TouchInit();//初始化触摸屏	
	  LCD_Init();
		LCD_DispString_EN(150,100,"start Calibration",WHITE,RED);

	 while(XPT2046_EXTI_Read() == XPT2046_EXTI_ActiveLevel);

	  while(XPT2046_Touch_Calibrate() !=0);
		qspi_flash_erase_block_4k(QSPI1,0);
		qspi_flash_write(QSPI1,(uint8_t *)&cal_value,XPT_Calibration_ADDR, sizeof(XPT2046_Calibration));
  }
#ifdef FLASH_4BYTES_ADDR
	qspi_flash_4Byte_address_enable(QSPI0, ENABLE);
#endif
	
#ifdef ADJUST_RC
	 outclk_init();
#else
 GPIO_SetPinMux(GPIO1, GPIO_Pin_17,GPIO_FUNCTION_0);//XPT_CS 用于flash
 GPIO_SetPinDir(GPIO1, GPIO_Pin_17,GPIO_Mode_OUT);//XPT_CS
 GPIO_SetBits(GPIO1, GPIO_Pin_17);//XPT_CS
//	//检查SD card 看是否有APP CODE 或者 RES升级
	 CheckUpade();
//	//检查GUILIB的启动信息，主要是关于版本等参数信息
//	 qspi_flash_erase_block_4k(QSPI1,4096);
	 checkGuiLibMsg();
	if(checkCapDly()<0){
		printf("cap dly error\r\n");
		while(1);
	}
	qspi_flash_SetDriverStrength(QSPI0,0);//驱动能力最强
	qspi_flash_enableQE(QSPI0, 0x02); //除了ESMT , 都需要这个  bit9 = QE
	qspi_flash_SetDriverStrength(QSPI1,0);//驱动能力最强
	qspi_flash_enableQE(QSPI1, 0x02); //除了ESMT , 都需要这个  bit9 = QE
	//read 8KB from Flash to RAM1
	qspi_flash_read(QSPI0, (uint8_t *)0x20008000, 0x8000, 0x2000);
	start_xip1();
	printf ("初始化xip1");
	startXIP();
#endif
	while(1)
	{
	}
}


void serial_init(void)
{
	UART_InitTypeDef UART_InitStructure;

	RCC_ResetAHBCLK(1<<AHBCLK_BIT_IOCON);
	RCC_ResetAHBCLK(1<<AHBCLK_BIT_UART1);
	RCC_UARTCLKSel(RCC_UARTCLK_SOURCE_SYSPLL);
	RCC_SETCLKDivider(RCC_CLOCKFREQ_UART1CLK, 32);

	GPIO_SetPinMux(GPIO0, GPIO_Pin_3, GPIO_FUNCTION_2);
  GPIO_SetPinMux(GPIO0, GPIO_Pin_2, GPIO_FUNCTION_2);//设置引脚复用

	UART_Reset(UART1);
	UART_StructInit(&UART_InitStructure);
	UART_Init(UART1, &UART_InitStructure);
	UART_Cmd(UART1, ENABLE);
}


/**
  * @brief  Retargets the C library printf function to the UART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
	/* Place your implementation of fputc here */
	/* e.g. write a character to the UART */
#ifdef SYSTEMLOADER_DEBUG
	UART_SendData(UART1, (uint8_t) ch);
	
	/* Loop until the end of transmission */
	while (UART_GetFlagStatus(UART1, UART_FLAG_TXFE) == RESET)
	{}
#endif 	
	return ch;
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  printf("Wrong parameters value: file %s on line %d\r\n", file, line);
  /* Infinite loop */
  while (1)
  {
  }
}
#endif





//		qspi_flash_read(QSPI1,(uint8_t *)&cal_value1,0,sizeof(XPT2046_Calibration));
//     printf("cal_flag: 0x%X\n",cal_value1.cal_flag);
//   for(i=0;i<5;++i)
//   {
//     printf("%d lcd_x=%d lcd_y=%d touch_x=%d touch_y=%d\n",i,cal_value1.lcd_x[i],cal_value1.lcd_y[i],cal_value1.touch_x[i],cal_value1.touch_y[i]);
//   }
//   for(i=0;i<7;++i)
//   {
//     printf("adjust[%d]=%d ",i,cal_value1.adjust[i]);
//   }
//   printf("\n");



