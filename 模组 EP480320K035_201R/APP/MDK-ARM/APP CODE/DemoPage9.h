#ifndef _DemoPage9_h
#define _DemoPage9_h
#include "gWidgetInfo.h"
#include "gStdint.h"
#include "gReturn.h"
#include "gResMsg.h"
static gui_uint32 OptionA_value = 0;
static gui_uint32 OptionB_value = 0;
static gui_uint32 OptionC_value = 0;
static gui_uint32 BinaryButton_value = 0;



#define OPwID 0x8001
static const PAGE_INFO OtherPage={
	{.wType = WIDGET_TYPE_PAGE , .wId = OPwID , .wVscope = WIDGET_VS_PRIVATE},
	{.x = 0 , .y = 0 , .width = 480 , .height = 320},
	.backMode = WIDGET_BACKMODE_COLOR , 
	.backColor = 0x631C,
	.picId = 3,
	.enterEvent = GUI_NULL,
	.leaveEvent = GUI_NULL,
	.pressEvent = GUI_NULL,
	.releaseEvent = GUI_NULL,
};
#define OPCutPic1wID 0x8002
static const CUTPICTURE_INFO OtherPageCutPic1= {
 {.wType = WIDGET_TYPE_CUTPICTURE , .wId = OPCutPic1wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 0x0000 , .y = 0x0000 , .width = 0x0034 , .height = 0x00140},
 .picId = 3,                          
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL 
};
#define OPCutPic2wID 0x8003
static const CUTPICTURE_INFO OtherPageCutPic2= {
 {.wType = WIDGET_TYPE_CUTPICTURE , .wId = OPCutPic2wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 0x01AC , .y = 0x0000 , .width = 0x0034 , .height = 0x00140},
 .picId = 3,                          
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL 
};
#define OPQRCodewID 0x8004
static const QRCODEBOX_INFO OtherPageQRCodeBox={
 {.wType = WIDGET_TYPE_QRCODEBOX , .wId = OPQRCodewID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 177 , .y = 63 , .width = 206 , .height = 206},  //width有效  height无效
 .dispLogo = 0,
 .backColor = 0x631C,
 .frontColor = 0x0000,                     
 .logoPicId = 0,
 .text = (char *)0x00, 	                                          
 .maxLen = 0x0030,                     
 .textLen = 0x0000,                      
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL  
};
#define OPTouchAreawID 0x8005
static const TOUCHAREA_INFO OtherPageTouchArea={  
  
 {.wType = WIDGET_TYPE_TOUCHAREA , .wId = OPTouchAreawID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 0x0034 , .y = 0x010F , .width = 0x0177 , .height = 0x0030},
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL     
};
#define OPTextBox1wID 0x8006	//A	
static const TEXTBOX_INFO OtherPageTextBox1={ 
 {.wType = WIDGET_TYPE_TEXTBOX , .wId = OPTextBox1wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 88 , .y = 88 , .width = 62 , .height = 30},
 .backMode = WIDGET_BACKMODE_COLOR,       // cutpic追加不了文字 pic不行             
 .keyId = 255,                          
 .fontlibId = 0,                     
 .borderWidth = 0,                   
 .borderColor = 0x00FF,              
 .picId = 6,                         
 .backColor = 0x631C,
 .frontColor = 0x0000,
 .xAlign = 0,                        
 .yAlign = 1,                       
 .isPassword = 0,                  
 .isDr = 0,   
 .text = (char *)0x05,                 
 .maxLen = 0x000A,                   
 .textLen = 0x0003,            
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL                
};
#define OPTextBox2wID 0x8007	//B	
static const TEXTBOX_INFO OtherPageTextBox2={
 {.wType = WIDGET_TYPE_TEXTBOX , .wId = OPTextBox2wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 88, .y = 156 , .width = 62 , .height = 30},
 .backMode = WIDGET_BACKMODE_COLOR,         
 .keyId = 255,                          
 .fontlibId = 0,                     
 .borderWidth = 0,                   
 .borderColor = 0x00FF,              
 .picId = 6,                         
 .backColor = 0x631C,
 .frontColor = 0x0000,
 .xAlign = 0,                        
 .yAlign = 1,                       
 .isPassword = 0,                  
 .isDr = 0,   
 .text = (char *)0x05,                 
 .maxLen = 0x000A,                   
 .textLen = 0x0006,            
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL                
};
#define OPTextBox3wID 0x8008	//C	
static const TEXTBOX_INFO OtherPageTextBox3={
 {.wType = WIDGET_TYPE_TEXTBOX , .wId = OPTextBox3wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 0x0058, .y = 0x00E0 , .width = 0x003E , .height = 0x001E},
 .backMode = WIDGET_BACKMODE_COLOR,               
 .keyId = 255,                          
 .fontlibId = 0,                     
 .borderWidth = 0,                   
 .borderColor = 0x00FF,              
 .picId = 6,                         
 .backColor = 0x631C,
 .frontColor = 0x0000,
 .xAlign = 0,                        
 .yAlign = 1,                       
 .isPassword = 0,                  
 .isDr = 0,   
 .text = (char *)0x05,                 
 .maxLen = 0x000A,                   
 .textLen = 0x0003,            
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL                
};
#define OPBinaryButtonwID 0x8009
static const BINARYBUTTON_INFO OtherPageBinaryButton={   
 {.wType = WIDGET_TYPE_BINARYBUTTON , .wId = OPBinaryButtonwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 0x0058 , .y = 0x0010 , .width = 0x0131 , .height = 0x001E},
 .backMode = WIDGET_BACKMODE_COLOR,              
 .fontlibId = 0,                      
 .borderWidth = 1,                   
 .borderColor = 0xFFDC, 
 .enablePicId = 5,
 .disenablePicId = 1,
 .enableBackColor = 0x0020,
 .disenableBackColor = 0x631C,
 .enableFontColor = 0x631C,  
 .disenableFontColor = 0xFFFF,
 .xAlign = 1,                        
 .yAlign = 1, 
 .isDr = 0,                         
 .value = 0,                  
 .text = (char *)0x00,    
 .maxLen = 0x0017,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL 
};
#define OPOptionBox1wID 0x800A
static const OPTIONBOX_INFO OtherPageOptionBox1={   
 {.wType = WIDGET_TYPE_OPTIONBOX , .wId = OPOptionBox1wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 64 , .y = 94 , .width = 20 , .height = 20},
 .backMode = WIDGET_BACKMODE_COLOR ,  
 .optionType = OPTIONBOX_TYPE_RECT,
 .backColor = 0x631C,
 .frontColor = 0xFFFF,
 .cPicId = 0,                      
 .nocPicId = 1,                    
 .value = 0,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL       
};
#define OPOptionBox2wID 0x800B
static const OPTIONBOX_INFO OtherPageOptionBox2={   
 {.wType = WIDGET_TYPE_OPTIONBOX , .wId = OPOptionBox2wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 64 , .y = 162 , .width = 20 , .height = 20},
 .backMode = WIDGET_BACKMODE_COLOR ,
 .optionType = OPTIONBOX_TYPE_CIRCLE,
 .backColor = 0x631C,
 .frontColor = 0xFFFF,
 .cPicId = 0,                      
 .nocPicId = 1,                    
 .value = 1,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL       
};
#define OPOptionBox3wID 0x800C
static const OPTIONBOX_INFO OtherPageOptionBox3={    
 {.wType = WIDGET_TYPE_OPTIONBOX , .wId = OPOptionBox3wID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 0x0040 , .y = 0x00E6 , .width = 0x0014 , .height = 0x0014},
 .backMode = WIDGET_BACKMODE_COLOR , 
 .optionType = OPTIONBOX_TYPE_RECT,
 .backColor = 0x631C,
 .frontColor = 0xFFFF,
 .cPicId = 0,                      
 .nocPicId = 1,                    
 .value = 0,
 .pressEvent = GUI_NULL,               
 .releaseEvent = GUI_NULL,             
 .updateEvent = GUI_NULL       
};










gui_Err OtherPageEnterEvent(gui_int32 argc , const char **argv);
gui_Err OtherPageLeaveEvent(gui_int32 argc , const char **argv);
static gui_Err BackToHome_releaseEvent(gui_int32 argc , const char **argv);
static gui_Err OtherPage_OptionA_releaseEvent(gui_int32 argc , const char **argv);
static gui_Err OtherPage_OptionB_releaseEvent(gui_int32 argc , const char **argv);
static gui_Err OtherPage_OptionC_releaseEvent(gui_int32 argc , const char **argv);
static gui_Err OtherPage_BinaryButton_releaseEvent(gui_int32 argc , const char **argv);
static gui_Err OtherPage_space_releaseEvent(gui_int32 argc , const char **argv);


#endif
