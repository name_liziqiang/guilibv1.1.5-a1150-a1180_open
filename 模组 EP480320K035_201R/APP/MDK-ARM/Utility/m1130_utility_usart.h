#ifndef  __M1130_UTILITY_USART_H
#define  __M1130_UTILITY_USART_H
#include "m1130_uart.h"

typedef enum
{
  UART0_GP2,
  UART0_GP7,
  UART1_GP0,
  UART1_GP2,
  UART2_GP0,
  UART2_GP7,
} UartPinGroup;

typedef enum
{
  DMA_NONBLOCK,
  DMA_BLOCK,
} DmaBlockStatus;

typedef struct
{
#define UART_RX_BUFFER_SIZE 256
  char rx_buf[UART_RX_BUFFER_SIZE];
  volatile int rxlen;
  volatile int rx_flag;
  volatile int rx_ptr;
} UartRecvBufferStruct;

void UART_Configuration(UART_TypeDef *UARTx, uint32_t BaudRate, UartPinGroup PinGroup);
int UART_DMASendData(UART_TypeDef *UARTx, char *buf, int size, DmaBlockStatus isDmaBlock, FunctionalState isEnIrq);
int UART_DMAReceiveData(UART_TypeDef *UARTx, char *buf, int size, DmaBlockStatus isDmaBlock, FunctionalState isEnIrq);

#endif //__M1130_UTILITY_USART_H
