#include "lcdInterface.h"
#include "m1130_gpio.h"
#include "lcdDriver.h"
#include <stdio.h>
#include <string.h>
#include "QspiDMA.h"


#define LCDINTERFACE_DELAY_NOP()     //__nop();__nop();__nop();__nop()
#define  Flash_Read_ByteMax          2000 //字节为单位 DMA最大传输 为511 块 （每块4字节）  这个值小于511 并且 小于LCD_SENDBUF_HALFWORD_MAX*2
#define  Lcd_SendBuf_HalfWord_Max    Flash_Read_ByteMax / 2 // 缓存区每行 是 
static __align(4) uint16_t  lcdSendBuff[2][Lcd_SendBuf_HalfWord_Max+2];
/*
*********************************************************************************************************************
*函数功能：LCD_Fill_UseOneData 将一个数据写N次到LCD显存
*          常用于清屏或者区域填充。开窗不由本函数完成，由调用函数完成，本函数只负责写数据
*入口参数：sPos       起点坐标
*          ePos       终点坐标
*          Data       写入的数据
*          WriteCount 写入的次数
*出口参数：无
*********************************************************************************************************************
*/
extern void LCD_WriteDatas(const uint16_t* Data, uint32_t WriteCount);
static int TransferData_FlashToLcd(uint32_t flashAddr, uint32_t PixDotCount)
{
//	printf("len:%d\r\n",sizeof(lcdSendBuff[0]));
	uint32_t operationCount = 0;
	uint32_t sendDots = 0;
//	uint32_t i;
	uint16_t *sendData = lcdSendBuff[0];
	DATA_QSPI->CTRL0_SET = QSPI_CTRL0_LOCK_CS;
	flash_ConfigReadFlashMsg(DATA_QSPI, FLASH_QSPIDMA_WRITE_CHANNEL, flashAddr);//配置flash
	QSPIDMA_ReadBytes(DATA_QSPI, FLASH_QSPIDMA_READ_CHANNEL, (uint8_t *)(&lcdSendBuff[0][0]),Flash_Read_ByteMax);
	if(PixDotCount > Lcd_SendBuf_HalfWord_Max){
		PixDotCount -= Lcd_SendBuf_HalfWord_Max;
		sendDots = Lcd_SendBuf_HalfWord_Max;
	}
	else {
		sendDots = PixDotCount;
		PixDotCount = 0;
	}
	operationCount = 0;

	while(1){
			QSPIDMA_WaitEnd(DATA_QSPI , FLASH_QSPIDMA_READ_CHANNEL);//等待 DMA 传输完成
		
			operationCount++;
			if(operationCount>1)operationCount=0;
			QSPIDMA_ReadBytes(DATA_QSPI, FLASH_QSPIDMA_READ_CHANNEL, (uint8_t *)(&lcdSendBuff[operationCount][0]),Flash_Read_ByteMax);//马上读下一批
			sendData = lcdSendBuff[1-operationCount];
//			for(i=sendDots;i>0;i--){
//				GPIO0->DT=(uint32_t)(*(sendData++)<<16);
//				LCD_WR_G();
//			}
			uint32_t WriteCount = sendDots/4;
			WriteCount = WriteCount<<2;//保证 写入的次数是4的整数倍
			uint32_t otherCount = sendDots%4;			
			LCD_WriteDatas(sendData, WriteCount);
			if(otherCount != 0){
				sendData+=WriteCount;
				//写入剩余的像素
				uint32_t i;
				for(i=0;i<otherCount;i++){
					GPIO0->DT=(uint32_t)(sendData[i])<<16;
					LCD_WR_G();
				}		
			}		
			if(PixDotCount == 0)break;
			else{
				if(PixDotCount >= Lcd_SendBuf_HalfWord_Max){
					sendDots = Lcd_SendBuf_HalfWord_Max;
					PixDotCount -= sendDots;
				}
				else{
					sendDots = PixDotCount;
					PixDotCount=0;
				}
			}
	}
	QSPIDMA_WaitEnd(DATA_QSPI , FLASH_QSPIDMA_READ_CHANNEL);
	DATA_QSPI->CTRL0_CLR = QSPI_CTRL0_LOCK_CS;
	//CTRL1切换为单线，D2D3 pinmux切换到GPIO
  // DATA_QSPI->CTRL1 = (CODE_QSPI->CTRL1 & 0xfffffff8) | 0x00;
	// PIO1->PIN0 = PIO1->PIN0 & 0xfffffffc;
	// PIO1->PIN3 = PIO1->PIN3 & 0xfffffffc;
	return 0;
}
/*
*********************************************************************************************************************
*函数功能：LCD_Fill_UseOneData 将一个数据写N次到LCD显存
*          常用于清屏或者区域填充。开窗不由本函数完成，由调用函数完成，本函数只负责写数据
*入口参数：sPos       起点坐标
*          ePos       终点坐标
*          Data       写入的数据
*          WriteCount 写入的次数
*出口参数：无
*********************************************************************************************************************
*/
extern void LCD_WriteOneData(uint32_t WriteCount);
static void LCD_Fill_UseOneData(LCD_POSITION sPos,LCD_POSITION ePos,const uint16_t Data,uint32_t WriteCount)
{
	Lcd_Driver.setcursor(sPos.x,sPos.y,ePos.x,ePos.y);
	Lcd_Driver.writeCMD(0x2c);//发送写GRAM指令
	LCD_RS_G();
	LCD_CS_D();	

	LCD_LOCK_OTHER_PORT();//锁住无关端口
	//写入数据
	if(WriteCount>=2)
	{
	GPIO0->DT=(uint32_t)Data<<16;
	LCD_WriteOneData(WriteCount);
	}
	else
	{
		//	//写入数据
	uint32_t i;
	for(i=0;i<WriteCount;i++){
		GPIO0->DT=(uint32_t)Data<<16;
		LCD_WR_G();
	}
	}
	LCD_UNLOCK_OTHER_PORT();//释放无关端口
	LCD_CS_G();
}
/*
*********************************************************************************************************************
*函数功能：LCD_Fill_UseContinuousData 使用一块连续的数据的进行填充
*          常用于显示一张完整的图像
*入口参数：sPos       写入的LCD起点坐标
*          Data       写入的数据
*          dataWidth  数据宽度
*          dataHeight 数据高度
*出口参数：无
*********************************************************************************************************************
*/
//static void LCD_Fill_UseContinuousData(LCD_POSITION sPos,const uint16_t* Data,uint16_t dataWidth,uint16_t dataHeight)
//{
//	uint32_t WriteCount=dataWidth*dataHeight;
//	Lcd_Driver.setcursor(sPos.x,sPos.y,sPos.x+dataWidth-1,sPos.y+dataHeight-1);
//	Lcd_Driver.writeCMD(0x2c);//发送写GRAM指令
//	LCD_RS_G();
//	LCD_CS_D();	

//	LCD_LOCK_OTHER_PORT();//锁住无关端口
//	//写入数据
//	uint32_t i;
//	for(i=WriteCount;i>0;i--){
//		GPIO0->DT=(uint32_t)(*Data++)<<16;
//		LCD_WR_D();
//		LCD_WR_G();
//	}
//	LCD_UNLOCK_OTHER_PORT();//释放无关端口
//	LCD_CS_G();
//}
//extern void LCD_WriteDatas(const uint16_t* Data, uint32_t WriteCount);
//图片数据地址，图片像素数量均是4的整数倍的时候  效率最高
static void LCD_Fill_UseContinuousData(LCD_POSITION sPos,const uint16_t* Data,uint16_t dataWidth,uint16_t dataHeight)
{
	uint32_t PixDotCount=dataWidth*dataHeight;
	Lcd_Driver.setcursor(sPos.x,sPos.y,sPos.x+dataWidth-1,sPos.y+dataHeight-1);
	Lcd_Driver.writeCMD(0x2c);//发送写GRAM指令
	LCD_RS_G();
	LCD_CS_D();	
	LCD_LOCK_OTHER_PORT();//锁住无关端口
	//写入数据
	if(((((uint32_t)Data) & 0x38000000))!=0x38000000){
		//数据在SRAM
		uint32_t i;
		for(i=PixDotCount;i>0;i--){
			GPIO0->DT=(uint32_t)(*Data++)<<16;
			LCD_WR_G();
		}
	}
	else{
		//数据在flash
		 uint32_t flashAddr = (uint32_t)Data-0x38000000;//XIP模式下得到的数据地址 需要还原成flash物理地址
		 enter_XIPCritical();
		 TransferData_FlashToLcd(flashAddr, PixDotCount);
		 quit_XIPCritical();
	}
	LCD_UNLOCK_OTHER_PORT();//释放无关端口
	LCD_CS_G();
}
/*
*********************************************************************************************************************
*函数功能：LCD_Fill_UsePartContinuousData  使用一块连续数据指定的一部分对LCD进行填充
*          常用于切图显示
*入口参数：DataBase     数据的基地址，注意它是连续数据块的起始地址，不是需要显示的区域的起始地址
*          WidthOffset  数据每一行的数据量（以uint16为基准进行的统计）
*          dispLoc      显示的地点位置，显示的宽度高度
*          dataPos      数据的起点位置
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_Fill_UsePartContinuousData(const uint16_t* DataBase,uint16_t WidthOffset,WIDGET_LOCATION dispLoc,LCD_POSITION dataPos)
{
	uint16_t (* Data)[WidthOffset];
	Data=(uint16_t(*)[WidthOffset])DataBase;//将一块连续的内存 转换成二位数组去访问
	Lcd_Driver.setcursor(dispLoc.x,dispLoc.y,dispLoc.x+dispLoc.width-1,dispLoc.y+dispLoc.height-1);
	Lcd_Driver.writeCMD(0x2c);//发送写GRAM指令
	LCD_RS_G();
	LCD_CS_D();	

	LCD_LOCK_OTHER_PORT();//锁住无关端口
	//写入数据
	uint32_t i,j;
	for(i=0;i<dispLoc.height;i++){
		for(j=0;j<dispLoc.width;j++){
			GPIO0->DT=(uint32_t)Data[dataPos.y+i][dataPos.x+j]<<16;
			LCD_WR_G();
		}
	}
	LCD_UNLOCK_OTHER_PORT();//释放无关端口
	LCD_CS_G();
}
/*
*********************************************************************************************************************
*函数功能：LCD_DrawPoint，LCD画一个点，使用LCD描述符前景色
*入口参数：x,y:坐标
*出口参数：无
*********************************************************************************************************************
*/ 
static void LCD_DrawPoint(uint16_t x,uint16_t y)
{
	LCD_POSITION sPos={x,y};
	LCD_POSITION ePos={x,y};
	LCD_Fill_UseOneData(sPos,ePos,lcdPara.fontcolor,1);	
}
/*
*********************************************************************************************************************
*函数功能：LCD_ReadPoint，LCD读一个点
*入口参数：x,y:坐标
*出口参数：uint32_t，该点的颜色RGB值
*********************************************************************************************************************
*/
__inline static void ReadBegin(void)
{
	LCD_CS_D();
	LCD_DATA_IN();
	LCD_RS_G();
	LCD_WR_G();
}
__inline static void ReadEnd(void)         
{                       
  LCD_CS_G();   
	LCD_DATA_OUT(); 
}
__inline static void LCD_ReadTrig(void)        
{    
  LCD_RD_D();       
  LCD_RD_G();     
}
static uint16_t LCD_ReadPoint(uint16_t x,uint16_t y)
{
	uint16_t r=0,g=0,b=0;
	LCD_POSITION sPos={x,y};
	LCD_POSITION ePos={x,y};	
	Lcd_Driver.setcursor(sPos.x,sPos.y,ePos.x,ePos.y);
	Lcd_Driver.writeCMD(0x2E);//发送写GRAM指令
	
  ReadBegin();       
	LCD_ReadTrig();     
	Lcd_Driver.readData();  //dummy Read    
				
	LCD_ReadTrig();         
	r = Lcd_Driver.readData()&0xf800;    

	LCD_ReadTrig();         
	b=Lcd_Driver.readData()&0xf800;     

	LCD_ReadTrig();         
	g=Lcd_Driver.readData()&0xfc00;   	
	
	ReadEnd();     

	return (((r>>11)<<11)|((g>>10)<<5)|(b>>11));
}
/*
*********************************************************************************************************************
*函数功能：LCD_Fill，使用LCD描述符的前景色填充LCD指定的一部分
*入口参数：sx sy起点坐标
*          ex ey终点坐标
*出口参数：无
*********************************************************************************************************************
*/
static void LCD_Fill(uint16_t sx,uint16_t sy,uint16_t ex,uint16_t ey)
{          
	uint32_t xlen=ex-sx+1;
	uint32_t ylen=ey-sy+1; 
	LCD_POSITION sPos={sx,sy};
	LCD_POSITION ePos={ex,ey};
	LCD_Fill_UseOneData(sPos,ePos,lcdPara.fontcolor,xlen*ylen);
}

const LCD_DESCRIPTOR Lcd_Des={
	LCD_DrawPoint,
	LCD_ReadPoint,
	LCD_Fill,
	LCD_Fill_UseOneData,
	LCD_Fill_UseContinuousData,
	LCD_Fill_UsePartContinuousData,
};
