#include "TouchInit.h"
#include "m1130_gpio.h"
#include "m1130_qspi.h"
  int32_t cal_value_adjust[7];
	XPT2046_Calibration *cal_value = (XPT2046_Calibration *)(0x38000000);


/*
*********************************************************************************************************************
*函数功能：TouchInit，触摸控制器启动初始化
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/ 

void TouchInit(void)
{
#ifdef TOUCH_USE_SPI
//	QSPI_InitTypeDef QSPI_InitStructure;
//	GPIO_SetPinMux(GPIO1, GPIO_Pin_15,GPIO_FUNCTION_0);//QSPI1_TF_CS 
//	GPIO_SetPinDir(GPIO1, GPIO_Pin_15,GPIO_Mode_OUT);  //QSPI1_TF_CS 
//	GPIO_SetPinMux(GPIO1, GPIO_Pin_17,GPIO_FUNCTION_0);//QSPI1_XPT_CS 
//	GPIO_SetPinDir(GPIO1, GPIO_Pin_17,GPIO_Mode_OUT);  //QSPI1_XPT_CS 
//	GPIO_SetPin(GPIO1, GPIO_Pin_15);//QSPI1_TF_CS 
//	GPIO_SetPin(GPIO1, GPIO_Pin_17);//QSPI1_XPT_CS 
//	
//	QSPI_InitStructure.QSPI_SlaveMode=QSPI_MASTER_MODE;
//	QSPI_InitStructure.QSPI_FrameLength=QSPI_FRAME_LENGTH_8Bit;
//	QSPI_InitStructure.QSPI_ModeSelect=QSPI_STD;
//	QSPI_InitStructure.QSPI_CPOL = QSPI_CPOL_Low;
//	QSPI_InitStructure.QSPI_CPHA = QSPI_CPHA_1Edge;
//	QSPI_InitStructure.QSPI_FirstBit = QSPI_FirstBit_MSB;
//	QSPI_InitStructure.QSPI_ClockDiv = (96<<8);
//	QSPI_InitStructure.QSPI_ClockRate = 0;//48/(48*(1+0));//1mhz
//	RCC->QUADSPI1CLKDIV = 8;//384/8=48mhz
//	
//	GPIO_SetPinMux(GPIO1, GPIO_Pin_10, GPIO_FUNCTION_3);//QSPI2_MOSI 
//	GPIO_SetPinMux(GPIO1, GPIO_Pin_11, GPIO_FUNCTION_3);//QSPI2_CLK 
//	GPIO_SetPinMux(GPIO1, GPIO_Pin_14, GPIO_FUNCTION_3);//QSPI2_MISO 	
//	
//	QSPI_HwInit(QSPI2, &QSPI_InitStructure);            //初始化QSPI1
//	
//	QSPI2->BUSY_DLY &= 0XFFFFFF00;
//	QSPI2->BUSY_DLY |= (8<<0);
//	QSPI2->BUSY_DLY &= 0xFFFFF0FF;	//clean [11:8]
//	QSPI2->BUSY_DLY |= (1<<8);      //设置一个合适的CAP_DLY
	
#else	
	GPIO_SetPinMux(GPIO1, GPIO_Pin_17,GPIO_FUNCTION_0);//QSPI1_XPT_CS 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_17,GPIO_Mode_OUT);  //QSPI1_XPT_CS 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_15,GPIO_FUNCTION_0);//QSPI1_TF_CS 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_15,GPIO_Mode_OUT);  //QSPI1_TF_CS 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_14,GPIO_FUNCTION_0);//QSPI1_MISO 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_14,GPIO_Mode_IN);   //QSPI1_MISO 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_10,GPIO_FUNCTION_0);//QSPI1_MOSI 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_10,GPIO_Mode_OUT);  //QSPI1_MOSI 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_11,GPIO_FUNCTION_0);//QSPI_CLK 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_11,GPIO_Mode_OUT);  //QSPI_CLK 	
#endif
cal_value_adjust[0]=cal_value->adjust[0];
cal_value_adjust[1]=cal_value->adjust[1];
cal_value_adjust[2]=cal_value->adjust[2];
cal_value_adjust[3]=cal_value->adjust[3];
cal_value_adjust[4]=cal_value->adjust[4];
cal_value_adjust[5]=cal_value->adjust[5];
cal_value_adjust[6]=cal_value->adjust[6];
}



