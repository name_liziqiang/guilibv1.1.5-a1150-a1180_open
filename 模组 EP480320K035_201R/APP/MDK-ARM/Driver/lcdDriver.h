#ifndef _lcddriver_h
#define _lcddriver_h
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：LCD驱动接口
 *实现功能：1、LCD的IO口访问（宏定义）
 *          2、LCD的设备描述符（包含内容如下）
 *            （1）、LCD宽度、高度、使用的背景颜色、字体颜色定义
 *            （2）、LCD的id、扫描方向定义
 *            （3）、LCD初始化接口
 *            （4）、LCD设定光标的数据接口
 *            （5）、LCD修改扫描方向函数接口
 *            （6）、LCD读写一个像素点函数接口
 *            （7）、LCD开启显示
 *            （8）、LCD关闭显示
 *            （9）、LCD清屏
 *          3、LCD支持的扫描方向枚举
 *          4、LCD读写宏定义
 *
 *
 *撰 写 人：Alpscale LCD Application development team
 *撰写时间：2019-7-22
 *测 试 人：Alpscale LCD Application development team
 *测试时间：2019-7-22
 *版 本 号：V1.01_2019-7-22
 *版 本 号：V1.02_2019-10-21  将头文件部分驱动 封装成 函数接口 移动到 LcdInterface 中去，将LcdInterface链接到代码的前8K 加速LCD的处理
 *                            code的前8K是运行在SRAM中的，具有相当可观的运行速度
 *版本说明：
 *                              
 *          函数清单
 *
 *V1.01：   功能实现
 *
 *V1.02：   移动部分驱动接口到LcdInterface加速LCD的读写速度
 */
 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "gStdint.h"
#include "gReturn.h"

/*
*remake: 一些所有关于端口的操作全部采用寄存器方式，加快CPU处理速度，详情请参考《STM32中文参考手册_V10》
*/
//LCD复位
#define  LCD_NRST()       //(GPIOA->BSRR|=1<<8)//set bit
#define  LCD_RST()        //(GPIOA->BRR|=1<<8)//clear bit
//LCD背光
#define  LCD_BG_OPEN()    GPIO0->DT_SET=GPIO_Pin_11//set bit    //LCD_BG 
#define  LCD_BG_CLOSE()   GPIO0->DT_CLR=GPIO_Pin_11//clear bit    //LCD_BG 
//功能引脚
#define  LCD_WR_G()       GPIO0->DT_SET=GPIO_Pin_4//set bit    LCD_WR 
#define  LCD_WR_D()       GPIO0->DT_CLR=GPIO_Pin_4//clear bit    LCD_WR 

#define  LCD_RD_G()       GPIO0->DT_SET=GPIO_Pin_8//set bit   LCD_RD 
#define  LCD_RD_D()       GPIO0->DT_CLR=GPIO_Pin_8//clear bit  LCD_RD 

#define  LCD_RS_G()       GPIO0->DT_SET=GPIO_Pin_10//set bit   LCD_RS 
#define  LCD_RS_D()       GPIO0->DT_CLR=GPIO_Pin_10//clear bit   LCD_RS 

#define  LCD_CS_G()       GPIO0->DT_SET=GPIO_Pin_9//set bit      LCD_CS 
#define  LCD_CS_D()       GPIO0->DT_CLR=GPIO_Pin_9//clear bit    LCD_CS 
//数据引脚
#define  LCD_DATA_OUT()   GPIO0_IT->DIR_SET=0xffff0000;//
#define  LCD_DATA_IN()    GPIO0_IT->DIR_CLR=0xffff0000;//

//锁住无关端口
#define  LCD_LOCK_OTHER_PORT()  GPIO0_IT->DATAMASK_SET=0x0000ffef;
//释放无关端口
#define  LCD_UNLOCK_OTHER_PORT()  GPIO0_IT->DATAMASK_CLR=0x0000ffef;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
*remake: 一些常用颜色
*/  
//画笔颜色
#define WHITE       0xFFFF
#define BLACK       0x0000    
#define BLUE        0x001F  
#define BRED        0XF81F
#define GRED        0XFFE0
#define GBLUE       0X07FF
#define RED         0xF800
#define MAGENTA     0xF81F
#define GREEN       0x07E0
#define CYAN        0x7FFF
#define YELLOW      0xFFE0
#define BROWN       0XBC40 //棕色
#define BRRED       0XFC07 //棕红色
#define GRAY        0X8430 //灰色
#define DARKBLUE    0X01CF //深蓝色
#define LIGHTBLUE   0X7D7C //浅蓝色  
#define GRAYBLUE    0X5458 //灰蓝色
#define LIGHTGREEN  0X841F //浅绿色 
#define LGRAY       0XC618 //浅灰色(PANNEL),窗体背景色
#define LGRAYBLUE   0XA651 //浅灰蓝色(中间层颜色)
#define LBBLUE      0X2B12 //浅棕蓝色(选择条目的反色)      
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////                                                   
/*

*remake: 粗略的短暂延时，用户LCD读写时序
*/                                                   
#define  DELAY()   __NOP();__NOP();__NOP()
/*
*remake: 对外的数据接口
*/
#define LCD_WIDTH       320   //LCD初始化的宽度和高度，描述符里面初始化是这个值，但是后期可能切换显示模式，导致宽度和高度发送了变化
#define LCD_HEIGHT      480

#define DIR_HORIZONTAL  1
#define DIR_VERTICAL    0

#define LCD_LuminanceMax 100

#define LCDINTERFACE_DELAY_NOP()    //__nop();__nop();__nop();__nop()
#define Flash_Read_ByteMax          2000 //字节为单位 DMA最大传输 为511 块 （每块4字节）  这个值小于511 并且 小于LCD_SENDBUF_HALFWORD_MAX*2
#define Lcd_SendBuf_HalfWord_Max    Flash_Read_ByteMax / 2 // 缓存区每行 是 
extern  __align(4) gui_uint16  lcdSendBuff[2][Lcd_SendBuf_HalfWord_Max+2];

typedef struct  _LCD_DRIVER{   
	//LCD描述符	
	void       (*writeData)(gui_uint16 data);
	gui_uint16 (*readData)(void);
	void 	  (*writeCMD)(gui_uint16 cmd);
	void 		(*setcursor)(gui_uint16 Xpos, gui_uint16 Ypos, gui_uint16 Xepos, gui_uint16 Yepos);//LCD 设置光标起始位置和结束位置
	void    (*scan_dir)(gui_uint8 dir);                                             	 //LCD扫描方向
	void 		(*open)(void);                                                             //LCD打开显示
	void 		(*close)(void);                                                            //LCD关闭显示
	void 		(*scroll)(gui_uint16 sx,gui_uint16 sy,gui_uint16 ex,gui_uint16 ey);                //LCD设置滚动区域
	void 		(*SetDim)(gui_uint16 pulse);
}LCD_DRIVER;  
extern const LCD_DRIVER Lcd_Driver; //管理LCD重要参数
#endif
