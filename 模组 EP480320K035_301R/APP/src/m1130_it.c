/**
  ******************************************************************************
  * @file    Project/M1130-EVAL/src/m1130_it.c 
  * @author  Alpscale Software Team 
  * @version V1.0.0
  * @date    10-21-2013
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, ALPHASCALE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2013 Alphascale</center></h2>
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include "m1130_it.h"
#include "m1130.h"
#include "m1130_uart.h"
#include "SystemUart.h"
#include "guiFunc.h"
#include "m1130_gpio.h"
#include "DemoApp.h"
#include "lcdInit.h"
#include "touchInterface.h"

static uint8_t time=0;

void commonDelay(volatile unsigned long time)
{
	while ( time )time--; 
}

void TIM16_IRQHandler(void)
{
	TIM16->SR=0;//清除中断标志
	guiTickHandle();//系统时基，1ms一次
	//GPIO0->DT_TOG=GPIO_Pin_24;
//	GPIO0->DT_TOG=GPIO_Pin_4;
//	GPIO0->DT_TOG=GPIO_Pin_5;
//	GPIO0->DT_TOG=GPIO_Pin_6;
}
void TIM17_IRQHandler(void)
{
	
	TIM17->SR=0;//清除中断标志
	time++;
	if(time==2)//2ms
	{
		time=0;
		Touch_Des.scan();
//		commonDelay(500);	
	}

}
void GPIO0_IRQHandler(void)
{
	if(GPIO_GetOriginalITStatus(GPIO0_IT,GPIO_Pin_2))
	{
		GPIO_EdgeITClear(GPIO0_IT,GPIO_Pin_2);
//		if(State==MAIN_PAGE)
//		{

//		}	
//		if(State==OFF_PAGE) {Start=1;LcdSetDim(100);guiJumpPage(StartPage,StartPageLeaveEvent,StartPageEnterEvent);}
		printf("UP\r\n");	
	}
	if(GPIO_GetOriginalITStatus(GPIO0_IT,GPIO_Pin_3))
	{
		GPIO_EdgeITClear(GPIO0_IT,GPIO_Pin_3);
////		if(State==MAIN_PAGE)
////		{
////			left_flag=!left_flag;
////			if(left_flag==0) guiSetWidgetPictureID(MPPic_leftwID , 10);
////		}
		printf("LIFT\r\n");
	}
}

void GPIO1_IRQHandler(void)
{
	if(GPIO_GetOriginalITStatus(GPIO1_IT,GPIO_Pin_29))
	{
//		if(Lock_flag==0)
//		{
//			guiJumpPage(LockPage,LockPageLeaveEvent,LockPageEnterEvent);
//			LcdSetDim(0);		
//			Lock_flag=1;			
//		}
//		else
//		{
//			LcdSetDim(100);		
//			Lock_flag=0;
//		}
		printf("OK\r\n");
		GPIO_EdgeITClear(GPIO1_IT,GPIO_Pin_29);
	}
	if(GPIO_GetOriginalITStatus(GPIO1_IT,GPIO_Pin_30))
	{
		GPIO_EdgeITClear(GPIO1_IT,GPIO_Pin_30);
//		if(State==MAIN_PAGE) guiJumpPage(ShutPage,ShutPageLeaveEvent,ShutPageEnterEvent);
		printf("DOWN\r\n");	
	}
	if(GPIO_GetOriginalITStatus(GPIO1_IT,GPIO_Pin_31))
	{
		GPIO_EdgeITClear(GPIO1_IT,GPIO_Pin_31);
		printf("RIGHT\r\n");
	}	
}

void UART0_IRQHandler(void)
{
	uint8_t data;
	if((UART0->INTR&UART_IT_RXIS)!=0){
		//接收中断
		UART0->INTR_SET=UART_IT_RXIS;
		data=UART0->DATA;
		if(data) data=0;
//		Com_Des.inputChar(data);
	}
	else if((UART0->INTR&UART_IT_RTIS)!=0){
		UART0->INTR_SET=UART_IT_RTIS;
		data=UART0->DATA;
		if(data) data=0;
//		Com_Des.inputChar(data);
//		Com_Des.inputEnd();
	}
}

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
// void NMI_Handler(void)
// {
// }

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
	printf("HardFault_Handler");
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSV_Handler exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
  /* Decrement the TimingDelay variable */
}

/******************************************************************************/
/*            m1130 Peripherals Interrupt Handlers                        */
/******************************************************************************/

/******************************************************************************/
/*                 m1130 Peripherals Interrupt Handlers                        */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_m1130.s).                                                    */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/**
  * @}
  */ 

/**
  * @}
  */ 

//加入以下代码,支持printf函数,而不需要选择use MicroLIB	  
//#pragma import(__use_no_semihosting)             
//标准库需要的支持函数                 
struct __FILE 
{ 
	int handle; 
	/* Whatever you require here. If the only file you are using is */ 
	/* standard output using printf() for debugging, no file handling */ 
	/* is required. */ 
}; 
/* FILE is typedef’ d in stdio.h. */ 
//FILE __stdout;       
//定义_sys_exit()以避免使用半主机模式    
int _sys_exit(int x) 
{ 
	x = x; 
	return 0;
} 
//__use_no_semihosting was requested, but _ttywrch was 
void _ttywrch(int ch) 
{ 
	ch = ch; 
} 
//重定向fputc函数
//printf的输出，指向fputc，由fputc输出到串口
//这里使用串口0(USART0)输出printf信息
int fputc(int ch, FILE *f)
{   
//#if USE_PRINTF_DEBUG	
	while((UART0->STAT&UART_FLAG_TXFE)==0);//等待上一次串口数据发送完成  
	UART0->DATA = ch;//写DR,串口1将发送数据
//#endif 
	return ch;
}

int fgetc(FILE *stream)
{
	return 0;
}


/******************* (C) COPYRIGHT 2013 ALPHASCALE  *****END OF FILE****/
