#include "DemoApp.h"
#include "guiFunc.h"
#include "stdio.h"
#include "string.h"
#define TestwID 0x1111
static const PAGE_INFO Test={
 {.wType = WIDGET_TYPE_PAGE , .wId = TestwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 0 , .y = 0 , .width = 480 , .height = 320},
 .backMode = WIDGET_BACKMODE_COLOR , 
 .backColor = 0x0000,
 .picId = 255
};
#define TestPICwID 0x2222	
static const PICTUREBOX_INFO TestPIC={
 {.wType = WIDGET_TYPE_PICTUREBOX , .wId = TestPICwID , .wVscope = WIDGET_VS_PRIVATE},
 {.x = 10 , .y = 10 , .width = 28 , .height = 28},    
 .picId = 0
};
gui_Err TestEnterEvent(gui_int32 argc , const char **argv)
{
	
//	guiCreateWidget((const void *)&Test);//创建的第一个控件一定是page
//	guiCreateWidget((const void *)&TestPIC);
	return GUI_EOK;
}
void guiMainPageInit(void)
{
	guiJumpPage(GUI_NULL,GUI_NULL,LOGOPageEnterEvent);//进入开机界面
}

/*****************************************紫芯**************************************/


/*********************************************
*************开机界面的进入事件****************
**********************************************/
gui_Err LOGOPageEnterEvent(gui_int32 argc , const char **argv)
{
	
	guiCreateWidget((const void *)&LOGOPage);//创建的第一个控件一定是page
	guiCreateWidget((const void *)&LOGOPageGTimer);
	guiCreateWidget((const void *)&LOGOPageVIRTUALGIF);
	guiCreateWidget((const void *)&LOGOQRCode);
	guiCreateWidget((const void *)&LOGOPIC);
	/*为按钮控件注册松开事件，如果没有注册，你写的事件将无效，也就不能执行响应的动作*/
	guiRegisterEvent(EVENT_REGISTER_TYPE_TIMEOUT , LOGOGTimerwID , GetGIF_timeoutEvent);
	guiSetWidgetText(LOGOQRCodewID,26,"http://www.alphascale.com");
	return GUI_EOK;
}
/*********************************************
*************开机界面的离开事件****************
**********************************************/
gui_Err LOGOPageLeaveEvent(gui_int32 argc , const char **argv)
{	
	return GUI_EOK;
}
/****************************************************
*************只在开机界面有效的超时事件****************
*****************************************************/

static gui_Err GetGIF_timeoutEvent(gui_int32 argc , const char **argv)
{
	static gui_uint16 picid;
	guiGetWidgetPictureID(LOGOVIRTUALGIFwID,&picid);
	if(picid==254) guiJumpPage(MPwID,MainPageLeaveEvent,MainPageEnterEvent);
	return GUI_EOK;
}





























