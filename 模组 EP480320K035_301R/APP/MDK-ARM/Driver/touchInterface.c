
#include "stdio.h"
#include "touchInterface.h"
#include "lcddriver.h"
#include "guiFunc.h"
#include "guiConfig.h"



#define VALID_AD_OFFSET   100  //有效触摸AD 的最大偏移量，两次ad的差值进行比较，超过这个值认为无效
#define VALID_AD_COUNT    5		 //连续5次采样
static uint16_t LastXad=0;
static uint16_t LastYad=0;
static uint16_t adValidCount=0;
extern uint16_t touchReadX(void);
extern uint16_t touchReadY(void);

static uint16_t touch_read_rotation=0;
#define TSLIDE_MIN_OFFSET         8//判断滑动的最小偏移量
/*
*********************************************************************************************************************
*函数功能：触摸控制器  有效  触摸点处理函数
*入口参数：xad 触摸点X坐标AD值
*         yad 触摸点Y坐标AD值
*出口参数：无
*********************************************************************************************************************
*/ 
static void TouchPosHandle(uint16_t xad,uint16_t yad)
{
	LCD_POSITION PosBuff;
//	printf("获取坐标\r\n");
	if(Touch_Des.touchtype==TOUCH_RES){//若是电阻屏
//		printf("xad:%05d   yad:%05d\r\n",xad,yad);		
		if((xad<Touch_Des.x_min)|(xad>Touch_Des.x_max)){
				PosBuff=INPUTMSG_TOUCHVALUE_NULL;//那就发送一个空坐标，
				guiSendMsgTouch(1,&PosBuff);
				return;
//			printf("坐标为无效坐标");
			}
		if((yad<Touch_Des.y_min)|(yad>Touch_Des.y_max)){
				PosBuff=INPUTMSG_TOUCHVALUE_NULL;//那就发送一个空坐标，
				guiSendMsgTouch(1,&PosBuff);
//			printf("坐标为无效坐标");
			return;
		}
//	printf("坐标为有效坐标");
		uint16_t x=Touch_Des.xfac*xad-Touch_Des.xoff;
		uint16_t y=Touch_Des.yfac*yad-Touch_Des.yoff;
		
		if(DIR_HORIZONTAL)//如果屏幕是横屏
		{
			Touch_Des.y[0]=LCD_WIDTH-x;
			Touch_Des.x[0]=y;			
			
			if((x>LCD_WIDTH)||(y>LCD_HEIGHT)){
					PosBuff=INPUTMSG_TOUCHVALUE_NULL;//那就发送一个空坐标，
			}
			else{
//				printf("x:%03d   y:%03d\r\n",Touch_Des.x[0],Touch_Des.y[0]);
				PosBuff=(LCD_POSITION){Touch_Des.x[0],Touch_Des.y[0]};
			}							
		}
		else{
			Touch_Des.x[0]=x;//LCD_WIDTH-x;
			Touch_Des.y[0]=y;//LCD_HEIGHT-y;//根据产品的特性，我们的触摸器远点在右下角			
//			if((x>LCD_HEIGHT)||(y>LCD_WIDTH)){
			if((x>LCD_WIDTH)||(y>LCD_HEIGHT)){
					PosBuff=INPUTMSG_TOUCHVALUE_NULL;//那就发送一个空坐标，
			}
			else{
				PosBuff=(LCD_POSITION){Touch_Des.x[0],Touch_Des.y[0]};				
			}			
		}
		guiSendMsgTouch(1,&PosBuff);
	}
}
/*
*********************************************************************************************************************
*函数功能：触摸控制器定时扫描接口
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/ 
void Touch_Scan(void)
{
	uint16_t count;
	static uint16_t xad=0;
	static uint16_t yad=0;
	//uint32_t tick=tick=Get_SystemTick();
	//不能一次读两个通道，会导致数据出问题，
	if((touch_read_rotation%2)==0){
		xad=touchReadX();
	}
	else {
		yad=touchReadY();
	}
	touch_read_rotation++;
	if(touch_read_rotation>=2){
		touch_read_rotation=0;
//		printf("xad:%05d   yad:%05d\r\n",xad,yad);
		if(adValidCount==0){
			adValidCount++;
			LastXad=xad;
			LastYad=yad;//第一次检测到
		}
		else{
			count=0;
			//检测X坐标是否在合理的误差范围内
			if(xad>=LastXad){
				if((xad-LastXad)>VALID_AD_OFFSET)count++;
			}
			else{
				if((LastXad-xad)>VALID_AD_OFFSET)count++;
			}
			//检测Y坐标是否在合理的误差范围内
			if(yad>=LastYad){
				if((yad-LastYad)>VALID_AD_OFFSET)count++;
			}
			else{
				if((LastYad-yad)>VALID_AD_OFFSET)count++;
			}

			if(count)adValidCount=0;//有一个不符合范围，重新来
			else {
				LastXad=xad;
				LastYad=yad;
				adValidCount++;
				if(adValidCount>=VALID_AD_COUNT)TouchPosHandle(xad,yad);//检测到了合理的值
			}
		}
		//printf("scan need time is %d\r\n",Get_SystemTick()-tick);
	}
}
/*
*********************************************************************************************************************
*函数功能：Touch_Adjust，触摸控制器校准函数
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/ 
void Touch_Adjust(void)
{
	
}
/*
*********************************************************************************************************************
*触摸控制器设备描述符
*********************************************************************************************************************
*/ 
TOUCH_DESCRIPTOR Touch_Des={   
	//LCD描述符	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//扫描触摸屏.0,屏幕扫描;1,物理坐标;	 	
	Touch_Scan,
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//触摸屏校准 
	Touch_Adjust,
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//两个方向上AD的最大值和最小值
	205,
	3915,
	
	210,
	3950,

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//当前坐标
	//电容屏有最多5组坐标,电阻屏则用x[0],y[0]代表:此次扫描时,触屏的坐标,用
	0,0,0,0,0,
	0,0,0,0,0,	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////								
	//触摸状态、按下？松开？滑动？见TOUCH_STATE枚举
	TOUCH_RELEASE,					
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////								
	//触摸屏校准系数，用于AD到触摸像素点的转换，后期应该保存起来，上电读取
					
	-0.08625,
	-0.12834,	

	-338, 
	-507,	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////								
	//输入设备类型，电阻触摸、电容触摸、按钮触摸，前期只能电阻触摸，
	TOUCH_RES
};
