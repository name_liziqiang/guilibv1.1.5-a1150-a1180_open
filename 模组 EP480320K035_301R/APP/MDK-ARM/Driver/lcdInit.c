#include "lcdInit.h"
#include "lcdDriver.h"
#include "m1130_gpio.h"
#include "m1130_tim.h"
#include "stdio.h"

#define LCD_ILI9488_CMD Lcd_Driver.writeCMD
#define LCD_ILI9488_INDEX Lcd_Driver.writeData
/*
*********************************************************************************************************************
*函数功能：LCDinitDelay LCD初始化模块延时函数
*入口参数：dly  延时时间
*出口参数：无
*********************************************************************************************************************
*/
void LCDDelay(unsigned int dly)
{
    unsigned int i,j;
    for(i=0;i<dly;i++)
    	for(j=0;j<255;j++);
}
void LCD_DIM_init(uint16_t arr,uint16_t psc,uint16_t pulse)
{
	//TIM15  CH1 输出PWM
	RCC->AHBCLKCTRL0_SET|=(1<<17);//开启定时器时钟
	RCC->AHBCLKCTRL0_SET|=(1<<3);
	RCC->PWMCLKDIV = 0X01;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	GPIO_InitTypeDef GPIO_InitStructure; 
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Function = GPIO_FUNCTION_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_Init(GPIO0, &GPIO_InitStructure);
	
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Prescaler=psc;//96Mhz/(95+1)=1Mhz
	TIM_TimeBaseStructure.TIM_Period=arr ;//fpwm=9600/(1199+1)=80Khz
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure);
	
	//SYSAHBCLKDIV=4 在SystemInit已经初始化了 AHB时钟96Mhz	
	TIM_OCStructInit(&TIM_OCInitStructure);
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1; //选择定时器模式:TIM脉冲宽度调制模式2
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable; //比较输出使能
	TIM_OCInitStructure.TIM_Pulse = pulse; //设置待装入捕获比较寄存器的脉冲值
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High; //输出极性:TIM输出比较极性高
	TIM_OC1Init(TIM1, &TIM_OCInitStructure);  //根据TIM_OCInitStruct中指定的参数初始化外设TIMx     //TIM1_CH1

	TIM_OC1PreloadConfig(TIM1,TIM_OCPreload_Enable);
	TIM_ARRPreloadConfig(TIM1,ENABLE);
	TIM_CtrlPWMOutputs(TIM1, ENABLE);
	TIM_Cmd(TIM1,ENABLE);
}

void LcdSetDim(uint16_t pulse)
{
	TIM_SetCompare1(TIM1 , pulse);
}
void ILI9488_CTC35_Initial_Code(void) 
{
// VCI=2.8V
//************* Reset LCD Driver ****************// 
//LCD_nRESET = 1; 
//Delayms(1); // Delay 1ms 
//LCD_nRESET = 0; 
//Delayms(10); // Delay 10ms // This delay time is necessary 
//LCD_nRESET = 1; 
//Delayms(120); // Delay 120 ms 
 
//************* Start Initial Sequence **********// 
LCD_ILI9488_CMD(0xE0); 
LCD_ILI9488_INDEX(0x00); 
LCD_ILI9488_INDEX(0x01); 
LCD_ILI9488_INDEX(0x02); 
LCD_ILI9488_INDEX(0x04); 
LCD_ILI9488_INDEX(0x14); 
LCD_ILI9488_INDEX(0x09); 
LCD_ILI9488_INDEX(0x3F); 
LCD_ILI9488_INDEX(0x57); 
LCD_ILI9488_INDEX(0x4D); 
LCD_ILI9488_INDEX(0x05); 
LCD_ILI9488_INDEX(0x0B); 
LCD_ILI9488_INDEX(0x09); 
LCD_ILI9488_INDEX(0x1A); 
LCD_ILI9488_INDEX(0x1D); 
LCD_ILI9488_INDEX(0x0F);  
 
LCD_ILI9488_CMD(0xE1); 
LCD_ILI9488_INDEX(0x00); 
LCD_ILI9488_INDEX(0x1D); 
LCD_ILI9488_INDEX(0x20); 
LCD_ILI9488_INDEX(0x02); 
LCD_ILI9488_INDEX(0x0E); 
LCD_ILI9488_INDEX(0x03); 
LCD_ILI9488_INDEX(0x35); 
LCD_ILI9488_INDEX(0x12); 
LCD_ILI9488_INDEX(0x47); 
LCD_ILI9488_INDEX(0x02); 
LCD_ILI9488_INDEX(0x0D); 
LCD_ILI9488_INDEX(0x0C); 
LCD_ILI9488_INDEX(0x38); 
LCD_ILI9488_INDEX(0x39); 
LCD_ILI9488_INDEX(0x0F); 

LCD_ILI9488_CMD(0xC0); 
LCD_ILI9488_INDEX(0x18); 
LCD_ILI9488_INDEX(0x16); 
 
LCD_ILI9488_CMD(0xC1); 
LCD_ILI9488_INDEX(0x41); 

LCD_ILI9488_CMD(0xC5); 
LCD_ILI9488_INDEX(0x00); 
LCD_ILI9488_INDEX(0x24); 
LCD_ILI9488_INDEX(0x80); 

LCD_ILI9488_CMD(0x36); 
LCD_ILI9488_INDEX(0x08); 

LCD_ILI9488_CMD(0x3A); //Interface Mode Control
LCD_ILI9488_INDEX(0x55);

LCD_ILI9488_CMD(0XB0);  //Interface Mode Control  
LCD_ILI9488_INDEX(0x00); 
LCD_ILI9488_CMD(0xB1);   //Frame rate 70HZ  
LCD_ILI9488_INDEX(0xB0); 

LCD_ILI9488_CMD(0xB4); 
LCD_ILI9488_INDEX(0x02);   

LCD_ILI9488_CMD(0xB6); //RGB/MCU Interface Control
LCD_ILI9488_INDEX(0x02); 
LCD_ILI9488_INDEX(0x22); 

LCD_ILI9488_CMD(0xE9); 
LCD_ILI9488_INDEX(0x00);
 
LCD_ILI9488_CMD(0XF7);    
LCD_ILI9488_INDEX(0xA9); 
LCD_ILI9488_INDEX(0x51); 
LCD_ILI9488_INDEX(0x2C); 
LCD_ILI9488_INDEX(0x82);

LCD_ILI9488_CMD(0x11); 
LCDDelay(120); 
LCD_ILI9488_CMD(0x29); 
} 
/*
*********************************************************************************************************************
*函数功能：LCD_Init，LCD初始化，初始化LCD的硬件
*入口参数：无
*出口参数：无
*********************************************************************************************************************
*/
void LCD_Init(void)
{
	//初始化GPIO
	GPIO0_IT->DIR_SET=0xffff0000;//DB0~15输出 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_29,GPIO_FUNCTION_0);//LCD_BG 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_28,GPIO_FUNCTION_0);//LCD_RD 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_4,GPIO_FUNCTION_0);//LCD_WR 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_3,GPIO_FUNCTION_0);//LCD_RS 
	GPIO_SetPinMux(GPIO1, GPIO_Pin_30,GPIO_FUNCTION_0);//LCD_CS 
	GPIO_SetPinMux(GPIO0, GPIO_Pin_2,GPIO_FUNCTION_0);//LCD_RST
	
	GPIO_SetPinDir(GPIO0, GPIO_Pin_4,GPIO_Mode_OUT);//LCD_WR 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_29,GPIO_Mode_OUT);//LCD_BG 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_28,GPIO_Mode_OUT);//LCD_RD 	
	GPIO_SetPinDir(GPIO0, GPIO_Pin_3,GPIO_Mode_OUT);//LCD_RS 
	GPIO_SetPinDir(GPIO1, GPIO_Pin_30,GPIO_Mode_OUT);//LCD_CS 
	GPIO_SetPinDir(GPIO0, GPIO_Pin_2,GPIO_Mode_OUT);//LCD_RST		
	
//	GPIO_SetPinMux(GPIO1, GPIO_Pin_15,GPIO_FUNCTION_0);//QSPI1_TF_CS 
//	GPIO_SetPinDir(GPIO1, GPIO_Pin_15,GPIO_Mode_OUT);  //QSPI1_TF_CS 
//	GPIO_SetPin(GPIO1, GPIO_Pin_15);
	LCD_BG_CLOSE();
//		LCD_BG_OPEN();	
	LCD_NRST();
	LCDDelay(100);
	LCD_RST();
	LCDDelay(100);
	LCD_NRST();
	Lcd_Driver.writeCMD(0x01);//software reset
  LCDDelay(120);
	
	//关闭显示
	Lcd_Driver.writeCMD(0x28); 

  ILI9488_CTC35_Initial_Code();

//	//此命令关闭睡眠模式
//	Lcd_Driver.writeCMD(0x11); 
//	//开启反显，正常0000显示黑色，ffff显示白色，如果不发这个命令，会显示反的
	Lcd_Driver.writeCMD(0x20); 
//	//开启显示
	Lcd_Driver.writeCMD(0x29); 

	Lcd_Driver.scan_dir(DIR_HORIZONTAL);//测试程序切换到横屏
	LCD_BG_OPEN();	
//	LCD_DIM_init(100,95,0);  //10Khz 50占空比	
	printf("LCD_Init  \r\n");
}
