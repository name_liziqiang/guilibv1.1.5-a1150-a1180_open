#ifndef _touchdriver_h
#define _touchdriver_h

#include <stdint.h>
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *功能模块：TOUCH驱动模块
 *实现功能：1、TOUCH硬件接口
 *          2、TOUCH描述符信息定义
 *
 *撰 写 人：Alpscale LCD Application development team
 *撰写时间：2019-7-29
 *测 试 人：Alpscale LCD Application development team
 *测试时间：2019-7-29
 *版 本 号：V1.01_2019-7-29
 *版本说明：
 *                              
 *          函数清单
 *
 *V1.01：   功能实现
 *
 *V1.02：
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//1、TOUCH硬件接口

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define DOUTPROT     GPIO1->DT
//#define DOUTPIN      GPIO_Pin_30
#define DOUTPIN      GPIO_Pin_14//QSPI1_MISO 
#define TDIN_D()     GPIO1->DT_CLR=GPIO_Pin_10//QSPI1_MOSI 
#define TDIN_G()     GPIO1->DT_SET=GPIO_Pin_10//QSPI1_MOSI 

#define TCLK_D()     GPIO1->DT_CLR=GPIO_Pin_11//QSPI_CLK 
#define TCLK_G()     GPIO1->DT_SET=GPIO_Pin_11//QSPI_CLK 

#define TCS_D()      GPIO1->DT_CLR=GPIO_Pin_18//QSPI1_XPT_CS 
#define TCS_G()      GPIO1->DT_SET=GPIO_Pin_18//QSPI1_XPT_CS 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//#define TOUCH_USE_SPI  

uint16_t touchReadX(void);
uint16_t touchReadY(void);		

#endif
