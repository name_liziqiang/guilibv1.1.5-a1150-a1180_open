/**
  ******************************************************************************
  * @file    system_1800.h
  * @author  MCD Application Team
  * @version V1.0.1
  * @date    20-April-2012
  * @brief   CMSIS Cortex-M0 Device Peripheral Access Layer System Header File.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 ALPSCALE</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/** @addtogroup CMSIS
  * @{
  */

/** @addtogroup alpscale_system
  * @{
  */  
  
/**
  * @brief Define to prevent recursive inclusion
  */
#ifndef __SYSTEM_M1130_H
#define __SYSTEM_M1130_H

#ifdef __cplusplus
 extern "C" {
#endif 
#include "m1130.h"
/** @addtogroup M1800_System_Includes
  * @{
  */

/**
  * @}
  */


/** @addtogroup M1800_System_Exported_types
  * @{
  */

extern uint32_t SystemCoreClock;          /*!< System Clock Frequency (Core Clock) */

/**
  * @}
  */

/** @addtogroup M1800_System_Exported_Constants
  * @{
  */

/**
  * @}
  */

/** @addtogroup M1800_System_Exported_Macros
  * @{
  */

/**
  * @}
  */

/** @addtogroup M1800_System_Exported_Functions
  * @{
  */
extern uint32_t Get_SystemTick(void);  
extern void SystemInit(void);
extern void SystemCoreClockUpdate(void);
/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /*__SYSTEM_M1130_H */

/**
  * @}
  */
  
/**
  * @}
  */  
/************************ (C) COPYRIGHT ALPSCALE *****END OF FILE****/
